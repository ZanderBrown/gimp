subdir('glib')

gdk_sources = [
    'gdk.c',
    'gdkcolor.c',
    'gdkcursor.c',
    'gdkdraw.c',
    'gdkfont.c',
    'gdkgc.c',
    'gdkglobals.c',
    'gdkimage.c',
    'gdkpixmap.c',
    'gdkrectangle.c',
    'gdkvisual.c',
    'gdkwindow.c',
]


gtk_conf = configuration_data()

is_resource_base = cc.compiles('''
  #define XLIB_ILLEGAL_ACCESS
  #include <X11/Xlib.h>
  
  int
  main ()
  {
    Display *display;
  
    return 0;
  
    display->resource_base;
  }
  ''',
  args: ['-Werror'],
  name : 'Resource base field in XDisplay')
if is_resource_base
  gtk_conf.set('RESOURCE_BASE', 'gdk_display->resource_base')
else
  gtk_conf.set('RESOURCE_BASE', 'gdk_display->private3')
endif


gtk_config_h = configure_file(output: 'config.h', configuration: gtk_conf)

gtk_sources = [
    gtk_config_h,
    'gtk.c',
    'gtkaccelerator.c',
    'gtkalignment.c',
    'gtkbox.c',
    'gtkbutton.c',
    'gtkcallback.c',
    'gtkcontainer.c',
    'gtkdata.c',
    'gtkdraw.c',
    'gtkdrawingarea.c',
    'gtkentry.c',
    'gtkevent.c',
    'gtkfilesel.c',
    'gtkframe.c',
    'gtkgc.c',
    'gtkglobals.c',
    'gtklist.c',
    'gtklistbox.c',
    'gtkmenu.c',
    'gtkmisc.c',
    'gtkobserver.c',
    'gtkoptionmenu.c',
    'gtkruler.c',
    'gtkscale.c',
    'gtkscroll.c',
    'gtkscrollbar.c',
    'gtkstyle.c',
    'gtktable.c',
    'gtkwidget.c',
    'gtkwindow.c',
    'fnmatch.c',
]

gdk_deps = [
    glib_dep,
    dependency('x11'),
    dependency('xext'),
    cc.find_library('m', required: false),
]
gdk_lib = library('gdk', gdk_sources,
                  dependencies: gdk_deps,
                  install_dir: pkglibdir,
                  install_rpath: pkglibdir,
                  install: true)
gdk_inc = include_directories('.')
gdk_dep = declare_dependency (include_directories: gdk_inc,
                              link_with: gdk_lib,
                              dependencies: gdk_deps)

gtk_deps = [gdk_dep]
gtk_lib = library('gtk', gtk_sources,
                  dependencies: gtk_deps,
                  install_dir: pkglibdir,
                  install_rpath: pkglibdir,
                  install: true)
gtk_inc = include_directories('.')
gtk_dep = declare_dependency (sources: [gtk_config_h],
                              include_directories: gtk_inc,
                              link_with: gtk_lib,
                              dependencies: gtk_deps)


executable('testgtk', ['testgtk.c'], dependencies: [gtk_dep])

subdir('tests')
