#!/usr/bin/bash

flatpak-builder fbuild                                                \
        --force-clean                                                 \
        --repo=gimp-repo                                              \
        org.gnome.zbrown.GIMP.json

flatpak build-bundle                                                  \
        --runtime-repo="https://flathub.org/repo/flathub.flatpakrepo" \
        gimp-repo                                                     \
        gimp-dev.flatpak                                              \
        org.gnome.zbrown.GIMP                                         
