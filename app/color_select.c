/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include "appenv.h"
#include "actionarea.h"
#include "color_select.h"
#include "colormaps.h"
#include "errors.h"
#include "gimprc.h"
#include "visual.h"

#define XY_DEF_WIDTH       256
#define XY_DEF_HEIGHT      256
#define Z_DEF_WIDTH        20
#define Z_DEF_HEIGHT       256
#define COLOR_AREA_WIDTH   96
#define COLOR_AREA_HEIGHT  32

#define COLOR_AREA_MASK GDK_EXPOSURE_MASK | \
                        GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | \
			GDK_BUTTON1_MOTION_MASK | GDK_ENTER_NOTIFY_MASK

typedef enum {
  HUE = 0,
  SATURATION,
  VALUE,
  RED,
  GREEN,
  BLUE,
  HUE_SATURATION,
  HUE_VALUE,
  SATURATION_VALUE,
  RED_GREEN,
  RED_BLUE,
  GREEN_BLUE
} ColorSelectFillType;

typedef enum {
  UPDATE_VALUES = 1 << 0,
  UPDATE_POS = 1 << 1,
  UPDATE_XY_COLOR = 1 << 2,
  UPDATE_Z_COLOR = 1 << 3,
  UPDATE_NEW_COLOR = 1 << 4,
  UPDATE_ORIG_COLOR = 1 << 5
} ColorSelectUpdateType;

typedef struct _ColorSelectFill ColorSelectFill;
typedef void (*ColorSelectFillUpdateProc) (ColorSelectFill *);

struct _ColorSelectFill {
  unsigned char *buffer;
  int y;
  int width;
  int height;
  int *values;
  ColorSelectFillUpdateProc update;
};

static void color_select_update (ColorSelectP, ColorSelectUpdateType);
static void color_select_update_values (ColorSelectP);
static void color_select_update_rgb_values (ColorSelectP);
static void color_select_update_hsv_values (ColorSelectP);
static void color_select_update_pos (ColorSelectP);
static void color_select_update_sliders (ColorSelectP);
static void color_select_update_colors (ColorSelectP, int);

static void color_select_disconnect (GtkObserver *, GtkData *);
static void color_select_ok_callback (GtkWidget *, gpointer, gpointer);
static void color_select_cancel_callback (GtkWidget *, gpointer, gpointer);
static gint color_select_xy_events (GtkWidget *, GdkEvent *);
static gint color_select_z_events (GtkWidget *, GdkEvent *);
static gint color_select_color_events (GtkWidget *, GdkEvent *);
static gint color_select_slider_update (GtkObserver *, GtkData *);
static gint color_select_toggle_update (GtkObserver *, GtkData *);

static void color_select_image_fill (ImageBuf, ColorSelectFillType, int *);

static void color_select_draw_z_marker (ColorSelectP, int);
static void color_select_draw_xy_marker (ColorSelectP, int);

static void color_select_update_red (ColorSelectFill *);
static void color_select_update_green (ColorSelectFill *);
static void color_select_update_blue (ColorSelectFill *);
static void color_select_update_hue (ColorSelectFill *);
static void color_select_update_saturation (ColorSelectFill *);
static void color_select_update_value (ColorSelectFill *);
static void color_select_update_red_green (ColorSelectFill *);
static void color_select_update_red_blue (ColorSelectFill *);
static void color_select_update_green_blue (ColorSelectFill *);
static void color_select_update_hue_saturation (ColorSelectFill *);
static void color_select_update_hue_value (ColorSelectFill *);
static void color_select_update_saturation_value (ColorSelectFill *);

static ColorSelectFillUpdateProc update_procs[] =
{
  color_select_update_hue,
  color_select_update_saturation,
  color_select_update_value,
  color_select_update_red,
  color_select_update_green,
  color_select_update_blue,
  color_select_update_hue_saturation,
  color_select_update_hue_value,
  color_select_update_saturation_value,
  color_select_update_red_green,
  color_select_update_red_blue,
  color_select_update_green_blue,
};

static ActionAreaItem action_items[2] =
{
  { "OK", color_select_ok_callback, NULL, NULL },
  { "Cancel", color_select_cancel_callback, NULL, NULL },
};

ColorSelectP
color_select_new (r, g, b, callback)
     int r, g, b;
     ColorSelectCallback callback;
{
  static char *toggle_titles[6] = { "Hue", "Saturation", "Value", "Red", "Green", "Blue" };
  static gfloat slider_max_vals[6] = { 360, 100, 100, 255, 255, 255 };
  static gfloat slider_incs[6] = { 0.1, 0.1, 0.1, 1.0, 1.0, 1.0 };

  ColorSelectP csp;
  GtkWidget *main_vbox;
  GtkWidget *main_hbox;
  GtkWidget *xy_frame;
  GtkWidget *z_frame;
  GtkWidget *colors_frame;
  GtkWidget *colors_hbox;
  GtkWidget *action_area;
  GtkWidget *right_vbox;
  GtkWidget *cspace_hbox;
  GtkWidget *toggle_vbox;
  GtkWidget *slider_vbox;
  GtkWidget *toggle_label;
  GtkWidget *slider;
  GtkDataWidget *cspace_data;
  int i;

  csp = xmalloc (sizeof (_ColorSelect));

  csp->callback = callback;
  csp->gc = NULL;
  csp->z_color_fill = HUE;
  csp->xy_color_fill = SATURATION_VALUE;

  csp->values[RED] = csp->orig_values[0] = r;
  csp->values[GREEN] = csp->orig_values[1] = g;
  csp->values[BLUE] = csp->orig_values[2] = b;
  color_select_update_hsv_values (csp);
  color_select_update_pos (csp);

  csp->shell = gtk_window_new ("Color Selection", GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_uposition (csp->shell, color_select_x, color_select_y);

  main_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (csp->shell, main_vbox);

  main_hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (main_hbox, 2);
  gtk_box_pack (main_vbox, main_hbox, TRUE, TRUE, 2, GTK_PACK_START);

  xy_frame = gtk_frame_new (NULL);
  gtk_frame_set_type (xy_frame, GTK_SHADOW_IN);
  gtk_box_pack (main_hbox, xy_frame, FALSE, FALSE, 2, GTK_PACK_START);

  csp->xy_color = gtk_drawing_area_new (XY_DEF_WIDTH, XY_DEF_HEIGHT,
					color_select_xy_events,
					COLOR_AREA_MASK);
  gtk_widget_set_user_data (csp->xy_color, csp);
  gtk_container_add (xy_frame, csp->xy_color);
  csp->xy_color_image = image_buf_create (COLOR_BUF, XY_DEF_WIDTH, XY_DEF_HEIGHT);

  z_frame = gtk_frame_new (NULL);
  gtk_frame_set_type (z_frame, GTK_SHADOW_IN);
  gtk_box_pack (main_hbox, z_frame, FALSE, FALSE, 2, GTK_PACK_START);

  csp->z_color = gtk_drawing_area_new (Z_DEF_WIDTH, Z_DEF_HEIGHT,
				       color_select_z_events,
				       COLOR_AREA_MASK);
  gtk_widget_set_user_data (csp->z_color, csp);
  gtk_container_add (z_frame, csp->z_color);
  csp->z_color_image = image_buf_create (COLOR_BUF, Z_DEF_WIDTH, Z_DEF_HEIGHT);

  /*  The right vertical box with old/new color area and color space sliders  */
  right_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (right_vbox, 2);
  gtk_box_pack (main_hbox, right_vbox, TRUE, TRUE, 0, GTK_PACK_START);

  /*  The old/new color area  */
  {
    colors_frame = gtk_frame_new (NULL);
    gtk_frame_set_type (colors_frame, GTK_SHADOW_IN);
    gtk_box_pack (right_vbox, colors_frame, FALSE, FALSE, 0, GTK_PACK_START);

    colors_hbox = gtk_hbox_new (TRUE, 0);
    gtk_container_set_border_width (colors_hbox, 0);
    gtk_container_add (colors_frame, colors_hbox);

    csp->new_color = gtk_drawing_area_new (COLOR_AREA_WIDTH, COLOR_AREA_HEIGHT,
					   color_select_color_events,
					   GDK_EXPOSURE_MASK);
    gtk_widget_set_user_data (csp->new_color, csp);
    gtk_box_pack (colors_hbox, csp->new_color, TRUE, TRUE, 0, GTK_PACK_START);

    csp->orig_color = gtk_drawing_area_new (COLOR_AREA_WIDTH, COLOR_AREA_HEIGHT,
					    color_select_color_events,
					    GDK_EXPOSURE_MASK);
    gtk_widget_set_user_data (csp->orig_color, csp);
    gtk_box_pack (colors_hbox, csp->orig_color, TRUE, TRUE, 0, GTK_PACK_START);
  }

  /*  The color space sliders and toggle buttons  */
  {
    cspace_hbox = gtk_hbox_new (FALSE, 0);
    gtk_container_set_border_width (cspace_hbox, 2);
    gtk_box_pack (right_vbox, cspace_hbox, TRUE, TRUE, 0, GTK_PACK_START);

    toggle_vbox = gtk_vbox_new (FALSE, 0);
    gtk_container_set_border_width (toggle_vbox, 0);
    gtk_box_pack (cspace_hbox, toggle_vbox, FALSE, FALSE, 0, GTK_PACK_START);

    slider_vbox = gtk_vbox_new (TRUE, 0);
    gtk_container_set_border_width (slider_vbox, 0);
    gtk_box_pack (cspace_hbox, slider_vbox, TRUE, TRUE, 0, GTK_PACK_START);

    cspace_data = (GtkDataWidget *) gtk_data_widget_new (NULL);

    for (i = 0; i < 6; i++)
      {
	csp->toggles[i] = gtk_radio_button_new ((GtkData *) cspace_data);
	gtk_box_pack (toggle_vbox, csp->toggles[i], TRUE, FALSE, 0, GTK_PACK_START);
	toggle_label = gtk_label_new (toggle_titles[i]);
	gtk_label_set_alignment (toggle_label, 0.0, 0.5);
	gtk_container_add (csp->toggles[i], toggle_label);

	csp->slider_data[i] = (GtkDataAdjustment *)
	  gtk_data_adjustment_new (1.0, 0.0, slider_max_vals[i], slider_incs[i], 1.0, 1.0);
	slider = gtk_hscale_new (csp->slider_data[i]);
	gtk_box_pack (slider_vbox, slider, FALSE, FALSE, 0, GTK_PACK_START);
	gtk_scale_set_value_pos (slider, GTK_POS_TOP);
	csp->slider_observer[i].update = color_select_slider_update;
	csp->slider_observer[i].disconnect = color_select_disconnect;
	csp->slider_observer[i].user_data = csp;
	gtk_data_attach ((GtkData *) csp->slider_data[i], &csp->slider_observer[i]);

	gtk_widget_show (slider);
	gtk_widget_show (toggle_label);
	gtk_widget_show (csp->toggles[i]);
      }

    cspace_data->widget = csp->toggles[0];
    gtk_data_notify ((GtkData *) cspace_data);
    csp->cspace_observer.update = color_select_toggle_update;
    csp->cspace_observer.disconnect = color_select_disconnect;
    csp->cspace_observer.user_data = csp;
    gtk_data_attach ((GtkData *) cspace_data, &csp->cspace_observer);
  }

  /*  The action area  */
  action_items[0].user_data = csp;
  action_items[1].user_data = csp;
  action_area = build_action_area (action_items, 2);
  gtk_box_pack (main_vbox, action_area, TRUE, TRUE, 2, GTK_PACK_START);

  color_select_image_fill (csp->z_color_image, csp->z_color_fill, csp->values);
  color_select_image_fill (csp->xy_color_image, csp->xy_color_fill, csp->values);

  gtk_widget_show (toggle_vbox);
  gtk_widget_show (slider_vbox);
  gtk_widget_show (cspace_hbox);
  gtk_widget_show (csp->orig_color);
  gtk_widget_show (csp->new_color);
  gtk_widget_show (colors_hbox);
  gtk_widget_show (colors_frame);
  gtk_widget_show (right_vbox);
  gtk_widget_show (csp->z_color);
  gtk_widget_show (z_frame);
  gtk_widget_show (csp->xy_color);
  gtk_widget_show (xy_frame);
  gtk_widget_show (action_area);
  gtk_widget_show (main_hbox);
  gtk_widget_show (main_vbox);
  gtk_widget_show (csp->shell);
  install_colormap (RGB);

  return csp;
}

void
color_select_show (csp)
     ColorSelectP csp;
{
  if (csp)
    gtk_widget_show (csp->shell);
}

void
color_select_hide (csp)
     ColorSelectP csp;
{
  if (csp)
    gtk_widget_hide (csp->shell);
}

void
color_select_free (csp)
     ColorSelectP csp;
{
  if (csp)
    {
      gtk_widget_destroy (csp->shell);
      gdk_gc_destroy (csp->gc);
      image_buf_destroy (csp->xy_color_image);
      image_buf_destroy (csp->z_color_image);
      xfree (csp);
    }
}

void
color_select_set_color (csp, r, g, b, set_current)
     ColorSelectP csp;
     int r, g, b;
     int set_current;
{
  if (csp)
    {
      csp->orig_values[0] = r;
      csp->orig_values[1] = g;
      csp->orig_values[2] = b;

      color_select_update_colors (csp, 1);

      if (set_current)
	{
	  csp->values[RED] = r;
	  csp->values[GREEN] = g;
	  csp->values[BLUE] = b;

	  color_select_update_hsv_values (csp);
	  color_select_update_pos (csp);
	  color_select_update_sliders (csp);
	  color_select_update_colors (csp, 0);

	  color_select_update (csp, UPDATE_Z_COLOR);
	  color_select_update (csp, UPDATE_XY_COLOR);
	}
    }
}

static void
color_select_update (csp, update)
     ColorSelectP csp;
     ColorSelectUpdateType update;
{
  if (csp)
    {
      if (update & UPDATE_POS)
	color_select_update_pos (csp);

      if (update & UPDATE_VALUES)
	{
	  color_select_update_values (csp);
	  color_select_update_sliders (csp);

	  if (!(update & UPDATE_NEW_COLOR))
	    color_select_update_colors (csp, 0);
	}

      if (update & UPDATE_XY_COLOR)
	{
	  color_select_image_fill (csp->xy_color_image, csp->xy_color_fill, csp->values);
	  image_buf_put (csp->xy_color_image, csp->xy_color->window, csp->gc);
	  color_select_draw_xy_marker (csp, 1);
	}

      if (update & UPDATE_Z_COLOR)
	{
	  color_select_image_fill (csp->z_color_image, csp->z_color_fill, csp->values);
	  image_buf_put (csp->z_color_image, csp->z_color->window, csp->gc);
	  color_select_draw_z_marker (csp, 1);
	}

      if (update & UPDATE_NEW_COLOR)
	color_select_update_colors (csp, 0);

      if (update & UPDATE_ORIG_COLOR)
	color_select_update_colors (csp, 1);
    }
}

static void
color_select_update_values (csp)
     ColorSelectP csp;
{
  if (csp)
    {
      switch (csp->z_color_fill)
	{
	case RED:
	  csp->values[BLUE] = csp->pos[0];
	  csp->values[GREEN] = csp->pos[1];
	  csp->values[RED] = csp->pos[2];
	  break;
	case GREEN:
	  csp->values[BLUE] = csp->pos[0];
	  csp->values[RED] = csp->pos[1];
	  csp->values[GREEN] = csp->pos[2];
	  break;
	case BLUE:
	  csp->values[GREEN] = csp->pos[0];
	  csp->values[RED] = csp->pos[1];
	  csp->values[BLUE] = csp->pos[2];
	  break;
	case HUE:
	  csp->values[VALUE] = csp->pos[0] * 100 / 255;
	  csp->values[SATURATION] = csp->pos[1] * 100 / 255;
	  csp->values[HUE] = csp->pos[2] * 360 / 255;
	  break;
	case SATURATION:
	  csp->values[VALUE] = csp->pos[0] * 100 / 255;
	  csp->values[HUE] = csp->pos[1] * 360 / 255;
	  csp->values[SATURATION] = csp->pos[2] * 100 / 255;
	  break;
	case VALUE:
	  csp->values[SATURATION] = csp->pos[0] * 100 / 255;
	  csp->values[HUE] = csp->pos[1] * 360 / 255;
	  csp->values[VALUE] = csp->pos[2] * 100 / 255;
	  break;
	}

      switch (csp->z_color_fill)
	{
	case RED:
	case GREEN:
	case BLUE:
	  color_select_update_hsv_values (csp);
	  break;
	case HUE:
	case SATURATION:
	case VALUE:
	  color_select_update_rgb_values (csp);
	  break;
	}
    }
}

static void
color_select_update_rgb_values (csp)
     ColorSelectP csp;
{
  float h, s, v;
  float f, p, q, t;

  if (csp)
    {
      h = csp->values[HUE];
      s = csp->values[SATURATION] / 100.0;
      v = csp->values[VALUE] / 100.0;

      if (s == 0)
	{
	  csp->values[RED] = v * 255;
	  csp->values[GREEN] = v * 255;
	  csp->values[BLUE] = v * 255;
	}
      else
	{
	  if (h == 360)
	    h = 0;

	  h /= 60;
	  f = h - (int) h;
	  p = v * (1 - s);
	  q = v * (1 - (s * f));
	  t = v * (1 - (s * (1 - f)));

	  switch ((int) h)
	    {
	    case 0:
	      csp->values[RED] = v * 255;
	      csp->values[GREEN] = t * 255;
	      csp->values[BLUE] = p * 255;
	      break;
	    case 1:
	      csp->values[RED] = q * 255;
	      csp->values[GREEN] = v * 255;
	      csp->values[BLUE] = p * 255;
	      break;
	    case 2:
	      csp->values[RED] = p * 255;
	      csp->values[GREEN] = v * 255;
	      csp->values[BLUE] = t * 255;
	      break;
	    case 3:
	      csp->values[RED] = p * 255;
	      csp->values[GREEN] = q * 255;
	      csp->values[BLUE] = v * 255;
	      break;
	    case 4:
	      csp->values[RED] = t * 255;
	      csp->values[GREEN] = p * 255;
	      csp->values[BLUE] = v * 255;
	      break;
	    case 5:
	      csp->values[RED] = v * 255;
	      csp->values[GREEN] = p * 255;
	      csp->values[BLUE] = q * 255;
	      break;
	    }
	}
    }
}

static void
color_select_update_hsv_values (csp)
     ColorSelectP csp;
{
  int r, g, b;
  float h, s, v;
  int min, max;
  int delta;

  if (csp)
    {
      r = csp->values[RED];
      g = csp->values[GREEN];
      b = csp->values[BLUE];

      if (r > g)
	{
	  if (r > b)
	    max = r;
	  else
	    max = b;

	  if (g < b)
	    min = g;
	  else
	    min = b;
	}
      else
	{
	  if (g > b)
	    max = g;
	  else
	    max = b;

	  if (r < b)
	    min = r;
	  else
	    min = b;
	}

      v = max;

      if (max != 0)
	s = (max - min) / (float) max;
      else
	s = 0;

      if (s == 0)
	h = 0;
      else
	{
	  delta = max - min;
	  if (r == max)
	    h = (g - b) / (float) delta;
	  else if (g == max)
	    h = 2 + (b - r) / (float) delta;
	  else if (b == max)
	    h = 4 + (r - g) / (float) delta;
	  h *= 60;

	  if (h < 0)
	    h += 360;
	}

      csp->values[HUE] = h;
      csp->values[SATURATION] = s * 100;
      csp->values[VALUE] = v * 100 / 255;
    }
}

static void
color_select_update_pos (csp)
     ColorSelectP csp;
{
  if (csp)
    {
      switch (csp->z_color_fill)
	{
	case RED:
	  csp->pos[0] = csp->values[BLUE];
	  csp->pos[1] = csp->values[GREEN];
	  csp->pos[2] = csp->values[RED];
	  break;
	case GREEN:
	  csp->pos[0] = csp->values[BLUE];
	  csp->pos[1] = csp->values[RED];
	  csp->pos[2] = csp->values[GREEN];
	  break;
	case BLUE:
	  csp->pos[0] = csp->values[GREEN];
	  csp->pos[1] = csp->values[RED];
	  csp->pos[2] = csp->values[BLUE];
	  break;
	case HUE:
	  csp->pos[0] = csp->values[VALUE] * 255 / 100;
	  csp->pos[1] = csp->values[SATURATION] * 255 / 100;
	  csp->pos[2] = csp->values[HUE] * 255 / 360;
	  break;
	case SATURATION:
	  csp->pos[0] = csp->values[VALUE] * 255 / 100;
	  csp->pos[1] = csp->values[HUE] * 255 / 360;
	  csp->pos[2] = csp->values[SATURATION] * 255 / 100;
	  break;
	case VALUE:
	  csp->pos[0] = csp->values[SATURATION] * 255 / 100;
	  csp->pos[1] = csp->values[HUE] * 255 / 360;
	  csp->pos[2] = csp->values[VALUE] * 255 / 100;
	  break;
	}
    }
}

static void
color_select_update_sliders (csp)
     ColorSelectP csp;
{
  int i;

  if (csp)
    {
      for (i = 0; i < 6; i++)
	{
	  csp->slider_data[i]->value = (gfloat) csp->values[i];
	  gtk_data_detach ((GtkData *) csp->slider_data[i], &csp->slider_observer[i]);
	  gtk_data_notify ((GtkData *) csp->slider_data[i]);
	  gtk_data_attach ((GtkData *) csp->slider_data[i], &csp->slider_observer[i]);
	}
    }
}

static void
color_select_update_colors (csp, which)
     ColorSelectP csp;
     int which;
{
  GdkWindow *window;
  GdkColor color;
  int red, green, blue;
  int width, height;

  if (csp)
    {
      if (which)
	{
	  window = csp->orig_color->window;
	  color.pixel = old_color_pixel;
	  red = csp->orig_values[0];
	  green = csp->orig_values[1];
	  blue = csp->orig_values[2];
	}
      else
	{
	  window = csp->new_color->window;
	  color.pixel = new_color_pixel;
	  red = csp->values[RED];
	  green = csp->values[GREEN];
	  blue = csp->values[BLUE];
	}

      width = window->width;
      height = window->height;

      store_color (&color.pixel, red, green, blue);

      if (csp->gc)
	{
	  gdk_gc_set_foreground (csp->gc, &color);
	  gdk_draw_rectangle (window, csp->gc, 1,
			      0, 0, width, height);
	}
    }
}

static void
color_select_disconnect (observer, data)
     GtkObserver * observer;
     GtkData * data;
{
}

static void
color_select_ok_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  ColorSelectP csp;

  csp = (ColorSelectP) client_data;
  if (csp)
    {
      if (csp->callback)
	(* csp->callback) (csp->values[RED], csp->values[GREEN], csp->values[BLUE], 0);
    }
}

static void
color_select_cancel_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  ColorSelectP csp;

  csp = (ColorSelectP) client_data;
  if (csp)
    {
      if (csp->callback)
	(* csp->callback) (csp->orig_values[0], csp->orig_values[1], csp->orig_values[2], 1);
    }
}

static gint
color_select_xy_events (widget, event)
     GtkWidget * widget;
     GdkEvent * event;
{
  GdkEventButton *bevent;
  GdkEventMotion *mevent;
  ColorSelectP csp;
  int tx, ty;

  csp = (ColorSelectP) gtk_widget_get_user_data (widget);
  if (!csp)
    return FALSE;

  switch (event->type)
    {
    case GDK_EXPOSE:
      if (!csp->gc)
	csp->gc = gdk_gc_new (widget->window);

      image_buf_put (csp->xy_color_image, csp->xy_color->window, csp->gc);
      color_select_draw_xy_marker (csp, 1);
      break;

    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;

      color_select_draw_xy_marker (csp, 1);

      csp->pos[0] = bevent->x;
      csp->pos[1] = 255 - bevent->y;

      if (csp->pos[0] < 0)
	csp->pos[0] = 0;
      if (csp->pos[0] > 255)
	csp->pos[0] = 255;
      if (csp->pos[1] < 0)
	csp->pos[1] = 0;
      if (csp->pos[1] > 255)
	csp->pos[1] = 255;

      gdk_pointer_grab (csp->xy_color->window, FALSE,
			GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			NULL, NULL, bevent->time);
      color_select_draw_xy_marker (csp, 1);

      color_select_update (csp, UPDATE_VALUES);
      break;

    case GDK_BUTTON_RELEASE:
      bevent = (GdkEventButton *) event;

      color_select_draw_xy_marker (csp, 1);

      csp->pos[0] = bevent->x;
      csp->pos[1] = 255 - bevent->y;

      if (csp->pos[0] < 0)
	csp->pos[0] = 0;
      if (csp->pos[0] > 255)
	csp->pos[0] = 255;
      if (csp->pos[1] < 0)
	csp->pos[1] = 0;
      if (csp->pos[1] > 255)
	csp->pos[1] = 255;

      gdk_pointer_ungrab (bevent->time);
      color_select_draw_xy_marker (csp, 1);
      color_select_update (csp, UPDATE_VALUES);
      break;

    case GDK_MOTION_NOTIFY:
      mevent = (GdkEventMotion *) event;
      if (mevent->is_hint)
	{
	  gdk_window_get_pointer (widget->window, &tx, &ty, NULL);
	  mevent->x = tx;
	  mevent->y = ty;
	}

      color_select_draw_xy_marker (csp, 1);

      csp->pos[0] = mevent->x;
      csp->pos[1] = 255 - mevent->y;

      if (csp->pos[0] < 0)
	csp->pos[0] = 0;
      if (csp->pos[0] > 255)
	csp->pos[0] = 255;
      if (csp->pos[1] < 0)
	csp->pos[1] = 0;
      if (csp->pos[1] > 255)
	csp->pos[1] = 255;

      color_select_draw_xy_marker (csp, 1);
      color_select_update (csp, UPDATE_VALUES);
      break;

    case GDK_ENTER_NOTIFY:
      install_colormap (RGB);
      break;

    default:
      break;
    }

  return FALSE;
}

static gint
color_select_z_events (widget, event)
     GtkWidget * widget;
     GdkEvent * event;
{
  GdkEventButton *bevent;
  GdkEventMotion *mevent;
  ColorSelectP csp;
  int tx, ty;

  csp = (ColorSelectP) gtk_widget_get_user_data (widget);
  if (!csp)
    return FALSE;

  switch (event->type)
    {
    case GDK_EXPOSE:
      if (!csp->gc)
	csp->gc = gdk_gc_new (widget->window);

      image_buf_put (csp->z_color_image, csp->z_color->window, csp->gc);
      color_select_draw_z_marker (csp, 1);
      break;

    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;

      color_select_draw_z_marker (csp, 1);

      csp->pos[2] = 255 - bevent->y;
      if (csp->pos[2] < 0)
	csp->pos[2] = 0;
      if (csp->pos[2] > 255)
	csp->pos[2] = 255;

      gdk_pointer_grab (csp->z_color->window, FALSE,
			GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			NULL, NULL, bevent->time);
      color_select_draw_z_marker (csp, 1);
      color_select_update (csp, UPDATE_VALUES);
      break;

    case GDK_BUTTON_RELEASE:
      bevent = (GdkEventButton *) event;

      color_select_draw_z_marker (csp, 1);

      csp->pos[2] = 255 - bevent->y;
      if (csp->pos[2] < 0)
	csp->pos[2] = 0;
      if (csp->pos[2] > 255)
	csp->pos[2] = 255;

      gdk_pointer_ungrab (bevent->time);
      color_select_draw_z_marker (csp, 1);
      color_select_update (csp, UPDATE_VALUES | UPDATE_XY_COLOR);
      break;

    case GDK_MOTION_NOTIFY:
      mevent = (GdkEventMotion *) event;
      if (mevent->is_hint)
	{
	  gdk_window_get_pointer (widget->window, &tx, &ty, NULL);
	  mevent->x = tx;
	  mevent->y = ty;
	}

      color_select_draw_z_marker (csp, 1);

      csp->pos[2] = 255 - mevent->y;
      if (csp->pos[2] < 0)
	csp->pos[2] = 0;
      if (csp->pos[2] > 255)
	csp->pos[2] = 255;

      color_select_draw_z_marker (csp, 1);
      color_select_update (csp, UPDATE_VALUES);
      break;

    case GDK_ENTER_NOTIFY:
      install_colormap (RGB);
      break;

    default:
      break;
    }

  return FALSE;
}

static gint
color_select_color_events (widget, event)
     GtkWidget * widget;
     GdkEvent * event;
{
  ColorSelectP csp;

  csp = (ColorSelectP) gtk_widget_get_user_data (widget);
  if (!csp)
    return FALSE;

  switch (event->type)
    {
    case GDK_EXPOSE:
      if (!csp->gc)
	csp->gc = gdk_gc_new (widget->window);

      if (widget == csp->new_color)
	color_select_update (csp, UPDATE_NEW_COLOR);
      else if (widget == csp->orig_color)
	color_select_update (csp, UPDATE_ORIG_COLOR);
      break;

    default:
      break;
    }

  return FALSE;
}

static gint
color_select_slider_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  ColorSelectP csp;
  GtkDataAdjustment *adj_data;
  int old_values[6];
  int update_z_marker;
  int update_xy_marker;
  int i, j;

  csp = (ColorSelectP) observer->user_data;
  adj_data = (GtkDataAdjustment *) data;

  if (csp)
    {
      for (i = 0; i < 6; i++)
	if (csp->slider_data[i] == adj_data)
	  break;

      for (j = 0; j < 6; j++)
	old_values[j] = csp->values[j];

      csp->values[i] = (int) adj_data->value;

      if ((i >= HUE) && (i <= VALUE))
	color_select_update_rgb_values (csp);
      else if ((i >= RED) && (i <= BLUE))
	color_select_update_hsv_values (csp);
      color_select_update_sliders (csp);

      update_z_marker = 0;
      update_xy_marker = 0;
      for (j = 0; j < 6; j++)
	{
	  if (j == csp->z_color_fill)
	    {
	      if (old_values[j] != csp->values[j])
		update_z_marker = 1;
	    }
	  else
	    {
	      if (old_values[j] != csp->values[j])
		update_xy_marker = 1;
	    }
	}

      if (update_z_marker)
	{
	  color_select_draw_z_marker (csp, 1);
	  color_select_update (csp, UPDATE_POS | UPDATE_XY_COLOR);
	  color_select_draw_z_marker (csp, 1);
	}
      else
	{
	  if (update_z_marker)
	    color_select_draw_z_marker (csp, 1);
	  if (update_xy_marker)
	    color_select_draw_xy_marker (csp, 1);

	  color_select_update (csp, UPDATE_POS);

	  if (update_z_marker)
	    color_select_draw_z_marker (csp, 1);
	  if (update_xy_marker)
	    color_select_draw_xy_marker (csp, 1);
	}

      color_select_update (csp, UPDATE_NEW_COLOR);
    }

  return 0;
}

static gint
color_select_toggle_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataWidget *widget_data;
  ColorSelectP csp;
  ColorSelectFillType type = HUE;
  int i;

  widget_data = (GtkDataWidget *) data;
  csp = (ColorSelectP) observer->user_data;

  if (csp)
    {
      for (i = 0; i < 6; i++)
	if (widget_data->widget == csp->toggles[i])
	  type = (ColorSelectFillType) i;

      switch (type)
	{
	case HUE:
	  csp->z_color_fill = HUE;
	  csp->xy_color_fill = SATURATION_VALUE;
	  break;
	case SATURATION:
	  csp->z_color_fill = SATURATION;
	  csp->xy_color_fill = HUE_VALUE;
	  break;
	case VALUE:
	  csp->z_color_fill = VALUE;
	  csp->xy_color_fill = HUE_SATURATION;
	  break;
	case RED:
	  csp->z_color_fill = RED;
	  csp->xy_color_fill = GREEN_BLUE;
	  break;
	case GREEN:
	  csp->z_color_fill = GREEN;
	  csp->xy_color_fill = RED_BLUE;
	  break;
	case BLUE:
	  csp->z_color_fill = BLUE;
	  csp->xy_color_fill = RED_GREEN;
	  break;
	default:
	  break;
	}

      color_select_update (csp, UPDATE_POS);
      color_select_update (csp, UPDATE_Z_COLOR | UPDATE_XY_COLOR);
    }

  return 0;
}

static void
color_select_image_fill (csi, type, values)
     ImageBuf csi;
     ColorSelectFillType type;
     int *values;
{
  ColorSelectFill csf;
  int height;

  csf.buffer = xmalloc (image_buf_width (csi) * 3);

  csf.update = update_procs[type];

  csf.y = -1;
  csf.width = image_buf_width (csi);
  csf.height = image_buf_height (csi);
  csf.values = values;

  height = image_buf_height (csi);
  if (height > 0)
    while (height--)
      {
	(* csf.update) (&csf);
	image_buf_draw_row (csi, csf.buffer, 0, csf.y, image_buf_width (csi));
      }

  xfree (csf.buffer);
}

static void
color_select_draw_z_marker (csp, update)
     ColorSelectP csp;
     int update;
{
  int width;
  int y;

  if (csp->gc)
    {
      y = 255 - csp->pos[2];
      width = image_buf_width (csp->z_color_image);
      if (width <= 0)
	return;

      gdk_gc_set_function (csp->gc, GDK_INVERT);
      gdk_draw_line (csp->z_color->window, csp->gc, 0, y, width, y);
      gdk_gc_set_function (csp->gc, GDK_COPY);
    }
}

static void
color_select_draw_xy_marker (csp, update)
     ColorSelectP csp;
     int update;
{
  int width;
  int height;
  int x, y;

  if (csp->gc)
    {
      x = csp->pos[0];
      y = 255 - csp->pos[1];
      width = image_buf_width (csp->xy_color_image);
      height = image_buf_height (csp->xy_color_image);
      if ((width <= 0) || (height <= 0))
	return;

      gdk_gc_set_function (csp->gc, GDK_INVERT);
      gdk_draw_line (csp->xy_color->window, csp->gc, 0, y, width, y);
      gdk_draw_line (csp->xy_color->window, csp->gc, x, 0, x, height);
      gdk_gc_set_function (csp->gc, GDK_COPY);
    }
}

static void
color_select_update_red (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  int i, r;

  p = csf->buffer;

  csf->y += 1;
  r = (csf->height - csf->y + 1) * 255 / csf->height;

  if (r < 0)
    r = 0;
  if (r > 255)
    r = 255;

  for (i = 0; i < csf->width; i++)
    {
      *p++ = r;
      *p++ = 0;
      *p++ = 0;
    }
}

static void
color_select_update_green (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  int i, g;

  p = csf->buffer;

  csf->y += 1;
  g = (csf->height - csf->y + 1) * 255 / csf->height;

  if (g < 0)
    g = 0;
  if (g > 255)
    g = 255;

  for (i = 0; i < csf->width; i++)
    {
      *p++ = 0;
      *p++ = g;
      *p++ = 0;
    }
}

static void
color_select_update_blue (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  int i, b;

  p = csf->buffer;

  csf->y += 1;
  b = (csf->height - csf->y + 1) * 255 / csf->height;

  if (b < 0)
    b = 0;
  if (b > 255)
    b = 255;

  for (i = 0; i < csf->width; i++)
    {
      *p++ = 0;
      *p++ = 0;
      *p++ = b;
    }
}

static void
color_select_update_hue (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  float h, f;
  int r, g, b;
  int i;

  p = csf->buffer;

  csf->y += 1;
  h = csf->y * 360 / csf->height;

  h = 360 - h;

  if (h < 0)
    h = 0;
  if (h >= 360)
    h = 0;

  h /= 60;
  f = (h - (int) h) * 255;

  switch ((int) h)
    {
    case 0:
      r = 255;
      g = f;
      b = 0;
      break;
    case 1:
      r = 255 - f;
      g = 255;
      b = 0;
      break;
    case 2:
      r = 0;
      g = 255;
      b = f;
      break;
    case 3:
      r = 0;
      g = 255 - f;
      b = 255;
      break;
    case 4:
      r = f;
      g = 0;
      b = 255;
      break;
    case 5:
      r = 255;
      g = 0;
      b = 255 - f;
      break;
    }

  for (i = 0; i < csf->width; i++)
    {
      *p++ = r;
      *p++ = g;
      *p++ = b;
    }
}

static void
color_select_update_saturation (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  int s;
  int i;

  p = csf->buffer;

  csf->y += 1;
  s = csf->y * 255 / csf->height;

  if (s < 0)
    s = 0;
  if (s > 255)
    s = 255;

  s = 255 - s;

  for (i = 0; i < csf->width; i++)
    {
      *p++ = s;
      *p++ = s;
      *p++ = s;
    }
}

static void
color_select_update_value (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  int v;
  int i;

  p = csf->buffer;

  csf->y += 1;
  v = csf->y * 255 / csf->height;

  if (v < 0)
    v = 0;
  if (v > 255)
    v = 255;

  v = 255 - v;

  for (i = 0; i < csf->width; i++)
    {
      *p++ = v;
      *p++ = v;
      *p++ = v;
    }
}

static void
color_select_update_red_green (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  int i, r, b;
  float g, dg;

  p = csf->buffer;

  csf->y += 1;
  b = csf->values[BLUE];
  r = (csf->height - csf->y + 1) * 255 / csf->height;

  if (r < 0)
    r = 0;
  if (r > 255)
    r = 255;

  g = 0;
  dg = 255.0 / csf->width;

  for (i = 0; i < csf->width; i++)
    {
      *p++ = r;
      *p++ = g;
      *p++ = b;

      g += dg;
    }
}

static void
color_select_update_red_blue (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  int i, r, g;
  float b, db;

  p = csf->buffer;

  csf->y += 1;
  g = csf->values[GREEN];
  r = (csf->height - csf->y + 1) * 255 / csf->height;

  if (r < 0)
    r = 0;
  if (r > 255)
    r = 255;

  b = 0;
  db = 255.0 / csf->width;

  for (i = 0; i < csf->width; i++)
    {
      *p++ = r;
      *p++ = g;
      *p++ = b;

      b += db;
    }
}

static void
color_select_update_green_blue (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  int i, g, r;
  float b, db;

  p = csf->buffer;

  csf->y += 1;
  r = csf->values[RED];
  g = (csf->height - csf->y + 1) * 255 / csf->height;

  if (g < 0)
    g = 0;
  if (g > 255)
    g = 255;

  b = 0;
  db = 255.0 / csf->width;

  for (i = 0; i < csf->width; i++)
    {
      *p++ = r;
      *p++ = g;
      *p++ = b;

      b += db;
    }
}

static void
color_select_update_hue_saturation (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  float h, v, s, ds;
  int f;
  int i;

  p = csf->buffer;

  csf->y += 1;
  h = (255 - csf->y) * 359 / csf->height;

  if (h < 0)
    h = 0;
  if (h > 359)
    h = 359;

  h /= 60;
  f = (h - (int) h) * 255;

  s = 0;
  ds = 1.0 / csf->width;

  v = csf->values[VALUE] / 100.0;

  switch ((int) h)
    {
    case 0:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255;
	  *p++ = v * (255 - (s * (255 - f)));
	  *p++ = v * 255 * (1 - s);

	  s += ds;
	}
      break;
    case 1:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * (255 - s * f);
	  *p++ = v * 255;
	  *p++ = v * 255 * (1 - s);

	  s += ds;
	}
      break;
    case 2:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255 * (1 - s);
	  *p++ = v *255;
	  *p++ = v * (255 - (s * (255 - f)));

	  s += ds;
	}
      break;
    case 3:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255 * (1 - s);
	  *p++ = v * (255 - s * f);
	  *p++ = v * 255;

	  s += ds;
	}
      break;
    case 4:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * (255 - (s * (255 - f)));
	  *p++ = v * (255 * (1 - s));
	  *p++ = v * 255;

	  s += ds;
	}
      break;
    case 5:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255;
	  *p++ = v * 255 * (1 - s);
	  *p++ = v * (255 - s * f);

	  s += ds;
	}
      break;
    }
}

static void
color_select_update_hue_value (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  float h, v, dv, s;
  int f;
  int i;

  p = csf->buffer;

  csf->y += 1;
  h = (255 - csf->y) * 359 / csf->height;

  if (h < 0)
    h = 0;
  if (h > 359)
    h = 359;

  h /= 60;
  f = (h - (int) h) * 255;

  v = 0;
  dv = 1.0 / csf->width;

  s = csf->values[SATURATION] / 100.0;

  switch ((int) h)
    {
    case 0:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255;
	  *p++ = v * (255 - (s * (255 - f)));
	  *p++ = v * 255 * (1 - s);

	  v += dv;
	}
      break;
    case 1:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * (255 - s * f);
	  *p++ = v * 255;
	  *p++ = v * 255 * (1 - s);

	  v += dv;
	}
      break;
    case 2:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255 * (1 - s);
	  *p++ = v *255;
	  *p++ = v * (255 - (s * (255 - f)));

	  v += dv;
	}
      break;
    case 3:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255 * (1 - s);
	  *p++ = v * (255 - s * f);
	  *p++ = v * 255;

	  v += dv;
	}
      break;
    case 4:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * (255 - (s * (255 - f)));
	  *p++ = v * (255 * (1 - s));
	  *p++ = v * 255;

	  v += dv;
	}
      break;
    case 5:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255;
	  *p++ = v * 255 * (1 - s);
	  *p++ = v * (255 - s * f);

	  v += dv;
	}
      break;
    }
}

static void
color_select_update_saturation_value (csf)
     ColorSelectFill *csf;
{
  unsigned char *p;
  float h, v, dv, s;
  int f;
  int i;

  p = csf->buffer;

  csf->y += 1;
  s = (float) csf->y / csf->height;

  if (s < 0)
    s = 0;
  if (s > 1)
    s = 1;

  s = 1 - s;

  h = (float) csf->values[HUE];
  if (h >= 360)
    h -= 360;
  h /= 60;
  f = (h - (int) h) * 255;

  v = 0;
  dv = 1.0 / csf->width;

  switch ((int) h)
    {
    case 0:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255;
	  *p++ = v * (255 - (s * (255 - f)));
	  *p++ = v * 255 * (1 - s);

	  v += dv;
	}
      break;
    case 1:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * (255 - s * f);
	  *p++ = v * 255;
	  *p++ = v * 255 * (1 - s);

	  v += dv;
	}
      break;
    case 2:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255 * (1 - s);
	  *p++ = v *255;
	  *p++ = v * (255 - (s * (255 - f)));

	  v += dv;
	}
      break;
    case 3:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255 * (1 - s);
	  *p++ = v * (255 - s * f);
	  *p++ = v * 255;

	  v += dv;
	}
      break;
    case 4:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * (255 - (s * (255 - f)));
	  *p++ = v * (255 * (1 - s));
	  *p++ = v * 255;

	  v += dv;
	}
      break;
    case 5:
      for (i = 0; i < csf->width; i++)
	{
	  *p++ = v * 255;
	  *p++ = v * 255 * (1 - s);
	  *p++ = v * (255 - s * f);

	  v += dv;
	}
      break;
    }
}
