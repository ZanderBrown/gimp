/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "autodialog.h"
#include "cursorutil.h"
#include "flip_tool.h"
#include "gdisplay.h"
#include "temp_buf.h"
#include "transform_core.h"

#define FLIP       0

typedef struct _FlipOptions FlipOptions;

struct _FlipOptions
{
  ToolType    type;
};

/*  forward function declarations  */
static Tool *     tools_new_flip_horz  (void);
static Tool *     tools_new_flip_vert  (void);
static TempBuf *  flip_tool_flip_horz  (Tool *, void *);
static TempBuf *  flip_tool_flip_vert  (Tool *, void *);
static void       flip_change_type     (int);

/*  Static variables  */
static FlipOptions *flip_options = NULL;


static void
flip_type_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  flip_change_type ((int) client_data);
}

static FlipOptions *
create_flip_options ()
{
  FlipOptions *options;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *radio_frame;
  GtkWidget *radio_box;
  GtkWidget *radio_button;
  GtkData *owner = NULL;
  int i;
  char *button_names[2] =
  {
    "Horizontal",
    "Vertical",
  };

  /*  the new options structure  */
  options = (FlipOptions *) xmalloc (sizeof (FlipOptions));
  options->type = FLIP_HORZ;

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  
  /*  the main label  */
  label = gtk_label_new ("Flip Tool Options");
  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  the radio frame and box  */
  radio_frame = gtk_frame_new (NULL);
  gtk_box_pack (vbox, radio_frame, FALSE, FALSE, 0, GTK_PACK_START);

  radio_box = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (radio_box, 0);
  gtk_container_add (radio_frame, radio_box);

  /*  the radio buttons  */
  for (i = 0; i < 2; i++)
    {
      radio_button = gtk_radio_button_new (owner);
      if (!i)
	owner = gtk_button_get_owner (radio_button);
      gtk_box_pack (radio_box, radio_button, FALSE, FALSE, 0, GTK_PACK_START);
      gtk_callback_add (gtk_button_get_state (radio_button),
			flip_type_callback,
			(void *) ((int) FLIP_HORZ + i));
      label = gtk_label_new (button_names[i]);
      gtk_label_set_alignment (label, 0.0, 0.5);
      gtk_container_add (radio_button, label);
      gtk_widget_show (label);
      gtk_widget_show (radio_button);
    }
  gtk_widget_show (radio_box);
  gtk_widget_show (radio_frame);

  /*  Register this selection options widget with the main tools options dialog  */
  tools_register_options (FLIP_HORZ, vbox);
  tools_register_options (FLIP_VERT, vbox);

  return options;
}

void *
flip_tool_transform_horz (tool, gdisp_ptr, state)
     Tool * tool;
     gpointer gdisp_ptr;
     int state;
{
  TransformCore * transform_core;

  transform_core = (TransformCore *) tool->private;

  switch (state)
    {
    case INIT :
      transform_info = NULL;
      break;

    case MOTION :
      break;

    case RECALC :
      break;

    case FINISH :
      transform_core->trans_info[FLIP] *= -1.0;
      return (flip_tool_flip_horz (tool, gdisp_ptr));
      break;
    }

  return NULL;
}

void *
flip_tool_transform_vert (tool, gdisp_ptr, state)
     Tool * tool;
     gpointer gdisp_ptr;
     int state;
{
  TransformCore * transform_core;

  transform_core = (TransformCore *) tool->private;

  switch (state)
    {
    case INIT :
      transform_info = NULL;
      break;

    case MOTION :
      break;

    case RECALC :
      break;

    case FINISH :
      transform_core->trans_info[FLIP] *= -1.0;
      return (flip_tool_flip_vert (tool, gdisp_ptr));
      break;
    }

  return NULL;
}

Tool *
tools_new_flip ()
{
  if (! flip_options)
    flip_options = create_flip_options ();

  switch (flip_options->type)
    {
    case FLIP_HORZ:
      return tools_new_flip_horz ();
      break;
    case FLIP_VERT:
      return tools_new_flip_vert ();
      break;
    default:
      return NULL;
      break;
    }
}

void
tools_free_flip_tool (tool)
     Tool * tool;
{
  transform_core_free (tool);
}

static Tool *
tools_new_flip_horz ()
{
  Tool * tool;
  TransformCore * private;

  tool = transform_core_new (FLIP_HORZ, NON_INTERACTIVE);

  private = tool->private;

  /*  set the rotation specific transformation attributes  */
  private->trans_func = flip_tool_transform_horz;
  private->trans_info[FLIP] = 1.0;
    
  return tool;
}

static Tool *
tools_new_flip_vert ()
{
  Tool * tool;
  TransformCore * private;

  tool = transform_core_new (FLIP_VERT, NON_INTERACTIVE);

  private = tool->private;

  /*  set the rotation specific transformation attributes  */
  private->trans_func = flip_tool_transform_vert;
  private->trans_info[FLIP] = 1.0;

  return tool;
}

static TempBuf *
flip_tool_flip_horz (tool, gdisp_ptr)
     Tool * tool;
     void * gdisp_ptr;
{
  TransformCore * transform_core;
  GDisplay * gdisp;
  TempBuf * orig;
  TempBuf * new;
  int i, j, b;
  int rowstride;
  unsigned char * src, * s;
  unsigned char * dest, * d;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;
  orig = transform_core->original;

  if (!orig)
    return NULL;

  if (transform_core->trans_info[FLIP] > 0)
    {
      new = temp_buf_copy (orig, NULL);
      new->x = orig->x;
      new->y = orig->y;
    }
  else
    {
      new = temp_buf_new (orig->width, orig->height, orig->bytes,
			  orig->x, orig->y, NULL);

      rowstride = orig->width * orig->bytes;
      src = temp_buf_data (orig);
      dest = temp_buf_data (new);

      for (i = 0; i < orig->height; i++)
	{
	  s = src + (rowstride - orig->bytes);
	  d = dest;

	  j = orig->width;
	  while (j--)
	    {
	      for (b = 0; b < orig->bytes; b++)
		d[b] = s[b];
	      d += orig->bytes;
	      s -= orig->bytes;
	    }

	  src += rowstride;
	  dest += rowstride;
	}
    }

  return new;
}

static TempBuf *
flip_tool_flip_vert (tool, gdisp_ptr)
     Tool * tool;
     void * gdisp_ptr;
{
  TransformCore * transform_core;
  GDisplay * gdisp;
  TempBuf * orig;
  TempBuf * new;
  int i;
  int rowstride;
  unsigned char * src;
  unsigned char * dest;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;
  orig = transform_core->original;

  if (!orig)
    return NULL;

  if (transform_core->trans_info[FLIP] > 0)
    {
      new = temp_buf_copy (orig, NULL);
      new->x = orig->x;
      new->y = orig->y;
    }
  else
    {
      new = temp_buf_new (orig->width, orig->height, orig->bytes,
			  orig->x, orig->y, NULL);

      rowstride = orig->width * orig->bytes;
      src = temp_buf_data (orig) + rowstride * (orig->height - 1);
      dest = temp_buf_data (new);

      for (i = 0; i < orig->height; i++)
	{
	  memcpy (dest, src, rowstride);
	  src -= rowstride;
	  dest += rowstride;
	}
    }

  return new;
}

static void
flip_change_type (new_type)
     int new_type;
{
  if (flip_options->type != new_type)
    {
      /*  change the type, free the old tool, create the new tool  */
      flip_options->type = new_type;
      
      tools_select (flip_options->type);
    }
}
