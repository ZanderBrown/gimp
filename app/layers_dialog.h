/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef  __LAYERS_DIALOG_H__
#define  __LAYERS_DIALOG_H__

void   layers_dialog_create  (void);
void   layers_dialog_update_image_list (void);
void   layers_dialog_dirty_layer (void *);
void   layers_dialog_remove_layer (void *);
void   layers_dialog_add_layer (int, int);
void   layers_dialog_flush (void);
void   layers_dialog_free    (void);

#endif  /*  __LAYERS_DIALOG_H__  */
