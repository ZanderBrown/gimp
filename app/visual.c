/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <math.h>
#include "appenv.h"
#include "errors.h"
#include "gimprc.h"
#include "visual.h"

/*  Global variables  */
GdkVisual *color_visual;  /* RGB color visual  */
GdkVisual *gray_visual;	  /* Grayscale visual  */
int emulate_gray;         /* emulate gray with color? */

/*  These arrays are calculated for quick 24 bit to 16 color conversions  */
unsigned long  lookup_red [256];
unsigned long  lookup_green [256];
unsigned long  lookup_blue [256];


/*  We need to select the visuals with highest depth for the
 *  display, unless the user has explicitly requested a lesser
 *  depth.
 */

typedef struct _visual_type
{
  GdkVisualType class;
  gint depth;
} visual_type;

visual_type color_visual_types[] =
{
  { GDK_VISUAL_TRUE_COLOR, 24 },
  { GDK_VISUAL_DIRECT_COLOR, 24 },
  { GDK_VISUAL_TRUE_COLOR, 16 },
  { GDK_VISUAL_DIRECT_COLOR, 16 },
  { GDK_VISUAL_TRUE_COLOR, 15 },
  { GDK_VISUAL_DIRECT_COLOR, 15 },
  { GDK_VISUAL_PSEUDO_COLOR, 8 },
};

int num_color_visual_types = sizeof (color_visual_types) / sizeof (visual_type);


static void
fill_lookup_array (array, shift, prec, gamma)
     unsigned long * array;
     int shift;
     int prec;
     double gamma;
{
  int i;
  int val;
  double ind;
  double one_over_gamma;

  if (gamma != 0.0)
    one_over_gamma = 1.0 / gamma;

  for (i = 0; i < 256; i++)
    {
      ind = (double) i / 255.0;
      val = (int) (255 * pow (ind, one_over_gamma));
      array [i] = ((val >> prec) << shift);
    }
}


void
get_standard_visuals ()
{
  int type;

  /*  find color visual  */
  color_visual = NULL;

  for (type = 0; type < num_color_visual_types; type++)
    if ((color_visual = gdk_visual_get_best_with_both (color_visual_types[type].depth,
						       color_visual_types[type].class)))
      {
	if ((color_visual->type == GDK_VISUAL_TRUE_COLOR) ||
	    (color_visual->type == GDK_VISUAL_DIRECT_COLOR))
	  {
	    /*  initialize 24 bit to 16 bit color lookup acceleration  */
	    fill_lookup_array (lookup_red,
			       color_visual->red_shift, (8 - color_visual->red_prec),
			       gamma_val);
	    fill_lookup_array (lookup_green,
			       color_visual->green_shift, (8 - color_visual->green_prec),
			       gamma_val);
	    fill_lookup_array (lookup_blue,
			       color_visual->blue_shift, (8 - color_visual->blue_prec),
			       gamma_val);
	  }
	break;
      }

  if (!color_visual)
    fatal_error ("Unable to find suitable visual for image display.");

  gray_visual = color_visual;
  if ((color_visual->type == GDK_VISUAL_TRUE_COLOR) ||
      (color_visual->type == GDK_VISUAL_DIRECT_COLOR))
    emulate_gray = 1;
  else
    emulate_gray = 0;
}
