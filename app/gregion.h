/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GREGION_H__
#define __GREGION_H__

#include "boundary.h"
#include "temp_buf.h"

/* OPERATIONS */

#define ADD     0
#define SUB     1
#define REPLACE 2

/*  Half way point where a region is no longer visible in a selection  */
#define HALF_WAY 127

typedef struct _gregion GRegion;

struct _gregion
{
  MaskBuf *   mask;            /*  actual mask data                      */
  int         width;           /*  width of mask in pixels               */
  int         height;          /*  height of mask in pixels              */
  int         id;              /*  unique mask id                        */

  int         empty;           /*  is the region empty?                  */
  int         bounds_known;    /*  do we need to recalculate the bounds  */
  int         boundary_known;  /*  have we recalculated the boundary?    */
  BoundSeg  * boundary;        /*  outline of selected region            */
  int         num_segments;    /*  number of lines in boundary           */

  int         x1, y1;          /*  coordinates for bounding box          */
  int         x2, y2;          /*  lower right hand coordinate           */
};


GRegion *     gregion_new             (int, int);
GRegion *     gregion_copy            (GRegion *, GRegion *);
BoundSeg *    gregion_boundary        (GRegion *, int *);
void          gregion_free            (GRegion *);
int           gregion_bounds          (GRegion *, int *, int *, int *, int *);
int           gregion_value           (GRegion *, int, int);
int           gregion_is_empty        (GRegion *);
void          gregion_add_segment     (GRegion *, int, int, int, int);
void          gregion_sub_segment     (GRegion *, int, int, int, int);
void          gregion_combine_rect    (GRegion *, int, int, int, int, int);
void          gregion_combine_ellipse (GRegion *, int, int, int, int, int, int);
void          gregion_combine_region  (GRegion *, int, GRegion *);
void          gregion_feather         (GRegion *, GRegion *, double, int);
void          gregion_push_undo       (int, GRegion *);
void          gregion_clear           (int, GRegion *);
void          gregion_invert          (int, GRegion *);
void          gregion_sharpen         (int, GRegion *);
void          gregion_all             (int, GRegion *);
void          gregion_border          (int, GRegion *, int);

#endif  /*  __GREGION_H__  */
