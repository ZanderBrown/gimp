/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "appenv.h"
#include "actionarea.h"
#include "autodialog.h"
#include "boundary.h"
#include "by_color_select.h"
#include "colormaps.h"
#include "draw_core.h"
#include "general.h"
#include "gimage_mask.h"
#include "gimprc.h"
#include "gdisplay.h"
#include "image_buf.h"
#include "rect_select.h"
#include "visual.h"

#define DEFAULT_FUZZINESS 15
#define PREVIEW_WIDTH   256
#define PREVIEW_HEIGHT  256
#define PREVIEW_EVENT_MASK  GDK_EXPOSURE_MASK | \
                            GDK_BUTTON_PRESS_MASK | \
                            GDK_ENTER_NOTIFY_MASK


typedef struct _ByColorSelect ByColorSelect;

struct _ByColorSelect
{
  int  x, y;         /*  Point from which to execute seed fill  */
  int  operation;    /*  add, subtract, normal color selection  */
  int  replace;      /*  replace the current selection          */
};

typedef struct _ByColorDialog ByColorDialog;

struct _ByColorDialog
{
  GtkWidget   *shell;
  GtkWidget   *preview;
  GtkWidget   *gimage_name;
  GtkObserver  fuzziness_observer;
  ImageBuf     image;
  GdkGC       *gc;

  int          threshold; /*  threshold value for color select  */
  int          operation; /*  Add, Subtract, Replace  */
  GImage      *gimage;    /*  gimage which is currently under examination  */
};

/*  by_color select action functions  */

static void   by_color_select_button_press   (Tool *, GdkEventButton *, gpointer);
static void   by_color_select_button_release (Tool *, GdkEventButton *, gpointer);
static void   by_color_select_motion         (Tool *, GdkEventMotion *, gpointer);
static void   by_color_select_control        (Tool *, int, gpointer);

static ByColorDialog *  by_color_select_new_dialog     (void);
static void             by_color_select_render         (ByColorDialog *, GImage *);
static void             by_color_select_draw           (ByColorDialog *, GImage *);
static gint             by_color_select_preview_events (GtkWidget *, GdkEvent *);
static void             by_color_select_type_callback  (GtkWidget *, gpointer, gpointer);
static void             by_color_select_reset_callback (GtkWidget *, gpointer, gpointer);
static void             by_color_select_close_callback (GtkWidget *, gpointer, gpointer);
static gint             by_color_select_fuzzy_update   (GtkObserver *, GtkData *);
static void             by_color_select_disconnect     (GtkObserver *, GtkData *);
static void             by_color_select_preview_button_press (ByColorDialog *, GdkEventButton *);


static SelectionOptions *by_color_options = NULL;
static ByColorDialog *by_color_dialog = NULL;


/*  by_color selection machinery  */

static int
is_pixel_sufficiently_different (col1, col2, antialias, threshold, bytes, has_alpha)
     unsigned char * col1;
     unsigned char * col2;
     int antialias;
     int threshold;
     int bytes;
     int has_alpha;
{
  int diff;
  int max;
  int b;
  int alpha;

  max = 0;
  alpha = (has_alpha) ? bytes - 1 : bytes;

  /*  if there is an alpha channel, never select transparent regions  */
  if (has_alpha && col2[alpha] == 0)
    return 0;

  for (b = 0; b < alpha; b++)
    {
      diff = col1[b] - col2[b];
      diff = abs (diff);
      if (diff > max)
	max = diff;
    }

  if (antialias)
    {
      float aa;

      aa = 1.5 - ((float) max / threshold);
      if (aa <= 0)
	return 0;
      else if (aa < 0.5)
	return (unsigned char) (aa * 512);
      else
	return 255;
    }
  else
    {
      if (max > threshold)
	return 0;
      else
	return 255;
    }
}

static void
by_color_select_color (gimage, region, col, antialias, threshold, has_alpha)
     GImage *gimage;
     GRegion *region;
     unsigned char *col;
     int antialias;
     int threshold;
     int has_alpha;
{
  /*  Scan over the gimage's active layer, finding pixels within the specified
   *  threshold from the given R, G, & B values.  If antialiasing is on,
   *  use the same antialiasing scheme as in fuzzy_select.  Modify the gimage's
   *  mask to reflect the additional selection
   */
  unsigned char *image_data;
  unsigned char *mask_data;
  unsigned char rgb[MAX_CHANNELS];
  int width, height;
  int bytes, alpha;
  int image_rowstride;
  int mask_rowstride;
  int i, j;

  /*  Get the image information  */
  image_data = gimage_data (gimage);
  bytes = gimage_bytes (gimage);
  alpha = bytes - 1;
  width = gimage_width (gimage);
  height = gimage_height (gimage);
  image_rowstride = bytes * width;

  mask_data = mask_buf_data (region->mask);
  mask_rowstride = region->width;

  /*  iterate over the entire image  */
  for (i = 0; i < height; i++)
    {
      for (j = 0; j < width; j++)
	{
	  /*  Get the rgb values for the color  */
	  gimage_get_color (gimage, rgb, image_data);

	  /*  Plug the alpha channel in there  */
	  if (has_alpha)
	    rgb[alpha] = image_data[alpha];

	  /*  Find how closely the colors match  */
	  *mask_data = is_pixel_sufficiently_different (col, rgb, antialias,
							threshold, bytes, has_alpha);

	  image_data += bytes;
	  mask_data ++;
	}
    }
}


/*  by_color select action functions  */

static void
by_color_select_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay *gdisp;
  ByColorSelect *by_color_sel;

  gdisp = (GDisplay *) gdisp_ptr;
  by_color_sel = (ByColorSelect *) tool->private;

  if (!by_color_dialog)
    return;

  by_color_sel->x = bevent->x;
  by_color_sel->y = bevent->y;

  tool->state = ACTIVE;
  tool->gdisp_ptr = gdisp_ptr;

  /*  Defaults  */
  by_color_sel->replace = FALSE;
  by_color_sel->operation = ADD;

  /*  Based on modifiers, and the "by color" dialog's selection mode  */
  if (bevent->state & GDK_SHIFT_MASK)
    by_color_sel->operation = ADD;
  else if (bevent->state & GDK_CONTROL_MASK)
    by_color_sel->operation = SUB;
  else
    {
      by_color_sel->replace = (by_color_dialog->operation == REPLACE);
      if (by_color_dialog->operation == SUB)
	by_color_sel->operation = SUB;
    }

  /*  Make sure the "by color" select dialog is visible  */
  if (! GTK_WIDGET_VISIBLE (by_color_dialog->shell))
    gtk_widget_show (by_color_dialog->shell);

  /*  Update the by_color_dialog's active gdisp pointer  */
  if (by_color_dialog->gimage)
    by_color_dialog->gimage->by_color_select = FALSE;
  by_color_dialog->gimage = gdisp->gimage;
  gdisp->gimage->by_color_select = TRUE;
}

static void
by_color_select_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  ByColorSelect * by_color_sel;
  GDisplay * gdisp;
  GRegion * new_region;
  int x, y;
  int bytes;
  int has_alpha;
  unsigned char * image_data;

  gdisp = (GDisplay *) gdisp_ptr;
  by_color_sel = (ByColorSelect *) tool->private;

  tool->state = INACTIVE;

  /*  First take care of the case where the user "cancels" the action  */
  if (! (bevent->state & GDK_BUTTON3_MASK))
    {
      gdisplay_untransform_coords (gdisp, by_color_sel->x, by_color_sel->y, &x, &y, TRUE, 0);
      image_data = gimage_data (gdisp->gimage);
      bytes = gimage_bytes (gdisp->gimage);
      has_alpha = gimage_has_alpha (gdisp->gimage);
      image_data += y * bytes * gimage_width (gdisp->gimage) + x * bytes;

      new_region = gregion_new (gdisp->gimage->width,
				gdisp->gimage->height);

      by_color_select_color (gdisp->gimage, new_region, image_data,
			     by_color_options->antialias,
			     by_color_dialog->threshold, has_alpha);

      /*  if applicable, replace the current selection  */
      /*  or insure that a floating selection is anchored down...  */
      if (by_color_sel->replace)
	gimage_mask_clear (gdisp->gimage);
      else
	gimage_mask_anchor (gdisp->gimage);

      if (by_color_options->feather)
	gregion_feather (new_region,
			 gdisp->gimage->region,
			 by_color_options->feather_radius,
			 by_color_sel->operation);
      else
	gregion_combine_region (gdisp->gimage->region,
				by_color_sel->operation,
				new_region);

      /*  show selection on all views  */
      gdisplays_selection_visibility (gdisp->gimage->ID, 1);
      gdisplays_flush ();

      /*  update the preview window  */
      by_color_select_render (by_color_dialog, gdisp->gimage);
      by_color_select_draw (by_color_dialog, gdisp->gimage);

      gregion_free (new_region);
    }
}

static void
by_color_select_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
}

static void
by_color_select_control (tool, action, gdisp_ptr)
     Tool *tool;
     int action;
     gpointer gdisp_ptr;
{
  ByColorSelect * by_color_sel;

  by_color_sel = (ByColorSelect *) tool->private;

  switch (action)
    {
    case PAUSE :
      break;
    case RESUME :
      break;
    case HALT :
      if (by_color_dialog)
	{
	  if (by_color_dialog->gimage)
	    by_color_dialog->gimage->by_color_select = FALSE;
	  by_color_dialog->gimage = NULL;
	  by_color_select_close_callback (NULL, (gpointer) by_color_dialog, NULL);
	}
      break;
    }
}

Tool *
tools_new_by_color_select ()
{
  Tool * tool;
  ByColorSelect * private;

  /*  The tool options  */
  if (!by_color_options)
    by_color_options = create_selection_options (BY_COLOR_SELECT);

  /*  The "by color" dialog  */
  if (!by_color_dialog)
    by_color_dialog = by_color_select_new_dialog ();
  else
    if (!GTK_WIDGET_VISIBLE (by_color_dialog->shell))
      gtk_widget_show (by_color_dialog->shell);

  tool = (Tool *) xmalloc (sizeof (Tool));
  private = (ByColorSelect *) xmalloc (sizeof (ByColorSelect));

  tool->type = BY_COLOR_SELECT;
  tool->state = INACTIVE;
  tool->scroll_lock = 1;  /*  Disallow scrolling  */
  tool->private = (void *) private;
  tool->button_press_func = by_color_select_button_press;
  tool->button_release_func = by_color_select_button_release;
  tool->motion_func = by_color_select_motion;
  tool->arrow_keys_func = standard_arrow_keys_func;
  tool->control_func = by_color_select_control;

  return tool;
}

void
tools_free_by_color_select (tool)
     Tool * tool;
{
  ByColorSelect * by_color_sel;

  by_color_sel = (ByColorSelect *) tool->private;

  /*  Close the color select dialog  */
  if (by_color_dialog)
    {
      if (by_color_dialog->gimage)
	by_color_dialog->gimage->by_color_select = FALSE;
      by_color_select_close_callback (NULL, (gpointer) by_color_dialog, NULL);
    }

  xfree (by_color_sel);
}

void
by_color_select_initialize (gimage_ptr)
     void *gimage_ptr;
{
  GImage *gimage;

  gimage = (GImage *) gimage_ptr;

  /*  update the preview window  */
  if (by_color_dialog)
    {
      by_color_dialog->gimage = gimage;
      gimage->by_color_select = TRUE;
      by_color_select_render (by_color_dialog, gimage);
      by_color_select_draw (by_color_dialog, gimage);
    }
}


/****************************/
/*  Select by Color dialog  */
/****************************/

/*  the action area structure  */
static ActionAreaItem action_items[] =
{
  { "Close", by_color_select_close_callback, NULL, NULL }
};

ByColorDialog *
by_color_select_new_dialog ()
{
  ByColorDialog *bcd;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *frame;
  GtkWidget *options_box;
  GtkWidget *label;
  GtkWidget *util_box;
  GtkWidget *push_button;
  GtkWidget *slider;
  GtkWidget *action_area;
  GtkWidget *radio_box;
  GtkWidget *radio_button;
  GtkData   *data;
  GtkData   *owner = NULL;
  int i;
  char *button_names[3] =
  {
    "Replace",
    "Add",
    "Subtract"
  };
  int button_values[3] =
  {
    REPLACE,
    ADD,
    SUB
  };

  gtk_push_visual (gray_visual);
  gtk_push_colormap (graycmap);

  bcd = xmalloc (sizeof (ByColorDialog));
  bcd->image = NULL;
  bcd->gc = NULL;
  bcd->gimage = NULL;
  bcd->operation = REPLACE;
  bcd->threshold = DEFAULT_FUZZINESS;

  /*  The shell and main vbox  */
  bcd->shell = gtk_window_new ("By Color Selection", GTK_WINDOW_TOPLEVEL);
  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (bcd->shell, vbox);

  /*  The horizontal box containing preview  & options box */
  hbox = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);

  /*  The preview  */
  util_box = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (util_box, 0);
  gtk_box_pack (hbox, util_box, FALSE, FALSE, 0, GTK_PACK_START);
  frame = gtk_frame_new (NULL);
  gtk_frame_set_type (frame, GTK_SHADOW_IN);
  gtk_box_pack (util_box, frame, FALSE, FALSE, 0, GTK_PACK_START);
  bcd->preview = gtk_drawing_area_new (PREVIEW_WIDTH, PREVIEW_HEIGHT,
				       by_color_select_preview_events,
				       PREVIEW_EVENT_MASK);
  gtk_widget_set_user_data (bcd->preview, (gpointer) bcd);
  gtk_container_add (frame, bcd->preview);

  /*  create the image buf for the preview  */
  bcd->image = image_buf_create (GRAY_BUF, PREVIEW_WIDTH, PREVIEW_HEIGHT);

  gtk_widget_show (bcd->preview);
  gtk_widget_show (frame);
  gtk_widget_show (util_box);

  /*  options box  */
  options_box = gtk_vbox_new (FALSE, 10);
  gtk_container_set_border_width (options_box, 5);
  gtk_box_pack (hbox, options_box, TRUE, TRUE, 0, GTK_PACK_START);

  /*  Create the active brush label  */
  util_box = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (util_box, 0);
  gtk_box_pack (options_box, util_box, FALSE, FALSE, 0, GTK_PACK_START);
  bcd->gimage_name = gtk_label_new ("Inactive");
  gtk_box_pack (util_box, bcd->gimage_name, FALSE, FALSE, 2, GTK_PACK_START);

  gtk_widget_show (bcd->gimage_name);
  gtk_widget_show (util_box);

  /*  Create the selection mode radio box  */
  frame = gtk_frame_new ("Selection Mode");
  gtk_box_pack (options_box, frame, FALSE, FALSE, 0, GTK_PACK_START);

  radio_box = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (radio_box, 0);
  gtk_container_add (frame, radio_box);

  /*  the radio buttons  */
  for (i = 0; i < 3; i++)
    {
      radio_button = gtk_radio_button_new (owner);
      if (!i)
	owner = gtk_button_get_owner (radio_button);
      gtk_box_pack (radio_box, radio_button, FALSE, FALSE, 0, GTK_PACK_START);
      gtk_callback_add (gtk_button_get_state (radio_button),
			by_color_select_type_callback,
			(gpointer) button_values[i]);
      label = gtk_label_new (button_names[i]);
      gtk_label_set_alignment (label, 0.0, 0.5);
      gtk_container_add (radio_button, label);
      gtk_widget_show (label);
      gtk_widget_show (radio_button);
    }
  gtk_widget_show (radio_box);
  gtk_widget_show (frame);

  /*  Create the opacity scale widget  */
  util_box = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (util_box, 0);
  gtk_box_pack (options_box, util_box, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Fuzziness Threshold");
  gtk_box_pack (util_box, label, FALSE, FALSE, 2, GTK_PACK_START);
  data = (GtkData *) gtk_data_adjustment_new (bcd->threshold, 0.0, 255.0, 1.0, 1.0, 1.0);
  slider = gtk_hscale_new ((GtkDataAdjustment *) data);
  gtk_box_pack (util_box, slider, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (slider, GTK_POS_TOP);
  bcd->fuzziness_observer.update = by_color_select_fuzzy_update;
  bcd->fuzziness_observer.disconnect = by_color_select_disconnect;
  bcd->fuzziness_observer.user_data = bcd;
  gtk_data_attach (data, &bcd->fuzziness_observer);

  gtk_widget_show (label);
  gtk_widget_show (slider);
  gtk_widget_show (util_box);

  /*  The reset push button  */
  push_button = gtk_push_button_new ();
  gtk_box_pack (options_box, push_button, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_callback_add (gtk_button_get_state (push_button),
		    by_color_select_reset_callback,
		    (gpointer) bcd);
  label = gtk_label_new ("Reset");
  gtk_container_add (push_button, label);

  gtk_widget_show (label);
  gtk_widget_show (push_button);

  /*  The action area  */
  action_items[0].user_data = bcd;
  action_area = build_action_area (action_items, 1);
  gtk_box_pack (vbox, action_area, TRUE, TRUE, 2, GTK_PACK_START);

  gtk_widget_show (action_area);
  gtk_widget_show (options_box);
  gtk_widget_show (hbox);
  gtk_widget_show (vbox);
  gtk_widget_show (bcd->shell);

  gtk_pop_visual ();
  gtk_pop_colormap ();

  return bcd;
}

static void
by_color_select_render (bcd, gimage)
     ByColorDialog *bcd;
     GImage *gimage;
{
  MaskBuf * mask_buf;
  MaskBuf * scaled_buf = NULL;
  unsigned char *buf;
  PixelRegion srcPR, destPR;
  unsigned char * src;
  int width, height;
  int i;
  int new_buf;

  mask_buf = gimage_mask (gimage);
  if ((mask_buf->width > PREVIEW_WIDTH) ||
      (mask_buf->height > PREVIEW_HEIGHT))
    {
      if (((float) mask_buf->width / (float) PREVIEW_WIDTH) >
	  ((float) mask_buf->height / (float) PREVIEW_HEIGHT))
	{
	  width = PREVIEW_WIDTH;
	  height = (mask_buf->height * PREVIEW_WIDTH) / mask_buf->width;
	}
      else
	{
	  width = (mask_buf->width * PREVIEW_HEIGHT) / mask_buf->height;
	  height = PREVIEW_HEIGHT;
	}

      new_buf = 1;
    }
  else
    {
      width = mask_buf->width;
      height = mask_buf->height;

      new_buf = 0;
    }

  if ((width != image_buf_width (bcd->image)) ||
      (height != image_buf_height (bcd->image)))
    {
      image_buf_destroy (bcd->image);
      bcd->image = image_buf_create (GRAY_BUF, width, height);
      gtk_widget_set_usize (bcd->preview, width, height);
    }

  /*  clear the image buf  */
  buf = (unsigned char *) xmalloc (image_buf_row_bytes (bcd->image));
  memset (buf, 0, image_buf_row_bytes (bcd->image));
  for (i = 0; i < image_buf_height (bcd->image); i++)
    image_buf_draw_row (bcd->image, buf, 0, i, image_buf_width (bcd->image));
  xfree (buf);

  /*  if the region is empty, no need to scale and update again  */
  if (gregion_is_empty (gimage->region))
    return;

  if (new_buf)
    {
      srcPR.bytes = mask_buf->bytes;
      srcPR.w = mask_buf->width;
      srcPR.h = mask_buf->height;
      srcPR.x = srcPR.y = 0;
      srcPR.rowstride = srcPR.bytes * mask_buf->width;
      srcPR.data = mask_buf_data (mask_buf);

      scaled_buf = mask_buf_new (width, height);
      destPR.x = 0;
      destPR.y = 0;
      destPR.w = width;
      destPR.h = height;
      destPR.rowstride = srcPR.bytes * width;
      destPR.data = mask_buf_data (scaled_buf);

      scale_region (&srcPR, &destPR);
    }
  else
    scaled_buf = mask_buf;

  /*  Get the pointer into the brush mask data  */
  src = mask_buf_data (scaled_buf);

  for (i = 0; i < height; i++)
    {
      image_buf_draw_row (bcd->image, src, 0, i, width);

      src += scaled_buf->width;
    }

  if (new_buf)
    mask_buf_free (scaled_buf);
}

static void
by_color_select_draw (bcd, gimage)
     ByColorDialog *bcd;
     GImage *gimage;
{
  /*  Draw the image buf to the preview window  */
  if (bcd->gc)
    image_buf_put (bcd->image, bcd->preview->window, bcd->gc);

  /*  Update the gimage label to reflect the displayed gimage name  */
  gtk_label_set (bcd->gimage_name, prune_filename (gimage->filename));
}

static gint
by_color_select_preview_events (widget, event)
     GtkWidget *widget;
     GdkEvent *event;
{
  ByColorDialog *bcd;
  GdkEventButton *bevent;

  bcd = (ByColorDialog *) gtk_widget_get_user_data (widget);

  switch (event->type)
    {
    case GDK_EXPOSE:
      /*  If this is the first exposure  */
      if (!bcd->gc)
	bcd->gc = gdk_gc_new (widget->window);

      image_buf_put (bcd->image, bcd->preview->window, bcd->gc);
      break;

    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;

      by_color_select_preview_button_press (bcd, bevent);
      break;

    case GDK_ENTER_NOTIFY:
      install_colormap (GRAY);
      break;

    default:
      break;
    }

  return FALSE;
}

static void
by_color_select_type_callback (widget, client_data, call_data)
     GtkWidget *widget;
     gpointer client_data;
     gpointer call_data;
{
  if (by_color_dialog)
    by_color_dialog->operation = (int) client_data;
}

static void
by_color_select_reset_callback (widget, client_data, call_data)
     GtkWidget *widget;
     gpointer client_data;
     gpointer call_data;
{
  ByColorDialog *bcd;

  bcd = (ByColorDialog *) client_data;
  if (!bcd->gimage)
    return;

  /*  reset the mask  */
  gimage_mask_clear (bcd->gimage);

  /*  show selection on all views  */
  gdisplays_selection_visibility (bcd->gimage->ID, 1);
  gdisplays_flush ();

  /*  update the preview window  */
  by_color_select_render (bcd, bcd->gimage);
  by_color_select_draw (bcd, bcd->gimage);
}

static void
by_color_select_close_callback (widget, client_data, call_data)
     GtkWidget *widget;
     gpointer client_data;
     gpointer call_data;
{
  ByColorDialog *bcd;

  bcd = (ByColorDialog *) client_data;
  if (GTK_WIDGET_VISIBLE (bcd->shell))
    gtk_widget_hide (bcd->shell);
}

static gint
by_color_select_fuzzy_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  ByColorDialog *bcd;

  adj_data = (GtkDataAdjustment *) data;
  bcd = (ByColorDialog *) observer->user_data;

  bcd->threshold = (int) adj_data->value;

  return FALSE;
}

static void
by_color_select_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

static void
by_color_select_preview_button_press (bcd, bevent)
     ByColorDialog *bcd;
     GdkEventButton *bevent;
{
  GRegion * new_region;
  int x, y;
  int bytes;
  int has_alpha;
  unsigned char * image_data;
  int replace, operation;

  if (!bcd->gimage)
    return;

  /*  Defaults  */
  replace = FALSE;
  operation = ADD;

  /*  Based on modifiers, and the "by color" dialog's selection mode  */
  if (bevent->state & GDK_SHIFT_MASK)
    operation = ADD;
  else if (bevent->state & GDK_CONTROL_MASK)
    operation = SUB;
  else
    {
      replace = (by_color_dialog->operation == REPLACE);
      if (by_color_dialog->operation == SUB)
	operation = SUB;
    }

  /*  Find x, y and modify selection  */
  x = gimage_width (bcd->gimage) * bevent->x / image_buf_width (bcd->image);
  y = gimage_height (bcd->gimage) * bevent->y / image_buf_height (bcd->image);

  image_data = gimage_data (bcd->gimage);
  bytes = gimage_bytes (bcd->gimage);
  has_alpha = gimage_has_alpha (bcd->gimage);
  image_data += y * bytes * gimage_width (bcd->gimage) + x * bytes;

  new_region = gregion_new (bcd->gimage->width,
			    bcd->gimage->height);

  by_color_select_color (bcd->gimage, new_region, image_data,
			 by_color_options->antialias,
			 bcd->threshold, has_alpha);

  /*  if applicable, replace the current selection  */
  /*  or insure that a floating selection is anchored down...  */
  if (replace)
    gimage_mask_clear (bcd->gimage);
  else
    gimage_mask_anchor (bcd->gimage);

  if (by_color_options->feather)
    gregion_feather (new_region,
		     bcd->gimage->region,
		     by_color_options->feather_radius,
		     operation);
  else
    gregion_combine_region (bcd->gimage->region,
			    operation,
			    new_region);

  /*  show selection on all views  */
  gdisplays_selection_visibility (bcd->gimage->ID, 1);
  gdisplays_flush ();

  /*  update the preview window  */
  by_color_select_render (bcd, bcd->gimage);
  by_color_select_draw (bcd, bcd->gimage);

  gregion_free (new_region);
}
