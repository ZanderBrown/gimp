/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include "appenv.h"
#include "errors.h"
#include "floating_sel.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "interface.h"
#include "layer.h"
#include "undo.h"


/*  feathering variables  */
double feather_radius = 5;
int    border_radius = 5;


MaskBuf *
gimage_mask (gimage)
     GImage * gimage;
{
  return gimage->region->mask;
}


BoundSeg *
gimage_mask_boundary (gimage, num_segs)
     GImage * gimage;
     int * num_segs;
{
  /*  If there is a floating selection, return that boundary
   *  instead of the selection mask boundary.
   */
  if (gimage->floating_sel >= 0)
    return floating_sel_boundary (gimage, num_segs);
  else
    return gregion_boundary (gimage->region, num_segs);
}


BoundSeg *
gimage_mask_2nd_boundary (gimage, num_segs)
     GImage * gimage;
     int * num_segs;
{
  /*  If there is a floating selection,
   *  returns gimage selection mask boundary.
   */
  if (gimage->floating_sel >= 0)
    return gregion_boundary (gimage->region, num_segs);
  else
    {
      *num_segs = 0;
      return NULL;
    }
}


int
gimage_mask_bounds (gimage, x1, y1, x2, y2)
     GImage * gimage;
     int * x1, * y1, * x2, * y2;
{
  /*  If there is a floating selection, return the bounds of that
   *  instead of the selection mask bounds.
   */
  if (gimage->floating_sel >= 0)
    return floating_sel_bounds (gimage, x1, y1, x2, y2);
  else
    return gregion_bounds (gimage->region, x1, y1, x2, y2);
}


int
gimage_mask_2nd_bounds (gimage, x1, y1, x2, y2)
     GImage * gimage;
     int * x1, * y1, * x2, * y2;
{
  /*  If there is a floating selection
   *  gimage selection mask bounds
   */
  if (gimage->floating_sel >= 0)
    return gregion_bounds (gimage->region, x1, y1, x2, y2);
  else
    {
      *x1 = *y1 = 0;
      *x2 = *y2 = 0;
      return FALSE;
    }
}


int
gimage_mask_value (gimage, x, y)
     GImage * gimage;
     int x, y;
{
  /*  If there is a floating selection, return the value of that
   *  instead of the selection mask value.
   */
  if (gimage->floating_sel >= 0)
    return floating_sel_value (gimage, x, y);
  else
    return gregion_value (gimage->region, x, y);
}


TempBuf *
gimage_mask_extract (gimage, cut_gimage)
     GImage * gimage;
     int cut_gimage;
{
  TempBuf * buf;
  MaskBuf * sel_mask;
  Channel * channel;
  Layer * layer;
  PixelRegion srcPR, destPR, maskPR;
  unsigned char bg[MAX_CHANNELS];
  int bytes, type;
  int x1, y1, x2, y2;

  /*  If there is a floating sel, call a different extract routine  */
  if (gimage->floating_sel >= 0)
    return floating_sel_extract (gimage, cut_gimage);

  if (gimage_mask_bounds (gimage, &x1, &y1, &x2, &y2))
    {
      /*  How many bytes in the temp buffer?  */
      switch (gimage_type (gimage))
	{
	case RGB_GIMAGE: case RGBA_GIMAGE:
	  bytes = 4; type = RGB; break;
	case GRAY_GIMAGE: case GRAYA_GIMAGE:
	  bytes = 2; type = GRAY; break;
	case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
	  bytes = 4; type = INDEXED; break;
	}

      /*  get the selection mask */
      sel_mask = gimage->region->mask;

      gimage_get_background (gimage, bg);

      /*  If a cut was specified push an undo  */
      if (cut_gimage)
	{
	  if (gimage->active_channel >= 0)
	    {
	      if ((channel = channel_get_ID (gimage->active_channel)))
		channel_apply_image (channel, x1, y1, x2, y2, NULL);
	    }
	  else
	    {
	      /*  anchor layer  */
	      if ((layer = layer_get_ID (gimage->active_layer)))
		layer_apply_image (layer, x1, y1, x2, y2, NULL, 1);
	    }
	}

      /*  Allocate the temp buffer  */
      buf = temp_buf_new ((x2 - x1), (y2 - y1), bytes, x1, y1, NULL);

      /* configure the pixel regions  */
      srcPR.bytes = gimage_bytes (gimage);
      srcPR.w = (x2 - x1);
      srcPR.h = (y2 - y1);
      srcPR.rowstride = gimage_width (gimage) * srcPR.bytes;
      srcPR.data = gimage_data (gimage) + y1 * srcPR.rowstride +
	x1 * srcPR.bytes;
      
      destPR.bytes = buf->bytes;
      destPR.rowstride = buf->width * buf->bytes;
      destPR.data = temp_buf_data (buf);

      maskPR.bytes = sel_mask->bytes;
      maskPR.rowstride = sel_mask->width;
      maskPR.data = mask_buf_data (sel_mask) + y1 * maskPR.rowstride + 
	x1 * maskPR.bytes;

      extract_from_region (&srcPR, &destPR, &maskPR, gimage_cmap (gimage),
			   bg, type, gimage_has_alpha (gimage), cut_gimage);

      if (cut_gimage)
	/*  Clear the region  */
	gregion_clear (gimage->ID, gimage->region);

      return buf;
    }
  else
    return NULL;
}


Layer *
gimage_mask_float (gimage)
     GImage * gimage;
{
  Layer * layer;
  TempBuf * buf;

  /*  If there is no region to float, or there is already a floating selection,
   *  Issue a warning and return NULL
   */
  if (gregion_is_empty (gimage->region))
    {
      message_box ("Float Select: No selection to float.", NULL, NULL);
      return NULL;
    }
  if (gimage->floating_sel != -1)
    {
      message_box ("Float Select: A floating selection exists already.", NULL, NULL);
      return NULL;
    }

  /*  Start an undo group  */
  undo_push_group_start (FLOAT_MASK_UNDO);

  /*  Cut the selected region  */
  buf = gimage_mask_extract (gimage, 1);

  /*  Create a new layer from the buffer  */
  layer = layer_from_buf (gimage, buf, 0, 0, "Floating Selection",
			  OPAQUE, NORMAL);

  /*  Set the offsets  */
  layer->offset_x = buf->x;
  layer->offset_y = buf->y;

  /*  Free the temp buffer  */
  temp_buf_free (buf);

  /*  Floating selection swap undo  */
  undo_push_fs_swap (gimage->ID);

  /*  End an undo group  */
  undo_push_group_end ();

  /*  add the floating layer to the gimage  */
  gimage_add_floating_layer (gimage, layer);
  
  /*  invalidate the gimage's boundary variables  */
  gimage->boundary_known = FALSE;

  return layer;
}


void
gimage_mask_clear (gimage)
     GImage * gimage;
{
  /* turn selection off */
  gdisplays_selection_visibility (gimage->ID, 0);

  if (gimage->floating_sel >= 0)
    floating_sel_clear (gimage);
  else
    gregion_clear (gimage->ID, gimage->region);

  gdisplays_flush ();
}


void
gimage_mask_anchor (gimage)
     GImage * gimage;
{
  /* turn selection off */
  gdisplays_selection_visibility (gimage->ID, 0);

  if (gimage->floating_sel >= 0)
    floating_sel_anchor (gimage);
  else
    /*  push the current gregion onto the undo stack  */
    gregion_push_undo (gimage->ID, gimage->region);

  gdisplays_flush ();
}


void
gimage_mask_invert (gimage)
     GImage * gimage;
{
  /* turn selection off */
  gdisplays_selection_visibility (gimage->ID, 0);

  if (gimage->floating_sel >= 0)
    floating_sel_invert (gimage);
  else
    gregion_invert (gimage->ID, gimage->region);

  /* turn selection on */
  gdisplays_selection_visibility (gimage->ID, 1);

  gdisplays_flush ();
}


void
gimage_mask_sharpen (gimage)
     GImage * gimage;
{
  /*  No need to play with the selection visibility
   *  because sharpen will not change the outline
   */
  if (gimage->floating_sel >= 0)
    floating_sel_sharpen (gimage);
  else
    gregion_sharpen (gimage->ID, gimage->region);
}


void
gimage_mask_all (gimage)
     GImage * gimage;
{
  /* turn selection off */
  gdisplays_selection_visibility (gimage->ID, 0);

  if (gimage->floating_sel >= 0)
    floating_sel_all (gimage);
  else
    gregion_all (gimage->ID, gimage->region);

  /* turn selection on */
  gdisplays_selection_visibility (gimage->ID, 1);
  gdisplays_flush ();
}


void
gimage_mask_none (gimage)
     GImage * gimage;
{
  /* turn selection off */
  gdisplays_selection_visibility (gimage->ID, 0);

  if (gimage->floating_sel >= 0)
    floating_sel_clear (gimage);
  else
    gregion_clear (gimage->ID, gimage->region);

  gdisplays_flush ();
}


void
gimage_mask_feather (gimage)
     GImage * gimage;
{
  /* turn selection off */
  gdisplays_selection_visibility (gimage->ID, 0);

  if (gimage->floating_sel >= 0)
    floating_sel_feather (gimage, feather_radius);
  else
    {
      /*  push the current gregion onto the undo stack  */
      gregion_push_undo (gimage->ID, gimage->region);
      /*  feather the region  */
      gregion_feather (gimage->region, gimage->region, feather_radius, REPLACE);
    }

  /* turn selection on */
  gdisplays_selection_visibility (gimage->ID, 1);
  gdisplays_flush ();
}


void
gimage_mask_border (gimage)
     GImage * gimage;
{
  /* turn selection off */
  gdisplays_selection_visibility (gimage->ID, 0);

  if (gimage->floating_sel >= 0)
    floating_sel_border (gimage, border_radius);
  else
    /*  feather the region  */
    gregion_border (gimage->ID, gimage->region, border_radius);

  /* turn selection on */
  gdisplays_selection_visibility (gimage->ID, 1);
  gdisplays_flush ();
}
