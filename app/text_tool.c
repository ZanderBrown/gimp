/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "appenv.h"
#include "actionarea.h"
#include "buildmenu.h"
#include "colormaps.h"
#include "edit_selection.h"
#include "errors.h"
#include "floating_sel.h"
#include "gdisplay.h"
#include "general.h"
#include "global_edit.h"
#include "interface.h"
#include "palette.h"
#include "selection.h"
#include "text_tool.h"
#include "tools.h"
#include "visual.h"

#define FONT_LIST_WIDTH  125
#define FONT_LIST_HEIGHT 200

#define PIXELS 0
#define POINTS 1

#define FOUNDRY   0
#define FAMILY    1
#define WEIGHT    2
#define SLANT     3
#define SET_WIDTH 4
#define SPACING   10

#define SUPERSAMPLE 3

typedef struct _TextTool TextTool;
struct _TextTool
{
  GtkWidget *shell;
  GtkWidget *main_vbox;
  GtkWidget *font_list;
  GtkWidget *size_menu;
  GtkWidget *size_text;
  GtkWidget *text_frame;
  GtkWidget *the_text;
  GtkWidget *menus[5];
  GtkWidget **foundry_items;
  GtkWidget **weight_items;
  GtkWidget **slant_items;
  GtkWidget **set_width_items;
  GtkWidget **spacing_items;
  GtkWidget *antialias_toggle;
  GtkObserver antialias_observer;
  GtkStyle  *style;
  GdkFont *font;
  int click_x;
  int click_y;
  int font_index;
  int size_type;
  int antialias;
  int foundry;
  int weight;
  int slant;
  int set_width;
  int spacing;
  void *gdisp_ptr;
};

typedef struct _FontInfo FontInfo;
struct _FontInfo
{
  char *family;         /* The font family this info struct describes. */
  int *foundries;       /* An array of valid foundries. */
  int *weights;         /* An array of valid weights. */
  int *slants;          /* An array of valid slants. */
  int *set_widths;      /* An array of valid set widths. */
  int *spacings;        /* An array of valid spacings */
  int **combos;         /* An array of valid combinations of the above 5 items */
  int ncombos;          /* The number of elements in the "combos" array */
  link_ptr fontnames;   /* An list of valid fontnames.
			 * This is used to make sure a family/foundry/weight/slant/set_width
			 *  combination is valid.
			 */
};

static void       text_button_press       (Tool *, GdkEventButton *, gpointer);
static void       text_button_release     (Tool *, GdkEventButton *, gpointer);
static void       text_motion             (Tool *, GdkEventMotion *, gpointer);
static void       text_control            (Tool *, int, gpointer);

static void       text_resize_text_widget (TextTool *);
static void       text_create_dialog      (TextTool *);
static void       text_ok_callback        (GtkWidget *, gpointer, gpointer);
static void       text_cancel_callback    (GtkWidget *, gpointer, gpointer);
static void       text_pixels_callback    (GtkWidget *, gpointer, gpointer);
static void       text_points_callback    (GtkWidget *, gpointer, gpointer);
static void       text_foundry_callback   (GtkWidget *, gpointer, gpointer);
static void       text_weight_callback    (GtkWidget *, gpointer, gpointer);
static void       text_slant_callback     (GtkWidget *, gpointer, gpointer);
static void       text_set_width_callback (GtkWidget *, gpointer, gpointer);
static void       text_spacing_callback   (GtkWidget *, gpointer, gpointer);
static gint       text_size_key_function  (guint, guint, gpointer);
static gint       text_antialias_update   (GtkObserver *, GtkData *);
static gint       text_font_item_update   (GtkObserver *, GtkData *);
static void       text_font_item_disconnect(GtkObserver *, GtkData *);
static void       text_disconnect         (GtkObserver *, GtkData *);
static void       text_validate_combo     (TextTool *, int);

static void       text_get_fonts          (void);
static void       text_insert_font        (FontInfo **, int *, char *);
static link_ptr   text_insert_field       (link_ptr, char *, int);
static char*      text_get_field          (char *, int);
static int        text_field_to_index     (char **, int, char *);
static int        text_is_xlfd_font_name  (char *);

static void       text_load_font          (TextTool *);
static void       text_render             (TextTool *);
static void       text_gdk_image_to_region (GdkImage *, int, int, int, PixelRegion *);


static ActionAreaItem action_items[] =
{
  { "OK", text_ok_callback, NULL, NULL },
  { "Cancel", text_cancel_callback, NULL, NULL },
};

static MenuItem size_metric_items[] =
{
  { "Pixels", 0, 0, text_pixels_callback, (gpointer) PIXELS, NULL, NULL },
  { "Points", 0, 0, text_points_callback, (gpointer) POINTS, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL }
};

static TextTool *the_text_tool = NULL;
static FontInfo **font_info;
static int nfonts = -1;

static link_ptr foundries = NULL;
static link_ptr weights = NULL;
static link_ptr slants = NULL;
static link_ptr set_widths = NULL;
static link_ptr spacings = NULL;

static char **foundry_array = NULL;
static char **weight_array = NULL;
static char **slant_array = NULL;
static char **set_width_array = NULL;
static char **spacing_array = NULL;

static int nfoundries = 0;
static int nweights = 0;
static int nslants = 0;
static int nset_widths = 0;
static int nspacings = 0;

static void *text_options = NULL;

Tool*
tools_new_text ()
{
  Tool * tool;

  if (! text_options)
    text_options = tools_register_no_options (TEXT, "Text Tool Options");

  tool = xmalloc (sizeof (Tool));
  if (!the_text_tool)
    {
      the_text_tool = xmalloc (sizeof (TextTool));
      the_text_tool->shell = NULL;
      the_text_tool->font_list = NULL;
      the_text_tool->size_menu = NULL;
      the_text_tool->size_text = NULL;
      the_text_tool->text_frame = NULL;
      the_text_tool->the_text = NULL;
      the_text_tool->style = NULL;
      the_text_tool->font = NULL;
      the_text_tool->font_index = 0;
      the_text_tool->size_type = PIXELS;
      the_text_tool->antialias = 1;
      the_text_tool->foundry = 0;
      the_text_tool->weight = 0;
      the_text_tool->slant = 0;
      the_text_tool->set_width = 0;
      the_text_tool->spacing = 0;
    }

  tool->type = TEXT;
  tool->state = INACTIVE;
  tool->scroll_lock = 1;  /* Do not allow scrolling */
  tool->private = (void *) the_text_tool;
  tool->button_press_func = text_button_press;
  tool->button_release_func = text_button_release;
  tool->motion_func = text_motion;
  tool->arrow_keys_func = edit_sel_arrow_keys_func;
  tool->control_func = text_control;

  return tool;
}

void
tools_free_text (tool)
     Tool * tool;
{
}

static void
text_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  TextTool * text_tool;

  gdisp = gdisp_ptr;
  text_tool = tool->private;
  text_tool->gdisp_ptr = gdisp_ptr;

  tool->state = ACTIVE;
  tool->gdisp_ptr = gdisp_ptr;

  /*  Check if the pointer was clicked inside a selection...If so, start
   *  an edit selection, which allows the selection to be dragged.
   */
  if (gdisplay_mask_value (gdisp, bevent->x, bevent->y) > HALF_WAY)
    {
      gdk_pointer_grab (gdisp->canvas->window, FALSE,
			GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			NULL, NULL, bevent->time);

      init_edit_selection (tool, gdisp_ptr, bevent);
      return;
    }

  gdisplay_untransform_coords (gdisp, bevent->x, bevent->y,
			       &text_tool->click_x, &text_tool->click_y,
			       TRUE, 0);

  if (!text_tool->shell)
    text_create_dialog (text_tool);

  switch (gdisp->gimage->type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE:
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      if (!GTK_WIDGET_VISIBLE (text_tool->antialias_toggle))
	gtk_widget_show (text_tool->antialias_toggle);
      break;
    case INDEXED_GIMAGE:
      if (GTK_WIDGET_VISIBLE (text_tool->antialias_toggle))
	gtk_widget_hide (text_tool->antialias_toggle);
      break;
    }

  if (!GTK_WIDGET_VISIBLE (text_tool->shell))
    gtk_widget_show (text_tool->shell);
}

static void
text_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  tool->state = INACTIVE;
}

static void
text_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
}

static void
text_control (tool, action, gdisp_ptr)
     Tool *tool;
     int action;
     gpointer gdisp_ptr;
{
}

static void
text_resize_text_widget (text_tool)
     TextTool * text_tool;
{
  /*
  if (text_tool->font)
    {
      if (text_tool->style)
	{
	  gtk_style_detach (text_tool->style);
	  gtk_style_destroy (text_tool->style);
	}
      text_tool->style = gtk_style_new (-1);
      text_tool->style->font = text_tool->font;
    }

  if (text_tool->style)
    {
      gtk_style_attach (text_tool->style, text_tool->the_text->window);
      gtk_widget_set_style (text_tool->the_text, text_tool->style);

      gtk_widget_show (text_tool->the_text);
    }
  */
}

static void
text_create_dialog (text_tool)
     TextTool * text_tool;
{
  GtkWidget *top_hbox;
  GtkWidget *right_vbox;
  GtkWidget *text_hbox;
  GtkWidget *list_box;
  GtkWidget *list_item;
  GtkWidget *size_opt_menu;
  GtkWidget *toggle_label;
  GtkWidget *menu_table;
  GtkWidget *menu_label;
  GtkWidget *option_menu;
  GtkWidget **menu_items[5];
  GtkWidget *action_area;
  GtkWidget *alignment;
  GtkData *item_state;
  GtkObserver *item_observer;
  int nmenu_items[5];
  char *menu_strs[5];
  char **menu_item_strs[5];
  GtkCallback menu_callbacks[5];
  MenuItem *items;
  int i, j;

  /* Create the shell and vertical & horizontal boxes */
  text_tool->shell = gtk_window_new ("Text Tool", GTK_WINDOW_TOPLEVEL);
  text_tool->main_vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_add (text_tool->shell, text_tool->main_vbox);
  top_hbox = gtk_hbox_new (FALSE, 10);
  gtk_container_set_border_width (top_hbox, 0);
  gtk_box_pack (text_tool->main_vbox, top_hbox, TRUE, TRUE, 0, GTK_PACK_START);

  /* Create the font listbox  */
  list_box = gtk_listbox_new ();
  gtk_listbox_set_shadow_type (list_box, GTK_SHADOW_IN);
  gtk_box_pack (top_hbox, list_box, TRUE, TRUE, 0, GTK_PACK_START);
  text_tool->font_list = gtk_listbox_get_list (list_box);
  gtk_widget_set_usize (text_tool->font_list, FONT_LIST_WIDTH, FONT_LIST_HEIGHT);
  gtk_list_set_selection_mode (text_tool->font_list, GTK_SELECTION_BROWSE);
  if (nfonts == -1)
    text_get_fonts ();
  for (i = 0; i < nfonts; i++)
    {
      list_item = gtk_list_item_new_with_label (font_info[i]->family);
      gtk_container_add (text_tool->font_list, list_item);
      gtk_widget_show (list_item);
      item_observer = (GtkObserver *) xmalloc (sizeof (GtkObserver));
      item_state = gtk_list_item_get_state (list_item);
      item_observer->update = text_font_item_update;
      item_observer->disconnect = text_font_item_disconnect;
      item_observer->user_data = (gpointer) i;
      gtk_data_attach (item_state, item_observer);
    }

  /* Create the box to hold options  */
  right_vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (right_vbox, 0);
  gtk_box_pack (top_hbox, right_vbox, TRUE, TRUE, 2, GTK_PACK_START);

  /* Create the text hbox, size text, and fonts size metric option menu */
  text_hbox = gtk_hbox_new (FALSE, 5);
  gtk_container_set_border_width (text_hbox, 0);
  gtk_box_pack (right_vbox, text_hbox, FALSE, FALSE, 0, GTK_PACK_START);
  text_tool->size_text = gtk_text_entry_new ();
  gtk_text_entry_set_key_function (text_tool->size_text, text_size_key_function, text_tool);
  gtk_widget_set_usize (text_tool->size_text, 75, 25);
  gtk_text_entry_set_text (text_tool->size_text, "50");
  gtk_box_pack (text_hbox, text_tool->size_text, TRUE, TRUE, 0, GTK_PACK_START);

  /* Create the size menu */
  size_metric_items[0].user_data = text_tool;
  size_metric_items[1].user_data = text_tool;
  text_tool->size_menu = build_menu (size_metric_items, NULL);
  size_opt_menu = gtk_option_menu_new ();
  gtk_box_pack (text_hbox, size_opt_menu, FALSE, FALSE, 0, GTK_PACK_START);

  /* create the text entry widget */
  text_tool->text_frame = gtk_frame_new (NULL);
  gtk_frame_set_type (text_tool->text_frame, GTK_SHADOW_NONE);
  gtk_box_pack (text_tool->main_vbox, text_tool->text_frame, FALSE, FALSE, 5, GTK_PACK_START);
  text_tool->the_text = gtk_text_entry_new ();
  gtk_container_add (text_tool->text_frame, text_tool->the_text);

  /* create the antialiasing toggle button  */
  text_tool->antialias_toggle = gtk_check_button_new ();
  gtk_box_pack (right_vbox, text_tool->antialias_toggle, FALSE, FALSE, 0, GTK_PACK_START);
  toggle_label = gtk_label_new ("Antialiasing");
  gtk_label_set_alignment (toggle_label, 0.0, 0.5);
  gtk_container_add (text_tool->antialias_toggle, toggle_label);
  gtk_widget_show (toggle_label);
  text_tool->antialias_observer.update = text_antialias_update;
  text_tool->antialias_observer.disconnect = text_disconnect;
  text_tool->antialias_observer.user_data = text_tool;

  /* Allocate the arrays for the foundry, weight, slant, set_width and spacing menu items */
  text_tool->foundry_items = (GtkWidget **) xmalloc (sizeof (GtkWidget *) * nfoundries);
  text_tool->weight_items = (GtkWidget **) xmalloc (sizeof (GtkWidget *) * nweights);
  text_tool->slant_items = (GtkWidget **) xmalloc (sizeof (GtkWidget *) * nslants);
  text_tool->set_width_items = (GtkWidget **) xmalloc (sizeof (GtkWidget *) * nset_widths);
  text_tool->spacing_items = (GtkWidget **) xmalloc (sizeof (GtkWidget *) * nspacings);

  menu_items[0] = text_tool->foundry_items;
  menu_items[1] = text_tool->weight_items;
  menu_items[2] = text_tool->slant_items;
  menu_items[3] = text_tool->set_width_items;
  menu_items[4] = text_tool->spacing_items;

  nmenu_items[0] = nfoundries;
  nmenu_items[1] = nweights;
  nmenu_items[2] = nslants;
  nmenu_items[3] = nset_widths;
  nmenu_items[4] = nspacings;

  menu_strs[0] = "Foundry";
  menu_strs[1] = "Weight";
  menu_strs[2] = "Slant";
  menu_strs[3] = "Set width";
  menu_strs[4] = "Spacing";

  menu_item_strs[0] = foundry_array;
  menu_item_strs[1] = weight_array;
  menu_item_strs[2] = slant_array;
  menu_item_strs[3] = set_width_array;
  menu_item_strs[4] = spacing_array;

  menu_callbacks[0] = text_foundry_callback;
  menu_callbacks[1] = text_weight_callback;
  menu_callbacks[2] = text_slant_callback;
  menu_callbacks[3] = text_set_width_callback;
  menu_callbacks[4] = text_spacing_callback;

  menu_table = gtk_table_new (5, 2, FALSE);
  gtk_container_set_border_width (menu_table, 0);
  gtk_box_pack (right_vbox, menu_table, TRUE, TRUE, 0, GTK_PACK_START);

  /* Create the other menus */
  for (i = 0; i < 5; i++)
    {
      menu_label = gtk_label_new (menu_strs[i]);
      gtk_label_set_alignment (menu_label, 0.0, 0.5);
      gtk_table_attach (menu_table, menu_label, 0, 1, i, i + 1,
			FALSE, TRUE, 0,
			FALSE, FALSE, 5);

      if (menu_item_strs[i])
	{
	  items = (MenuItem *) xmalloc (sizeof (MenuItem) * (nmenu_items[i] + 1));
	  for (j = 0; j < nmenu_items[i]; j++)
	    {
	      items[j].label = menu_item_strs[i][j];
	      items[j].accelerator_key = 0;
	      items[j].accelerator_mods = 0;
	      items[j].callback = menu_callbacks[i];
	      items[j].user_data = text_tool;
	      items[j].subitems = NULL;
	    }
	  items[j].label = NULL;
	}

      alignment = gtk_alignment_new (0.0, 0.0, 0.0, 0.0);
      gtk_table_attach (menu_table, alignment, 1, 2, i, i + 1,
			TRUE, TRUE, 10,
			FALSE, FALSE, 5);
      text_tool->menus[i] = build_menu (items, NULL);
      option_menu = gtk_option_menu_new ();
      gtk_container_add (alignment, option_menu);

      gtk_widget_show (menu_label);
      gtk_widget_show (option_menu);
      gtk_widget_show (alignment);

      gtk_option_menu_set_menu (option_menu, text_tool->menus[i]);
      xfree (items);
    }

  /* Create the action area */
  action_items[0].user_data = text_tool;
  action_items[1].user_data = text_tool;
  action_area = build_action_area (action_items, 2);
  gtk_box_pack (text_tool->main_vbox, action_area, FALSE, FALSE, 0, GTK_PACK_END);

  /* Show the widgets */
  gtk_widget_show (menu_table);
  gtk_widget_show (text_tool->antialias_toggle);
  gtk_widget_show (text_tool->size_text);
  gtk_widget_show (size_opt_menu);
  gtk_widget_show (text_hbox);
  gtk_widget_show (list_box);
  gtk_widget_show (right_vbox);
  gtk_widget_show (top_hbox);
  gtk_widget_show (text_tool->the_text);
  gtk_widget_show (text_tool->text_frame);
  gtk_widget_show (action_area);
  gtk_widget_show (text_tool->main_vbox);
  gtk_widget_show (text_tool->shell);

  /* Post initialization */
  gtk_option_menu_set_menu (size_opt_menu, text_tool->size_menu);
  gtk_data_attach (gtk_button_get_state (text_tool->antialias_toggle),
		   &text_tool->antialias_observer);

  if (nfonts)
    text_load_font (text_tool);
}

static void
text_ok_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer   client_data;
     gpointer   call_data;
{
  TextTool * text_tool;

  text_tool = (TextTool *) client_data;

  if (GTK_WIDGET_VISIBLE (text_tool->shell))
    gtk_widget_hide (text_tool->shell);

  text_render (text_tool);
}

static void
text_cancel_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer   client_data;
     gpointer   call_data;
{
  TextTool * text_tool;

  text_tool = (TextTool *) client_data;

  if (GTK_WIDGET_VISIBLE (text_tool->shell))
    gtk_widget_hide (text_tool->shell);
}

static gint
text_font_item_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  FontInfo *font;
  int index;
  GtkDataInt *int_data;

  int_data = (GtkDataInt*) data;

  /*  Is this font being selected?  */
  if (int_data->value != GTK_STATE_SELECTED)
    return FALSE;

  index = (int) observer->user_data;

  the_text_tool->font_index = index;

  text_load_font (the_text_tool);
  font = font_info[the_text_tool->font_index];

  if (the_text_tool->foundry && !font->foundries[the_text_tool->foundry])
    {
      the_text_tool->foundry = 0;
      /*XtVaSetValues (text_tool->menus[1], XmNmenuHistory, text_tool->foundry_items[0], NULL);*/
    }
  if (the_text_tool->weight && !font->weights[the_text_tool->weight])
    {
      the_text_tool->weight = 0;
      /*XtVaSetValues (text_tool->menus[2], XmNmenuHistory, text_tool->weight_items[0], NULL);*/
    }
  if (the_text_tool->slant && !font->slants[the_text_tool->slant])
    {
      the_text_tool->slant = 0;
      /*XtVaSetValues (text_tool->menus[3], XmNmenuHistory, text_tool->slant_items[0], NULL);*/
    }
  if (the_text_tool->set_width && !font->set_widths[the_text_tool->set_width])
    {
      the_text_tool->set_width = 0;
      /*XtVaSetValues (text_tool->menus[4], XmNmenuHistory, text_tool->set_width_items[0], NULL);*/
    }
  if (the_text_tool->spacing && !font->spacings[the_text_tool->spacing])
    {
      the_text_tool->spacing = 0;
      /*XtVaSetValues (text_tool->menus[5], XmNmenuHistory, text_tool->spacing_items[0], NULL);*/
    }

  /*
  for (i = 0; i < nfoundries; i++)
    gtk_widget_set_state (text_tool->foundry_items[i], font->foundries[i] ?
			  GTK_STATE_NORMAL : GTK_STATE_INSENSITIVE);
  for (i = 0; i < nweights; i++)
    gtk_widget_set_state (text_tool->weight_items[i], font->weights[i] ?
			  GTK_STATE_NORMAL : GTK_STATE_INSENSITIVE);
  for (i = 0; i < nslants; i++)
    gtk_widget_set_state (text_tool->slant_items[i], font->slants[i] ?
			  GTK_STATE_NORMAL : GTK_STATE_INSENSITIVE);
  for (i = 0; i < nset_widths; i++)
    gtk_widget_set_state (text_tool->set_width_items[i], font->set_widths[i] ?
			  GTK_STATE_NORMAL : GTK_STATE_INSENSITIVE);
  for (i = 0; i < nspacings; i++)
    gtk_widget_set_state (text_tool->spacing_items[i], font->spacings[i] ?
			  GTK_STATE_NORMAL : GTK_STATE_INSENSITIVE);
			  */

  return FALSE;
}

static void
text_font_item_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  xfree (observer);
}

static void
text_pixels_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  TextTool *text_tool;

  text_tool = (TextTool *) client_data;
  text_tool->size_type = PIXELS;

  text_load_font (text_tool);
}

static void
text_points_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  TextTool *text_tool;

  text_tool = (TextTool *) client_data;
  text_tool->size_type = POINTS;

  text_load_font (text_tool);
}

static void
text_foundry_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  the_text_tool->foundry = (int) client_data;
  text_validate_combo (the_text_tool, 0);

  text_load_font (the_text_tool);
}

static void
text_weight_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  the_text_tool->weight = (int) client_data;
  text_validate_combo (the_text_tool, 1);

  text_load_font (the_text_tool);
}

static void
text_slant_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  the_text_tool->slant = (int) client_data;
  text_validate_combo (the_text_tool, 2);

  text_load_font (the_text_tool);
}

static void
text_set_width_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  the_text_tool->set_width = (int) client_data;
  text_validate_combo (the_text_tool, 3);

  text_load_font (the_text_tool);
}

static void
text_spacing_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  the_text_tool->spacing = (int) client_data;
  text_validate_combo (the_text_tool, 4);

  text_load_font (the_text_tool);
}

static gint
text_size_key_function  (keyval, state, client_data)
     guint keyval;
     guint state;
     gpointer client_data;
{
  TextTool *text_tool;
  gchar *text;
  gint return_val;

  text_tool = (TextTool *) client_data;
  return_val = FALSE;

  if (keyval == XK_Return)
    {
      return_val = TRUE;

      text = gtk_text_entry_get_text (text_tool->size_text);
      text_load_font (text_tool);
    }

  return return_val;
}

static gint
text_antialias_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataInt *int_data;
  TextTool *text_tool;

  text_tool = (TextTool *) observer->user_data;
  int_data = (GtkDataInt *) data;

  if (int_data->value == GTK_STATE_ACTIVATED)
    text_tool->antialias = TRUE;
  else if (int_data->value == GTK_STATE_DEACTIVATED)
    text_tool->antialias = FALSE;

  return FALSE;
}

static void
text_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

static void
text_validate_combo (text_tool, which)
     TextTool *text_tool;
     int which;
{
  FontInfo *font;
  int which_val;
  int new_combo[5];
  int best_combo[5];
  int best_matches;
  int matches;
  int i;

  font = font_info[text_tool->font_index];

  switch (which)
    {
    case 0:
      which_val = text_tool->foundry;
      break;
    case 1:
      which_val = text_tool->weight;
      break;
    case 2:
      which_val = text_tool->slant;
      break;
    case 3:
      which_val = text_tool->set_width;
      break;
    case 4:
      which_val = text_tool->spacing;
      break;
    }

  best_matches = -1;
  best_combo[0] = 0;
  best_combo[1] = 0;
  best_combo[2] = 0;
  best_combo[3] = 0;
  best_combo[4] = 0;

  for (i = 0; i < font->ncombos; i++)
    {
      /* we must match the which field */
      if (font->combos[i][which] == which_val)
	{
	  matches = 0;
	  new_combo[0] = 0;
	  new_combo[1] = 0;
	  new_combo[2] = 0;
	  new_combo[3] = 0;
	  new_combo[4] = 0;

	  if ((text_tool->foundry == 0) || (text_tool->foundry == font->combos[i][0]))
	    {
	      matches++;
	      if (text_tool->foundry)
		new_combo[0] = font->combos[i][0];
	    }
	  if ((text_tool->weight == 0) || (text_tool->weight == font->combos[i][1]))
	    {
	      matches++;
	      if (text_tool->weight)
		new_combo[1] = font->combos[i][1];
	    }
	  if ((text_tool->slant == 0) || (text_tool->slant == font->combos[i][2]))
	    {
	      matches++;
	      if (text_tool->slant)
		new_combo[2] = font->combos[i][2];
	    }
	  if ((text_tool->set_width == 0) || (text_tool->set_width == font->combos[i][3]))
	    {
	      matches++;
	      if (text_tool->set_width)
		new_combo[3] = font->combos[i][3];
	    }
	  if ((text_tool->spacing == 0) || (text_tool->spacing == font->combos[i][4]))
	    {
	      matches++;
	      if (text_tool->spacing)
		new_combo[4] = font->combos[i][4];
	    }

	  /* if we get all 5 matches simply return */
	  if (matches == 5)
	    return;

	  if (matches > best_matches)
	    {
	      best_matches = matches;
	      best_combo[0] = new_combo[0];
	      best_combo[1] = new_combo[1];
	      best_combo[2] = new_combo[2];
	      best_combo[3] = new_combo[3];
	      best_combo[4] = new_combo[4];
	    }
	}
    }

  text_tool->foundry = best_combo[0];
  text_tool->weight = best_combo[1];
  text_tool->slant = best_combo[2];
  text_tool->set_width = best_combo[3];
  text_tool->spacing = best_combo[4];

  /*
    XtVaSetValues (text_tool->menus[1],
    XmNmenuHistory, text_tool->foundry_items[text_tool->foundry],
    NULL);
    XtVaSetValues (text_tool->menus[2],
    XmNmenuHistory, text_tool->weight_items[text_tool->weight],
    NULL);
    XtVaSetValues (text_tool->menus[3],
    XmNmenuHistory, text_tool->slant_items[text_tool->slant],
    NULL);
    XtVaSetValues (text_tool->menus[4],
    XmNmenuHistory, text_tool->set_width_items[text_tool->set_width],
    NULL);
    XtVaSetValues (text_tool->menus[5],
    XmNmenuHistory, text_tool->spacing_items[text_tool->spacing],
    NULL);
    */
}

static void
text_get_fonts ()
{
  char **fontnames;
  char *fontname;
  char *field;
  link_ptr temp_list;
  int num_fonts;
  int index;
  int i, j;

  /* construct a valid font pattern */

  fontnames = XListFonts (DISPLAY, "-*-*-*-*-*-*-0-0-75-75-*-0-*-*", 32767, &num_fonts);

  /* the maximum size of the table is the number of font names returned */
  font_info = xmalloc (sizeof (FontInfo**) * num_fonts);

  /* insert the fontnames into a table */
  nfonts = 0;
  for (i = 0; i < num_fonts; i++)
    if (text_is_xlfd_font_name (fontnames[i]))
      {
	text_insert_font (font_info, &nfonts, fontnames[i]);

	foundries = text_insert_field (foundries, fontnames[i], FOUNDRY);
	weights = text_insert_field (weights, fontnames[i], WEIGHT);
	slants = text_insert_field (slants, fontnames[i], SLANT);
	set_widths = text_insert_field (set_widths, fontnames[i], SET_WIDTH);
	spacings = text_insert_field (spacings, fontnames[i], SPACING);
      }

  XFreeFontNames (fontnames);

  nfoundries = list_length (foundries) + 1;
  nweights = list_length (weights) + 1;
  nslants = list_length (slants) + 1;
  nset_widths = list_length (set_widths) + 1;
  nspacings = list_length (spacings) + 1;

  foundry_array = xmalloc (sizeof (char*) * nfoundries);
  weight_array = xmalloc (sizeof (char*) * nweights);
  slant_array = xmalloc (sizeof (char*) * nslants);
  set_width_array = xmalloc (sizeof (char*) * nset_widths);
  spacing_array = xmalloc (sizeof (char*) * nspacings);

  i = 1;
  temp_list = foundries;
  while (temp_list)
    {
      foundry_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  i = 1;
  temp_list = weights;
  while (temp_list)
    {
      weight_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  i = 1;
  temp_list = slants;
  while (temp_list)
    {
      slant_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  i = 1;
  temp_list = set_widths;
  while (temp_list)
    {
      set_width_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  i = 1;
  temp_list = spacings;
  while (temp_list)
    {
      spacing_array[i++] = temp_list->data;
      temp_list = temp_list->next;
    }

  foundry_array[0] = "*";
  weight_array[0] = "*";
  slant_array[0] = "*";
  set_width_array[0] = "*";
  spacing_array[0] = "*";

  for (i = 0; i < nfonts; i++)
    {
      font_info[i]->foundries = xmalloc (sizeof (int) * nfoundries);
      font_info[i]->weights = xmalloc (sizeof (int) * nweights);
      font_info[i]->slants = xmalloc (sizeof (int) * nslants);
      font_info[i]->set_widths = xmalloc (sizeof (int) * nset_widths);
      font_info[i]->spacings = xmalloc (sizeof (int) * nspacings);
      font_info[i]->ncombos = list_length (font_info[i]->fontnames);
      font_info[i]->combos = xmalloc (sizeof (int*) * font_info[i]->ncombos);

      for (j = 0; j < nfoundries; j++)
	font_info[i]->foundries[j] = 0;
      for (j = 0; j < nweights; j++)
	font_info[i]->weights[j] = 0;
      for (j = 0; j < nslants; j++)
	font_info[i]->slants[j] = 0;
      for (j = 0; j < nset_widths; j++)
	font_info[i]->set_widths[j] = 0;
      for (j = 0; j < nspacings; j++)
	font_info[i]->spacings[j] = 0;

      font_info[i]->foundries[0] = 1;
      font_info[i]->weights[0] = 1;
      font_info[i]->slants[0] = 1;
      font_info[i]->set_widths[0] = 1;
      font_info[i]->spacings[0] = 1;

      j = 0;
      temp_list = font_info[i]->fontnames;
      while (temp_list)
	{
	  fontname = temp_list->data;
	  temp_list = temp_list->next;

	  font_info[i]->combos[j] = xmalloc (sizeof (int) * 5);

	  field = text_get_field (fontname, FOUNDRY);
	  index = text_field_to_index (foundry_array, nfoundries, field);
	  font_info[i]->foundries[index] = 1;
	  font_info[i]->combos[j][0] = index;
	  free (field);

	  field = text_get_field (fontname, WEIGHT);
	  index = text_field_to_index (weight_array, nweights, field);
	  font_info[i]->weights[index] = 1;
	  font_info[i]->combos[j][1] = index;
	  free (field);

	  field = text_get_field (fontname, SLANT);
	  index = text_field_to_index (slant_array, nslants, field);
	  font_info[i]->slants[index] = 1;
	  font_info[i]->combos[j][2] = index;
	  free (field);

	  field = text_get_field (fontname, SET_WIDTH);
	  index = text_field_to_index (set_width_array, nset_widths, field);
	  font_info[i]->set_widths[index] = 1;
	  font_info[i]->combos[j][3] = index;
	  free (field);

	  field = text_get_field (fontname, SPACING);
	  index = text_field_to_index (spacing_array, nspacings, field);
	  font_info[i]->spacings[index] = 1;
	  font_info[i]->combos[j][4] = index;
	  free (field);

	  j += 1;
	}
    }
}

static void
text_insert_font (table, ntable, fontname)
     FontInfo **table;
     int *ntable;
     char *fontname;
{
  FontInfo *temp_info;
  char *family;
  int lower, upper;
  int middle, cmp;

  /* insert a fontname into a table */
  family = text_get_field (fontname, FAMILY);
  if (!family)
    return;

  lower = 0;
  if (*ntable > 0)
    {
      /* Do a binary search to determine if we have already encountered
       *  a font from this family.
       */
      upper = *ntable;
      while (lower < upper)
	{
	  middle = (lower + upper) >> 1;

	  cmp = strcmp (family, table[middle]->family);
	  if (cmp == 0)
	    {
	      table[middle]->fontnames = add_to_list (table[middle]->fontnames, xstrdup (fontname));
	      return;
	    }
	  else if (cmp < 0)
	    upper = middle;
	  else if (cmp > 0)
	    lower = middle+1;
	}
    }

  /* Add another entry to the table for this new font family */
  table[*ntable] = xmalloc (sizeof (FontInfo));
  table[*ntable]->family = family;
  table[*ntable]->foundries = NULL;
  table[*ntable]->weights = NULL;
  table[*ntable]->slants = NULL;
  table[*ntable]->set_widths = NULL;
  table[*ntable]->fontnames = NULL;
  table[*ntable]->fontnames = add_to_list (table[*ntable]->fontnames, xstrdup (fontname));
  (*ntable)++;

  /* Quickly insert the entry into the table in sorted order
   *  using a modification of insertion sort and the knowledge
   *  that the entries proper position in the table was determined
   *  above in the binary search and is contained in the "lower"
   *  variable.
   */
  if (*ntable > 1)
    {
      temp_info = table[*ntable - 1];

      upper = *ntable - 1;
      while (lower != upper)
	{
	  table[upper] = table[upper-1];
	  upper -= 1;
	}

      table[lower] = temp_info;
    }
}

static link_ptr
text_insert_field (list, fontname, field_num)
     link_ptr list;
     char *fontname;
     int field_num;
{
  link_ptr temp_list;
  link_ptr prev_list;
  link_ptr new_list;
  char *field;
  int cmp;

  field = text_get_field (fontname, field_num);
  if (!field)
    return list;

  temp_list = list;
  prev_list = NULL;

  while (temp_list)
    {
      cmp = strcmp (field, temp_list->data);
      if (cmp == 0)
	{
	  free (field);
	  return list;
	}
      else if (cmp < 0)
	{
	  new_list = alloc_list ();
	  new_list->data = field;
	  new_list->next = temp_list;
	  if (prev_list)
	    {
	      prev_list->next = new_list;
	      return list;
	    }
	  else
	    return new_list;
	}
      else
	{
	  prev_list = temp_list;
	  temp_list = temp_list->next;
	}
    }

  new_list = alloc_list ();
  new_list->data = field;
  new_list->next = NULL;
  if (prev_list)
    {
      prev_list->next = new_list;
      return list;
    }
  else
    return new_list;
}

static char*
text_get_field (fontname, field_num)
     char *fontname;
     int field_num;
{
  char *t1, *t2;
  char *field;

  /* we assume this is a valid fontname...that is, it has 14 fields */

  t1 = fontname;
  while (*t1 && (field_num >= 0))
    if (*t1++ == '-')
      field_num--;

  t2 = t1;
  while (*t2 && (*t2 != '-'))
    t2++;

  if (t1 != t2)
    {
      field = xmalloc (1 + (long) t2 - (long) t1);
      strncpy (field, t1, (long) t2 - (long) t1);
      field[(long) t2 - (long) t1] = 0;
      return field;
    }

  return xstrdup ("(nil)");
}

static int
text_field_to_index (table, ntable, field)
     char **table;
     int ntable;
     char *field;
{
  int i;

  for (i = 0; i < ntable; i++)
    if (strcmp (field, table[i]) == 0)
      return i;

  return -1;
}

static int
text_is_xlfd_font_name (fontname)
     char *fontname;
{
  int i;

  i = 0;
  while (*fontname)
    if (*fontname++ == '-')
      i++;

  return (i == 14);
}

static void
text_load_font (text_tool)
     TextTool * text_tool;
{
  float size;
  char fontname[2048];
  char pixel_size[12], point_size[12];
  char *size_text;
  char *foundry_str;
  char *weight_str;
  char *slant_str;
  char *set_width_str;
  char *spacing_str;

  if (text_tool->font)
    {
      gdk_font_free (text_tool->font);
      text_tool->font = NULL;
    }

  size_text = gtk_text_entry_get_text (text_tool->size_text);
  size = atof (size_text);

  if (size > 0)
    {
      switch (text_tool->size_type)
	{
	case PIXELS:
	  sprintf (pixel_size, "%d", (int) size);
	  sprintf (point_size, "*");
	  break;
	case POINTS:
	  sprintf (pixel_size, "*");
	  sprintf (point_size, "%d", (int) (size * 10));
	  break;
	}

      foundry_str = foundry_array[text_tool->foundry];
      if (strcmp (foundry_str, "(nil)") == 0)
	foundry_str = "";
      weight_str = weight_array[text_tool->weight];
      if (strcmp (weight_str, "(nil)") == 0)
	weight_str = "";
      slant_str = slant_array[text_tool->slant];
      if (strcmp (slant_str, "(nil)") == 0)
	slant_str = "";
      set_width_str = set_width_array[text_tool->set_width];
      if (strcmp (set_width_str, "(nil)") == 0)
	set_width_str = "";
      spacing_str = spacing_array[text_tool->spacing];
      if (strcmp (spacing_str, "(nil)") == 0)
	spacing_str = "";

      /* create the fontname */
      sprintf (fontname, "-%s-%s-%s-%s-%s-*-%s-%s-75-75-%s-*-*-*",
	       foundry_str,
	       font_info[text_tool->font_index]->family,
	       weight_str,
	       slant_str,
	       set_width_str,
	       pixel_size, point_size,
	       spacing_str);

      if (fontname)
	text_tool->font = gdk_font_load (fontname);

      text_resize_text_widget (text_tool);
    }
}

static void
text_render (text_tool)
     TextTool * text_tool;
{
  static int pixmap_sizes[2] = { 200, 201 };

  GdkFont *font;
  GdkPixmap *pixmap;
  GdkImage *image;
  GdkGC *gc;
  GdkColor black, white;
  GDisplay *gdisp;
  Layer *layer;
  MaskBuf *mask, *newmask;
  PixelRegion textPR, maskPR;
  int layer_type;
  int antialias;
  unsigned char color[4];
  char fontname[256];
  char *text, *str;
  char *size_text;
  int nstrs;
  int line_width, line_height;
  int pixmap_width, pixmap_height;
  int text_width, text_height;
  int width, height;
  char pixel_size[12], point_size[12];
  float size;
  char *foundry_str;
  char *weight_str;
  char *slant_str;
  char *set_width_str;
  char *spacing_str;
  int x, y, i, j, k;

  /* get the text */
  text = gtk_text_entry_get_text (text_tool->the_text);
  size_text = gtk_text_entry_get_text (text_tool->size_text);
  size = atof (size_text);

  if ((size > 0) && text && (strlen (text) > 0))
    {
      gdisp = text_tool->gdisp_ptr;

      /*  determine the layer type  */
      layer_type = gimage_type_with_alpha (gdisp->gimage);

      /* scale the text based on the antialiasing amount */
      if (text_tool->antialias)
	{
	  antialias = SUPERSAMPLE;
	  size *= antialias;
	}
      else
	antialias = 1;

      switch (text_tool->size_type)
	{
	case PIXELS:
	  sprintf (pixel_size, "%d", (int) size);
	  sprintf (point_size, "*");
	  break;
	case POINTS:
	  sprintf (pixel_size, "*");
	  sprintf (point_size, "%d", (int) (size * 10));
	  break;
	}

      foundry_str = foundry_array[text_tool->foundry];
      if (strcmp (foundry_str, "(nil)") == 0)
	foundry_str = "";
      weight_str = weight_array[text_tool->weight];
      if (strcmp (weight_str, "(nil)") == 0)
	weight_str = "";
      slant_str = slant_array[text_tool->slant];
      if (strcmp (slant_str, "(nil)") == 0)
	slant_str = "";
      set_width_str = set_width_array[text_tool->set_width];
      if (strcmp (set_width_str, "(nil)") == 0)
	set_width_str = "";
      spacing_str = spacing_array[text_tool->spacing];
      if (strcmp (spacing_str, "(nil)") == 0)
	spacing_str = "";

      /* create the fontname */
      sprintf (fontname, "-%s-%s-%s-%s-%s-*-%s-%s-75-75-%s-*-*-*",
	       foundry_str,
	       font_info[text_tool->font_index]->family,
	       weight_str,
	       slant_str,
	       set_width_str,
	       pixel_size, point_size,
	       spacing_str);

      /* load the font in */
      font = gdk_font_load (fontname);
      if (!font)
	fatal_error ("unable to load font");

      /* determine the bounding box of the text */
      width = -1;
      height = 0;
      line_height = font->ascent + font->descent;

      nstrs = 0;
      str = strtok (text, "\n");
      while (str)
	{
	  nstrs += 1;

	  line_width = gdk_string_width (font, str);
	  if (line_width > width)
	    width = line_width;
	  height += line_height;

	  str = strtok (NULL, "\n");
	}

      /* We limit the largest pixmap we create to approximately 200x200.
       * This is approximate since it depends on the amount of antialiasing.
       * Basically, we want the width and height to be divisible by the antialiasing
       *  amount. (Which lies in the range 1-10).
       * This avoids problems on some X-servers (Xinside) which have problems
       *  with large pixmaps. (Specifically pixmaps which are larger - width
       *  or height - than the screen).
       */
      pixmap_width = (width < pixmap_sizes[text_tool->antialias])
	? width : pixmap_sizes[text_tool->antialias];
      pixmap_height = (height < pixmap_sizes[text_tool->antialias])
	? height : pixmap_sizes[text_tool->antialias];

      /* determine the actual text size based on the amount of antialiasing */
      text_width = width / antialias;
      text_height = height / antialias;

      /* create the pixmap of depth 1...sometimes called a bitmap */
      pixmap = gdk_pixmap_new (gdisp->canvas->window, pixmap_width, pixmap_height, 1);

      /* create the gc */
      gc = gdk_gc_new (pixmap);
      gdk_gc_set_font (gc, font);
      /*  get black and white pixels for this gdisplay  */
      black.pixel = BlackPixel (DISPLAY, DefaultScreen (DISPLAY));
      white.pixel = WhitePixel (DISPLAY, DefaultScreen (DISPLAY));

      /* Render the text into the pixmap.
       * Since the pixmap may not fully bound the text (because we limit its size)
       *  we must tile it around the texts actual bounding box.
       */
      mask = mask_buf_new (text_width, text_height);

      maskPR.bytes = mask->bytes;
      maskPR.w = mask->width;
      maskPR.h = mask->height;
      maskPR.rowstride = mask->width * maskPR.bytes;
      maskPR.data = mask_buf_data (mask);

      for (i = 0; i < height; i += pixmap_height)
	{
	  for (j = 0; j < width; j += pixmap_width)
	    {
	      /* erase the pixmap */
	      gdk_gc_set_foreground (gc, &white);
	      gdk_draw_rectangle (pixmap, gc, 1, 0, 0, pixmap_width, pixmap_height);
	      gdk_gc_set_foreground (gc, &black);

	      /* adjust the x and y values */
	      x = -j;
	      y = font->ascent - i;
	      str = text;

	      for (k = 0; k < nstrs; k++)
		{
		  gdk_draw_string (pixmap, gc, x, y, str);
		  str += strlen (str) + 1;
		  y += line_height;
		}

	      /* create the GdkImage */
	      image = gdk_image_get (pixmap, 0, 0, pixmap_width, pixmap_height);

	      if (!image)
		fatal_error ("sanity check failed: could not get gdk image");

	      if (image->depth != 1)
		fatal_error ("sanity check failed: image should have 1 bit per pixel");

	      /* configure the text mask pixel region */
	      maskPR.data = mask_buf_data (mask) + (i / antialias) *
		maskPR.rowstride + (j / antialias) * maskPR.bytes;
	      maskPR.x = j / antialias;
	      maskPR.y = i / antialias;

	      /* convert the GdkImage bitmap to a region */
	      text_gdk_image_to_region (image, pixmap_width, pixmap_height,
					antialias, &maskPR);

	      /* free the image */
	      gdk_image_destroy (image);
	    }
	}

      /*  Crop the mask buffer  */
      newmask = crop_buffer (mask);
      if (newmask != mask)
	mask_buf_free (mask);

      if (newmask)
	{
	  layer = layer_new (0, newmask->width, newmask->height, layer_type,
			     "Floating Selection", OPAQUE, NORMAL_MODE);

	  /*  color the layer buffer  */
	  palette_get_foreground (&color[RED_PIX], &color[GREEN_PIX],
				  &color[BLUE_PIX]);
	  color[layer->bytes - 1] = OPAQUE;
	  textPR.bytes = layer->bytes;
	  textPR.w = layer->width;
	  textPR.h = layer->height;
	  textPR.rowstride = layer->width * textPR.bytes;
	  textPR.data = layer_data (layer);
	  color_region (&textPR, color);

	  /*  apply the text mask  */
	  maskPR.bytes = 1;
	  maskPR.rowstride = newmask->width;
	  maskPR.data = mask_buf_data (newmask);

	  apply_mask_to_region (&textPR, &maskPR, OPAQUE);

	  /*  Set the layer offsets  */
	  layer->offset_x = text_tool->click_x;
	  layer->offset_y = text_tool->click_y;

	  /* instantiate the text as the new floating selection */
	  floating_sel_replace (gdisp->gimage, layer, 0);
	  gdisplays_flush ();

	  mask_buf_free (newmask);
	}

      /* free the pixmap */
      gdk_pixmap_destroy (pixmap);

      /* free the gc */
      gdk_gc_destroy (gc);

      /* free the font */
      gdk_font_free (font);
    }
}

static void
text_gdk_image_to_region (image, width, height, scale, textPR)
     GdkImage *image;
     int width, height, scale;
     PixelRegion * textPR;
{
  int black_pixel;
  int pixel;
  int value;
  int scalex, scaley;
  int scale2;
  int x, y;
  int i, j;
  unsigned char * data, * d;

  width /= scale;
  height /= scale;
  width = ((textPR->x + width) > textPR->w) ? textPR->w - textPR->x : width;
  height = ((textPR->y + height) > textPR->h) ? textPR->h - textPR->y : height;

  scale2 = scale * scale;
  black_pixel = BlackPixel (DISPLAY, DefaultScreen (DISPLAY));
  data = textPR->data;

  for (y = 0, scaley = 0; y < height; y++, scaley += scale)
    {
      d = data;
      for (x = 0, scalex = 0; x < width; x++, scalex += scale)
	{
	  /* calculate the pixel value */

	  value = 0;
	  for (i = scaley; i < scaley + scale; i++)
	    for (j = scalex; j < scalex + scale; j++)
	      {
		pixel = gdk_image_get_pixel (image, j, i);
		if (pixel == black_pixel)
		  value += 255;
	      }
	  value = value / scale2;

	  /*  store the alpha value in the data  */
	  d[0] = (unsigned char) value;
	  d += textPR->bytes;
	}

      data += textPR->rowstride;
    }
}
