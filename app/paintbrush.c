/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <math.h>
#include "appenv.h"
#include "autodialog.h"
#include "brushes.h"
#include "errors.h"
#include "gdisplay.h"
#include "paint_funcs.h"
#include "paint_core.h"
#include "palette.h"
#include "paintbrush.h"
#include "selection.h"
#include "tools.h"

/*  defines  */
#define             PAINT_LEFT_THRESHOLD  0.05

typedef struct _PaintOptions PaintOptions;
struct _PaintOptions
{
  double fade_out;

  GtkObserver fade_out_observer;
};

/*  forward function declarations  */
static void         paintbrush_motion      (Tool *);

/*  local variables  */
static PaintOptions *paint_options = NULL;


static gint
paintbrush_scale_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  double *scale_val;

  scale_val = (double *) observer->user_data;
  adj_data = (GtkDataAdjustment *) data;
  *scale_val = adj_data->value;

  return FALSE;
}

static void
paintbrush_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

PaintOptions *
create_paint_options ()
{
  PaintOptions *options;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *fade_out_scale;
  GtkData *fade_out_scale_data;

  /*  the new options structure  */
  options = (PaintOptions *) xmalloc (sizeof (PaintOptions));
  options->fade_out = 0.0;

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  
  /*  the main label  */
  label = gtk_label_new ("Paintbrush Options");
  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  the fade-out scale  */
  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  
  label = gtk_label_new ("Fade Out");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  fade_out_scale_data = gtk_data_adjustment_new (0.0, 0.0, 1000.0, 1.0, 1.0, 1.0);
  fade_out_scale = gtk_hscale_new ((GtkDataAdjustment *) fade_out_scale_data);
  gtk_box_pack (hbox, fade_out_scale, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (fade_out_scale, GTK_POS_TOP);
  options->fade_out_observer.update = paintbrush_scale_update;
  options->fade_out_observer.disconnect = paintbrush_disconnect;
  options->fade_out_observer.user_data = &options->fade_out;
  gtk_data_attach (fade_out_scale_data, &options->fade_out_observer);
  gtk_widget_show (fade_out_scale);
  gtk_widget_show (hbox);

  /*  Register this selection options widget with the main tools options dialog  */
  tools_register_options (PAINTBRUSH, vbox);

  return options;
}

void *
paintbrush_paint_func (tool, state)
     Tool * tool;
     int state;
{
  PaintCore * paint_core;

  paint_core = (PaintCore *) tool->private;

  switch (state)
    {
    case INIT_PAINT :
      break;

    case MOTION_PAINT :
      paintbrush_motion (tool);
      break;

    case FINISH_PAINT :
      break;

    default : 
      break;
    }

  return NULL;
}


Tool *
tools_new_paintbrush ()
{
  Tool * tool;
  PaintCore * private;

  if (! paint_options)
    paint_options = create_paint_options ();

  tool = paint_core_new (PAINTBRUSH);

  private = (PaintCore *) tool->private;
  private->paint_func = paintbrush_paint_func;

  return tool;
}


void
tools_free_paintbrush (tool)
     Tool * tool;
{
  paint_core_free (tool);
}


void
paintbrush_motion (tool)
     Tool * tool;
{
  GDisplay * gdisp;
  PaintCore * paint_core;
  TempBuf * area;
  double x, paint_left;
  unsigned char blend = OPAQUE;
  unsigned char col[MAX_CHANNELS];

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  gimage_get_foreground (gdisp->gimage, col);

  /*  Get a region which can be used to paint to  */
  if (! (area = paint_core_get_paint_area (tool, 0)))
    return;

  /*  factor in the fade out value  */
  if (paint_options->fade_out)
    {
      /*  Model the amount of paint left as a gaussian curve  */
      x = ((double) paint_core->distance / (double) paint_options->fade_out);
      paint_left = exp (- x * x * 0.5);

      blend = (int) (255 * paint_left);
    }

  if (blend)
    {
      /*  set the alpha channel  */
      col[area->bytes - 1] = OPAQUE;

      /*  color the pixels  */
      color_pixels (temp_buf_data (area), col,
		    area->width * area->height, area->bytes);

      /*  paste the newly painted canvas to the gimage which is being worked on  */
      paint_core_paste_canvas (tool, blend, (int) (get_brush_opacity () * 255),
			       get_brush_paint_mode (), SOFT, CONSTANT);
    }
}

