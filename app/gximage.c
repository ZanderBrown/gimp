/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "gimage.h"
#include "gximage.h"
#include "visual.h"
#include "errors.h"

typedef struct _GXImage  GXImage;

struct _GXImage
{
  long width, height;		/*  width and height of ximage structure    */
  int  bits_per_pixel;          /*  Bits per pixel (ZPixmap format)         */
  long bytes_per_line;		/*  bytes per line of the ximage structure  */
  
  GdkVisual *visual;		/*  visual appropriate to our depth         */
  GdkGC *gc;			/*  graphics context                        */

  unsigned char *data;		/*  actual ximage data buffer  */

  GdkImage *image;		/*  private data  */
};


/*  The static gximages for drawing to windows  */
static GXImage * rgb_gximage;
static GXImage * gray_gximage;

#define QUANTUM   32

/*  STATIC functions  */

static GXImage *
create_gximage (visual, width, height)
     GdkVisual *visual;
     int width, height;
{
  GXImage * gximage;

  gximage = (GXImage *) xmalloc (sizeof (GXImage));

  gximage->visual = visual;
  gximage->gc = NULL;

  gximage->image = gdk_image_new (GDK_IMAGE_FASTEST, visual, width, height);

  return gximage;
}

static void
delete_gximage (gximage)
     GXImage *gximage;
{
  gdk_image_destroy (gximage->image);
  if (gximage->gc)
    gdk_gc_destroy (gximage->gc);
  xfree (gximage);
}

/****************************************************************/


/*  Function definitions  */

void
gximage_init ()
{
  rgb_gximage = create_gximage (color_visual, GXIMAGE_WIDTH, GXIMAGE_HEIGHT);

  if (emulate_gray)
    gray_gximage = rgb_gximage;
  else
    gray_gximage = create_gximage (gray_visual, GXIMAGE_WIDTH, GXIMAGE_HEIGHT);
}

void
gximage_free ()
{
  delete_gximage (rgb_gximage);
  if (! emulate_gray)
    delete_gximage (gray_gximage);
}

unsigned char *
get_gximage_data  (type)
     int type;
{
  switch (type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE: case INDEXED_GIMAGE:
      return rgb_gximage->image->mem;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      return gray_gximage->image->mem;
      break;
    }
  
  return NULL;
}

int
get_gximage_bpp  (type)
     int type;
{
  switch (type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE: case INDEXED_GIMAGE:
      return rgb_gximage->image->bpp;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      return gray_gximage->image->bpp;
      break;
    }

  return 0;
}

int
get_gximage_bpl  (type)
     int type;
{
  switch (type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE: case INDEXED_GIMAGE:
      return rgb_gximage->image->bpl;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      return gray_gximage->image->bpl;
      break;
    }

  return 0;
}

void
put_gximage (win, type, x, y, w, h)
     GdkWindow *win;
     int type;
     int x, y;
     int w, h;
{
  GXImage * gximage;

  switch (type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE: case INDEXED_GIMAGE:
      gximage = rgb_gximage;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      gximage = gray_gximage;
      break;
    }

  /*  create the GC if it doesn't yet exist  */
  if (! gximage->gc)
    gximage->gc = gdk_gc_new (win);

  gdk_draw_image (win, gximage->gc, gximage->image,
		  0, 0, x, y, w, h);

  /*  sync the draw image to make sure it has been displayed before continuing  */
  gdk_flush ();
}



