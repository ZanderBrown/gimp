/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "actionarea.h"
#include "airbrush.h"
#include "bezier_select.h"
#include "blend.h"
#include "bucket_fill.h"
#include "by_color_select.h"
#include "clone.h"
#include "color_picker.h"
#include "convolve.h"
#include "crop.h"
#include "cursorutil.h"
#include "eraser.h"
#include "gdisplay.h"
#include "tools.h"
#include "ellipse_select.h"
#include "flip_tool.h"
#include "free_select.h"
#include "fuzzy_select.h"
#include "gimprc.h"
#include "interface.h"
#include "iscissors.h"
#include "magnify.h"
#include "move.h"
#include "paintbrush.h"
#include "pencil.h"
#include "rect_select.h"
#include "text_tool.h"
#include "tools.h"
#include "transform_tool.h"

/* Global Data */

Tool * active_tool = NULL;

/* Local Data */

static GtkWidget *options_shell = NULL;
static GtkWidget *options_vbox = NULL;

static int global_tool_ID = 0;

ToolInfo tool_info[] =
{
  { GDK_TCROSS, GDK_TCROSS, NULL, 0 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 1 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 2 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 3 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 4 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 5 },
  { GDK_FLEUR, GDK_FLEUR, NULL, 6 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 7 },
  { GDK_CROSS, GDK_CROSS, NULL, 8 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 9 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 9 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 9 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 9 },
  { GDK_BI_ARROW_HORZ, GDK_BI_ARROW_HORZ, NULL, 10 },
  { GDK_BI_ARROW_VERT, GDK_BI_ARROW_VERT, NULL, 10 },
  { GDK_TEXT_CURSOR, GDK_TEXT_CURSOR, NULL, 11 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 12 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 13 },
  { GDK_TCROSS, GDK_TCROSS, NULL, 14 },
  { GDK_PENCIL, GDK_PENCIL, NULL, 15 },
  { GDK_PENCIL, GDK_PENCIL, NULL, 16 },
  { GDK_PENCIL, GDK_PENCIL, NULL, 17 },
  { GDK_PENCIL, GDK_PENCIL, NULL, 18 },
  { GDK_PENCIL, GDK_PENCIL, NULL, 19 },
  { GDK_PENCIL, GDK_PENCIL, NULL, 20 },

  /*  Non-toolbox tools  */
  { GDK_TCROSS, GDK_TCROSS, NULL, -1 }
};


/*  Local function declarations  */

static void tools_options_dialog_callback (GtkWidget *, gpointer, gpointer);


/* Function definitions */

static void
tools_free (tool)
     Tool * tool;
{
  switch (tool->type)
    {
    case RECT_SELECT:
      tools_free_rect_select (tool);
      break;
    case ELLIPSE_SELECT:
      tools_free_ellipse_select (tool);
      break;
    case FREE_SELECT:
      tools_free_free_select (tool);
      break;
    case FUZZY_SELECT:
      tools_free_fuzzy_select (tool);
      break;
    case BEZIER_SELECT:
      tools_free_bezier_select (tool);
      break;
    case ISCISSORS:
      tools_free_iscissors (tool);
      break;
    case CROP:
      tools_free_crop (tool);
      break;
    case MOVE:
      tools_free_move_tool (tool);
      break;
    case MAGNIFY:
      tools_free_magnify (tool);
      break;
    case ROTATE:
      tools_free_transform_tool (tool);
      break;
    case SCALE:
      tools_free_transform_tool (tool);
      break;
    case SHEAR:
      tools_free_transform_tool (tool);
      break;
    case PERSPECTIVE:
      tools_free_transform_tool (tool);
      break;
    case FLIP_HORZ:
      tools_free_flip_tool (tool);
      break;
    case FLIP_VERT:
      tools_free_flip_tool (tool);
      break;
    case TEXT:
      tools_free_text (tool);
      break;
    case COLOR_PICKER:
      tools_free_color_picker (tool);
      break;
    case BUCKET_FILL:
      tools_free_bucket_fill (tool);
      break;
    case BLEND:
      tools_free_blend (tool);
      break;
    case PENCIL:
      tools_free_pencil (tool);
      break;
    case PAINTBRUSH:
      tools_free_paintbrush (tool);
      break;
    case ERASER:
      tools_free_eraser (tool);
      break;
    case AIRBRUSH:
      tools_free_airbrush (tool);
      break;
    case CLONE:
      tools_free_clone (tool);
      break;
    case CONVOLVE:
      tools_free_convolve (tool);
      break;
    case BY_COLOR_SELECT:
      tools_free_by_color_select (tool);
      break;
    default:
      return;
      break;
    }

  if (tool_info[(int) tool->type].tool_options)
    gtk_widget_hide (tool_info[(int) tool->type].tool_options);

  gdisplays_remove_tool_cursor ();

  xfree (tool);
}


void
tools_select (type)
     ToolType type;
{
  if (active_tool)
    {
      if (active_tool->type == type)
	return;

      tools_free (active_tool);
    }
  
  /*  Install the new tool cursor  */
  gdisplays_install_tool_cursor (tool_info [(int) type].normal_cursor);

  switch (type)
    {
    case RECT_SELECT:
      active_tool = tools_new_rect_select ();
      break;
    case ELLIPSE_SELECT:
      active_tool = tools_new_ellipse_select ();
      break;
    case FREE_SELECT:
      active_tool = tools_new_free_select ();
      break;
    case FUZZY_SELECT:
      active_tool = tools_new_fuzzy_select ();
      break;
    case BEZIER_SELECT:
      active_tool = tools_new_bezier_select ();
      break;
    case ISCISSORS:
      active_tool = tools_new_iscissors ();
      break;
    case MOVE:
      active_tool = tools_new_move_tool ();
      break;
    case MAGNIFY:
      active_tool = tools_new_magnify ();
      break;
    case CROP:
      active_tool = tools_new_crop ();
      break;
    case ROTATE:
      active_tool = tools_new_transform_tool ();
      break;
    case SCALE:
      active_tool = tools_new_transform_tool ();
      break;
    case SHEAR:
      active_tool = tools_new_transform_tool ();
      break;
    case PERSPECTIVE:
      active_tool = tools_new_transform_tool ();
      break;
    case FLIP_HORZ:
      active_tool = tools_new_flip ();
      break;
    case FLIP_VERT:
      active_tool = tools_new_flip ();
      break;
    case TEXT:
      active_tool = tools_new_text ();
      break;
    case COLOR_PICKER:
      active_tool = tools_new_color_picker ();
      break;
    case BUCKET_FILL:
      active_tool = tools_new_bucket_fill ();
      break;
    case BLEND:
      active_tool = tools_new_blend ();
      break;
    case PENCIL:
      active_tool = tools_new_pencil ();
      break;
    case PAINTBRUSH:
      active_tool = tools_new_paintbrush ();
      break;
    case ERASER:
      active_tool = tools_new_eraser ();
      break;
    case AIRBRUSH:
      active_tool = tools_new_airbrush ();
      break;
    case CLONE:
      active_tool = tools_new_clone ();
      break;
    case CONVOLVE:
      active_tool = tools_new_convolve ();
      break;
    case BY_COLOR_SELECT:
      active_tool = tools_new_by_color_select ();
      break;
    default:
      return;
      break;
    }

  if (tool_info[(int) active_tool->type].tool_options)
    gtk_widget_show (tool_info[(int) active_tool->type].tool_options);

  /*  Set the paused count variable to 0  */
  active_tool->paused_count = 0;
  active_tool->ID = global_tool_ID++;
}


void
tools_options_dialog_new ()
{
  GtkWidget *options_frame;
  GtkWidget *main_vbox;
  GtkWidget *action_area;
  ActionAreaItem action_items[1] = 
  {
    { "Close", tools_options_dialog_callback, NULL, NULL }
  };

  /*  The shell and main vbox  */
  options_shell = gtk_window_new ("Tool Options", GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_uposition (options_shell, tool_options_x, tool_options_y);

  main_vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (main_vbox, 5);
  gtk_container_add (options_shell, main_vbox);

  options_frame = gtk_frame_new (NULL);
  gtk_frame_set_type (options_frame, GTK_SHADOW_ETCHED_IN);
  gtk_box_pack (main_vbox, options_frame, TRUE, TRUE, 0, GTK_PACK_START);

  options_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (options_vbox, 0);
  gtk_container_add (options_frame, options_vbox);

  action_items[0].user_data = options_shell;
  action_area = build_action_area (action_items, 1);
  gtk_box_pack (main_vbox, action_area, FALSE, FALSE, 5, GTK_PACK_START);

  gtk_widget_show (action_area);
  gtk_widget_show (options_vbox);
  gtk_widget_show (options_frame);
  gtk_widget_show (main_vbox);
}


void
tools_options_dialog_show ()
{
  gtk_widget_show (options_shell);
}


void
tools_options_dialog_free ()
{
  gtk_widget_destroy (options_shell);
}


void
tools_register_options (type, options)
     ToolType type;
     GtkWidget *options;
{
  /*  need to check whether the widget is visible...this can happen
   *  because some tools share options such as the transformation tools
   */
  if (! GTK_WIDGET_VISIBLE (options))
    {
      gtk_box_pack (options_vbox, options, TRUE, TRUE, 0, GTK_PACK_START);
      gtk_widget_show (options);
    }
  tool_info [(int) type].tool_options = options;
}

void *
tools_register_no_options (tool_type, tool_title)
     ToolType tool_type;
     char *tool_title;
{
  GtkWidget *vbox;
  GtkWidget *label;

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  
  /*  the main label  */
  label = gtk_label_new (tool_title);
  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  this tool has no special options  */
  label = gtk_label_new ("This tool has no options.");
  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  Register this selection options widget with the main tools options dialog  */
  tools_register_options (tool_type, vbox);

  return (void *) 1;
}

void
active_tool_control (action, gdisp_ptr)
     int action;
     void * gdisp_ptr;
{
  if (active_tool)
    {
      if (active_tool->gdisp_ptr == gdisp_ptr)
	{
	  switch (action)
	    {
	    case PAUSE :
	      if (active_tool->state == ACTIVE)
		{
		  if (! active_tool->paused_count)
		    {
		      active_tool->state = PAUSED;
		      (* active_tool->control_func) (active_tool, action, gdisp_ptr);
		    }
		}
	      active_tool->paused_count++;

	      break;
	    case RESUME :
	      active_tool->paused_count--;
	      if (active_tool->state == PAUSED)
		{
		  if (! active_tool->paused_count)
		    {
		      active_tool->state = ACTIVE;
		      (* active_tool->control_func) (active_tool, action, gdisp_ptr);
		    }
		}
	      break;
	    case HALT :
	      active_tool->state = INACTIVE;
	      (* active_tool->control_func) (active_tool, action, gdisp_ptr);
	      break;
	    }
	}
    }
}


void
standard_arrow_keys_func (tool, kevent, gdisp_ptr)
     Tool *tool;
     GdkEventKey *kevent;
     gpointer gdisp_ptr;
{
}


static void
tools_options_dialog_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  GtkWidget *shell;

  shell = (GtkWidget *) client_data;
  gtk_widget_hide (shell);
}
