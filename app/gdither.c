/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <assert.h>
#include <stdlib.h>
#include "appenv.h"
#include "errors.h"
#include "colormaps.h"
#include "gdither.h"

static void free_ordered_dither ();

/*
 *  Global variables
 *
 */

long *red_ordered_dither;	/* array of information for ordered dithering */
long *green_ordered_dither;	/* array of information for ordered dithering */
long *blue_ordered_dither;	/* array of information for ordered dithering */

matrix ordered_dither_matrices[NUM_DITHER_MATRICES+1];


/*
static matrix DM =
{
      0,192, 48,240, 12,204, 60,252,  3,195, 51,243, 15,207, 63,255,
    128, 64,176,112,140, 76,188,124,131, 67,179,115,143, 79,191,127,
     32,224, 16,208, 44,236, 28,220, 35,227, 19,211, 47,239, 31,223,
    160, 96,144, 80,172,108,156, 92,163, 99,147, 83,175,111,159, 95,
      8,200, 56,248,  4,196, 52,244, 11,203, 59,251,  7,199, 55,247,
    136, 72,184,120,132, 68,180,116,139, 75,187,123,135, 71,183,119,
     40,232, 24,216, 36,228, 20,212, 43,235, 27,219, 39,231, 23,215,
    168,104,152, 88,164,100,148, 84,171,107,155, 91,167,103,151, 87,
      2,194, 50,242, 14,206, 62,254,  1,193, 49,241, 13,205, 61,253,
    130, 66,178,114,142, 78,190,126,129, 65,177,113,141, 77,189,125,
     34,226, 18,210, 46,238, 30,222, 33,225, 17,209, 45,237, 29,221,
    162, 98,146, 82,174,110,158, 94,161, 97,145, 81,173,109,157, 93,
     10,202, 58,250,  6,198, 54,246,  9,201, 57,249,  5,197, 53,245,
    138, 74,186,122,134, 70,182,118,137, 73,185,121,133, 69,181,117,
     42,234, 26,218, 38,230, 22,214, 41,233, 25,217, 37,229, 21,213,
    170,106,154, 90,166,102,150, 86,169,105,153, 89,165,101,149, 85

};
*/

static matrix DM =
{
  { 0,  32, 8,  40, 2,  34, 10, 42 },
  { 48, 16, 56, 24, 50, 18, 58, 26 },
  { 12, 44, 4,  36, 14, 46, 6,  38 },
  { 60, 28, 52, 20, 62, 30, 54, 22 },
  { 3,  35, 11, 43, 1,  33, 9,  41 },
  { 51, 19, 59, 27, 49, 17, 57, 25 },
  { 15, 47, 7,  39, 13, 45, 5,  37 },
  { 63, 31, 55, 23, 61, 29, 53, 21 }
};

/*
   matrix DM = {
   0,  8,  2,  10,
   12, 4,  14, 6,
   3,  11, 1,  9,
   15, 7,  13, 5
   };
 */

void
init_ordered_dither ()
{
  int i, j, k;
  unsigned char low_shade, high_shade;
  unsigned short index;
  long red_mult, green_mult;
  float red_matrix_width;
  float green_matrix_width;
  float blue_matrix_width;
  float red_colors_per_shade;
  float green_colors_per_shade;
  float blue_colors_per_shade;

  red_mult = shades_g * shades_b;
  green_mult = shades_b;

  red_colors_per_shade = 255.0 / (shades_r - 1);
  red_matrix_width = red_colors_per_shade / NUM_DITHER_MATRICES;

  green_colors_per_shade = 255.0 / (shades_g - 1);
  green_matrix_width = green_colors_per_shade / NUM_DITHER_MATRICES;

  blue_colors_per_shade = 255.0 / (shades_b - 1);
  blue_matrix_width = blue_colors_per_shade / NUM_DITHER_MATRICES;

  /*  alloc the ordered dither arrays for accelerated dithering  */

  red_ordered_dither = (long *) xmalloc (sizeof (long) * 256);
  green_ordered_dither = (long *) xmalloc (sizeof (long) * 256);
  blue_ordered_dither = (long *) xmalloc (sizeof (long) * 256);


  /*  setup the ordered_dither_matrices  */

  for (i = 0; i <= NUM_DITHER_MATRICES; i++)
    {
      for (j = 0; j < DITHER_LEVEL; j++)
	{
	  for (k = 0; k < DITHER_LEVEL; k++)
	    {
	      ordered_dither_matrices[i][j][k] = (DM[j][k] < i) ? 1 : 0;
	    }
	}
    }

  /*  setup arrays containing three bytes of information for red, green, & blue  */
  /*  the arrays contain :
   *    1st byte:    low end shade value
   *    2nd byte:    high end shade value
   *    3rd & 4th bytes:    ordered dither matrix index
   */

  for (i = 0; i < 256; i++)
    {

      /*  setup the red information  */
      {
	low_shade = (unsigned char) (i / red_colors_per_shade);
	if (low_shade == (shades_r - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short) 
	  (((float) i - low_shade*red_colors_per_shade) /
	   red_matrix_width);

	low_shade *= red_mult;
	high_shade *= red_mult;

	red_ordered_dither[i] = (index << 16) + (high_shade << 8) + (low_shade);
      }


      /*  setup the green information  */
      {
	low_shade = (unsigned char) (i / green_colors_per_shade);
	if (low_shade == (shades_g - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short) 
	  (((float) i - low_shade*green_colors_per_shade) /
	   green_matrix_width);

	low_shade *= green_mult;
	high_shade *= green_mult;

	green_ordered_dither[i] = (index << 16) + (high_shade << 8) + (low_shade);
      }


      /*  setup the blue information  */
      {
	low_shade = (unsigned char) (i / blue_colors_per_shade);
	if (low_shade == (shades_b - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short) 
	  (((float) i - low_shade*blue_colors_per_shade) /
	   blue_matrix_width);

	blue_ordered_dither[i] = (index << 16) + (high_shade << 8) + (low_shade);
      }
    }
}

static void
free_ordered_dither ()
{
  xfree (red_ordered_dither);
  xfree (green_ordered_dither);
  xfree (blue_ordered_dither);
}

void
gdither_init ()
{
  /*  sets up data structures for accelerated dithering algorithms  */
  init_ordered_dither ();
}

void
gdither_free ()
{
  free_ordered_dither ();
}


/*
 *  The Algorithms
 *
 */


void
dither_segment (src, dest, x, y, width)
     unsigned char * src;
     unsigned char * dest;
     int x, y;
     int width;
{
  long r, g, b;
  long ra, ga, ba;
  short xmod, ymod;

  ymod = y & 0x7;
  xmod = x & 0x7;

  if (width > 0)
    while (width--)
      {
	r = red_ordered_dither[*src++];
	g = green_ordered_dither[*src++];
	b = blue_ordered_dither[*src++];
	
	ra = (ordered_dither_matrices[SECONDWORD(r)][ymod][xmod]) ?
	  SECONDBYTE(r) : FIRSTBYTE(r);
	ga = (ordered_dither_matrices[SECONDWORD(g)][ymod][xmod]) ?
	  SECONDBYTE(g) : FIRSTBYTE(g);
	ba = (ordered_dither_matrices[SECONDWORD(b)][ymod][xmod]) ?
	  SECONDBYTE(b) : FIRSTBYTE(b);
	
	*dest++ = color_cube [ra + ga + ba];
	
	xmod++;
	xmod &= 0x7;
      }

}
