/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "appenv.h"
#include "autodialog.h"
#include "blend.h"
#include "brush_select.h"
#include "draw_core.h"
#include "fuzzy_select.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "interface.h"
#include "paint_funcs.h"
#include "palette.h"
#include "selection.h"
#include "tools.h"
#include "undo.h"

/*  target size  */
#define  TARGET_HEIGHT     15
#define  TARGET_WIDTH      15

#define    SQR(x) ((x) * (x))

/*  the Blend structures  */
typedef enum
{
  Linear,
  BiLinear,
  Radial,
  Square,
  Conical
} GradientType;

typedef struct _BlendTool BlendTool;

struct _BlendTool
{
  DrawCore *      core;       /*  Core select object          */

  int             startx;     /*  starting x coord            */
  int             starty;     /*  starting y coord            */

  int             endx;       /*  ending x coord              */
  int             endy;       /*  ending y coord              */

};

typedef struct _BlendOptions BlendOptions;

struct _BlendOptions
{
  double    opacity;
  double    offset;
  GradientType gradient_type;
  int       paint_mode;

  GtkObserver opacity_observer;
  GtkObserver offset_observer;
};

/*  local function prototypes  */
static void  blend_button_press          (Tool *, GdkEventButton *, gpointer);
static void  blend_button_release        (Tool *, GdkEventButton *, gpointer);
static void  blend_motion                (Tool *, GdkEventMotion *, gpointer);
static void  blend_control               (Tool *, int, gpointer);

static void  gradient_fill_region        (PixelRegion *, int, GradientType,
					  int, int, int, int);
static void  gradient_fill_line_conical  (unsigned char *, double,
					  double *, int, int, int, int, int);
static void  gradient_fill_line_square   (unsigned char *, double,
					  int, int, int, int, int);
static void  gradient_fill_line_radial   (unsigned char *, double,
					  int, int, int, int, int);
static void  gradient_fill_line_linear   (unsigned char *, double,
					  double *, int, int, int, int, int);
static void  gradient_fill_line_bilinear (unsigned char *, double,
					  double *, int, int, int, int, int);

/*  local variables  */
static unsigned char fg_col [3];
static unsigned char bg_col [3];

static BlendOptions *blend_options = NULL;


static gint
blend_scale_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  double *scale_val;

  scale_val = (double *) observer->user_data;
  adj_data = (GtkDataAdjustment *) data;
  *scale_val = adj_data->value;

  return FALSE;
}

static void
blend_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

static void
blend_type_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  blend_options->gradient_type = (GradientType) client_data;
}

static void
blend_mode_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  blend_options->paint_mode = (int) client_data;
}

static BlendOptions *
create_blend_options ()
{
  BlendOptions *options;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *option_menu;
  GtkWidget *menu;
  GtkWidget *radio_frame;
  GtkWidget *radio_box;
  GtkWidget *radio_button;
  GtkWidget *opacity_scale;
  GtkData *opacity_scale_data;
  GtkWidget *offset_scale;
  GtkData *offset_scale_data;
  GtkData *owner = NULL;
  int i;
  char *button_names[5] =
  {
    "Linear",
    "Bi-Linear",
    "Radial",
    "Square",
    "Conical"
  };

  /*  the new options structure  */
  options = (BlendOptions *) xmalloc (sizeof (BlendOptions));
  options->opacity = 100.0;
  options->offset = 0.0;
  options->gradient_type = Linear;
  options->paint_mode = NORMAL;

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  
  /*  the main label  */
  label = gtk_label_new ("Blend Options");
  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  the opacity scale  */
  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  
  label = gtk_label_new ("Blend Opacity");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  opacity_scale_data = gtk_data_adjustment_new (100.0, 0.0, 100.0, 1.0, 1.0, 1.0);
  opacity_scale = gtk_hscale_new ((GtkDataAdjustment *) opacity_scale_data);
  gtk_box_pack (hbox, opacity_scale, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (opacity_scale, GTK_POS_TOP);
  options->opacity_observer.update = blend_scale_update;
  options->opacity_observer.disconnect = blend_disconnect;
  options->opacity_observer.user_data = &options->opacity;
  gtk_data_attach (opacity_scale_data, &options->opacity_observer);
  gtk_widget_show (opacity_scale);
  gtk_widget_show (hbox);

  /*  the offset scale  */
  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  
  label = gtk_label_new ("Blend Offset");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  offset_scale_data = gtk_data_adjustment_new (0.0, 0.0, 100.0, 1.0, 1.0, 1.0);
  offset_scale = gtk_hscale_new ((GtkDataAdjustment *) offset_scale_data);
  gtk_box_pack (hbox, offset_scale, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (offset_scale, GTK_POS_TOP);
  options->offset_observer.update = blend_scale_update;
  options->offset_observer.disconnect = blend_disconnect;
  options->offset_observer.user_data = &options->offset;
  gtk_data_attach (offset_scale_data, &options->offset_observer);
  gtk_widget_show (offset_scale);
  gtk_widget_show (hbox);

  /*  the paint mode menu  */
  hbox = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Mode:");
  gtk_box_pack (hbox, label, FALSE, FALSE, 2, GTK_PACK_START);
  menu = create_paint_mode_menu (blend_mode_callback);
  option_menu = gtk_option_menu_new ();
  gtk_box_pack (hbox, option_menu, FALSE, FALSE, 2, GTK_PACK_START);
  
  gtk_widget_show (label);
  gtk_widget_show (option_menu);
  gtk_widget_show (hbox);

  /*  the radio frame and box  */
  radio_frame = gtk_frame_new (NULL);
  gtk_box_pack (vbox, radio_frame, FALSE, FALSE, 0, GTK_PACK_START);

  radio_box = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (radio_box, 0);
  gtk_container_add (radio_frame, radio_box);

  /*  the radio buttons  */
  for (i = 0; i < 5; i++)
    {
      radio_button = gtk_radio_button_new (owner);
      if (!i)
	owner = gtk_button_get_owner (radio_button);
      gtk_box_pack (radio_box, radio_button, FALSE, FALSE, 0, GTK_PACK_START);
      gtk_callback_add (gtk_button_get_state (radio_button),
			blend_type_callback,
			(void *) i);
      label = gtk_label_new (button_names[i]);
      gtk_label_set_alignment (label, 0.0, 0.5);
      gtk_container_add (radio_button, label);
      gtk_widget_show (label);
      gtk_widget_show (radio_button);
    }
  gtk_widget_show (radio_box);
  gtk_widget_show (radio_frame);


  /*  Register this selection options widget with the main tools options dialog  */
  tools_register_options (BLEND, vbox);

  /*  Post initialization  */
  gtk_option_menu_set_menu (option_menu, menu);

  return options;
}


static void
blend_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  BlendTool * blend_tool;

  gdisp = (GDisplay *) gdisp_ptr;
  blend_tool = (BlendTool *) tool->private;

  switch (gimage_type (gdisp->gimage))
    {
    case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
      message_box ("Blend: Invalid for indexed images.", NULL, NULL);
      return;

      break;
    default:
      break;
    }

  /*  Keep the coordinates of the target  */
  gdisplay_untransform_coords (gdisp, bevent->x, bevent->y, 
			       &blend_tool->startx, &blend_tool->starty, FALSE, 1);

  blend_tool->endx = blend_tool->startx;
  blend_tool->endy = blend_tool->starty;
  
  /*  Make the tool active and set the gdisplay which owns it  */
  gdk_pointer_grab (gdisp->canvas->window, FALSE,
		    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
		    NULL, NULL, bevent->time);

  tool->gdisp_ptr = gdisp_ptr;
  tool->state = ACTIVE;

  /*  Start drawing the blend tool  */
  draw_core_start (blend_tool->core, gdisp->canvas->window, tool);
}

static void
blend_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GImage * gimage;
  BlendTool * blend_tool;
  TempBuf *buf;
  PixelRegion bufPR;
  int has_alpha;
  int x1, y1, x2, y2;

  gimage = ((GDisplay *) gdisp_ptr)->gimage;
  blend_tool = (BlendTool *) tool->private;

  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  /*  if the 3rd button isn't pressed, fill the selected region  */
  if (! (bevent->state & GDK_BUTTON3_MASK) &&
      ((blend_tool->startx != blend_tool->endx) ||
       (blend_tool->starty != blend_tool->endy)))
    {
      gimage_mask_bounds (gimage, &x1, &y1, &x2, &y2);

      has_alpha = gimage_has_alpha (gimage);

      buf = temp_buf_new ((x2 - x1), (y2 - y1),
			  gimage_bytes (gimage), 0, 0, NULL);
      bufPR.bytes = buf->bytes;
      bufPR.x = x1;
      bufPR.y = y1;
      bufPR.w = buf->width;
      bufPR.h = buf->height;
      bufPR.rowstride = buf->width * bufPR.bytes;
      bufPR.data = temp_buf_data (buf);

      gradient_fill_region (&bufPR, has_alpha, blend_options->gradient_type,
			    blend_tool->startx - x1, blend_tool->starty - y1, 
			    blend_tool->endx - x1, blend_tool->endy - y1);

      gimage_apply_image (gimage, &bufPR, 1, (blend_options->opacity * 255) / 100,
			  blend_options->paint_mode);

      /*  update the image  */
      gimage_update (gimage, bufPR.x, bufPR.y, bufPR.w, bufPR.h);
      gdisplays_flush ();

      /*  free the temporary buffer  */
      temp_buf_free (buf);
    }

  draw_core_stop (blend_tool->core, tool);
  tool->state = INACTIVE;
}

static void
blend_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  BlendTool * blend_tool;

  gdisp = (GDisplay *) gdisp_ptr;
  blend_tool = (BlendTool *) tool->private;

  /*  undraw the current tool  */
  draw_core_pause (blend_tool->core, tool);

  /*  Get the current coordinates  */
  gdisplay_untransform_coords (gdisp, mevent->x, mevent->y, 
			       &blend_tool->endx, &blend_tool->endy, FALSE, 1);

  /*  redraw the current tool  */
  draw_core_resume (blend_tool->core, tool);
}

static void
blend_draw (tool)
     Tool * tool;
{
  GDisplay * gdisp;
  BlendTool * blend_tool;
  int tx1, ty1, tx2, ty2;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  blend_tool = (BlendTool *) tool->private;

  gdisplay_transform_coords (gdisp, blend_tool->startx, blend_tool->starty,
			     &tx1, &ty1, 1);
  gdisplay_transform_coords (gdisp, blend_tool->endx, blend_tool->endy,
			     &tx2, &ty2, 1);

  /*  Draw start target  */
  gdk_draw_line (blend_tool->core->win, blend_tool->core->gc, 
		 tx1 - (TARGET_WIDTH >> 1), ty1,
		 tx1 + (TARGET_WIDTH >> 1), ty1);
  gdk_draw_line (blend_tool->core->win, blend_tool->core->gc, 
		 tx1, ty1 - (TARGET_HEIGHT >> 1),
		 tx1, ty1 + (TARGET_HEIGHT >> 1));

  /*  Draw end target  */
  gdk_draw_line (blend_tool->core->win, blend_tool->core->gc, 
		 tx2 - (TARGET_WIDTH >> 1), ty2,
		 tx2 + (TARGET_WIDTH >> 1), ty2);
  gdk_draw_line (blend_tool->core->win, blend_tool->core->gc, 
		 tx2, ty2 - (TARGET_HEIGHT >> 1),
		 tx2, ty2 + (TARGET_HEIGHT >> 1));

  /*  Draw the line between the start and end coords  */
  gdk_draw_line (blend_tool->core->win, blend_tool->core->gc, 
		 tx1, ty1, tx2, ty2);
}

static void
blend_control (tool, action, gdisp_ptr)
     Tool * tool;
     int action;
     gpointer gdisp_ptr;
{
  BlendTool * blend_tool;

  blend_tool = (BlendTool *) tool->private;

  switch (action)
    {
    case PAUSE : 
      draw_core_pause (blend_tool->core, tool);
      break;
    case RESUME :
      draw_core_resume (blend_tool->core, tool);
      break;
    case HALT :
      draw_core_stop (blend_tool->core, tool);
      break;
    }
}

static void  
gradient_fill_line_conical (data, dist, axis, width, has_alpha, bytes, x, y)
     unsigned char * data;
     double dist;
     double * axis;
     int width;
     int has_alpha;
     int bytes;
     int x, y;
{
  int i;
  int b;
  int alpha;
  int y_sqr;
  double vec[2];
  double r;
  double rat;

  alpha = (has_alpha) ? bytes - 1 : bytes;

  if (dist == 0.0)
    return;
  else
    {
      /*  calculate the offset from the start in pixels  */
      y_sqr = y * y;

      for (i = 0; i < width; i++)
	{
	  r = sqrt (x * x + y_sqr);
	  if (r != 0.0)
	    {
	      vec[0] = x / r;
	      vec[1] = y / r;
	      rat = axis[0] * vec[0] + axis[1] * vec[1];
	      if (rat > 1.0)
		rat = 1.0;
	      if (rat < -1.0)
		rat = -1.0;

	      /*  This cool idea is courtesy Josh MacDonald, Ali Rahimi--
	       *  two more xcf losers
	       */
	      rat = fabs (acos (rat)) / M_PI;
	      rat = pow (rat, (blend_options->offset / 10) + 1);
	    }
	  else
	    rat = 0.5;
	  
	  x++;
	  
	  for (b = 0; b < alpha; b++)
	    *data++ = fg_col[b] + (bg_col[b] - fg_col[b]) * rat;

	  if (has_alpha)
	    *data++ = OPAQUE;
	}
    }
}

static void  
gradient_fill_line_square (data, dist, width, has_alpha, bytes, x, y)
     unsigned char * data;
     double dist;
     int width;
     int has_alpha;
     int bytes;
     int x, y;
{
  int i;
  int b;
  int y_abs;
  int alpha;
  double r;
  double offset;
  double rat;

  alpha = (has_alpha) ? bytes - 1 : bytes;

  if (!dist)
    return;
  else
    {
      /*  calculate the offset from the start as a value between 0 and 1  */
      offset = blend_options->offset / 100.0;
      y_abs = abs (y);

      for (i = 0; i < width; i++)
	{
	  r = MAXIMUM (abs (x), y_abs);
	  rat = r / dist;
	  
	  x++;
	  
	  for (b = 0; b < alpha; b++)
	    {
	      if (rat >= 1.0)
		*data++ = bg_col[b];
	      else if (rat < offset)
		*data++ = fg_col[b];
	      else
		*data++ = fg_col[b] + (bg_col[b] - fg_col[b]) *
		  ((rat - offset) / (1.0 - offset));
	    }

	  if (has_alpha)
	    *data++ = OPAQUE;
	}
    }
}

static void  
gradient_fill_line_radial (data, dist, width, has_alpha, bytes, x, y)
     unsigned char * data;
     double dist;
     int width;
     int has_alpha;
     int bytes;
     int x, y;
{
  int i;
  int b;
  int y_sqr;
  int alpha;
  double r;
  double offset;
  double rat;

  alpha = (has_alpha) ? bytes - 1 : bytes;

  if (!dist)
    return;
  else
    {
      /*  calculate the radial offset from the start as a value between 0 and 1  */
      offset = blend_options->offset / 100.0;
      y_sqr = SQR (y);

      for (i = 0; i < width; i++)
	{
	  r = SQR (x) + y_sqr;
	  rat = r / dist;
	  
	  x++;
	  
	  for (b = 0; b < alpha; b++)
	    {
	      if (rat >= 1.0)
		*data++ = bg_col[b];
	      else if (rat < offset)
		*data++ = fg_col[b];
	      else
		*data++ = fg_col[b] + (bg_col[b] - fg_col[b]) *
		  ((rat - offset) / (1.0 - offset));
	    }

	  if (has_alpha)
	    *data++ = OPAQUE;
	}
    }
}

static void  
gradient_fill_line_linear  (data, dist, vec, width, has_alpha, bytes, x, y)
     unsigned char * data;
     double dist;
     double *vec;
     int width;
     int has_alpha;
     int bytes;
     int x, y;
{
  int i;
  int b;
  int alpha;
  double y_part;
  double r;
  double rat;

  alpha = (has_alpha) ? bytes - 1 : bytes;

  if (!dist)
    return;
  else
    {
      y_part = vec[1] * y;

      for (i = 0; i < width; i++)
	{
	  r = vec[0] * x + y_part;
	  rat = r / dist;
	  
	  x++;
	  
	  for (b = 0; b < alpha; b++)
	    {
	      if (rat < 0.0)
		*data++ = fg_col[b];
	      else if (rat > 1.0)
		*data++ = bg_col[b];
	      else
		*data++ = fg_col[b] + (bg_col[b] - fg_col[b]) * rat;
	    }

	  if (has_alpha)
	    *data++ = OPAQUE;
	}
    }
}

static void  
gradient_fill_line_bilinear  (data, dist, vec, width, has_alpha, bytes, x, y)
     unsigned char * data;
     double dist;
     double *vec;
     int width;
     int has_alpha;
     int bytes;
     int x, y;
{
  int i;
  int b;
  int alpha;
  double offset;
  double y_part;
  double r;
  double rat;

  alpha = (has_alpha) ? bytes - 1 : bytes;

  if (!dist)
    return;
  else
    {
      /*  calculate linear offset from the start line outward  */
      offset = blend_options->offset / 100.0;
      y_part = vec[1] * y;

      for (i = 0; i < width; i++)
	{
	  r = vec[0] * x + y_part;
	  rat = r / dist;
	  
	  x++;
	  
	  for (b = 0; b < alpha; b++)
	    {
	      if (rat <= -1.0 || rat >= 1.0)
		*data++ = bg_col[b];
	      else if (fabs (rat) < offset)
		*data++ = fg_col[b];
	      else
		*data++ = fg_col[b] + (bg_col[b] - fg_col[b]) *
		  ((fabs (rat) - offset) / (1.0 - offset));
	    }

	  if (has_alpha)
	    *data++ = OPAQUE;
	}
    }
}

static void
gradient_fill_region (PR, has_alpha, type, sx, sy, ex, ey)
     PixelRegion *PR;
     int has_alpha;
     GradientType type;
     int sx, sy;
     int ex, ey;
{
  int i;
  double dist;
  double vec[2];
  unsigned char * data;

  /* get the foreground and background colors  */
  palette_get_background (&bg_col[0], &bg_col[1], &bg_col[2]);
  palette_get_foreground (&fg_col[0], &fg_col[1], &fg_col[2]);

  data = PR->data;

  switch (type)
    {
    case Radial:
      /* If this is a radial fill, calculate the distance between start and end  */
      dist = SQR (ex - sx) + SQR (ey - sy);
      break;

    case Conical:
      /* If this is a conical fill, calculate:
       * 1) distance between start and end
       * 2) vector from start to end, normalized
       */
      dist = sqrt (SQR (ex - sx) + SQR (ey - sy));
      if (dist > 0.0)
	{
	  vec[0] = (ex - sx) / dist;
	  vec[1] = (ey - sy) / dist;
	}
      break;

    case Square:
      /* If this is a square fill, calculate the distance between start and end  */
      dist = MAXIMUM (abs (ex - sx), abs (ey - sy));
      break;

    case Linear: case BiLinear:
      /* If this is a linear fill, calculate:
       * 1) distance between start and end
       * 2) vector from start to end, normalized
       */
      dist = sqrt (SQR (ex - sx) + SQR (ey - sy));
      if (dist > 0.0)
	{
	  vec[0] = (ex - sx) / dist;
	  vec[1] = (ey - sy) / dist;
	}
      break;

    }
       
  for (i = 0; i < PR->h; i++)
    {
      switch (type)
	{
	case Radial:
	  gradient_fill_line_radial (data, dist, PR->w, has_alpha,
				     PR->bytes, -sx, i - sy);
	  break;
	  
	case Conical:
	  gradient_fill_line_conical (data, dist, vec, PR->w, has_alpha,
				      PR->bytes, -sx, i - sy);
	  break;
	  
	case Square:
	  gradient_fill_line_square (data, dist, PR->w, has_alpha,
				     PR->bytes, -sx, i - sy);
	  break;
	  
	case Linear:
	  gradient_fill_line_linear (data, dist, vec, PR->w, has_alpha,
				     PR->bytes, -sx, i - sy);
	  break;

	case BiLinear:
	  gradient_fill_line_bilinear (data, dist, vec, PR->w, has_alpha,
				       PR->bytes, -sx, i - sy);
	  break;
	}

      data += PR->rowstride;
    }
}

/****************************/
/*  Global blend functions  */
/****************************/

Tool *
tools_new_blend ()
{
  Tool * tool;
  BlendTool * private;

  if (! blend_options)
    blend_options = create_blend_options ();

  tool = (Tool *) xmalloc (sizeof (Tool));
  private = (BlendTool *) xmalloc (sizeof (BlendTool));

  private->core = draw_core_new (blend_draw);

  tool->type = BLEND;
  tool->state = INACTIVE;
  tool->scroll_lock = 1;  /*  Disallow scrolling  */
  tool->private = private;
  tool->button_press_func = blend_button_press;
  tool->button_release_func = blend_button_release;
  tool->motion_func = blend_motion;
  tool->arrow_keys_func = standard_arrow_keys_func;
  tool->control_func = blend_control;

  return tool;
}

void
tools_free_blend (tool)
     Tool * tool;
{
  BlendTool * blend_tool;

  blend_tool = (BlendTool *) tool->private;

  if (tool->state == ACTIVE)
    draw_core_stop (blend_tool->core, tool);

  draw_core_free (blend_tool->core);

  xfree (blend_tool);
}


