/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "gimage_mask.h"
#include "paint_funcs.h"
#include "shadow_ops.h"

void
gimage_merge_shadow (gimage)
     GImage * gimage;
{
  /*  because gimage_apply_image desires as input a pixel region,
   *  we'll package the shadow buffer into a pix-region and pass
   *  it to gimage_apply_image
   */
  PixelRegion shadowPR;
  int x1, y1, x2, y2;

  gimage_mask_bounds (gimage, &x1, &y1, &x2, &y2);

  shadowPR.bytes     = gimage_bytes (gimage);
  shadowPR.x         = x1;
  shadowPR.y         = y1;
  shadowPR.w         = (x2 - x1);
  shadowPR.h         = (y2 - y1);
  shadowPR.rowstride = shadowPR.bytes * gimage_width (gimage);
  shadowPR.data      = gimage->shadow_buf + y1 * shadowPR.rowstride + 
    x1 * shadowPR.bytes;

  gimage_apply_image (gimage, &shadowPR, 1, OPAQUE, REPLACE_MODE);
}


