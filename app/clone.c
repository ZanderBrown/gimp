 /* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <string.h>
#include "appenv.h"
#include "autodialog.h"
#include "brushes.h"
#include "errors.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "interface.h"
#include "paint_funcs.h"
#include "paint_core.h"
#include "patterns.h"
#include "clone.h"
#include "selection.h"
#include "tools.h"

#define TARGET_HEIGHT  15
#define TARGET_WIDTH   15

typedef enum
{
  ImageClone,
  PatternClone
} CloneType;

typedef struct _CloneOptions CloneOptions;
struct _CloneOptions
{
  CloneType type;
  int       aligned;

  GtkObserver aligned_observer;
};

/*  forward function declarations  */
static void         clone_draw            (Tool *);
static void         clone_motion          (Tool *);
static void	    clone_line_image      (GImage *, GImage *, unsigned char *,
					   unsigned char *, int, int, int, int);
static void         clone_line_pattern    (GImage *, GPatternP, unsigned char *,
					   int, int, int, int);

/*  local variables  */
static int          target_x = 0;             /*                         */
static int          target_y = 0;             /*  position of clone src  */
static int          align_x = 0;              /*  x offset to src        */
static int          align_y = 0;              /*  y offset to src        */
static int          first = 1;
static int          trans_tx, trans_ty;       /*  transformed target  */
static int          src_gdisp_ID;             /*  ID of source gdisplay  */
static CloneOptions *clone_options = NULL;


static gint
clone_toggle_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataInt *int_data;
  int *toggle_val;
  
  toggle_val = (int *) observer->user_data;
  int_data = (GtkDataInt *) data;
  
  if (int_data->value == GTK_STATE_ACTIVATED)
    *toggle_val = TRUE;
  else if (int_data->value == GTK_STATE_DEACTIVATED)
    *toggle_val = FALSE;

  return FALSE;
}

static void
clone_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

static void
clone_type_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  clone_options->type =(CloneType) client_data;
}

static CloneOptions *
create_clone_options ()
{
  CloneOptions *options;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *aligned_toggle;
  GtkWidget *radio_frame;
  GtkWidget *radio_box;
  GtkWidget *radio_button;
  GtkData *owner = NULL;
  int i;
  char *button_names[2] =
  {
    "Image Source",
    "Pattern Source"
  };

  /*  the new options structure  */
  options = (CloneOptions *) xmalloc (sizeof (CloneOptions));
  options->type = ImageClone;
  options->aligned = TRUE;

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  
  /*  the main label  */
  label = gtk_label_new ("Clone Tool Options");
  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  the radio frame and box  */
  radio_frame = gtk_frame_new (NULL);
  gtk_box_pack (vbox, radio_frame, FALSE, FALSE, 0, GTK_PACK_START);

  radio_box = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (radio_box, 0);
  gtk_container_add (radio_frame, radio_box);

  /*  the radio buttons  */
  for (i = 0; i < 2; i++)
    {
      radio_button = gtk_radio_button_new (owner);
      if (!i)
	owner = gtk_button_get_owner (radio_button);
      gtk_box_pack (radio_box, radio_button, FALSE, FALSE, 0, GTK_PACK_START);
      gtk_callback_add (gtk_button_get_state (radio_button),
			clone_type_callback,
			(void *) i);
      label = gtk_label_new (button_names[i]);
      gtk_label_set_alignment (label, 0.0, 0.5);
      gtk_container_add (radio_button, label);
      gtk_widget_show (label);
      gtk_widget_show (radio_button);
    }
  gtk_widget_show (radio_box);
  gtk_widget_show (radio_frame);

  /*  the aligned toggle button  */
  aligned_toggle = gtk_check_button_new ();
  gtk_box_pack (vbox, aligned_toggle, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Aligned");
  gtk_label_set_alignment (label, 0.0, 0.5);
  gtk_container_add (aligned_toggle, label);
  gtk_widget_show (label);
  options->aligned_observer.update = clone_toggle_update;
  options->aligned_observer.disconnect = clone_disconnect;
  options->aligned_observer.user_data = &options->aligned;
  gtk_data_attach (gtk_button_get_state (aligned_toggle),
		   &options->aligned_observer);
  gtk_widget_show (aligned_toggle);

  /*  Register this selection options widget with the main tools options dialog  */
  tools_register_options (CLONE, vbox);

  return options;
}

void *
clone_paint_func (tool, state)
     Tool * tool;
     int state;
{
  GDisplay * gdisp;
  GDisplay * src_gdisp;
  PaintCore * paint_core;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  switch (state)
    {
    case MOTION_PAINT :
      draw_core_pause (paint_core->core, tool);
      clone_motion (tool);
      break;

    case INIT_PAINT :
      if (paint_core->state & ControlMask)
	{
	  src_gdisp_ID = gdisp->ID;
	  target_x = paint_core->curx;
	  target_y = paint_core->cury;
	  align_x = align_y = 0;
	  first = 1;

	  if (clone_options->type == PatternClone)
	    if (!get_active_pattern ())
	      message_box ("No patterns available for this operation.", NULL, NULL);
	}
      break;

    case FINISH_PAINT :
      draw_core_stop (paint_core->core, tool);
      return NULL;
      break;

    default :
      break;
    }

  /*  Calculate the coordinates of the target  */
  src_gdisp = gdisplay_get_ID (src_gdisp_ID);
  if (!src_gdisp)
    {
      src_gdisp_ID = gdisp->ID;
      src_gdisp = gdisplay_get_ID (src_gdisp_ID);
    }      

  /*  If alignment is on, add the target to the current position  */
  if (clone_options->aligned)
    gdisplay_transform_coords (src_gdisp, paint_core->curx + align_x, 
			       paint_core->cury + align_y, &trans_tx, &trans_ty, 1);
  else
    gdisplay_transform_coords (src_gdisp, target_x, target_y,
			       &trans_tx, &trans_ty, 1);

  if (state == INIT_PAINT)
    /*  Initialize the tool drawing core  */
    draw_core_start (paint_core->core,
		     src_gdisp->canvas->window,
		     tool);
  else if (state == MOTION_PAINT)
    draw_core_resume (paint_core->core, tool);

  return NULL;
}

Tool *
tools_new_clone ()
{
  Tool * tool;
  PaintCore * private;

  if (! clone_options)
    clone_options = create_clone_options ();

  tool = paint_core_new (CLONE);

  private = (PaintCore *) tool->private;
  private->paint_func = clone_paint_func;
  private->core->draw_func = clone_draw;

  /*  Initialize the src ID to -1  */
  src_gdisp_ID = -1;

  return tool;
}

void
tools_free_clone (tool)
     Tool * tool;
{
  paint_core_free (tool);
}

static void
clone_draw (tool)
     Tool * tool;
{
  PaintCore * paint_core;

  paint_core = (PaintCore *) tool->private;

  if (clone_options->type == ImageClone)
    {
      gdk_draw_line (paint_core->core->win, paint_core->core->gc, 
		     trans_tx - (TARGET_WIDTH >> 1), trans_ty,
		     trans_tx + (TARGET_WIDTH >> 1), trans_ty);
      gdk_draw_line (paint_core->core->win, paint_core->core->gc, 
		     trans_tx, trans_ty - (TARGET_HEIGHT >> 1),
		     trans_tx, trans_ty + (TARGET_HEIGHT >> 1));
    }
}

static void
clone_motion (tool)
     Tool * tool;
{
  GDisplay * gdisp;
  GDisplay * src_gdisp = NULL;
  PaintCore * paint_core;
  unsigned char * s;
  unsigned char * d;
  TempBuf * orig;
  TempBuf * area;
  int y;
  int x1, y1, x2, y2;
  int offset_x, offset_y;
  int has_alpha;
  PixelRegion srcPR, destPR;
  GPatternP pattern;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  x1 = paint_core->curx;
  y1 = paint_core->cury;
  x2 = paint_core->lastx;
  y2 = paint_core->lasty;

  /*  If the control key is down, move the src target and return */
  if (paint_core->state & ControlMask)
    {
      target_x = x1;
      target_y = y1;
      align_x = align_y = 0;
      first = 1;
      return;
    }
  /*  otherwise, update the target  */
  else
    {
      if (clone_options->aligned)
	{
	  if (first)
	    {
	      align_x = target_x - x1;
	      align_y = target_y - y1;
	      first = 0;
	    }
	  
	  /*  find offset to src area  */
	  offset_x = align_x;
	  offset_y = align_y;
	}
      else
	{
	  target_x += (x1 - x2);
	  target_y += (y1 - y2);

	  /*  find offset to src area  */
	  offset_x = target_x - x1;
	  offset_y = target_y - y1;
	}
    }

  /*  Find the source gdisplay */
  src_gdisp = gdisplay_get_ID (src_gdisp_ID);

  /*  If the src_gdisp no longer exists, do nothing  */
  if (!src_gdisp)
    return;

  /*  Get a region which can be used to paint to  */
  if (! (area = paint_core_get_paint_area (tool, 0)))
    return;

  /*  Determine whether the source image has an alpha channel  */
  has_alpha = gimage_has_alpha (src_gdisp->gimage);

  switch (clone_options->type)
    {
    case ImageClone:
      /*  Set the paint area to transparent  */
      memset (temp_buf_data (area), 0, area->width * area->height * area->bytes);

      /*  If the source gimage is different from the destination,
       *  then we should copy straight from the destination image
       *  to the canvas.
       *  Otherwise, we need a call to get_orig_image to make sure
       *  we get a copy of the unblemished (offset) image
       */
      if (src_gdisp->gimage->ID != gdisp->gimage->ID)
	{
	  x1 = BOUNDS (area->x + offset_x, 0, gimage_width (src_gdisp->gimage));
	  y1 = BOUNDS (area->y + offset_y, 0, gimage_height (src_gdisp->gimage));
	  x2 = BOUNDS (area->x + offset_x + area->width,
		       0, gimage_width (src_gdisp->gimage));
	  y2 = BOUNDS (area->y + offset_y + area->height,
		       0, gimage_height (src_gdisp->gimage));
	  
	  if (!(x2 - x1) || !(y2 - y1))
	    return;
	  
	  srcPR.bytes = gimage_bytes (src_gdisp->gimage);
	  srcPR.w = x2 - x1;
	  srcPR.h = y2 - y1;
	  srcPR.rowstride = srcPR.bytes * gimage_width (src_gdisp->gimage);
	  srcPR.data = gimage_data (src_gdisp->gimage) +
	    y1 * srcPR.rowstride + x1 * srcPR.bytes;
	}
      else
	{
	  x1 = BOUNDS (area->x + offset_x, 0, gimage_width (gdisp->gimage));
	  y1 = BOUNDS (area->y + offset_y, 0, gimage_height (gdisp->gimage));
	  x2 = BOUNDS (area->x + offset_x + area->width,
		       0, gimage_width (gdisp->gimage));
	  y2 = BOUNDS (area->y + offset_y + area->height,
		       0, gimage_height (gdisp->gimage));
	  
	  if (!(x2 - x1) || !(y2 - y1))
	    return;
	  
	  /*  get the original image  */
	  orig = paint_core_get_orig_image (tool, x1, y1, x2, y2);

	  srcPR.bytes = orig->bytes;
	  srcPR.w = x2 - x1;
	  srcPR.h = y2 - y1;
	  srcPR.rowstride = srcPR.bytes * orig->width;
	  srcPR.data = temp_buf_data (orig);
	}

      offset_x = x1 - (area->x + offset_x);
      offset_y = y1 - (area->y + offset_y);

      /*  configure the destination  */
      destPR.bytes = area->bytes;
      destPR.w = srcPR.w;
      destPR.h = srcPR.h;
      destPR.rowstride = destPR.bytes * area->width;
      destPR.data = temp_buf_data (area) + offset_y * destPR.rowstride +
	offset_x * destPR.bytes;
      break;

    case PatternClone:
      pattern = get_active_pattern ();
	  
      if (!pattern)
	return;

      destPR.bytes = area->bytes;
      destPR.w = area->width;
      destPR.h = area->height;
      destPR.rowstride = destPR.bytes * area->width;
      destPR.data = temp_buf_data (area);
      break;
    }

  s = srcPR.data;
  d = destPR.data;
  for (y = 0; y < destPR.h; y++)
    {
      switch (clone_options->type)
	{
	case ImageClone:
	  clone_line_image (gdisp->gimage, src_gdisp->gimage, s, d,
			    has_alpha, srcPR.bytes, destPR.bytes, destPR.w);
	  s += srcPR.rowstride;
	  break;
	case PatternClone:
	  clone_line_pattern (gdisp->gimage, pattern, d,
			      area->x + offset_x, area->y + y + offset_y,
			      destPR.bytes, destPR.w);
	  break;
	}

      d += destPR.rowstride;
    }
  
  /*  paste the newly painted canvas to the gimage which is being worked on  */
  paint_core_paste_canvas (tool, OPAQUE, (int) (get_brush_opacity () * 255),
			   get_brush_paint_mode (), SOFT, CONSTANT);
}

static void
clone_line_image (dest, src, s, d, has_alpha, src_bytes, dest_bytes, width)
     GImage * dest, * src;
     unsigned char * s, * d;
     int has_alpha;
     int src_bytes;
     int dest_bytes;
     int width;
{
  unsigned char rgb[3];
  int src_alpha, dest_alpha;

  src_alpha = src_bytes - 1;
  dest_alpha = dest_bytes - 1;

  while (width--)
    {
      gimage_get_color (src, rgb, s);
      gimage_transform_color (dest, rgb, d, RGB);
      
      if (has_alpha)
	d[dest_alpha] = s[src_alpha];
      else
	d[dest_alpha] = OPAQUE;
      
      s += src_bytes;
      d += dest_bytes;
    }
}

static void
clone_line_pattern (dest, pattern, d, x, y, bytes, width)
     GImage * dest;
     GPatternP pattern;
     unsigned char * d;
     int x, y;
     int bytes;
     int width;
{
  unsigned char *pat, *p;
  int color, alpha;
  int i;

  /*  Make sure x, y are positive  */
  while (x < 0)
    x += pattern->mask->width;
  while (y < 0)
    y += pattern->mask->height;

  /*  Get a pointer to the appropriate scanline of the pattern buffer  */
  pat = temp_buf_data (pattern->mask) +
    (y % pattern->mask->height) * pattern->mask->width * pattern->mask->bytes;
  color = (pattern->mask->bytes == 3) ? RGB : GRAY;

  alpha = bytes - 1;

  for (i = 0; i < width; i++)
    {
      p = pat + ((i + x) % pattern->mask->width) * pattern->mask->bytes;

      gimage_transform_color (dest, p, d, color);
      
      d[alpha] = OPAQUE;
      
      d += bytes;
    }
}

