/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include <stdio.h>
#include "appenv.h"
#include "app_procs.h"
#include "autodialog.h"
#include "brushes.h"
#include "by_color_select.h"
#include "colormaps.h"
#include "commands.h"
#include "fileops.h"
#include "gdisplay_ops.h"
#include "general.h"
#include "gimage_mask.h"
#include "gimprc.h"
#include "global_edit.h"
#include "image_render.h"
#include "info_window.h"
#include "interface.h"
#include "layers_dialog.h"
#include "palette.h"
#include "patterns.h"
#include "scale.h"
#include "tools.h"
#include "undo.h"

typedef struct {
  int width_ID;
  int height_ID;
  int rgb_ID;
  int gray_ID;
  int background_ID;
  int white_ID;
  int transparent_ID;
  int width, height;
  int type;
  int fill_type;
  AutoDialog dlg;
} NewImageValues;


/*  new image local functions  */
static void new_image_callback (int, int, void *, void *);

/*  static variables  */
static   int          last_width = 256;
static   int          last_height = 256;
static   int          last_type = RGBA_GIMAGE;
static   int          last_fill_type = BACKGROUND_FILL;


/*  preferences local functions  */
static void preferences_callback (int, int, void *, void *);

/*  static variables  */
static   int          undo_levels_value;
static   int          last_transparency_type;
static   int          last_check_size;
static   AutoDialog   prefs_dlg = NULL;
static   int          undo_levels_ID;
static   int          trans_type_IDs[6];
static   int          check_size_IDs[3];


void
file_new_cmd_callback (GtkWidget *widget,
		       gpointer client_data,
		       gpointer call_data)
{
  GDisplay *gdisp;
  NewImageValues *vals;
  char buffer[16];
  int main_ID;
  int group_ID;
  int frame_ID;
  int label_ID;
  long state;

  /*  Before we try to determine the responsible gdisplay,
   *  make sure this wasn't called from the toolbox
   */
  if ((int) client_data)
    gdisp = gdisplay_active ();
  else
    gdisp = NULL;
  
  vals = xmalloc (sizeof (NewImageValues));
  vals->dlg = dialog_new ("New Image", new_image_callback, vals);
  if (gdisp)
    {
      vals->width = gdisp->gimage->width;
      vals->height = gdisp->gimage->height;
      vals->type = gdisp->gimage->type;
    }
  else
    {
      vals->width = last_width;
      vals->height = last_height;
      vals->type = last_type;
    }

  vals->fill_type = last_fill_type;

  dialog_new_item (vals->dlg, 0, ITEM_LABEL, "New Image", 0);
  main_ID = dialog_new_item (vals->dlg, 0, GROUP_ROWS, 0, 0);

  sprintf (buffer, "%d", vals->width);
  group_ID = dialog_new_item (vals->dlg, main_ID, GROUP_COLUMNS, 0, 0);
  label_ID = dialog_new_item (vals->dlg, group_ID, ITEM_LABEL, "Width", 0);
  vals->width_ID = dialog_new_item (vals->dlg, group_ID, ITEM_TEXT, buffer, 0);

  sprintf (buffer, "%d", vals->height);
  group_ID = dialog_new_item (vals->dlg, main_ID, GROUP_COLUMNS, 0, 0);
  label_ID = dialog_new_item (vals->dlg, group_ID, ITEM_LABEL, "Height", 0);
  vals->height_ID = dialog_new_item (vals->dlg, group_ID, ITEM_TEXT, buffer, 0);

  frame_ID = dialog_new_item (vals->dlg, main_ID, ITEM_FRAME, "Image Type", 0);
  group_ID = dialog_new_item (vals->dlg, frame_ID, GROUP_ROWS | GROUP_RADIO, 0, 0);
  vals->rgb_ID = dialog_new_item (vals->dlg, group_ID, ITEM_RADIO_BUTTON, "RGB", 0);
  vals->gray_ID = dialog_new_item (vals->dlg, group_ID, ITEM_RADIO_BUTTON, "Grayscale", 0);

  frame_ID = dialog_new_item (vals->dlg, main_ID, ITEM_FRAME, "Fill Type", 0);
  group_ID = dialog_new_item (vals->dlg, frame_ID, GROUP_ROWS | GROUP_RADIO, 0, 0);
  vals->background_ID = dialog_new_item (vals->dlg, group_ID, ITEM_RADIO_BUTTON, "Background", 0);
  vals->white_ID = dialog_new_item (vals->dlg, group_ID, ITEM_RADIO_BUTTON, "White", 0);
  vals->transparent_ID = dialog_new_item (vals->dlg, group_ID, ITEM_RADIO_BUTTON, "Transparent", 0);

  state = 1;
  switch (vals->type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE:
      dialog_change_item (vals->dlg, vals->rgb_ID, &state);
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      dialog_change_item (vals->dlg, vals->gray_ID, &state);
      break;
    }
  
  switch (vals->fill_type)
    {
    case BACKGROUND_FILL:
      dialog_change_item (vals->dlg, vals->background_ID, &state);
      break;
    case WHITE_FILL:
      dialog_change_item (vals->dlg, vals->white_ID, &state);
      break;
    case TRANSPARENT_FILL:
      dialog_change_item (vals->dlg, vals->transparent_ID, &state);
      break;
    }
      
  dialog_show (vals->dlg);
}

void
file_open_cmd_callback (GtkWidget *widget,
			gpointer client_data,
			gpointer call_data)
{
  file_open_callback (widget, client_data, call_data);
}

void
file_save_cmd_callback (GtkWidget *widget,
			gpointer client_data,
			gpointer call_data)
{
  file_save_callback (widget, client_data, call_data);
}

void
file_pref_cmd_callback (GtkWidget *widget,
			gpointer client_data,
			gpointer call_data)
{
  GDisplay * gdisp;
  int group_ID;
  int colgroup_ID;
  int radio_ID;
  int frame_ID;
  char buf [32];
  int button_set;

  gdisp = gdisplay_active ();

  if (!prefs_dlg)
    {
      prefs_dlg = dialog_new ("Preferences", preferences_callback, NULL);
      dialog_new_item (prefs_dlg, 0, ITEM_LABEL, "Preferences", NULL);

      group_ID = dialog_new_item (prefs_dlg, 0, GROUP_ROWS, NULL, NULL);
      colgroup_ID = dialog_new_item (prefs_dlg, group_ID, GROUP_COLUMNS, NULL, NULL);
      dialog_new_item (prefs_dlg, colgroup_ID, ITEM_LABEL, "Levels of undo:", NULL);
      sprintf (buf, "%d", levels_of_undo);
      undo_levels_value = levels_of_undo;
      undo_levels_ID = dialog_new_item (prefs_dlg, colgroup_ID, ITEM_TEXT, buf, NULL);

      frame_ID = dialog_new_item (prefs_dlg, group_ID, ITEM_FRAME,
				  "Transparency Types", NULL);
      radio_ID = dialog_new_item (prefs_dlg, frame_ID,
				  (GROUP_ROWS | GROUP_RADIO), NULL, NULL);

      last_transparency_type = transparency_type;

      button_set = (transparency_type == LIGHT_CHECKS);
      trans_type_IDs[0] = dialog_new_item (prefs_dlg, radio_ID, ITEM_RADIO_BUTTON,
					   "Light Checks", NULL);
      dialog_change_item (prefs_dlg, trans_type_IDs[0], &button_set);

      button_set = (transparency_type == GRAY_CHECKS);
      trans_type_IDs[1] = dialog_new_item (prefs_dlg, radio_ID, ITEM_RADIO_BUTTON,
					   "Mid-Tone Checks", NULL);
      dialog_change_item (prefs_dlg, trans_type_IDs[1], &button_set);

      button_set = (transparency_type == DARK_CHECKS);
      trans_type_IDs[2] = dialog_new_item (prefs_dlg, radio_ID, ITEM_RADIO_BUTTON,
					   "Dark Checks", NULL);
      dialog_change_item (prefs_dlg, trans_type_IDs[2], &button_set);

      button_set = (transparency_type == WHITE_ONLY);
      trans_type_IDs[3] = dialog_new_item (prefs_dlg, radio_ID, ITEM_RADIO_BUTTON,
					   "White Only", NULL);
      dialog_change_item (prefs_dlg, trans_type_IDs[3], &button_set);

      button_set = (transparency_type == GRAY_ONLY);
      trans_type_IDs[4] = dialog_new_item (prefs_dlg, radio_ID, ITEM_RADIO_BUTTON,
					   "Gray Only", NULL);
      dialog_change_item (prefs_dlg, trans_type_IDs[4], &button_set);

      button_set = (transparency_type == BLACK_ONLY);
      trans_type_IDs[5] = dialog_new_item (prefs_dlg, radio_ID, ITEM_RADIO_BUTTON,
					   "Black Only", NULL);
      dialog_change_item (prefs_dlg, trans_type_IDs[5], &button_set);
      

      frame_ID = dialog_new_item (prefs_dlg, group_ID, ITEM_FRAME,
				  "Check Sizes", NULL);
      radio_ID = dialog_new_item (prefs_dlg, frame_ID,
				  (GROUP_ROWS | GROUP_RADIO), NULL, NULL);

      last_check_size = transparency_size;

      button_set = (transparency_size == SMALL_CHECKS);
      check_size_IDs[0] = dialog_new_item (prefs_dlg, radio_ID, ITEM_RADIO_BUTTON,
					   "Small Checks", NULL);
      dialog_change_item (prefs_dlg, check_size_IDs[0], &button_set);

      button_set = (transparency_size == MEDIUM_CHECKS);
      check_size_IDs[1] = dialog_new_item (prefs_dlg, radio_ID, ITEM_RADIO_BUTTON,
					   "Medium Checks", NULL);
      dialog_change_item (prefs_dlg, check_size_IDs[1], &button_set);

      button_set = (transparency_size == LARGE_CHECKS);
      check_size_IDs[2] = dialog_new_item (prefs_dlg, radio_ID, ITEM_RADIO_BUTTON,
					   "Large Checks", NULL);
      dialog_change_item (prefs_dlg, check_size_IDs[2], &button_set);

      dialog_show (prefs_dlg);
    }
}

void
file_quit_cmd_callback (GtkWidget *widget,
			gpointer client_data,
			gpointer call_data)
{
  app_exit (0);
}

void
edit_cut_cmd_callback (GtkWidget *widget,
		       gpointer client_data,
		       gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  global_edit_cut (gdisp);
}

void
edit_copy_cmd_callback (GtkWidget *widget,
			gpointer client_data,
			gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  global_edit_copy (gdisp);
}

void
edit_paste_cmd_callback (GtkWidget *widget,
			 gpointer client_data,
			 gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  global_edit_paste (gdisp, 0);
}

void
edit_paste_into_cmd_callback (GtkWidget *widget,
			      gpointer client_data,
			      gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  global_edit_paste (gdisp, 1);
}

void
edit_clear_cmd_callback (GtkWidget *widget,
			 gpointer client_data,
			 gpointer call_data)
{
  GDisplay * gdisp;
  GImage * gimage;
  TempBuf *buf;
  PixelRegion bufPR;
  int x1, y1, x2, y2;
  unsigned char col[MAX_CHANNELS];

  gdisp = gdisplay_active ();
  gimage = gdisp->gimage;

  gimage_get_background (gimage, col);
  if (gimage_has_alpha (gimage))
      col [gimage_bytes(gimage) - 1] = OPAQUE;

  gimage_mask_bounds (gimage, &x1, &y1, &x2, &y2);

  buf = temp_buf_new ((x2 - x1), (y2 - y1),
		      gimage_bytes (gimage), 0, 0, col);

  bufPR.bytes = buf->bytes;
  bufPR.x = x1;
  bufPR.y = y1;
  bufPR.w = buf->width;
  bufPR.h = buf->height;
  bufPR.rowstride = buf->width * bufPR.bytes;
  bufPR.data = temp_buf_data (buf);

  gimage_apply_image (gimage, &bufPR, 1, OPAQUE, ERASE_MODE);

  /*  update the image  */
  gimage_update (gimage, bufPR.x, bufPR.y, bufPR.w, bufPR.h);
  gdisplays_flush ();

  /*  free the temporary buffer  */
  temp_buf_free (buf);
}

void
edit_undo_cmd_callback (GtkWidget *widget,
			gpointer client_data,
			gpointer call_data)
{
  undo_pop ();
  /*if (!undo_pop ())
    XBell (DISPLAY, 0);*/
}

void
edit_redo_cmd_callback (GtkWidget *widget,
			gpointer client_data,
			gpointer call_data)
{
  undo_redo ();
  /*if (!undo_redo ())
    XBell (DISPLAY, 0);*/
}

void
edit_named_cut_cmd_callback (GtkWidget *widget,
			     gpointer client_data,
			     gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  named_edit_cut (gdisp);
}

void
edit_named_copy_cmd_callback (GtkWidget *widget,
			      gpointer client_data,
			      gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  named_edit_copy (gdisp);
}

void
edit_named_paste_cmd_callback (GtkWidget *widget,
			       gpointer client_data,
			       gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  named_edit_paste (gdisp);
}

void
select_toggle_cmd_callback (GtkWidget *widget,
			    gpointer client_data,
			    gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  selection_hide (gdisp->select, (void *) gdisp);
  gdisplays_flush ();
}

void
select_invert_cmd_callback (GtkWidget *widget,
			    gpointer client_data,
			    gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  gimage_mask_invert (gdisp->gimage);
}

void
select_all_cmd_callback (GtkWidget *widget,
			 gpointer client_data,
			 gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  gimage_mask_all (gdisp->gimage);
}

void
select_none_cmd_callback (GtkWidget *widget,
			  gpointer client_data,
			  gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  gimage_mask_none (gdisp->gimage);
}

void
select_float_cmd_callback (GtkWidget *widget,
			   gpointer client_data,
			   gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  (void) gimage_mask_float (gdisp->gimage);

  gdisplays_selection_visibility (gdisp->gimage->ID, 1);
  gdisplays_flush ();
}

void
select_anchor_cmd_callback (GtkWidget *widget,
			    gpointer client_data,
			    gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  gimage_mask_anchor (gdisp->gimage);

  gdisplays_selection_visibility (gdisp->gimage->ID, 1);
  gdisplays_flush ();
}

void
select_sharpen_cmd_callback (GtkWidget *widget,
			     gpointer client_data,
			     gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  gimage_mask_sharpen (gdisp->gimage);
}

void
select_border_cmd_callback (GtkWidget *widget,
			    gpointer client_data,
			    gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  gimage_mask_border (gdisp->gimage);
}

void
select_feather_cmd_callback (GtkWidget *widget,
			     gpointer client_data,
			     gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  gimage_mask_feather (gdisp->gimage);
}

void
select_by_color_cmd_callback (GtkWidget *widget,
			      gpointer client_data,
			      gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();
  tools_select (BY_COLOR_SELECT);
  by_color_select_initialize ((void *) gdisp->gimage);
}

void
view_zoomin_cmd_callback (GtkWidget *widget,
			  gpointer client_data,
			  gpointer call_data)
{
  GDisplay *gdisp;
  
  gdisp = gdisplay_active ();

  change_scale (gdisp, ZOOMIN);
}

void
view_zoomout_cmd_callback (GtkWidget *widget,
			   gpointer client_data,
			   gpointer call_data)
{
  GDisplay *gdisp;
  
  gdisp = gdisplay_active ();

  change_scale (gdisp, ZOOMOUT);
}

void
view_window_info_cmd_callback (GtkWidget *widget,
			       gpointer client_data,
			       gpointer call_data)
{
  GDisplay * gdisp;

  gdisp = gdisplay_active ();

  if (! gdisp->window_info_dialog)
    gdisp->window_info_dialog = info_window_create ((void *) gdisp);

  info_dialog_popup (gdisp->window_info_dialog);
}

void
view_new_view_cmd_callback (GtkWidget *widget,
			    gpointer client_data,
			    gpointer call_data)
{
  GDisplay *gdisp;

  gdisp = gdisplay_active ();

  gdisplay_new_view (gdisp);
}

void
view_shrink_wrap_cmd_callback (GtkWidget *widget,
			       gpointer client_data,
			       gpointer call_data)
{
  GDisplay *gdisp;
  
  gdisp = gdisplay_active ();

  if (gdisp)
    resize_display (gdisp);
}

void
view_close_window_cmd_callback (GtkWidget *widget,
				gpointer client_data,
				gpointer call_data)
{
  GDisplay *gdisp;

  gdisp = gdisplay_active ();

  gdisplay_close_window (gdisp);
}

void
tools_select_cmd_callback (GtkWidget *widget,
			   gpointer client_data,
			   gpointer call_data)
{
  /*  Activate the approriate widget  */
  gtk_widget_activate (tool_widgets[tool_info[(int) client_data].toolbar_position]);
}

void
dialogs_brushes_cmd_callback (GtkWidget *widget,
			      gpointer client_data,
			      gpointer call_data)
{
  create_brush_dialog ();
}

void
dialogs_patterns_cmd_callback (GtkWidget *widget,
			       gpointer client_data,
			       gpointer call_data)
{
  create_pattern_dialog ();
}

void
dialogs_palette_cmd_callback (GtkWidget *widget,
			      gpointer client_data,
			      gpointer call_data)
{
  palette_create ();
}

void
dialogs_layers_cmd_callback (GtkWidget *widget,
			      gpointer client_data,
			      gpointer call_data)
{
  layers_dialog_create ();
}

void
dialogs_tools_options_cmd_callback (GtkWidget *widget,
				    gpointer client_data,
				    gpointer call_data)
{
  tools_options_dialog_show ();
}


/****************************************************/
/**           LOCAL FUNCTIONS                      **/
/****************************************************/


static void
new_image_callback (dialog_ID, item_ID, client_data, call_data)
     int dialog_ID, item_ID;
     void *client_data, *call_data;
{
  NewImageValues *vals;
  GImage *gimage;
  GDisplay *gdisplay;
  int type;

  vals = client_data;
  switch (item_ID)
    {
    case OK_ID:
      dialog_close (vals->dlg);

      if ((vals->width > 0) && (vals->height > 0))
	{
	  /*  update the last dimensions used  */
	  last_width = vals->width;
	  last_height = vals->height;
	  last_type = vals->type;
	  last_fill_type = vals->fill_type;

	  switch (vals->fill_type)
	    {
	    case BACKGROUND_FILL:
	    case WHITE_FILL:
	      type = vals->type;
	      break;
	    case TRANSPARENT_FILL:
	      type = (vals->type == RGB_GIMAGE) ? RGBA_GIMAGE : GRAYA_GIMAGE;
	      break;
	    }

	  gimage = gimage_new (vals->width, vals->height, type);
	  gimage->dirty = 0;

	  gimage_fill (gimage, vals->fill_type);
	  gdisplay = gdisplay_new (gimage, 0x0101);
	}
			   
      xfree (vals);
      break;
    case CANCEL_ID:
      dialog_close (vals->dlg);
      xfree (vals);
      break;
    default:
      if (item_ID == vals->width_ID)
	{
	  vals->width = atoi (call_data);
	}
      else if (item_ID == vals->height_ID)
	{
	  vals->height = atoi (call_data);
	}
      else if (item_ID == vals->rgb_ID)
	{
	  if (*((long*) call_data))
	    vals->type = RGB_GIMAGE;
	}
      else if (item_ID == vals->gray_ID)
	{
	  if (*((long*) call_data))
	    vals->type = GRAY_GIMAGE;
	}
      else if (item_ID == vals->background_ID)
	{
	  if (*((long*) call_data))
	    vals->fill_type = BACKGROUND_FILL;
	}
      else if (item_ID == vals->white_ID)
	{
	  if (*((long*) call_data))
	    vals->fill_type = WHITE_FILL;
	}
      else if (item_ID == vals->transparent_ID)
	{
	  if (*((long*) call_data))
	    vals->fill_type = TRANSPARENT_FILL;
	}

      break;
    }
}


static void
preferences_callback   (dialog_ID, item_ID, client_data, call_data)
     int dialog_ID, item_ID;
     void *client_data, *call_data;
{
  int new_trans = transparency_type;
  int new_size = transparency_size;

  switch (item_ID)
    {
    case OK_ID:
      dialog_close (prefs_dlg);
      prefs_dlg = NULL;
      break;

    case CANCEL_ID:
      dialog_close (prefs_dlg);
      prefs_dlg = NULL;
      levels_of_undo = undo_levels_value;

      /*  If the transparency type changed, update all displays  */
      if ((transparency_type != last_transparency_type) ||
	  (transparency_size != last_check_size))
	{
	  transparency_type = last_transparency_type;
	  transparency_size = last_check_size;
	  render_setup (transparency_type, transparency_size);
	  gdisplays_expose_full ();
	  gdisplays_flush ();
	}
      break;

    default:
      if (item_ID == undo_levels_ID)
	levels_of_undo = atoi ((char *) call_data);
      else if (item_ID == trans_type_IDs[0] && *((long*) call_data))
	new_trans = LIGHT_CHECKS;
      else if (item_ID == trans_type_IDs[1] && *((long*) call_data))
	new_trans = GRAY_CHECKS;
      else if (item_ID == trans_type_IDs[2] && *((long*) call_data))
	new_trans = DARK_CHECKS;
      else if (item_ID == trans_type_IDs[3] && *((long*) call_data))
	new_trans = WHITE_ONLY;
      else if (item_ID == trans_type_IDs[4] && *((long*) call_data))
	new_trans = GRAY_ONLY;
      else if (item_ID == trans_type_IDs[5] && *((long*) call_data))
	new_trans = BLACK_ONLY;
      else if (item_ID == check_size_IDs[0] && *((long*) call_data))
	new_size = SMALL_CHECKS;
      else if (item_ID == check_size_IDs[1] && *((long*) call_data))
	new_size = MEDIUM_CHECKS;
      else if (item_ID == check_size_IDs[2] && *((long*) call_data))
	new_size = LARGE_CHECKS;

      /*  If the transparency type changed, update all displays  */
      if ((transparency_type != new_trans) ||
	  (transparency_size != new_size))
	{
	  transparency_type = new_trans;
	  transparency_size = new_size;
	  render_setup (transparency_type, transparency_size);
	  gdisplays_expose_full ();
	  gdisplays_flush ();
	}
      break;
    }

}
