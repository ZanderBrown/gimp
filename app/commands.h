#ifndef __COMMANDS_H__
#define __COMMANDS_H__


#include "gtk.h"

void file_new_cmd_callback (GtkWidget *, gpointer, gpointer);
void file_open_cmd_callback (GtkWidget *, gpointer, gpointer);
void file_save_cmd_callback (GtkWidget *, gpointer, gpointer);
void file_pref_cmd_callback (GtkWidget *, gpointer, gpointer);
void file_quit_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_cut_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_copy_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_paste_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_paste_into_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_clear_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_undo_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_redo_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_named_cut_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_named_copy_cmd_callback (GtkWidget *, gpointer, gpointer);
void edit_named_paste_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_toggle_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_invert_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_all_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_none_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_float_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_anchor_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_sharpen_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_border_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_feather_cmd_callback (GtkWidget *, gpointer, gpointer);
void select_by_color_cmd_callback (GtkWidget *, gpointer, gpointer);
void view_zoomin_cmd_callback (GtkWidget *, gpointer, gpointer);
void view_zoomout_cmd_callback (GtkWidget *, gpointer, gpointer);
void view_window_info_cmd_callback (GtkWidget *, gpointer, gpointer);
void view_new_view_cmd_callback (GtkWidget *, gpointer, gpointer);
void view_shrink_wrap_cmd_callback (GtkWidget *, gpointer, gpointer);
void view_close_window_cmd_callback (GtkWidget *, gpointer, gpointer);
void tools_select_cmd_callback (GtkWidget *, gpointer, gpointer);
void dialogs_brushes_cmd_callback (GtkWidget *, gpointer, gpointer);
void dialogs_patterns_cmd_callback (GtkWidget *, gpointer, gpointer);
void dialogs_palette_cmd_callback (GtkWidget *, gpointer, gpointer);
void dialogs_layers_cmd_callback (GtkWidget *, gpointer, gpointer);
void dialogs_tools_options_cmd_callback (GtkWidget *, gpointer, gpointer);

#endif /* __COMMANDS_H__ */
