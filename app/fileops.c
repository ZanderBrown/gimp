/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <sys/wait.h>

#include "appenv.h"
#include "actionarea.h"
#include "errors.h"
#include "fileops.h"
#include "gdisplay.h"
#include "general.h"
#include "interface.h"
#include "linked.h"
#include "plug_in.h"
#include "buildmenu.h"
#include "general.h"


/*  for file error messages  */
extern int errno;

typedef struct _FileFilter _FileFilter, *FileFilter;
typedef struct _FileData _FileData, *FileData;
typedef struct _FileOptions _FileOptions, FileOptions;

struct _FileFilter {
  link_ptr extensions;
  char *prog;
  char *title;
};

struct _FileData {
  char *filename;
  char *prog;
};

struct _FileOptions {
  char *str1;
  long ID;
};


/*  Forward function declarations  */

static void file_open_ok_callback       (GtkWidget *, gpointer, gpointer);
static void file_save_ok_callback       (GtkWidget *, gpointer, gpointer);
static void file_cancel_callback        (GtkWidget *, gpointer, gpointer);
static void file_filter_open_callback   (GtkWidget *, gpointer, gpointer);
static void file_filter_save_callback   (GtkWidget *, gpointer, gpointer);

static char* find_file_filter           (char *);
static void call_file_filter            (char *, char *, long, void *);

static char* get_extension              (char *);

static void file_overwrite              (char *);
static void file_overwrite_yes_callback (GtkWidget *, gpointer, gpointer);
static void file_overwrite_no_callback  (GtkWidget *, gpointer, gpointer);

static void file_add                    (char *, char *);
static void file_plug_in_callback       (void *, void *);


/*  Some variables  */

static link_ptr file_filters = NULL;
static MenuItem *file_items;
static int num_file_items;

static FileOptions *save_options;
static FileOptions *open_options;

static link_ptr file_list = NULL;
static int file_is_opening = 0;


void
file_io_init ()
{
  FileFilter ff;
  link_ptr tmp;
  long n;

  n = 3;
  tmp = file_filters;
  do {
    n++;
    tmp = next_item (tmp);
  } while (tmp);

  file_items = (MenuItem *) xmalloc (sizeof (MenuItem) * n);
  num_file_items = n - 1;

  file_items[0].label = "extension";
  file_items[0].accelerator_key = 0;
  file_items[0].accelerator_mods = 0;
  file_items[0].callback = file_filter_open_callback;
  file_items[0].user_data = NULL;
  file_items[0].subitems = NULL;

  file_items[1].label = "-";
  file_items[1].accelerator_key = 0;
  file_items[1].accelerator_mods = 0;
  file_items[1].callback = NULL;
  file_items[1].user_data = NULL;
  file_items[1].subitems = NULL;

  n = 2;
  tmp = file_filters;
  while (tmp)
    {
      ff = tmp->data;
      file_items[n].label = ff->title;
      file_items[n].accelerator_key = 0;
      file_items[n].accelerator_mods = 0;
      file_items[n].callback = file_filter_open_callback;
      file_items[n].user_data = ff->prog;
      file_items[n].subitems = NULL;

      tmp = next_item (tmp);
      n++;
    }

  file_items[n].label = NULL;
}

void
add_file_filter (ext, prog, types, title)
     char *ext, *prog, *types, *title;
{
  FileFilter ff;
  FileFilter ff2;
  link_ptr last, tmp, new_link;
  char *t, *p;

  ff = xmalloc (sizeof (_FileFilter));
  ff->extensions = NULL;
  ff->prog = xstrdup (prog);
  ff->title = xstrdup (title);

  t = strtok (ext, ",");
  while (t)
    {
      p = xstrdup (t);
      t = p;
      while (*t)
	{
	  *t = tolower (*t);
	  t++;
	}
      ff->extensions = add_to_list (ff->extensions, p);
      t = strtok (NULL, ",");
    }

  /* create a sorted list of file filters */
  last = NULL;

  if (file_filters)
    {
      tmp = file_filters;
      while (tmp)
	{
	  ff2 = tmp->data;
	  if (strcmp (ff->title, ff2->title) < 0)
	    {
	      new_link = alloc_list ();
	      if (!new_link)
		fatal_error ("Unable to allocate memory");

	      new_link->data = ff;
	      new_link->next = tmp;
	      if (!last)
		file_filters = new_link;
	      else
		last->next = new_link;

	      if (tmp == file_filters)
		file_filters = new_link;
	      break;
	    }
	  last = tmp;
	  tmp = next_item (tmp);
	}

      if (!tmp)
	file_filters = append_to_list (file_filters, ff);
    }
  else
    file_filters = add_to_list (file_filters, ff);
}

static void
file_items_set_callbacks (callback)
     GtkCallback callback;
{
  int i;

  for (i = 0; i < num_file_items; i++)
    file_items[i].callback = callback;
}

static void*
file_add_options (filesel, type)
     GtkWidget *filesel;
     int type;
{
  GtkWidget *label, *frame, *hbox, *option_menu, *menu;
  FileOptions *value;

  frame = gtk_frame_new ("Options");
  gtk_frame_set_type (frame, GTK_SHADOW_ETCHED_IN);
  gtk_box_pack (gtk_file_selection_get_main_vbox (filesel),
		frame, FALSE, FALSE, 0, GTK_PACK_START);

  hbox = gtk_hbox_new (FALSE, 2);
  gtk_container_add (frame, hbox);
  gtk_container_set_border_width (hbox, 5);

  label = gtk_label_new ("Determine file plug-in by:");
  gtk_box_pack (hbox, label, FALSE, FALSE, 2, GTK_PACK_START);
  gtk_widget_show (label);

  option_menu = gtk_option_menu_new ();
  gtk_box_pack (hbox, option_menu, TRUE, TRUE, 2, GTK_PACK_START);
  gtk_widget_show (option_menu);

  switch (type)
    {
    case LOAD:
      file_items_set_callbacks (file_filter_open_callback);
      break;
    case SAVE:
      file_items_set_callbacks (file_filter_save_callback);
      break;
    }
  menu = build_menu (file_items, NULL);
  gtk_option_menu_set_menu (option_menu, menu);

  gtk_widget_show (hbox);
  gtk_widget_show (frame);

  value = xmalloc (sizeof (FileOptions));
  value->str1 = NULL;
  value->ID = 0;

  gtk_widget_set_user_data (menu, value);

  return value;
}

void
file_open_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  static GtkWidget *filesel = NULL;

  if (!filesel)
    {
      filesel = gtk_file_selection_new ("Open Image", NULL);

      open_options = file_add_options (filesel, LOAD);

      gtk_file_selection_set_ok_callback (filesel, file_open_ok_callback, open_options);
      gtk_file_selection_set_cancel_callback (filesel, file_cancel_callback, open_options);

      map_dialog (filesel);
    }

  if (! GTK_WIDGET_VISIBLE (filesel))
    gtk_widget_show (filesel);
}

void
file_save_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  static GtkWidget *filesel = NULL;
  GDisplay *gdisplay;

  gdisplay = gdisplay_active ();

  if (!filesel)
    {
      filesel = gtk_file_selection_new ("Save Image", NULL);

      save_options = file_add_options (filesel, SAVE);
      save_options->ID = gdisplay->gimage->ID;

      gtk_file_selection_set_ok_callback (filesel, file_save_ok_callback, save_options);
      gtk_file_selection_set_cancel_callback (filesel, file_cancel_callback, save_options);

      map_dialog (filesel);
    }

  if (! GTK_WIDGET_VISIBLE (filesel))
    gtk_widget_show (filesel);
}


int
file_open (filename, prog)
     char *filename, *prog;
{
  struct stat buf;
  int err;

  err = stat (filename, &buf);
  if (!err)
    {
      if (buf.st_mode & S_IFDIR)
	{
	  warning ("\"%s\" is a directory", filename);
	  return 0;
	}
    }
  else
    {
      if (errno == ENOENT)
	warning ("\"%s\" does not exist", filename);
      else
	warning ("unknown error");
      return 0;
    }

  file_add (filename, prog);

  return 1;
}

int
file_save (ID, filename, prog)
     long ID;
     char *filename, *prog;
{
  GImage *gimage;
  char *basename;
  struct stat buf;
  int err;

  err = stat (filename, &buf);
  if (!err)
    {
      if (buf.st_mode & S_IFDIR)
	{
	  warning ("\"%s\" is a directory (cannot overwrite)", filename);
	  return 0;
	}
    }

  basename = prune_filename (filename);
  if (basename && basename[0])
    {
      if (!prog)
	prog = find_file_filter (get_extension (filename));
      if (!prog)
	{
	  warning ("Unable to find file filter for: \"%s\"", filename);
	  return 0;
	}
      else
	{
	  gimage = gimage_get_ID (ID);
	  gimage->has_filename = 1;
	  if (gimage->filename)
	    xfree (gimage->filename);
	  gimage->filename = xstrdup (filename);
	  gdisplay_update_title (ID);
	  gimage->dirty = 0;

	  call_file_filter (prog, filename, SAVE, gimage);
	  return 1;
	}
    }
  else
    return 1;
}

static void
file_open_ok_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  char *filename;
  FileOptions *value;

  filename = (char *) call_data;
  value = (FileOptions *) client_data;

  if (file_open (filename, value->str1))
    file_cancel_callback (w, client_data, call_data);	  /*  close the dialog  */
}

static void
file_save_ok_callback (widget, client_data, call_data)
     GtkWidget *widget;
     gpointer client_data;
     gpointer call_data;
{
  static char *last_filename;
  static GtkWidget *last_widget;
  char *filename;
  FileOptions *value;
  char * str;
  struct stat buf;
  int err;

  if (call_data)
    {
      filename = (char *) call_data;

      err = stat (filename, &buf);
      if (!err)
	{
	  if (buf.st_mode & S_IFDIR)
	    {
	      warning ("\"%s\" is a directory (cannot overwrite)", filename);
	      return;
	    }

	  last_filename = filename;
	  last_widget = widget;
	  str = (char *) xmalloc (18 + strlen (filename));
	  sprintf (str, "Overwrite file \"%s\"", filename);
	  file_overwrite (str);
	  xfree (str);
	}
      else if (errno == ENOENT)
	{
	  value = (FileOptions *) client_data;
	  if (file_save (value->ID, filename, value->str1))
	    file_cancel_callback (widget, NULL, NULL);
	}
    }
  else
    {
      value = (FileOptions *) client_data;
      if (file_save (value->ID, last_filename, value->str1))
	file_cancel_callback (last_widget, NULL, NULL);
    }
}


static void
file_cancel_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  gtk_widget_hide (w);
}

static void
file_filter_open_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  open_options->str1 = client_data;
}

static void
file_filter_save_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  save_options->str1 = client_data;
}

static char*
find_file_filter (ext)
     char *ext;
{
  FileFilter ff;
  link_ptr tmp, tmp2;
  char * buf, *t;

  buf = xstrdup (ext);
  t = buf;
  while (*t)
    {
      *t = tolower (*t);
      t++;
    }

  tmp = file_filters;
  while (tmp)
    {
      ff = tmp->data;
      tmp = next_item (tmp);

      tmp2 = ff->extensions;
      while (tmp2)
	{
	  if (strcmp (tmp2->data, buf) == 0)
	    {
	      xfree (buf);
	      return ff->prog;
	    }
	  tmp2 = tmp2->next;
	}
    }

  xfree (buf);
  return NULL;
}

static void
call_file_filter (prog, filename, msg, data)
     char *prog, *filename;
     long msg;
     void *data;
{
  PlugInP plug_in;

  plug_in = make_plug_in (prog, data, NULL);
  if (plug_in_open (plug_in))
    {
      if (msg == LOAD)
	plug_in_set_callback (plug_in, file_plug_in_callback, NULL);
      plug_in_send_message (plug_in, msg, strlen (filename) + 1, filename);
    }
  else
    free_plug_in (plug_in);
}

static char*
get_extension (file)
     char *file;
{
  char *period = file;

  while (*file)
    if (*file++ == '.')
      period = file;

  return period;
}

static void
file_overwrite (str)
     char *str;
{
  static ActionAreaItem mbox_action_items[2] =
  {
    { "Yes", file_overwrite_yes_callback, NULL, NULL },
    { "No", file_overwrite_no_callback, NULL, NULL }
  };
  GtkWidget *mbox;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *action_area;

  mbox = gtk_window_new ("Overwrite File?", GTK_WINDOW_DIALOG);
  vbox = gtk_vbox_new (FALSE, 10);
  gtk_container_add (mbox, vbox);
  label = gtk_label_new (str);
  gtk_box_pack (vbox, label, TRUE, FALSE, 0, GTK_PACK_START);

  mbox_action_items[0].user_data = mbox;
  mbox_action_items[1].user_data = mbox;
  action_area = build_action_area (mbox_action_items, 2);
  gtk_box_pack (vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);

  gtk_widget_show (label);
  gtk_widget_show (action_area);
  gtk_widget_show (vbox);
  map_dialog (mbox);
}

static void
file_overwrite_yes_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  GtkWidget *mbox;

  mbox = (GtkWidget *) client_data;
  file_save_ok_callback (NULL, (gpointer) save_options, (gpointer) 0);
  gtk_widget_destroy (mbox);
}

static void
file_overwrite_no_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  GtkWidget *mbox;

  mbox = (GtkWidget *) client_data;
  gtk_widget_destroy (mbox);
}

static void
file_add (filename, prog)
     char *filename, *prog;
{
  FileData data;

  data = xmalloc (sizeof (_FileData));

  data->filename = xstrdup (filename);
  if (prog)
    data->prog = xstrdup (prog);
  else
    data->prog = NULL;

  file_list = append_to_list (file_list, data);

  if (!file_is_opening)
    file_plug_in_callback (NULL, 0);
}

static void
file_plug_in_callback (client_data, call_data)
     void *client_data;
     void *call_data;
{
  link_ptr tmp_link;
  FileData filedata;
  char *prog, *basename;
  char *ext;

  if ((int) call_data == 2)
    {
      tmp_link = file_list;
      while (tmp_link)
	{
	  filedata = tmp_link->data;
	  tmp_link = tmp_link->next;

	  if (filedata->filename)
	    xfree (filedata->filename);
	  if (filedata->prog)
	    xfree (filedata->prog);
	  xfree (filedata);
	}

      file_list = free_list (file_list);
    }

  if (file_list)
    {
      tmp_link = file_list;
      file_list = file_list->next;

      filedata = tmp_link->data;
      tmp_link->next = NULL;
      free_list (tmp_link);

      basename = prune_filename (filedata->filename);
      if (basename && basename[0])
	{
	  if (filedata->prog)
	    {
	      prog = filedata->prog;
	    }
	  else
	    {
	      ext = get_extension (basename);
	      prog = find_file_filter (ext);
	    }

	  if (!prog)
	    {
	      warning ("Unable to find file filter for: \"%s\"", filedata->filename);
	      if (filedata->filename)
		xfree (filedata->filename);
	      if (filedata->prog)
		xfree (filedata->prog);
	      xfree (filedata);

	      file_plug_in_callback (client_data, call_data);
	      return;
	    }
	  else
	    {
	      call_file_filter (prog, filedata->filename, LOAD, NULL);

	      if (filedata->filename)
		xfree (filedata->filename);
	      if (filedata->prog)
		xfree (filedata->prog);
	      xfree (filedata);

	      file_is_opening = 1;
	    }
	}
    }
  else
    file_is_opening = 0;
}
