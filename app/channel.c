/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "appenv.h"
#include "channel.h"
#include "errors.h"
#include "linked.h"
#include "paint_funcs.h"
#include "temp_buf.h"
#include "undo.h"

/*  static functions  */

static void channel_allocate (Channel *, int);

/*
 *  Static variables
 */
static int global_channel_ID = 1;
static link_ptr channel_list = NULL;


/********************************/
/*  Local function definitions  */

static void
channel_allocate (channel, size)
     Channel * channel;
     int size;
{
  channel->shmid = shmget (IPC_PRIVATE,
			   size,
			   IPC_CREAT|0777);
  
  if (channel->shmid < 0)
    {
      perror ("shmget failed");
      fatal_error ("shmget failed!");
    }

  channel->shmaddr = channel->data = 
    (unsigned char *) shmat (channel->shmid, 0, 0);

  if (channel->shmaddr < (void*) 0)
    fatal_error ("shmat failed!");
}

/**************************/
/*  Function definitions  */


Channel *
channel_new (gimage_ID, width, height, name, opacity, col)
     int gimage_ID;
     int width, height;
     char * name;
     int opacity;
     unsigned char * col;
{
  Channel * channel;
  int i;

  channel = (Channel *) xmalloc (sizeof (Channel));

  if (!name)
    name = "unnamed";

  channel->name = (char *) xmalloc (strlen (name) + 1);
  strcpy (channel->name, name);

  /*  set size information  */
  channel->width = width;
  channel->height = height;
  channel->bytes = 1;

  /*  allocate the memory for this channel  (shm)  */
  channel_allocate (channel, width * height);
  channel->visible = 1;

  /*  set the channel color and opacity  */
  for (i = 0; i < 3; i++)
    channel->col[i] = col[i];
  channel->opacity = opacity;

  /*  give this channel an ID  */
  channel->ID = global_channel_ID++;
  channel->gimage_ID = gimage_ID;

  /*  add the new channel to the global list  */
  channel_list = append_to_list (channel_list, (void *) channel);

  return channel;
}


Channel *
channel_copy (channel)
     Channel * channel;
{
  char * channel_name;
  Channel * new_channel;

  /*  formulate the new channel name  */
  channel_name = (char *) xmalloc (strlen (channel->name) + 6);
  sprintf (channel_name, "%s copy", channel->name);

  /*  allocate a new channel object  */
  new_channel = channel_new (channel->gimage_ID, channel->width, channel->height, channel_name,
			     channel->opacity, channel->col);

  /*  copy the contents across channels  */
  memcpy (new_channel->data, channel->data, channel->width * channel->height);

  /*  free up the channel_name memory  */
  xfree (channel_name);

  return new_channel;
}


Channel *
channel_get_ID (ID)
     int ID;
{
  link_ptr tmp = channel_list;
  Channel * channel;

  while (tmp)
    {
      channel = (Channel *) tmp->data;
      if (channel->ID == ID)
	return channel;
      
      tmp = next_item (tmp);
    }

  return NULL;
}


void
channel_delete (channel)
     Channel * channel;
{
  /*  remove this image from the global list  */
  channel_list = remove_from_list (channel_list, (void *) channel);

  /*  free the shared memory  */
  if (channel->shmaddr >= (void*) 0)
    {
      shmdt (channel->shmaddr);
      shmctl (channel->shmid, IPC_RMID, 0);
    }

  /*  free the channel name buffer  */
  xfree (channel->name);

  xfree (channel);
}


void
channel_apply_image (channel, x1, y1, x2, y2, buf)
     Channel * channel;
     int x1, y1, x2, y2;
     TempBuf * buf;
{
  /*  Need to push an undo operation  */
  if (! buf)
    undo_push_image (channel->gimage_ID, -1, channel->ID, x1, y1, x2, y2);
  else
    undo_push_image_mod (channel->gimage_ID, -1, channel->ID, buf);
}


/********************/
/* access functions */

unsigned char *
channel_data (channel)
     Channel * channel;
{
  return channel->data;
}


int
channel_toggle_visibility (channel)
     Channel * channel;
{
  channel->visible = !channel->visible;

  return channel->visible;
}
