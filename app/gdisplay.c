/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include "appenv.h"
#include "linked.h"
#include "colormaps.h"
#include "cursorutil.h"
#include "disp_callbacks.h"
#include "gdisplay.h"
#include "gdisplayP.h"
#include "gdisplay_ops.h"
#include "general.h"
#include "gimage_mask.h"
#include "gimprc.h"
#include "gximage.h"
#include "layers_dialog.h"
#include "image_render.h"
#include "info_window.h"
#include "interface.h"
#include "scale.h"
#include "scroll.h"
#include "tools.h"
#include "undo.h"
#include "visual.h"


#define SELECTION_OFF     0
#define SELECTION_ON      1
#define SELECTION_PAUSE   2
#define SELECTION_RESUME  3

#define OVERHEAD          25  /*  in units of pixel area  */

/* variable declarations */
link_ptr               display_list = NULL;
static int             display_num  = 1;
static GdkCursorType   current_tool_cursor = GDK_LEFT_ARROW;


#define ROUND(x) ((int) (x + 0.5))

#define MAX_TITLE_BUF 4096

static char *image_type_strs[] = 
{ 
  "RGB",
  "RGB-alpha",
  "grayscale",
  "grayscale-alpha",
  "indexed",
  "indexed-alpha"
};


/*  Local functions  */
static void       gdisplay_delete           (GDisplay *);
static link_ptr   gdisplay_free_area_list   (link_ptr);
static link_ptr   gdisplay_process_area_list(link_ptr, GArea *);
static void       gdisplay_flush            (GDisplay *);
static void       gdisplay_add_update_area  (GDisplay *, int, int, int, int);
static void       gdisplay_add_display_area (GDisplay *, int, int, int, int);
static void       gdisplay_paint_area       (GDisplay *, int, int, int, int);
static void       gdisplay_display_area     (GDisplay *, int, int, int, int);


GDisplay*
gdisplay_new (gimage, scale)
     GImage *gimage;
     unsigned int scale;
{
  GDisplay *gdisp;
  GdkVisual *visual;
  char title [MAX_TITLE_BUF];
  int instance;

  /* format the title */
  sprintf (title, "%s-%d.%d (%s)",
	   prune_filename (gimage->filename),
	   gimage->ID, gimage->instance_count,
	   image_type_strs[gimage->type]);

  instance = gimage->instance_count;
  gimage->instance_count++;
  gimage->ref_count++;
  
  visual = (gimage_gray (gimage)) ? gray_visual : color_visual;

  /*
   *  Set all GDisplay parameters...
   */
  gdisp = (GDisplay *) xmalloc (sizeof (GDisplay));

  gdisp->offset_x = gdisp->offset_y = 0;
  gdisp->scale = scale;
  gdisp->gimage = gimage;
  gdisp->window_info_dialog = NULL;
  gdisp->depth = visual->depth;
  gdisp->select = NULL;
  gdisp->ID = display_num++;
  gdisp->instance = instance;
  gdisp->update_areas = NULL;
  gdisp->display_areas = NULL;

  create_display_shell (&gdisp->shell, &gdisp->canvas,
			&gdisp->hsb, &gdisp->vsb,
			&gdisp->hrule, &gdisp->vrule,
			&gdisp->popup,
			&gdisp->hsbdata, &gdisp->vsbdata,
			&gdisp->hruledata, &gdisp->vruledata,
			gdisplay_canvas_events, CANVAS_EVENT_MASK,
			gimage->width, gimage->height,
			&scale, title, gimage->type);

  /*  set the gdisplay colormap type and install the appropriate colormap  */
  gdisp->color_type = (gimage_gray (gimage)) ? GRAY : RGB;
  /*install_colormap (gdisp->color_type);*/

  /*  set the user data  */
  gtk_widget_set_user_data (gdisp->shell, (gpointer) gdisp);
  gtk_widget_set_user_data (gdisp->canvas, (gpointer) gdisp);

  /*  add the new display to the list so that it isn't lost  */
  display_list = append_to_list (display_list, (void *) gdisp);

  return gdisp;
}


static void
gdisplay_delete (gdisp)
     GDisplay *gdisp;
{
  /*  stop any active tool  */
  active_tool_control (HALT, (void *) gdisp);

  /*  free the selection structure  */
  selection_free (gdisp->select);

  /*  free the area lists  */
  gdisplay_free_area_list (gdisp->update_areas);
  gdisplay_free_area_list (gdisp->display_areas);

  /*  free the gimage  */
  gimage_delete (gdisp->gimage);

  /*  insure that if a window information dialog exists, it is removed  */
  if (gdisp->window_info_dialog)
    info_window_free (gdisp->window_info_dialog);

  xfree (gdisp);
}


static link_ptr
gdisplay_free_area_list (list)
     link_ptr list;
{
  link_ptr l = list;
  GArea *ga;

  while (l)
    {
      /*  free the data  */
      ga = (GArea *) l->data;
      xfree (ga);

      l = next_item (l);
    }

  if (list)
    free_list (list);

  return NULL;
}


static link_ptr
gdisplay_process_area_list (list, ga1)
     link_ptr list;
     GArea * ga1;
{
  link_ptr new_list;
  link_ptr l = list;
  int area1, area2, area3;
  GArea *ga2;

  /*  start new list off  */
  new_list = add_to_list (NULL, ga1);
  while (l)
    {
      /*  process the data  */
      ga2 = (GArea *) l->data;
      area1 = (ga1->x2 - ga1->x1) * (ga1->y2 - ga1->y1) + OVERHEAD;
      area2 = (ga2->x2 - ga2->x1) * (ga2->y2 - ga2->y1) + OVERHEAD;
      area3 = (MAXIMUM (ga2->x2, ga1->x2) - MINIMUM (ga2->x1, ga1->x1)) *
	(MAXIMUM (ga2->y2, ga1->y2) - MINIMUM (ga2->y1, ga1->y1)) + OVERHEAD;

      if ((area1 + area2) < area3)
	new_list = add_to_list (new_list, ga2);
      else
	{
	  ga1->x1 = MINIMUM (ga1->x1, ga2->x1);
	  ga1->y1 = MINIMUM (ga1->y1, ga2->y1);
	  ga1->x2 = MAXIMUM (ga1->x2, ga2->x2);
	  ga1->y2 = MAXIMUM (ga1->y2, ga2->y2);

	  xfree (ga2);
	}

      l = next_item (l);
    }

  if (list)
    free_list (list);

  return new_list;
}


static void
gdisplay_flush (gdisp)
     GDisplay *gdisp;
{
  GArea * ga;
  link_ptr list;

  /*  Flush the items in the displays and updates lists  */
  
  /*  First the updates...  */
  list = gdisp->update_areas;
  while (list)
    {
      /*  Paint the area specified by the GArea  */
      ga = (GArea *) list->data;
      gdisplay_paint_area (gdisp, ga->x1, ga->y1,
			   (ga->x2 - ga->x1), (ga->y2 - ga->y1));

      list = next_item (list);
    }
  /*  Free the update lists  */
  gdisp->update_areas = gdisplay_free_area_list (gdisp->update_areas);


  /*  Next the displays...  */
  list = gdisp->display_areas;

  if (list)
    {
      /*  stop the currently active tool  */
      active_tool_control (PAUSE, (void *) gdisp);

      while (list)
	{
	  /*  Paint the area specified by the GArea  */
	  ga = (GArea *) list->data;
	  gdisplay_display_area (gdisp, ga->x1, ga->y1,
				 (ga->x2 - ga->x1), (ga->y2 - ga->y1));
	  
	  list = next_item (list);
	}
      /*  Free the update lists  */
      gdisp->display_areas = gdisplay_free_area_list (gdisp->display_areas);
      
      /* restart (and recalculate) the selection boundaries */
      selection_start (gdisp->select, TRUE);

      /* start the currently active tool */
      active_tool_control (RESUME, (void *) gdisp);
    }
}


void
gdisplay_remove_and_delete (gdisp)
     GDisplay *gdisp;
{
  /* remove the display from the list */
  display_list = remove_from_list (display_list, (void *) gdisp);
  gdisplay_delete (gdisp);

  /*  remove any instances of this gdisplay or the associated gimage
   *  from the undo stack.
   */
  undo_clean_stack ();
}


static void
gdisplay_add_update_area (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  GArea * ga;

  ga = (GArea *) xmalloc (sizeof (GArea));
  ga->x1 = BOUNDS (x, 0, gdisp->gimage->width);
  ga->y1 = BOUNDS (y, 0, gdisp->gimage->height);
  ga->x2 = BOUNDS (x + w, 0, gdisp->gimage->width);
  ga->y2 = BOUNDS (y + h, 0, gdisp->gimage->height);

  gdisp->update_areas = gdisplay_process_area_list (gdisp->update_areas, ga);
}


static void
gdisplay_add_display_area (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  GArea * ga;

  ga = (GArea *) xmalloc (sizeof (GArea));

  ga->x1 = BOUNDS (x, 0, gdisp->disp_width);
  ga->y1 = BOUNDS (y, 0, gdisp->disp_height);
  ga->x2 = BOUNDS (x + w, 0, gdisp->disp_width);
  ga->y2 = BOUNDS (y + h, 0, gdisp->disp_height);

  gdisp->display_areas = gdisplay_process_area_list (gdisp->display_areas, ga);
}


static void
gdisplay_paint_area (gdisp, x, y, w, h)
     GDisplay *gdisp;
     int x, y;
     int w, h;
{
  int x1, y1, x2, y2;

  /* construct the gimage data from the possibility of
   * multiple layers, layer masks, selection masks, arbitrary
   * masks, non-visible color channels, etc...
   * If the gimage type is not RGBA or GRAYA, we can ignore this step
   */
  gimage_construct (gdisp->gimage, x, y, w, h);

  /*  display the area  */
  gdisplay_transform_coords (gdisp, x, y, &x1, &y1, 0);
  gdisplay_transform_coords (gdisp, x + w, y + h, &x2, &y2, 0);
  gdisplay_expose_area (gdisp, x1, y1, (x2 - x1), (y2 - y1));
}


static void
gdisplay_display_area (gdisp, x, y, w, h)
     GDisplay *gdisp;
     int x, y;
     int w, h;
{
  int x2, y2;
  int dx, dy;
  int i, j;

  x2 = x + w;  y2 = y + h;

  /*  display the image in GXIMAGE_WIDTH x GXIMAGE_HEIGHT sized chunks */
  for (i = y; i < y2; i += GXIMAGE_HEIGHT)
    for (j = x; j < x2; j += GXIMAGE_WIDTH)
      {
	dx = (x2 - j < GXIMAGE_WIDTH) ? x2 - j : GXIMAGE_WIDTH;
	dy = (y2 - i < GXIMAGE_HEIGHT) ? y2 - i : GXIMAGE_HEIGHT;
	image_render (gdisp, j, i, dx, dy);
	put_gximage (gdisp->canvas->window, 
		     gdisp->gimage->type, 
		     j, i, dx, dy);
      }
}


int
gdisplay_mask_value (gdisp, x, y)
     GDisplay * gdisp;
     int x, y;
{
  /*  move the coordinates from screen space to image space  */
  gdisplay_untransform_coords (gdisp, x, y, &x, &y, FALSE, 1);

  return gimage_mask_value (gdisp->gimage, x, y);
}


int
gdisplay_mask_bounds (gdisp, x1, y1, x2, y2)
     GDisplay * gdisp;
     int * x1, * y1;
     int * x2, * y2;
{
  int x3, y3, x4, y4;

  if (! gimage_mask_bounds (gdisp->gimage, x1, y1, x2, y2))
    return FALSE;

  gdisplay_transform_coords (gdisp, *x1, *y1, x1, y1, 1);
  gdisplay_transform_coords (gdisp, *x2, *y2, x2, y2, 1);

  /*  Is there a second boundary?  */
  if (gimage_mask_2nd_bounds (gdisp->gimage, &x3, &y3, &x4, &y4))
    {
      float scale;

      /*  transform from gimp coordinates to screen coordinates  */
      scale = (SCALESRC (gdisp) == 1) ? SCALEDEST (gdisp) :
	1.0 / SCALESRC (gdisp);

      x3 = (int) (scale * x3 - gdisp->offset_x);
      y3 = (int) (scale * y3 - gdisp->offset_y);
      x4 = (int) (scale * x4 - gdisp->offset_x);
      y4 = (int) (scale * y4 - gdisp->offset_y);

      *x1 = MINIMUM (*x1, x3);
      *y1 = MINIMUM (*y1, y3);
      *x2 = MAXIMUM (*x2, x4);
      *y2 = MAXIMUM (*y2, y4);
    }

  /*  Make sure the extents are within bounds  */
  *x1 = BOUNDS (*x1, 0, gdisp->disp_width);
  *y1 = BOUNDS (*y1, 0, gdisp->disp_height);
  *x2 = BOUNDS (*x2, 0, gdisp->disp_width);
  *y2 = BOUNDS (*y2, 0, gdisp->disp_height);

  return TRUE;
}


void
gdisplay_transform_coords (gdisp, x, y, nx, ny, use_offsets)
     GDisplay * gdisp;
     int x, y;
     int *nx, *ny;
     int use_offsets;
{
  double scale;
  int offset_x, offset_y;

  /*  transform from gimp coordinates to screen coordinates  */
  scale = (SCALESRC (gdisp) == 1) ? SCALEDEST (gdisp) :
    1.0 / SCALESRC (gdisp);

  if (use_offsets)
    gimage_offsets (gdisp->gimage, &offset_x, &offset_y);
  else
    {
      offset_x = offset_y = 0;
    }

  *nx = (int) (scale * (x + offset_x) - gdisp->offset_x);
  *ny = (int) (scale * (y + offset_y) - gdisp->offset_y);
}


void
gdisplay_untransform_coords (gdisp, x, y, nx, ny, round, use_offsets)
     GDisplay * gdisp;
     int x, y;
     int *nx, *ny;
     int round;
     int use_offsets;
{
  double scale;
  int offset_x, offset_y;

  /*  transform from screen coordinates to gimp coordinates  */
  scale = (SCALESRC (gdisp) == 1) ? SCALEDEST (gdisp) :
    1.0 / SCALESRC (gdisp);

  if (use_offsets)
    gimage_offsets (gdisp->gimage, &offset_x, &offset_y);
  else
    {
      offset_x = offset_y = 0;
    }

  if (round)
    {
      *nx = ROUND ((x + gdisp->offset_x) / scale - offset_x);
      *ny = ROUND ((y + gdisp->offset_y) / scale - offset_y);
    }
  else
    {
      *nx = (int) ((x + gdisp->offset_x) / scale - offset_x);
      *ny = (int) ((y + gdisp->offset_y) / scale - offset_y);
    }
}


void
gdisplay_transform_coords_f (gdisp, x, y, nx, ny, use_offsets)
     GDisplay * gdisp;
     double x, y;
     double *nx, *ny;
     int use_offsets;
{
  double scale;
  int offset_x, offset_y;

  /*  transform from gimp coordinates to screen coordinates  */
  scale = (SCALESRC (gdisp) == 1) ? SCALEDEST (gdisp) :
    1.0 / SCALESRC (gdisp);

  if (use_offsets)
    gimage_offsets (gdisp->gimage, &offset_x, &offset_y);
  else
    {
      offset_x = offset_y = 0;
    }

  *nx = scale * (x + offset_x) - gdisp->offset_x;
  *ny = scale * (y + offset_y) - gdisp->offset_y;
}


void
gdisplay_untransform_coords_f (gdisp, x, y, nx, ny, use_offsets)
     GDisplay * gdisp;
     double x, y;
     double *nx, *ny;
     int use_offsets;
{
  double scale;
  int offset_x, offset_y;

  /*  transform from screen coordinates to gimp coordinates  */
  scale = (SCALESRC (gdisp) == 1) ? SCALEDEST (gdisp) :
    1.0 / SCALESRC (gdisp);

  if (use_offsets)
    gimage_offsets (gdisp->gimage, &offset_x, &offset_y);
  else
    {
      offset_x = offset_y = 0;
    }

  *nx = (x + gdisp->offset_x) / scale - offset_x;
  *ny = (y + gdisp->offset_y) / scale - offset_y;
}


void
gdisplay_expose_area (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  gdisplay_add_display_area (gdisp, x, y, w, h);
}


void
gdisplay_expose_full (gdisp)
     GDisplay * gdisp;
{
  gdisplay_add_display_area (gdisp, 0, 0,
			     gdisp->disp_width,
			     gdisp->disp_height);
}

/**************************************************/
/*  Functions independent of a specific gdisplay  */
/**************************************************/

GDisplay *
gdisplay_active ()
{
  GtkWidget *event_widget;
  GtkWidget *toplevel_widget;
  GdkEvent event;
  GDisplay *gdisp;

  /*  If the popup shell is valid, then get the gdisplay associated with that shell  */
  if (popup_shell)
    {
      gdisp = (GDisplay *) gtk_widget_get_user_data (popup_shell);
      popup_shell = NULL;
      return gdisp;
    }
  else
    {
      gtk_get_current_event (&event);
      event_widget = gtk_get_event_widget (&event);
      toplevel_widget = gtk_widget_get_toplevel (event_widget);
      return (GDisplay *) gtk_widget_get_user_data (toplevel_widget);
    }
}


GDisplay *
gdisplay_get_ID (ID)
     int ID;
{
  GDisplay *gdisp;
  link_ptr list = display_list;

  /*  Traverse the list of displays, returning the one that matches the ID  */
  /*  If no display in the list is a match, return NULL.                    */
  while (list)
    {
      gdisp = (GDisplay *) list->data;
      if (gdisp->ID == ID)
	return gdisp;

      list = next_item (list);
    }

  return NULL;
}


void
gdisplay_update_title (ID)
     int ID;
{
  GDisplay *gdisp;
  link_ptr list = display_list;
  char title [MAX_TITLE_BUF];

  /*  traverse the linked list of displays, handling each one  */
  while (list)
    {
      gdisp = (GDisplay *) list->data;
      if (gdisp->gimage->ID == ID)
	{
	  /* format the title */
	  sprintf (title, "%s-%d.%d (%s)",
		   prune_filename (gdisp->gimage->filename),
		   gdisp->gimage->ID, gdisp->instance,
		   image_type_strs[gdisp->gimage->type]);
  
	  /*XtVaSetValues (gdisp->shell,
			 XmNtitle, title,
			 NULL);*/
	}

      list = next_item (list);
    }
}


void
gdisplays_update_area (ID, x, y, w, h)
     int ID;
     int x, y;
     int w, h;
{
  GDisplay *gdisp;
  link_ptr list = display_list;
  int x1, y1, x2, y2;
  int count = 0;

  /*  traverse the linked list of displays  */
  while (list)
    {
      gdisp = (GDisplay *) list->data;
      if (gdisp->gimage->ID == ID)
	{
	  /*  We only need to update the first instance that
	      we find of this gimage ID.  Otherwise, we would
	      be reconverting the same region unnecessarily.   */
	  if (! count)
	    gdisplay_add_update_area (gdisp, x, y, w, h);
	  else
	    {
	      gdisplay_transform_coords (gdisp, x, y, &x1, &y1, 0);
	      gdisplay_transform_coords (gdisp, x + w, y + h, &x2, &y2, 0);
	      gdisplay_add_display_area (gdisp, x1, y1, (x2 - x1), (y2 - y1));
	    }
	  count++;
	}

      list = next_item (list);
    }
}


void
gdisplays_update_full (ID)
     int ID;
{
  GDisplay *gdisp;
  link_ptr list = display_list;
  int count = 0;

  /*  traverse the linked list of displays, handling each one  */
  while (list)
    {
      gdisp = (GDisplay *) list->data;
      if (gdisp->gimage->ID == ID)
	{
	  if (! count)
	    gdisplay_add_update_area (gdisp, 0, 0,
				      gdisp->gimage->width,
				      gdisp->gimage->height);
	  else
	    gdisplay_add_display_area (gdisp, 0, 0, 
				       gdisp->disp_width,
				       gdisp->disp_height);

	  count++;
	}

      list = next_item (list);
    }
}


void
gdisplays_expose_full ()
{
  GDisplay *gdisp;
  link_ptr list = display_list;

  /*  traverse the linked list of displays, handling each one  */
  while (list)
    {
      gdisp = (GDisplay *) list->data;
      gdisplay_expose_full (gdisp);
      list = next_item (list);
    }
}


void
gdisplays_selection_visibility (gimage_ID, function)
     int gimage_ID;
     int function;
{
  GDisplay *gdisp;
  link_ptr list = display_list;
  int count = 0;

  /*  traverse the linked list of displays, handling each one  */
  while (list)
    {
      gdisp = (GDisplay *) list->data;
      if (gdisp->gimage->ID == gimage_ID)
	{
	  if (function == SELECTION_OFF)
	    selection_invis (gdisp->select);
	  else if (function == SELECTION_ON)
	    selection_start (gdisp->select, TRUE);
	  else if (function == SELECTION_PAUSE)
	    selection_pause (gdisp->select);
	  else if (function == SELECTION_RESUME)
	    selection_resume (gdisp->select);
	  count++;
	}

      list = next_item (list);
    }
}


/*  install and remove tool cursor from all gdisplays...  */
void
gdisplays_install_tool_cursor (cursor_type)
     GdkCursorType cursor_type;
{
  link_ptr list = display_list;
  GDisplay * gdisp;

  current_tool_cursor = cursor_type;

  while (list)
    {
      gdisp = (GDisplay *) list->data;
      change_win_cursor (gdisp->canvas->window, cursor_type);
      list = next_item (list);
    }
}

void
gdisplays_remove_tool_cursor ()
{
  link_ptr list = display_list;
  GDisplay * gdisp;

  while (list)
    {
      gdisp = (GDisplay *) list->data;
      unset_win_cursor (gdisp->canvas->window);
      list = next_item (list);
    }
}


int
gdisplays_dirty ()
{
  int dirty = 0;
  link_ptr list = display_list;

  /*  traverse the linked list of displays  */
  while (list)
    {
      if (((GDisplay *) list->data)->gimage->dirty > 0)
	dirty = 1;
      list = next_item (list);
    }

  return dirty;
}


void
gdisplays_delete ()
{
  link_ptr list = display_list;

  /*  traverse the linked list of displays  */
  while (list)
    {
      gdisplay_delete ((GDisplay *) list->data);
      list = next_item (list);
    }

  /*  free up linked list data  */
  free_list (display_list);
}


void
gdisplays_flush ()
{
  link_ptr list = display_list;

  /*  traverse the linked list of displays  */
  while (list)
    {
      gdisplay_flush ((GDisplay *) list->data);
      list = next_item (list);
    }

  /*  for convenience, we call the layers dialog flush here  */
  layers_dialog_flush ();
}
