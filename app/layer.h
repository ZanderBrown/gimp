/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __LAYER_H__
#define __LAYER_H__

#include "channel.h"
#include "temp_buf.h"

/* structure declarations */

typedef struct _Layer Layer;

struct _Layer
{
  char * name;                  /*  name of the layer            */

  unsigned char * data;	        /*  raw r,g,b values             */
  int visible;                  /*  controls visibility          */
  int linked;                   /*  control linkage              */
  int preserve_trans;           /*  preserve transparency?       */

  Channel * mask;               /*  possible layer mask          */
  int apply_mask;               /*  controls mask application    */
  
  int offset_x, offset_y;       /*  offset of layer in image     */
  
  int type;                     /*  type of image                */
  int width, height;            /*  size of layer                */
  int bytes;                    /*  bytes per pixel              */
  
  int opacity;                  /*  layer opacity                */
  int mode;                     /*  layer combination mode       */
  
  int shmid;                    /*  Shared memory ID             */
  void * shmaddr;               /*  Shared memory address        */
  
  int ID;                       /*  provides a unique ID         */
  int gimage_ID;                /*  ID of gimage owner           */
  
  void * layer_widget;          /*  for the layers dialog        */
};


/* function declarations */

Layer *         layer_new (int, int, int, int, char *, int, int);
Layer *         layer_copy (Layer *);
Layer *         layer_from_buf (void *, TempBuf *, int, int, char *, int, int);
Channel *       layer_add_mask (Layer *, char *, int, unsigned char *);
Layer *         layer_get_ID (int);
void            layer_delete (Layer *);
void            layer_apply_mask (Layer *);
int             layer_anchor (Layer *);
void            layer_displace (Layer *, int, int);
void            layer_apply_image (Layer *, int, int, int, int, TempBuf *, int);

/* access functions */

unsigned char * layer_data (Layer *);
Channel *       layer_mask (Layer *);
int             layer_toggle_visibility (Layer *);
int             layer_toggle_linkage (Layer *);
int             layer_toggle_preserve_trans (Layer *);
int             layer_toggle_mask_application (Layer *);


#endif /* __LAYER_H__ */

