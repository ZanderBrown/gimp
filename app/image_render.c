#include <stdlib.h>
#include "appenv.h"
#include "colormaps.h"
#include "errors.h"
#include "gdither.h"
#include "gximage.h"
#include "image_render.h"
#include "scale.h"
#include "visual.h"


/*  accelerate transparency of image scaling  */
unsigned int  check_mod;
unsigned int  check_shift;
unsigned char blend_dark_check [0x10000];
unsigned char blend_light_check [0x10000];
unsigned char check_combos [6][2] = 
{
  { 204, 255 },
  { 102, 153 },
  { 0, 51 },
  { 255, 255 },
  { 127, 127 },
  { 0, 0 }
};


void
render_setup (check_type, check_size)
     int check_type;
     int check_size;
{
  int i, j;

  if (check_type < 0 || check_type > 5)
    fatal_error ("Invalid check_type argument to render_setup.");
  if (check_size < 0 || check_size > 2)
    fatal_error ("Invalid check_size argument to render_setup.");

  for (i = 0; i < 256; i++)
    for (j = 0; j < 256; j++)
      {
	blend_dark_check [(i << 8) + j] = (unsigned char) 
	  ((j * i + check_combos[check_type][0] * (255 - i)) / 255);
	blend_light_check [(i << 8) + j] = (unsigned char) 
	  ((j * i + check_combos[check_type][1] * (255 - i)) / 255);
      }

  switch (check_size)
    {
    case SMALL_CHECKS:
      check_mod = 0x3;
      check_shift = 2;
      break;
    case MEDIUM_CHECKS:
      check_mod = 0x7;
      check_shift = 3;
      break;
    case LARGE_CHECKS:
      check_mod = 0xf;
      check_shift = 4;
      break;
    }
}


/*  Render Image functions  */
typedef void (* RenderImageFunc)          (GDisplay *, int, int, int, int);

static  void render_image_indexed_8bit    (GDisplay *, int, int, int, int);
static  void render_image_indexed_a_8bit  (GDisplay *, int, int, int, int);
static  void render_image_gray_8bit       (GDisplay *, int, int, int, int);
static  void render_image_gray_a_8bit     (GDisplay *, int, int, int, int);
static  void render_image_rgb_8bit        (GDisplay *, int, int, int, int);
static  void render_image_rgb_a_8bit      (GDisplay *, int, int, int, int);

static  void render_image_indexed_16bit   (GDisplay *, int, int, int, int);
static  void render_image_indexed_a_16bit (GDisplay *, int, int, int, int);
static  void render_image_gray_16bit      (GDisplay *, int, int, int, int);
static  void render_image_gray_a_16bit    (GDisplay *, int, int, int, int);
static  void render_image_rgb_16bit       (GDisplay *, int, int, int, int);
static  void render_image_rgb_a_16bit     (GDisplay *, int, int, int, int);

static  void render_image_indexed_24bit   (GDisplay *, int, int, int, int);
static  void render_image_indexed_a_24bit (GDisplay *, int, int, int, int);
static  void render_image_gray_24bit      (GDisplay *, int, int, int, int);
static  void render_image_gray_a_24bit    (GDisplay *, int, int, int, int);
static  void render_image_rgb_24bit       (GDisplay *, int, int, int, int);
static  void render_image_rgb_a_24bit     (GDisplay *, int, int, int, int);


/*  Static variables accessed from all functions  */
static  unsigned char *src, *s;
static  unsigned char *dest;
static  unsigned char *d8_bit;
static  unsigned short *d16_bit;
static  unsigned long *d24_bit;
static  short bpp;
static  long width, height;
static  long srcwidth, destwidth, destlength;
static  unsigned char scalesrc, scaledest;
static  unsigned char *scale, *scalewalk;
static  unsigned char *cmap;
static  int val;
static  long i, j;
static  long sx, sy;
static  int bytes_per_pixel;
static  int initial;
static  int row, col;
static  int dark_light;
static  long ra, ga, ba;
static  int a, r, g, b;

/*  Render functions for 3 display depths and 6 image types  */

static  RenderImageFunc render_functions [3][6] =
{
  {
    render_image_rgb_8bit,
    render_image_rgb_a_8bit,
    render_image_gray_8bit,
    render_image_gray_a_8bit,
    render_image_indexed_8bit,
    render_image_indexed_a_8bit
  },

  {
    render_image_rgb_16bit,
    render_image_rgb_a_16bit,
    render_image_gray_16bit,
    render_image_gray_a_16bit,
    render_image_indexed_16bit,
    render_image_indexed_a_16bit
  },

  {
    render_image_rgb_24bit,
    render_image_rgb_a_24bit,
    render_image_gray_24bit,
    render_image_gray_a_24bit,
    render_image_indexed_24bit,
    render_image_indexed_a_24bit
  }
};


/*****************************************************************/
/*  This function is the core of the display--it offsets and     */
/*  scales the image according to the current parameters in the  */
/*  gdisp object.  It handles color, grayscale, 8, 15, 16, 24,   */
/*  & 32 bit output depths.                                      */
/*                                                               */
/*****************************************************************/

void
image_render (gdisp, x, y, w, h)
     GDisplay *gdisp;
     int x, y;       /* vert & horiz offsets */
     int w, h;       /* width and height to be scaled */
{
  int image_type;
  int depth_type;

  scalesrc = SCALESRC (gdisp);
  scaledest = SCALEDEST (gdisp);

  image_type = gimage_projection_type (gdisp->gimage);

  width = gdisp->gimage->width;
  height = gdisp->gimage->height;
  bpp = gimage_projection_bytes (gdisp->gimage);

  sy = UNSCALE (gdisp, (gdisp->offset_y + y));
  sx = UNSCALE (gdisp, (gdisp->offset_x + x));

  srcwidth = width * bpp;
  src = gimage_projection (gdisp->gimage);
  src += (sy * srcwidth) + (sx * bpp);

  destwidth =  get_gximage_bpl (image_type);
  bytes_per_pixel = get_gximage_bpp (image_type);
  destlength = w * bytes_per_pixel;
  dest = get_gximage_data (image_type);
  
  x += gdisp->offset_x;
  y += gdisp->offset_y;

  scale = accelerate_scaling (w, x, bpp, scalesrc, scaledest);

  i = y + h;
  initial = TRUE;

  /*  Find the image depth type  */
  switch (gdisp->depth)
    {
    case 8:
      depth_type = 0;
      break;
    case 15: case 16:
      depth_type = 1;
      break;
    case 24:
      depth_type = 2;
      break;
    default:
      warning ("depth not supported in image_render.\n");
      return;
      break;
    }

  /*  Render the image based on depth and image type  */
  (* render_functions [depth_type][image_type]) (gdisp, x, y, w, h);

  xfree (scale);
}




/*************************/
/*  8 Bit functions      */
/*************************/

static void
render_image_indexed_8bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  cmap = gimage_cmap (gdisp->gimage);
	  
  for ( ; y < i; y++)
    {
      row = y & 0x7;
      col = x & 0x7;
      s = src;
      d8_bit = (unsigned char *) dest;
      scalewalk = scale;
      j = w;
      
      /*  We must redraw EVERY line because we are
       *  dithering, and the results can differ when rendering
       *  identical pixel intensities at different locations
       */
      while (j--) 
	{
	  val = *s * 3;
	  r = red_ordered_dither[cmap[val++]];
	  g = green_ordered_dither[cmap[val++]];
	  b = blue_ordered_dither[cmap[val++]];
	  
	  ra = (ordered_dither_matrices[SECONDWORD(r)][row][col]) ?
	    SECONDBYTE(r) : FIRSTBYTE(r);
	  ga = (ordered_dither_matrices[SECONDWORD(g)][row][col]) ?
	    SECONDBYTE(g) : FIRSTBYTE(g);
	  ba = (ordered_dither_matrices[SECONDWORD(b)][row][col]) ?
	    SECONDBYTE(b) : FIRSTBYTE(b);
	  
	  *d8_bit++ = color_cube [ra + ga + ba];
	  
	  s += *scalewalk++;
	  col++;
	  col &= 0x7;
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
    }
}


static void
render_image_indexed_a_8bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
}

static void
render_image_gray_8bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      s = src;
      d8_bit = (unsigned char *) dest;
      if ((y % scaledest) && !initial)
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      *d8_bit++ = grays [s[GRAY_PIX]];
	      
	      s += *scalewalk++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_gray_a_8bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      col = x;
      dark_light = (y >> check_shift) + (x >> check_shift);
      s = src;
      d8_bit = (unsigned char *) dest;
      if ((y % scaledest) && !initial && (y & check_mod))
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      a = s[ALPHA_G_PIX] << 8;
	      if (dark_light & 0x1)
		g = blend_dark_check [(a | s[GRAY_PIX])];
	      else
		g = blend_light_check [(a | s[GRAY_PIX])];
	      
	      *d8_bit++ = grays [g];
	      
	      s += *scalewalk++;
	      col++;
	      if ( !(col & check_mod))
		dark_light++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_rgb_8bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      row = y & 0x7;
      col = x & 0x7;
      s = src;
      d8_bit = (unsigned char *) dest;
      scalewalk = scale;
      j = w;
      
      /*  We must redraw EVERY line because we are
       *  dithering, and the results can differ when rendering
       *  identical pixel intensities at different locations
       */
      while (j--) 
	{
	  r = red_ordered_dither[s[RED_PIX]];
	  g = green_ordered_dither[s[GREEN_PIX]];
	  b = blue_ordered_dither[s[BLUE_PIX]];
	  
	  ra = (ordered_dither_matrices[SECONDWORD(r)][row][col]) ?
	    SECONDBYTE(r) : FIRSTBYTE(r);
	  ga = (ordered_dither_matrices[SECONDWORD(g)][row][col]) ?
	    SECONDBYTE(g) : FIRSTBYTE(g);
	  ba = (ordered_dither_matrices[SECONDWORD(b)][row][col]) ?
	    SECONDBYTE(b) : FIRSTBYTE(b);
	  
	  *d8_bit++ = color_cube [ra + ga + ba];
	  
	  s += *scalewalk++;
	  col++;
	  col &= 0x7;
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
    }
}
     
static void
render_image_rgb_a_8bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  int dr, dc;

  for ( ; y < i; y++)
    {
      dr = y & 0x7;
      dc = x & 0x7;
      col = x;
      dark_light = (y >> check_shift) + (x >> check_shift);
      s = src;
      d8_bit = (unsigned char *) dest;
      scalewalk = scale;
      j = w;
      
      /*  We must redraw EVERY line because we are
       *  dithering, and the results can differ when rendering
       *  identical pixel intensities at different locations
       */
      while (j--) 
	{
	  a = s[ALPHA_PIX] << 8;
	  if (dark_light & 0x1)
	    {
	      r = blend_dark_check [(a | s[RED_PIX])];
	      g = blend_dark_check [(a | s[GREEN_PIX])];
	      b = blend_dark_check [(a | s[BLUE_PIX])];
	    }
	  else
	    {
	      r = blend_light_check [(a | s[RED_PIX])];
	      g = blend_light_check [(a | s[GREEN_PIX])];
	      b = blend_light_check [(a | s[BLUE_PIX])];
	    }
	  
	  r = red_ordered_dither[r];
	  g = green_ordered_dither[g];
	  b = blue_ordered_dither[b];
	  
	  ra = (ordered_dither_matrices[SECONDWORD(r)][dr][dc]) ?
	    SECONDBYTE(r) : FIRSTBYTE(r);
	  ga = (ordered_dither_matrices[SECONDWORD(g)][dr][dc]) ?
	    SECONDBYTE(g) : FIRSTBYTE(g);
	  ba = (ordered_dither_matrices[SECONDWORD(b)][dr][dc]) ?
	    SECONDBYTE(b) : FIRSTBYTE(b);
	  
	  *d8_bit++ = color_cube [ra + ga + ba];
	  
	  s += *scalewalk++;
	  col++;
	  dc = col & 0x7;
	  if (! (col & check_mod))
	    dark_light++;
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
    }
}


/*************************/
/*  15/16 Bit functions  */
/*************************/

static void
render_image_indexed_16bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  cmap = gimage_cmap (gdisp->gimage);
  
  for ( ; y < i; y++)
    {
      s = src;
      d16_bit = (unsigned short *) dest;
      if ((y % scaledest) && !initial)
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--)
	    {
	      val = *s * 3;
	      *d16_bit++ =
		COLOR_COMPOSE (cmap[val], cmap[val+1], cmap[val+2]);
	      s += *scalewalk++;
	    }
	  src += srcwidth * scalesrc;
	}
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_indexed_a_16bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
}

static void
render_image_gray_16bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      s = src;
      d16_bit = (unsigned short *) dest;
      if ((y % scaledest) && !initial)
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--)
	    {
	      g = s[GRAY_PIX];
	      
	      *d16_bit++ = COLOR_COMPOSE (g, g, g);
	      
	      s += *scalewalk++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_gray_a_16bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      col = x;
      dark_light = (y >> check_shift) + (x >> check_shift);
      s = src;
      d16_bit = (unsigned short *) dest;
      if ((y % scaledest) && !initial && (y & check_mod))
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      a = s[ALPHA_G_PIX] << 8;
	      if (dark_light & 0x1)
		g = blend_dark_check [(a | s[GRAY_PIX])];
	      else
		g = blend_light_check [(a | s[GRAY_PIX])];
	      
	      *d16_bit++ = 
		COLOR_COMPOSE (g, g, g);
	      
	      s += *scalewalk++;
	      col++;
	      if ( !(col & check_mod))
		dark_light++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_rgb_16bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      s = src;
      d16_bit = (unsigned short *) dest;
      if ((y % scaledest) && !initial)
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      *d16_bit++ = 
		COLOR_COMPOSE (s[RED_PIX], s[GREEN_PIX], s[BLUE_PIX]);
	      
	      s += *scalewalk++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_rgb_a_16bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      col = x;
      dark_light = (y >> check_shift) + (x >> check_shift);
      s = src;
      d16_bit = (unsigned short *) dest;
      if ((y % scaledest) && !initial && (y & check_mod))
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      a = s[ALPHA_PIX] << 8;
	      if (dark_light & 0x1)
		{
		  r = blend_dark_check [(a | s[RED_PIX])];
		  g = blend_dark_check [(a | s[GREEN_PIX])];
		  b = blend_dark_check [(a | s[BLUE_PIX])];
		}
	      else
		{
		  r = blend_light_check [(a | s[RED_PIX])];
		  g = blend_light_check [(a | s[GREEN_PIX])];
		  b = blend_light_check [(a | s[BLUE_PIX])];
		}
	      
	      *d16_bit++ = 
		COLOR_COMPOSE (r, g, b);
	      
	      s += *scalewalk++;
	      col++;
	      if ( !(col & check_mod))
		dark_light++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}


/*************************/
/*  24 Bit functions     */
/*************************/

static void
render_image_indexed_24bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  cmap = gimage_cmap (gdisp->gimage);
  
  for ( ; y < i; y++)
    {
      s = src;
      d24_bit = (unsigned long *) dest;
      if ((y % scaledest) && !initial)
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      val = *s * 3;
	      *d24_bit++ = 
		COLOR_COMPOSE (cmap[val], cmap[val+1], cmap[val+2]);
	      s += *scalewalk++;
	    }
	  src += srcwidth * scalesrc;
	}
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_indexed_a_24bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
}

static void
render_image_gray_24bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      s = src;
      d24_bit = (unsigned long *) dest;
      if ((y % scaledest) && !initial)
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      g = s[GRAY_PIX];
	      
	      *d24_bit++ = COLOR_COMPOSE (g, g, g);
	      
	      s += *scalewalk++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_gray_a_24bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      col = x;
      dark_light = (y >> check_shift) + (x >> check_shift);
      s = src;
      d24_bit = (unsigned long *) dest;
      if ((y % scaledest) && !initial && (y & check_mod))
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      a = s[ALPHA_G_PIX] << 8;
	      if (dark_light & 0x1)
		g = blend_dark_check [(a | s[GRAY_PIX])];
	      else
		g = blend_light_check [(a | s[GRAY_PIX])];
	      
	      *d24_bit++ = COLOR_COMPOSE (g, g, g);
	      s += *scalewalk++;
	      col++;
	      if ( !(col & check_mod))
		dark_light++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_rgb_24bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      s = src;
      d24_bit = (unsigned long *) dest;
      if ((y % scaledest) && !initial)
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      *d24_bit++ = 
		COLOR_COMPOSE (s[RED_PIX], s[GREEN_PIX], s[BLUE_PIX]);
	      s += *scalewalk++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}

static void
render_image_rgb_a_24bit (gdisp, x, y, w, h)
     GDisplay * gdisp;
     int x, y;
     int w, h;
{
  for ( ; y < i; y++)
    {
      col = x;
      dark_light = (y >> check_shift) + (x >> check_shift);
      s = src;
      d24_bit = (unsigned long *) dest;
      if ((y % scaledest) && !initial && (y & check_mod))
	memcpy (dest, dest - destwidth, destlength);
      else
	{
	  scalewalk = scale;
	  j = w;
	  
	  while (j--) 
	    {
	      a = s[ALPHA_PIX] << 8;
	      if (dark_light & 0x1)
		{
		  r = blend_dark_check [(a | s[RED_PIX])];
		  g = blend_dark_check [(a | s[GREEN_PIX])];
		  b = blend_dark_check [(a | s[BLUE_PIX])];
		}
	      else
		{
		  r = blend_light_check [(a | s[RED_PIX])];
		  g = blend_light_check [(a | s[GREEN_PIX])];
		  b = blend_light_check [(a | s[BLUE_PIX])];
		}
	      
	      *d24_bit++ = 
		COLOR_COMPOSE (r, g, b);
	      s += *scalewalk++;
	      col++;
	      if ( !(col & check_mod))
		dark_light++;
	    }
	}
      
      if (! ((y+1) % scaledest))
	src += srcwidth * scalesrc;
      
      dest += destwidth;
      initial = FALSE;
    }
}
