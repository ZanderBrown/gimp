/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "appenv.h"
#include "buildmenu.h"
#include "interface.h"

GtkWidget *
build_menu (items, table)
     MenuItem *items;
     GtkAcceleratorTable *table;
{
  GtkWidget *menu;
  GtkWidget *menu_item;
  GtkData *data;

  menu = gtk_menu_new ();

  while (items->label)
    {
      if (items->label[0] == '-')
	menu_item = gtk_menu_item_new ();
      else 
	{
	  menu_item = gtk_menu_item_new_with_label (items->label);
	  if (items->accelerator_key && table)
	    gtk_widget_install_accelerator (menu_item,
					    table,
					    items->accelerator_key,
					    items->accelerator_mods);
	}

      gtk_container_add (menu, menu_item);
	  
      if (items->callback)
	{
	  data = gtk_menu_item_get_state (menu_item);
	  gtk_callback_add (data, items->callback, items->user_data);
	}

      if (items->subitems)
	gtk_menu_item_set_submenu (menu_item, build_menu (items->subitems, table));

      gtk_widget_show (menu_item);
      items->widget = menu_item;

      items++;
    }

  return menu;
}

