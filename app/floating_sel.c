/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "appenv.h"
#include "floating_sel.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "interface.h"
#include "paint_funcs.h"
#include "undo.h"

/*  Local functions  */
static void  floating_sel_invalid_message (char *);


/************************************************************/
/*  Floating Selection functions                            */
/************************************************************/


BoundSeg *
floating_sel_boundary (gimage, num_segs)
     GImage * gimage;
     int * num_segs;
{
  Layer * layer;
  PixelRegion bPR;

  if (gimage->boundary_known)
    {
      *num_segs = gimage->num_segments;
      return gimage->boundary;
    }

  /* free the out of date boundary segments */
  if (gimage->boundary)
    xfree (gimage->boundary);

  layer = layer_get_ID (gimage->floating_sel);

  if (layer)
    {
      bPR.bytes = layer->bytes;
      bPR.w = layer->width;
      bPR.h = layer->height;
      bPR.rowstride = bPR.bytes * layer->width;
      bPR.data = layer_data (layer);
      gimage->boundary = find_mask_boundary (&bPR, &gimage->num_segments,
					     0, 0, layer->width, layer->height);
      *num_segs = gimage->num_segments;
      gimage->boundary_known = TRUE;
    }
  else
    {
      gimage->boundary = NULL;
      *num_segs = gimage->num_segments = 0;
      gimage->boundary_known = TRUE;
    }
  
  return gimage->boundary;
}


int
floating_sel_bounds (gimage, x1, y1, x2, y2)
     GImage * gimage;
     int * x1, * y1, * x2, * y2;
{
  Layer * layer;

  layer = layer_get_ID (gimage->floating_sel);

  if (layer)
    {
      *x1 = 0;
      *y1 = 0;
      *x2 = layer->width;
      *y2 = layer->height;
      
      return TRUE;
    }
  else
    return FALSE;
}


unsigned char
floating_sel_value (gimage, x, y)
     GImage * gimage;
     int x, y;
{
  Layer * layer;
  unsigned char * data;
  int alpha;

  layer = layer_get_ID (gimage->floating_sel);
  if (layer)
    {
      alpha = layer->bytes - 1;

      if (x < 0 || x >= layer->width || y < 0 || y >= layer->height)
	return 0;
      
      data = layer_data (layer) + y * layer->width * layer->bytes +
	x * layer->bytes;

      return data[alpha];
    }
  else
    return 0;
}


void
floating_sel_translate (gimage, off_x, off_y)
     GImage * gimage;
     int off_x, off_y;
{
  Layer * layer;
  int x1, y1, x2, y2;

  /*  If there is no layer, we need to cut and move the selection  */
  if (gimage->floating_sel == -1)
    /*  Float the gimage mask  */
    layer = gimage_mask_float (gimage);
  else if ((layer = layer_get_ID (gimage->floating_sel)))
    undo_push_fs_translate (gimage->ID);

  if (layer)
    {
      /*  find the old bounds  */
      x1 = layer->offset_x;
      y1 = layer->offset_y;
      x2 = x1 + layer->width;
      y2 = y1 + layer->height;
      
      layer->offset_x += off_x;
      layer->offset_y += off_y;
      
      /*  update the affected region  */
      gdisplays_update_area (gimage->ID, x1, y1, (x2 - x1), (y2 - y1));
      gdisplays_update_area (gimage->ID, x1 + off_x, y1 + off_y,
			     (x2 - x1), (y2 - y1));
    }
}


void
floating_sel_replace (gimage, float_layer, paste_into)
     GImage * gimage;
     Layer * float_layer;
     int paste_into;
{
  Layer * cur_float_layer;
  PixelRegion layerPR;
  int x1, y1, x2, y2;

  /*  Start an undo group for the fs_replace and image
   *  undos that happen on a floating sel replace.
   */
  undo_push_group_start (FS_REPLACE_UNDO);
  undo_push_fs_swap (gimage->ID);

  /*  unset the gimage floating selection so the image
   *  being applied to the gimage will affect the active
   *  layer and not the floating selection.
   */
  cur_float_layer = gimage_remove_layer (gimage, gimage->floating_sel);

  if (cur_float_layer)
    {
      layerPR.bytes     = cur_float_layer->bytes;
      layerPR.x         = cur_float_layer->offset_x;
      layerPR.y         = cur_float_layer->offset_y;
      layerPR.w         = cur_float_layer->width;
      layerPR.h         = cur_float_layer->height;
      layerPR.rowstride = cur_float_layer->width * layerPR.bytes;
      layerPR.data      = layer_data (cur_float_layer);
      
      gimage_apply_image (gimage, &layerPR, 1, cur_float_layer->opacity,
			  cur_float_layer->mode);

      x1 = layerPR.x;
      y1 = layerPR.y;
      x2 = x1 + layerPR.w;
      y2 = y1 + layerPR.h;
    }
  else
    {
      /*  get the gimage_mask bounds if they exist  */
      if (! gimage_mask_bounds (gimage, &x1, &y1, &x2, &y2))
	{
	  x1 = x2 = y1 = y2 = 0;
	}
      /*  if there is a selection and we're not pasting into it,
       *  we must remove it
       */
      else if (! paste_into)
	/*  Clear the region  */
	gregion_clear (gimage->ID, gimage->region);
    }


  if (float_layer)
    {
      /*  add the floating layer to the gimage  */
      gimage_add_floating_layer (gimage, float_layer);

      /*  invalidate the gimage's boundary variables  */
      gimage->boundary_known = FALSE;
  
      /*  update the display  */
      gdisplays_update_area (gimage->ID, x1, y1, (x2 - x1), (y2 - y1));
      gdisplays_update_area (gimage->ID,
			     float_layer->offset_x, float_layer->offset_y,
			     float_layer->width, float_layer->height);
    }
  else
    gdisplays_update_area (gimage->ID, x1, y1, (x2 - x1), (y2 - y1));

  /*  push an undo_group_end  */
  undo_push_group_end ();
}


void
floating_sel_swap (gimage, float_layer)
     GImage * gimage;
     Layer * float_layer;
{
  Layer * cur_float_layer;
  int x1, y1, x2, y2;

  /*  Floating selection swap undo  */
  undo_push_fs_swap (gimage->ID);

  /*  unset the gimage floating selection so the image
   *  being applied to the gimage will affect the active
   *  layer and not the floating selection.
   */
  cur_float_layer = gimage_remove_layer (gimage, gimage->floating_sel);

  if (cur_float_layer)
    {
      x1 = cur_float_layer->offset_x;
      y1 = cur_float_layer->offset_y;
      x2 = x1 + cur_float_layer->width;
      y2 = y1 + cur_float_layer->height;
    }
  else
    /*  get the gimage_mask bounds if they exist  */
    if (! gimage_mask_bounds (gimage, &x1, &y1, &x2, &y2))
      {
	x1 = x2 = y1 = y2 = 0;
      }

  if (float_layer)
    {
      /*  add the floating layer to the gimage  */
      gimage_add_floating_layer (gimage, float_layer);

      /*  invalidate the gimage's boundary variables  */
      gimage->boundary_known = FALSE;
  
      /*  update the display  */
      gdisplays_update_area (gimage->ID, x1, y1, (x2 - x1), (y2 - y1));
      gdisplays_update_area (gimage->ID,
			     float_layer->offset_x, float_layer->offset_y,
			     float_layer->width, float_layer->height);
    }
  else
    gdisplays_update_area (gimage->ID, x1, y1, (x2 - x1), (y2 - y1));
}


void
floating_sel_anchor (gimage)
     GImage * gimage;
{
  Layer * cur_float_layer;
  int x1, y1, x2, y2;
  MaskBuf * temp;
  int region_empty;
  PixelRegion layerPR;
  PixelRegion maskPR;
  PixelRegion tempPR;

  /*  this undo is special because it modifies the gimage sel mask  */
  undo_push_group_start (FS_ANCHOR_UNDO);
  undo_push_fs_swap (gimage->ID);

  /*  unset the gimage floating selection so the image
   *  being applied to the gimage will affect the active
   *  layer and not the floating selection.
   */
  cur_float_layer = gimage_remove_layer (gimage, gimage->floating_sel);

  if (cur_float_layer)
    {
      /*  merge the current floating selection  (if one exists)
       *  with the active layer...
       */
      x1 = BOUNDS (cur_float_layer->offset_x, 0, gimage->width);
      y1 = BOUNDS (cur_float_layer->offset_y, 0, gimage->height);
      x2 = BOUNDS (cur_float_layer->offset_x + cur_float_layer->width,
		   0, gimage->width);
      y2 = BOUNDS (cur_float_layer->offset_y + cur_float_layer->height,
		   0, gimage->height);

      layerPR.bytes     = cur_float_layer->bytes;
      layerPR.x         = x1;
      layerPR.y         = y1;
      layerPR.w         = (x2 - x1);
      layerPR.h         = (y2 - y1);
      layerPR.rowstride = cur_float_layer->width * layerPR.bytes;
      layerPR.data      = layer_data (cur_float_layer) +
	(y1 - cur_float_layer->offset_y) * layerPR.rowstride +
	(x1 - cur_float_layer->offset_x) * layerPR.bytes;
      
      gimage_apply_image (gimage, &layerPR, 1, cur_float_layer->opacity,
			  cur_float_layer->mode);
      
      /*  configure a PixelRegion for the gimage's sel mask  */
      maskPR.bytes = 1;
      maskPR.rowstride = gimage->width;
      maskPR.data = mask_buf_data (gimage_mask (gimage)) +
	y1 * maskPR.rowstride + x1;

      /*  allocate a temporary buffer for storing the alpha region  */
      temp = mask_buf_new (layerPR.w, layerPR.h);
      tempPR.bytes = 1;
      tempPR.w = temp->width;
      tempPR.h = temp->height;
      tempPR.rowstride = temp->width;
      tempPR.data = mask_buf_data (temp);

      /*  If there is a non-empty gimage mask, use it to limit
       *  the alpha value extraction process.  Otherwise, pass NULL
       *  for the mask
       */
      if (! (region_empty = gregion_is_empty (gimage->region)))
	extract_alpha_region (&layerPR, &maskPR, &tempPR);
      else
	extract_alpha_region (&layerPR, NULL, &tempPR);

      /*  Push a gregion undo before we modify the gregion  */
      gregion_push_undo (gimage->ID, gimage->region);

      /*  Now, clear the gimage selection mask and copy
       *  the old floating selection mask to it
       */
      if (! region_empty)  /* only clear if there is something to clear  */
	memset (mask_buf_data (gimage_mask (gimage)), 0,
		gimage->width * gimage->height);
      copy_region (&tempPR, &maskPR);

      /*  Free up the temporary alpha region  */
      mask_buf_free (temp);

      /* invalidate the current bounds and boundary of the gregion */
      gimage->region->bounds_known = FALSE;
      gimage->region->boundary_known = FALSE;
    }

  /*  push an undo_group_end  */
  undo_push_group_end ();
}


void
floating_sel_clear (gimage)
     GImage * gimage;
{
  Layer * cur_float_layer;
  PixelRegion layerPR;

  /*  Start a floating selection clear undo group  */
  undo_push_group_start (FS_CLEAR_UNDO);
  undo_push_fs_swap (gimage->ID);

  /*  unset the gimage floating selection so the image
   *  being applied to the gimage will affect the active
   *  layer and not the floating selection.
   */
  cur_float_layer = gimage_remove_layer (gimage, gimage->floating_sel);

  if (cur_float_layer)
    {
      layerPR.bytes     = cur_float_layer->bytes;
      layerPR.x         = cur_float_layer->offset_x;
      layerPR.y         = cur_float_layer->offset_y;
      layerPR.w         = cur_float_layer->width;
      layerPR.h         = cur_float_layer->height;
      layerPR.rowstride = cur_float_layer->width * layerPR.bytes;
      layerPR.data      = layer_data (cur_float_layer);
      
      gimage_apply_image (gimage, &layerPR, 1, cur_float_layer->opacity,
			  cur_float_layer->mode);
    }

  /*  Push a gregion undo before we modify the gregion  */
  gregion_push_undo (gimage->ID, gimage->region);

  /*  Now, clear the gimage selection mask and copy
   *  the old floating selection mask to it
   */
  memset (mask_buf_data (gimage_mask (gimage)), 0,
	  gimage->width * gimage->height);

  /* invalidate the current bounds and boundary of the gregion */
  gimage->region->bounds_known = FALSE;
  gimage->region->boundary_known = FALSE;

  /*  push an undo_group_end  */
  undo_push_group_end ();
}


TempBuf *
floating_sel_extract (gimage, cut_gimage)
     GImage * gimage;
     int cut_gimage;
{
  TempBuf * buf;
  Layer * layer;
  int bytes;
  int type;
  PixelRegion srcPR, destPR;

  layer = layer_get_ID (gimage->floating_sel);
  if (layer)
    {
      /*  How many bytes in the temp buffer?  */
      switch (layer->type)
	{
	case RGB_GIMAGE: case RGBA_GIMAGE:
	  bytes = 4; type = RGB; break;
	case GRAY_GIMAGE: case GRAYA_GIMAGE:
	  bytes = 2; type = GRAY; break;
	case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
	  bytes = 4; type = INDEXED; break;
	}
      /*  Allocate the temp buffer  */
      buf = temp_buf_new (layer->width, layer->height, bytes,
			  layer->offset_x, layer->offset_y, NULL);

      /* configure the pixel regions  */
      srcPR.bytes = layer->bytes;
      srcPR.w = layer->width;
      srcPR.h = layer->height;
      srcPR.rowstride = layer->width * srcPR.bytes;
      srcPR.data = layer_data (layer);
      
      destPR.bytes = buf->bytes;
      destPR.rowstride = buf->width * destPR.bytes;
      destPR.data = temp_buf_data (buf);

      extract_from_region (&srcPR, &destPR, NULL, gimage_cmap (gimage),
			   NULL, type, 1, 0);

      /*  If this is a cut operation, remove the floating sel  */
      if (cut_gimage)
	{
	  /*  first, push an undo  */
	  undo_push_fs_swap (gimage->ID);
	  /*  next, remove the floating selection  */
	  gimage_remove_layer (gimage, gimage->floating_sel);
	}

      return buf;
    }
  else
    return NULL;
}


void
floating_sel_all (gimage)
     GImage * gimage;
{
  Layer * float_layer;

  float_layer = layer_get_ID (gimage->floating_sel);

  if (float_layer)
    floating_sel_invalid_message ("Select/All");
}


void
floating_sel_sharpen (gimage)
     GImage * gimage;
{
  int alpha, size;
  unsigned char * data;
  Layer * float_layer;

  float_layer = layer_get_ID (gimage->floating_sel);

  if (float_layer)
    {
      /*  push the undo step  */
      undo_push_fs_mask (gimage->ID);

      /*  Set alpha value of all pixels to either OPAQUE or TRANSPARENT */
      alpha = float_layer->bytes - 1;
      size = float_layer->width * float_layer->height;
      data = layer_data (float_layer);
      while (size--)
	{
	  data[alpha] = (data[alpha] > 127) ? OPAQUE : TRANSPARENT;
	  data += float_layer->bytes;
	}

      /* invalidate the current boundary of the gimage */
      gimage->boundary_known = FALSE;
      gimage_update (gimage, 0, 0, float_layer->width, float_layer->height);
      gdisplays_flush ();
    }
}


void
floating_sel_invert (gimage)
     GImage * gimage;
{
  Layer * float_layer;

  float_layer = layer_get_ID (gimage->floating_sel);

  if (float_layer)
    floating_sel_invalid_message ("Select/Invert");
}


void
floating_sel_feather (gimage, radius)
     GImage * gimage;
     double radius;
{
  PixelRegion srcPR;
  Layer * float_layer;

  float_layer = layer_get_ID (gimage->floating_sel);

  if (float_layer)
    {
      /*  push the undo step  */
      undo_push_fs_mask (gimage->ID);

      srcPR.bytes = float_layer->bytes;
      srcPR.w = float_layer->width;
      srcPR.h = float_layer->height;
      srcPR.rowstride = srcPR.bytes * float_layer->width;
      srcPR.data = layer_data (float_layer);

      gaussian_blur_region (&srcPR, &srcPR, radius, REPLACE);

      /* invalidate the current boundary of the gimage */
      gimage->boundary_known = FALSE;
      gimage_update (gimage, 0, 0, float_layer->width, float_layer->height);
      gdisplays_flush ();
    }
}


void
floating_sel_border (gimage, radius)
     GImage * gimage;
     int radius;
{
  PixelRegion destPR;
  Layer * float_layer;
  BoundSeg * bs;
  int num_segs;

  float_layer = layer_get_ID (gimage->floating_sel);

  if (float_layer)
    {
      /*  push the undo step  */
      undo_push_fs_mask (gimage->ID);

      /*  Get the boundary  */
      bs = floating_sel_boundary (gimage, &num_segs);

      destPR.bytes = float_layer->bytes;
      destPR.w = float_layer->width;
      destPR.h = float_layer->height;
      destPR.rowstride = destPR.bytes * float_layer->width;
      destPR.data = layer_data (float_layer);

      border_region (&destPR, bs, num_segs, radius);

      /* invalidate the current boundary of the gimage */
      gimage->boundary_known = FALSE;
      gimage_update (gimage, 0, 0, float_layer->width, float_layer->height);
      gdisplays_flush ();
    }
}


static void
floating_sel_invalid_message (operation)
     char * operation;
{
  char buffer[256];

  sprintf (buffer, "%s: Invalid on floating selections.", operation);
  message_box (buffer, NULL, NULL);
}
