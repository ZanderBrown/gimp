/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "appenv.h"
#include "errors.h"
#include "autodialog.h"
#include "general.h"
#include "gimage.h"
#include "interface.h"
#include "linked.h"

#define TYPE_MASK       0x0000FFFF
#define BITSET(a, b)    ((a & b) == b)

static char * dialog_types[] =
{
  "",
  "GROUP_ROWS",
  "GROUP_COLUMNS",
  "GROUP_FORM",
  "ITEM_PUSH_BUTTON",
  "ITEM_CHECK_BUTTON",
  "ITEM_RADIO_BUTTON",
  "ITEM_IMAGE_MENU",
  "ITEM_SCALE",
  "ITEM_FRAME",
  "ITEM_LABEL",
  "ITEM_TEXT"
};

/*  Local functions  */
static AutoDialogItem dialog_find_item (AutoDialog, int);
static void dialog_delete_children (AutoDialog, AutoDialogItem);
static GtkWidget *dialog_create_image_menu (AutoDialog, char *, gpointer,
					    GtkWidget **, GtkWidget **);

/*  Callback and Update functions  */
static void dialog_image_menu_callback (GtkWidget *, gpointer, gpointer);
static void dialog_push_button_callback (GtkWidget *, gpointer, gpointer);
static gint dialog_toggle_button_update (GtkObserver *, GtkData *);
static gint dialog_radio_button_update (GtkObserver *, GtkData *);
static gint dialog_text_update (GtkObserver *, GtkData *);
static gint dialog_scale_update (GtkObserver *, GtkData *);
static void dialog_disconnect (GtkObserver *, GtkData *);

static int dialog_ID = 1;

AutoDialog
dialog_new (title, callback, callback_data)
     char *title;
     ItemCallback callback;
     void *callback_data;
{
  AutoDialog dlg;
  GtkWidget *action_area;

  dlg = xmalloc (sizeof (_AutoDialog));

  dlg->dialog_ID = dialog_ID++;
  dlg->default_ID = 0;
  dlg->next_item_ID = 1;
  dlg->items = NULL;
  dlg->callback = callback;
  dlg->callback_data = callback_data;

  /*  Create the main dialog shell  */
  dlg->shell = gtk_window_new (title, GTK_WINDOW_DIALOG);

  /*  Create the main dialog area  */
  dlg->dialog = gtk_vbox_new (FALSE, 10);
  gtk_container_add (dlg->shell, dlg->dialog);

  /*  Create the hbox for the action area  */
  action_area = gtk_hbox_new (TRUE, 5);
  gtk_container_set_border_width (action_area, 0);

  {
    int default_ID;
    int ok_ID;
    int cancel_ID;
    int help_ID;
    AutoDialogItem default_item;
    AutoDialogItem ok_item;
    AutoDialogItem cancel_item;
    AutoDialogItem help_item;

    default_ID = dialog_new_item (dlg, 0, ITEM_FRAME, NULL, NULL);
    
    ok_ID = dialog_new_item (dlg, 0, ITEM_PUSH_BUTTON, "OK", NULL);
    cancel_ID = dialog_new_item (dlg, 0, ITEM_PUSH_BUTTON, "Cancel", NULL);
    help_ID = dialog_new_item (dlg, 0, ITEM_PUSH_BUTTON, "Help", NULL);

    default_item = dialog_find_item (dlg, default_ID);
    ok_item = dialog_find_item (dlg, ok_ID);
    cancel_item = dialog_find_item (dlg, cancel_ID);
    help_item = dialog_find_item (dlg, help_ID);

    gtk_box_pack (dlg->dialog, default_item->widget, TRUE, TRUE, 0, GTK_PACK_START);
    gtk_box_pack (action_area, ok_item->widget, TRUE, TRUE, 0, GTK_PACK_START);
    gtk_box_pack (action_area, cancel_item->widget, TRUE, TRUE, 0, GTK_PACK_START);
    gtk_box_pack (action_area, help_item->widget, TRUE, TRUE, 0, GTK_PACK_START);

    gtk_widget_show (default_item->widget);
    gtk_widget_show (ok_item->widget);
    gtk_widget_show (cancel_item->widget);
    gtk_widget_show (help_item->widget);

    dlg->default_ID = default_ID;
  }

  gtk_box_pack (dlg->dialog, action_area, FALSE, FALSE, 0, GTK_PACK_START);

  gtk_widget_show (action_area);

  return dlg;
}

void
dialog_show (dlg)
     AutoDialog dlg;
{
  if (dlg)
    {
      if (!GTK_WIDGET_VISIBLE (dlg->dialog))
      gtk_widget_show (dlg->dialog);

      if (!GTK_WIDGET_VISIBLE (dlg->shell))
	map_dialog (dlg->shell);
    }
}

void
dialog_hide (dlg)
     AutoDialog dlg;
{
  if (dlg)
    if (GTK_WIDGET_VISIBLE (dlg->shell))
      gtk_widget_hide (dlg->shell);
}

void
dialog_close (dlg)
     AutoDialog dlg;
{
  link_ptr tmplink;
  AutoDialogItem dlgitem;
  
  if (dlg)
    {
      tmplink = dlg->items;
      while (tmplink)
	{
	  dlgitem = tmplink->data;
	  tmplink = tmplink->next;
	  
	  xfree (dlgitem);
	}
      free_list (dlg->items);
    
      gtk_widget_destroy (dlg->shell);
    }
}

int
dialog_new_item (dlg, parent_ID, type, data, extra)
     AutoDialog dlg;
     int parent_ID, type;
     void *data;
     void *extra;
{
  GtkWidget *parent_widget;
  GtkWidget *item_widget;
  GtkWidget *misc;
  GtkWidget *option_menu;
  GtkWidget *menu;
  AutoDialogItem parent_item;
  AutoDialogItem item;

  parent_item = dialog_find_item (dlg, parent_ID);
  if (parent_item)
    parent_widget = parent_item->widget;
  else
    parent_widget = dlg->dialog;

  /*  The new dialog item  */
  item = xmalloc (sizeof (_AutoDialogItem));
  item->item_ID = dlg->next_item_ID++;
  item->item_type = type;
  memset (item->data, 0, 32);
  item->parent = parent_item;
  item->children = NULL;
  item->gtk_data = NULL;
  item->observer.update = NULL;

  /*  Create the item  */
  switch (type & TYPE_MASK)
    {
    case GROUP_ROWS:
      item_widget = gtk_vbox_new (FALSE, 5);

      /*  If this is a row group within a row/col group, reduce spacing  */
      if ((parent_item->item_type & TYPE_MASK) == GROUP_ROWS ||
	  (parent_item->item_type & TYPE_MASK) == GROUP_COLUMNS)
	gtk_container_set_border_width (item_widget, 0);
      break;

    case GROUP_COLUMNS:
      item_widget = gtk_hbox_new (FALSE, 5);

      /*  If this is a col group within a row/col group, reduce spacing  */
      if ((parent_item->item_type & TYPE_MASK) == GROUP_ROWS ||
	  (parent_item->item_type & TYPE_MASK) == GROUP_COLUMNS)
	gtk_container_set_border_width (item_widget, 0);
      break;

    case ITEM_PUSH_BUTTON:
      item_widget = gtk_push_button_new ();
      gtk_callback_add (gtk_button_get_state (item_widget), dialog_push_button_callback, item);
      misc = gtk_label_new (data);
      gtk_container_add (item_widget, misc);
      gtk_widget_show (misc);
      break;

    case ITEM_RADIO_BUTTON:
      if (!parent_item->gtk_data)
	{
	  item_widget = gtk_radio_button_new (NULL);
	  parent_item->gtk_data = gtk_button_get_owner (item_widget);
	}
      else
	item_widget = gtk_radio_button_new (parent_item->gtk_data);

      item->gtk_data = parent_item->gtk_data;
      misc = gtk_label_new (data);
      gtk_label_set_alignment (misc, 0.0, 0.5);
      gtk_container_add (item_widget, misc);
      gtk_widget_show (misc);

      /*  Attach the item's observer to the owner data */
      item->observer.update = dialog_radio_button_update;
      item->observer.disconnect = dialog_disconnect;
      item->observer.user_data = item;
      break;

    case ITEM_CHECK_BUTTON:
      item_widget = gtk_check_button_new ();
      item->gtk_data = gtk_button_get_state (item_widget);
      misc = gtk_label_new (data);
      gtk_label_set_alignment (misc, 0.0, 0.5);
      gtk_container_add (item_widget, misc);
      gtk_widget_show (misc);

      /*  Attach the item's observer to the owner data */
      item->observer.update = dialog_toggle_button_update;
      item->observer.disconnect = dialog_disconnect;
      item->observer.user_data = item;
      break;

    case ITEM_IMAGE_MENU:
      item_widget = dialog_create_image_menu (dlg, data, extra, &option_menu, &menu);
      break;

    case ITEM_SCALE:
      {
	long *ldata;
	double min, max, val;
	double div;
	
	ldata = data;

	div = pow (10, ldata[3]);
	min = ldata[0] / div;
	max = ldata[1] / div;
	val = ldata[2] / div;
	item->gtk_data = gtk_data_adjustment_new (val, min, max, div, 1.0, 1.0);
	item_widget = gtk_hscale_new ((GtkDataAdjustment *) item->gtk_data);
	gtk_scale_set_value_pos (item_widget, GTK_POS_TOP);
	item->observer.update = dialog_scale_update;
	item->observer.disconnect = dialog_disconnect;
	item->observer.user_data = item;
      }
      break;

    case ITEM_FRAME:
      if (data && (strlen (data) > 0))
	item_widget = gtk_frame_new (data);
      else
	item_widget = gtk_frame_new (NULL);
      gtk_frame_set_type (item_widget, GTK_SHADOW_ETCHED_IN);
      gtk_container_set_border_width (item_widget, 0);
      break;

    case ITEM_LABEL:
      if (parent_item && (parent_item->item_type == ITEM_FRAME))
	{
	  gtk_frame_set_label (parent_widget, data);
	  return -1;
	}
      else
	item_widget = gtk_label_new (data);
      break;

    case ITEM_TEXT:
      item_widget = gtk_text_entry_new ();

      if (data && (strlen (data) > 0))
	gtk_text_entry_set_text (item_widget, data);

      /*XtAddCallback (item_widget, XmNvalueChangedCallback, dialog_item_callback, dlg);*/
      break;

    default:
      warning ("Unknown type for new item");
      return -1;      
    }

  /*  Add the item to the parent item container  */
  if (parent_item)
    {
      switch ((parent_item->item_type & TYPE_MASK))
	{
	case GROUP_ROWS: case GROUP_COLUMNS:
	  gtk_box_pack (parent_item->widget, item_widget, FALSE, FALSE, 0, GTK_PACK_START);
	  break;
	case ITEM_FRAME:
	  if (!parent_item->children)
	    gtk_container_add (parent_item->widget, item_widget);
	  else
	    {
	      warning ("Unable to add another widget to frame widget!");
	      warning ("Frame widgets can have only 1 child.");
	      warning ("Use a group widget instead.");
	      gtk_widget_destroy (item_widget);
	      return -1;
	    }
	  break;
	default:
	  warning ("Unable to add widget type %s to non-container type %s.",
		   dialog_types[(type & TYPE_MASK)],
		   dialog_types[(parent_item->item_type & TYPE_MASK)]);
	  gtk_widget_destroy (item_widget);
	  return -1;
	  break;
	}
      gtk_widget_show (item_widget);
    }

  item->widget = item_widget;

  if (parent_item)
    parent_item->children = add_to_list (parent_item->children, item);
  dlg->items = add_to_list (dlg->items, item);
  
  gtk_widget_set_user_data (item_widget, dlg);

  /*  Post initialization  */
  switch (type & TYPE_MASK)
    {
    case ITEM_SCALE:
      gtk_data_attach (item->gtk_data, &item->observer);
      break;
    case ITEM_RADIO_BUTTON:
      gtk_data_attach (parent_item->gtk_data, &item->observer);
      break;
    case ITEM_CHECK_BUTTON:
      gtk_data_attach (item->gtk_data, &item->observer);
      break;
    case ITEM_IMAGE_MENU:
      gtk_option_menu_set_menu (option_menu, menu);
      break;
    default:
      break;
    }
  
  return item->item_ID;
}

void
dialog_show_item (dlg, item_ID)
     AutoDialog dlg;
     int item_ID;
{
  AutoDialogItem item;

  item = dialog_find_item (dlg, item_ID);
  if (item && !GTK_WIDGET_VISIBLE (item->widget))
    gtk_widget_show (item->widget);
}

void
dialog_hide_item (dlg, item_ID)
     AutoDialog dlg;
     int item_ID;
{
  AutoDialogItem item;

  item = dialog_find_item (dlg, item_ID);
  if (item && GTK_WIDGET_VISIBLE (item->widget))
    gtk_widget_hide (item->widget);
}

void
dialog_change_item (dlg, item_ID, data)
     AutoDialog dlg;
     int item_ID;
     void *data;
{
  AutoDialogItem item;
  GtkDataWidget *widget_data;
  GtkDataAdjustment *adj_data;

  item = dialog_find_item (dlg, item_ID);
  if (item)
    {
      switch (item->item_type)
	{
	case ITEM_CHECK_BUTTON:
	  widget_data = (GtkDataWidget *) gtk_button_get_owner (item->widget);
	  if (*((long*) data))
	    widget_data->widget = item->widget;
	  else
	    widget_data->widget = NULL;

	  gtk_data_notify ((GtkData *) widget_data);
	  break;
	case ITEM_RADIO_BUTTON:
	  if (*((long*) data))
	    {
	      widget_data = (GtkDataWidget *) item->gtk_data;
	      widget_data->widget = item->widget;
	      gtk_data_notify (item->gtk_data);
	    }
	  else
	    return;
	  break;
	case ITEM_TEXT:
	  gtk_text_entry_set_text (item->widget, data);
	  break;
	case ITEM_SCALE:
	  adj_data = (GtkDataAdjustment *) item->gtk_data;
	  adj_data->value = (double) *((long*) data) / adj_data->step_increment;
	  gtk_data_notify (item->gtk_data);
	  break;
	default:
	  return;
	}
    }
}

void
dialog_delete_item (dlg, item_ID)
     AutoDialog dlg;
     int item_ID;
{
  AutoDialogItem item;

  item = dialog_find_item (dlg, item_ID);
  if (item)
    {
      dialog_delete_children (dlg, item);

      if (item->parent)
	item->parent->children = remove_from_list (item->parent->children, item);
      dlg->items = remove_from_list (dlg->items, item);

      if (GTK_WIDGET_VISIBLE (dlg->shell))
	{
	  gtk_widget_hide (dlg->shell);
	  gtk_widget_destroy (item->widget);
	  gtk_widget_show (dlg->shell);
	}
      else
	gtk_widget_destroy (item->widget);

      xfree (item);
    }
}


GtkWidget *
dialog_get_item_widget (dlg, item_ID)
     AutoDialog dlg;
     int item_ID;
{
  AutoDialogItem item;

  item = dialog_find_item (dlg, item_ID);
  if (item)
    return item->widget;
  return NULL;
}


static AutoDialogItem
dialog_find_item (dlg, ID)
     AutoDialog dlg;
     int ID;
{
  AutoDialogItem item;
  link_ptr tmp;

  item = NULL;
  
  if (!ID)
    {
      if (dlg->default_ID)
	ID = dlg->default_ID;
      else
	return NULL;
    }
  
  tmp = dlg->items;
  while (tmp)
    {
      item = tmp->data;
      tmp = next_item (tmp);
      
      if (item->item_ID == ID)
	return item;
    }
  
  warning ("Unable to find dialog item: %d", ID);
  
  return NULL;
}

static void
dialog_delete_children (dlg, item)
     AutoDialog dlg;
     AutoDialogItem item;
{
  AutoDialogItem child;
  link_ptr tmp;

  tmp = item->children;
  while (tmp)
    {
      child = tmp->data;

      dialog_delete_children (dlg, child);
      dlg->items = remove_from_list (dlg->items, child);
      xfree (child);

      tmp = next_item (tmp);
    }

  if (item->children)
    free_list (item->children);
}

static GtkWidget *
dialog_create_image_menu (dlg, title, image, option_menu, menu)
     AutoDialog dlg;
     char *title;
     void *image;
     GtkWidget **option_menu;
     GtkWidget **menu;
{
  extern link_ptr image_list;

  GImage *constrain;
  GImage *gimage;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *menu_item;
  char *menu_item_label;
  char *image_name;
  char constraint;
  link_ptr tmp;
  long ID;
  long *data;
  int num_items = 0;

  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  label = gtk_label_new (title);
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  *option_menu = gtk_option_menu_new ();
  gtk_box_pack (hbox, *option_menu, FALSE, FALSE, 0, GTK_PACK_END);

  *menu = gtk_menu_new ();

  constraint = title[123];
  ID = ((long*) title)[31];
  constrain = (ID == 0) ? image : gimage_get_ID (ID);

  tmp = image_list;
  while (tmp)
    {
      gimage = tmp->data;
      tmp = next_item (tmp);

      if (((constraint & IMAGE_CONSTRAIN_RGB) && 
	   (gimage_type (gimage) == RGB_GIMAGE ||
	    gimage_type (gimage) == RGBA_GIMAGE)) ||
	  ((constraint & IMAGE_CONSTRAIN_GRAY) &&
	   (gimage_type (gimage) == GRAY_GIMAGE ||
	    gimage_type (gimage) == GRAYA_GIMAGE)) ||
	  ((constraint & IMAGE_CONSTRAIN_INDEXED) &&
	   (gimage_type (gimage) == INDEXED_GIMAGE ||
	    gimage_type (gimage) == INDEXEDA_GIMAGE)))
	if ((gimage->width == constrain->width) &&
	    (gimage->height == constrain->height))
	  {
	    image_name = prune_filename (gimage->filename);
	    menu_item_label = (char *) xmalloc (strlen (image_name) + 15);
	    sprintf (menu_item_label, "%s-%d", image_name, gimage->ID);
	    menu_item = gtk_menu_item_new_with_label (menu_item_label);
	    gtk_callback_add (gtk_menu_item_get_state (menu_item),
			      dialog_image_menu_callback, dlg);
	    gtk_container_add (*menu, menu_item);
	    gtk_widget_show (menu_item);
	    
	    data = xmalloc (sizeof (long) * 2);
	    data[0] = dlg->next_item_ID - 1;
	    data[1] = (gimage->ID == constrain->ID) ? 0 : gimage->ID;
	    dlg->additional_data = data;

	    xfree (menu_item_label);
	    num_items ++;
	  }
    }

  if (!num_items)
    {
      menu_item = gtk_menu_item_new_with_label ("none");
      gtk_container_add (*menu, menu_item);
      gtk_widget_show (menu_item);
    }

  gtk_widget_show (label);
  gtk_widget_show (*option_menu);

  return hbox;
}

static void
dialog_image_menu_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  AutoDialog dlg;
  long *data;

  dlg = client_data;
  data = (long *) dlg->additional_data;

  if (dlg->callback)
    (* dlg->callback) (dlg->dialog_ID, data[0], dlg->callback_data, &data[1]);
}

static void
dialog_push_button_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  AutoDialogItem item;
  AutoDialog dlg;

  item = (AutoDialogItem) client_data;
  dlg = (AutoDialog) gtk_widget_get_user_data (item->widget);

  if (dlg->callback && item)
    (* dlg->callback) (dlg->dialog_ID, item->item_ID, dlg->callback_data, item->data);
}

static gint
dialog_toggle_button_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataInt *int_data;
  AutoDialogItem item;
  AutoDialog dlg;
  long *adata;

  item = (AutoDialogItem) observer->user_data;
  int_data = (GtkDataInt *) item->gtk_data;
  dlg = (AutoDialog) gtk_widget_get_user_data (item->widget);

  adata = (long*) item->data;

  if (int_data->value == GTK_STATE_ACTIVATED)
    {
      *adata = 1;
      if (dlg->callback && item)
	(* dlg->callback) (dlg->dialog_ID, item->item_ID, dlg->callback_data, item->data);
    }
  else if (int_data->value == GTK_STATE_DEACTIVATED)
    {
      *adata = 0;
      if (dlg->callback && item)
	(* dlg->callback) (dlg->dialog_ID, item->item_ID, dlg->callback_data, item->data);
    }

  return FALSE;
}

static gint
dialog_radio_button_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  AutoDialogItem item;
  AutoDialog dlg;
  GtkDataWidget *widget_data;
  long *adata;
	
  item = (AutoDialogItem) observer->user_data;
  dlg = (AutoDialog) gtk_widget_get_user_data (item->widget);
  widget_data = (GtkDataWidget *) data;

  adata = (long*) item->data;
  *adata = (item->widget == widget_data->widget);
  
  if (dlg->callback && item)
    (* dlg->callback) (dlg->dialog_ID, item->item_ID, dlg->callback_data, item->data);

  return FALSE;
}

static gint
dialog_text_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  AutoDialogItem item;
  AutoDialog dlg;

  item = observer->user_data;
  dlg = gtk_widget_get_user_data (item->widget);

  strncpy (item->data, gtk_text_entry_get_text (item->widget), 32);

  if (dlg->callback && item)
    (* dlg->callback) (dlg->dialog_ID, item->item_ID, dlg->callback_data, item->data);

  return FALSE;
}

static gint
dialog_scale_update (observer, gtk_data)
     GtkObserver *observer;
     GtkData *gtk_data;
{
  GtkDataAdjustment *adj_data;
  AutoDialogItem item;
  AutoDialog dlg;
  long *data;

  item = observer->user_data;
  dlg = gtk_widget_get_user_data (item->widget);

  adj_data = (GtkDataAdjustment *) gtk_data;
  data = (long*) item->data;
  *data = (long) (adj_data->value * adj_data->step_increment);

  if (dlg->callback && item)
    (* dlg->callback) (dlg->dialog_ID, item->item_ID, dlg->callback_data, item->data);

  return FALSE;
}

static void
dialog_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}
