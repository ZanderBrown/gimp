/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "errors.h"
#include "gdisplay.h"
#include "gdisplay_ops.h"
#include "info_window.h"
#include "scale.h"
#include "tools.h"

void
bounds_checking (gdisp)
     GDisplay *gdisp;
{
  int sx, sy;

  sx = SCALE(gdisp, gdisp->gimage->width);
  sy = SCALE(gdisp, gdisp->gimage->height);
  
  gdisp->offset_x = BOUNDS (gdisp->offset_x, 0,
			    LOWPASS (sx - gdisp->disp_width));
  
  gdisp->offset_y = BOUNDS (gdisp->offset_y, 0, 
			    LOWPASS (sy - gdisp->disp_height));
}


void
resize_display (gdisp)
     GDisplay *gdisp;
{
  /* freeze the active tool */
  active_tool_control (PAUSE, (void *) gdisp);

  gdisplay_shrink_wrap (gdisp);

  bounds_checking (gdisp);
  setup_scale (gdisp);

  gdisplays_flush ();
  
  /* re-enable the active tool */
  active_tool_control (RESUME, (void *) gdisp);
}


void
change_scale (gdisp, dir)
     GDisplay *gdisp;
     int dir;
{
  unsigned char scalesrc, scaledest;
  double offset_x, offset_y;
  long sx, sy;

  scalesrc = SCALESRC(gdisp);
  scaledest = SCALEDEST(gdisp);
  
  offset_x = gdisp->offset_x + (gdisp->disp_width/2.0);
  offset_y = gdisp->offset_y + (gdisp->disp_height/2.0);
  
  offset_x *= ((double) scalesrc / (double) scaledest);
  offset_y *= ((double) scalesrc / (double) scaledest);

  switch (dir)
    {
    case ZOOMIN :
      if (scalesrc > 1)
	scalesrc--;
      else
	if (scaledest < 0x10)
	  scaledest++;
      
      break;
    case ZOOMOUT :
      if (scaledest > 1)
	scaledest--;
      else
	if (scalesrc < 0x10)
	  scalesrc++;
      
      break;
    default :
      break;
    }
  
  sx = (gdisp->gimage->width * scaledest) / scalesrc;
  sy = (gdisp->gimage->height * scaledest) / scalesrc;
  
  /*  The slider value is a short, so make sure we are within its
      range.  If we are trying to scale past it, then stop the scale  */
  if (sx < 0xffff && sy < 0xffff)
    {
      gdisp->scale = (scaledest << 8) + scalesrc;
      
      /*  set the offsets  */
      offset_x *= ((double) scaledest / (double) scalesrc);
      offset_y *= ((double) scaledest / (double) scalesrc);
      
      gdisp->offset_x = (int) (offset_x - (gdisp->disp_width / 2));
      gdisp->offset_y = (int) (offset_y - (gdisp->disp_height / 2));
      
      /*  resize the image  */
      resize_display (gdisp);

      /*  update the gdisplay's info dialog  */
      if (gdisp->window_info_dialog)
	info_window_update (gdisp->window_info_dialog,
			    (void *) gdisp);
    }
}


void
setup_scale (gdisp)
     GDisplay *gdisp;
{
  gfloat sx, sy;
  gfloat step;

  sx = SCALE(gdisp, gdisp->gimage->width);
  sy = SCALE(gdisp, gdisp->gimage->height);
  step = SCALE(gdisp, 1);

  gdisp->hsbdata->value = gdisp->offset_x;
  gdisp->hsbdata->upper = sx;
  gdisp->hsbdata->page_size = gdisp->disp_width;
  gdisp->hsbdata->page_increment = (gdisp->disp_width / 2);
  gdisp->hsbdata->step_increment = step;

  gdisp->vsbdata->value = gdisp->offset_y;
  gdisp->vsbdata->upper = sy;
  gdisp->vsbdata->page_size = gdisp->disp_height;
  gdisp->vsbdata->page_increment = (gdisp->disp_height / 2);
  gdisp->vsbdata->step_increment = step;

  gtk_data_notify ((GtkData *) gdisp->hsbdata);
  gtk_data_notify ((GtkData *) gdisp->vsbdata);
}


unsigned char *
accelerate_scaling (width, start, bpp, scalesrc, scaledest)
     long width;
     long start;
     int bpp;
     int scalesrc;
     int scaledest;
{
  long i;
  unsigned char *scale;

  scale = (unsigned char *) xmalloc (sizeof (unsigned char) * width + 1);
  if (!scale)
    {
      warning ("Unable to allocate memory for accelerated scaling.");
      exit (1);
    }

  for (i = 0; i <= width; i++)
    scale[i] = ((i + start + 1) % scaledest) ? 0 : scalesrc * bpp;
    
  return scale;
}



