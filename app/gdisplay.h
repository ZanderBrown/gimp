/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GDISPLAY_H__
#define __GDISPLAY_H__

#include "gimage.h"
#include "info_dialog.h"
#include "linked.h"
#include "selection.h"
#include "timer.h"

/*
 *  Global variables
 *
 */

/*  some useful macros  */

#define  SCALESRC(g)    (g->scale & 0x00ff)
#define  SCALEDEST(g)   (g->scale >> 8)
#define  SCALE(g,x)     ((x * SCALEDEST(g)) / SCALESRC(g))
#define  UNSCALE(g,x)   ((x * SCALESRC(g)) / SCALEDEST(g))

#define LOWPASS(x) ((x>0) ? x : 0)
#define HIGHPASS(x,y) ((x>y) ? y : x)


typedef struct _GDisplay GDisplay;
struct _GDisplay
{
  int ID;                         /*  unique identifier for this gdisplay     */

  GtkWidget *shell;               /*  shell widget for this gdisplay          */
  GtkWidget *canvas;              /*  canvas widget for this gdisplay         */
  GtkWidget *hsb, *vsb;           /*  widgets for scroll bars                 */
  GtkWidget *hrule, *vrule;       /*  widgets for rulers                      */
  GtkWidget *popup;               /*  widget for popup menu                   */

  InfoDialog *window_info_dialog; /*  dialog box for image information        */

  GdkColormap *colormap;          /*  colormap of this window                 */
  int color_type;                 /*  is this an RGB or GRAY colormap         */

  GtkDataAdjustment *hsbdata;     /*  horizontal data information             */
  GtkDataAdjustment *vsbdata;     /*  vertical data information               */
  GtkObserver hsb_observer;       /*  horizontal data observer                */
  GtkObserver vsb_observer;       /*  vertical data observer                  */

  GtkDataAdjustment *hruledata;   /*  horizontal data information             */
  GtkDataAdjustment *vruledata;   /*  vertical data information               */

  GImage *gimage;	          /*  pointer to the associated gimage struct */
  int instance;                   /*  the instance # of this gdisplay as      */
                                  /*  taken from the gimage at creation       */

  int depth;   		          /*  depth of our drawables                  */
  int disp_width;                 /*  width of drawing area in the window     */
  int disp_height;                /*  height of drawing area in the window    */
  
  int offset_x, offset_y;         /*  offset of display image into raw image  */
  unsigned int scale; 	          /*  scale factor from original raw image    */
  
  Selection *select;              /*  Selection object                        */
  
  link_ptr update_areas;          /*  Update areas list                       */
  link_ptr display_areas;         /*  Display areas list                      */
};



/* member function declarations */

GDisplay * gdisplay_new                    (GImage *, unsigned int);
void       gdisplay_remove_and_delete      (GDisplay *);
int        gdisplay_mask_value             (GDisplay *, int, int);
int        gdisplay_mask_bounds            (GDisplay *, int *, int *, int *, int *);
void       gdisplay_transform_coords       (GDisplay *, int, int, int *, int *, int);
void       gdisplay_untransform_coords     (GDisplay *, int, int, int *,
					    int *, int, int);
void       gdisplay_transform_coords_f     (GDisplay *, double, double, double *,
					    double *, int);
void       gdisplay_untransform_coords_f   (GDisplay *, double, double, double *,
					    double *, int);
void       gdisplay_expose_area            (GDisplay *, int, int, int, int);
void       gdisplay_expose_full            (GDisplay *);

/*  function declarations  */

GDisplay * gdisplay_active                 (void);
GDisplay * gdisplay_get_ID                 (int);
void       gdisplay_update_title           (int);
void       gdisplays_update_area           (int, int, int, int, int);
void       gdisplays_update_full           (int);
void       gdisplays_expose_full           (void);
void       gdisplays_selection_visibility  (int, int);
void       gdisplays_install_tool_cursor   (GdkCursorType);
void       gdisplays_remove_tool_cursor    (void);
int        gdisplays_dirty                 (void);
void       gdisplays_delete                (void);
void       gdisplays_flush                 (void);


#endif /*  __GDISPLAY_H__  */
