/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <string.h>
#include "appenv.h"
#include "autodialog.h"
#include "draw_core.h"
#include "edit_selection.h"
#include "errors.h"
#include "free_select.h"
#include "gimage_mask.h"
#include "gdisplay.h"
#include "linked.h"
#include "rect_select.h"

typedef struct _free_select FreeSelect;

struct _free_select
{
  DrawCore *      core;       /*  Core select object                      */
  
  int             num_segs;   /*  Number of points in the polygon         */

  int             op;         /*  selection operation (ADD, SUB, etc)     */
  int             replace;    /*  replace current selection?              */
};


#define DEFAULT_MAX_INC  1024
#define ROUND(x) ((int) (x + 0.5))
#define SUPERSAMPLE      3
#define SUPERSAMPLE2     9

#define NO   0
#define YES  1

/*  The global array of XPoints for drawing the polygon...  */
static GdkSegment  *segs = NULL;
static int          max_segs = 0;
static int          first_x, first_y;
static SelectionOptions *free_options = NULL;


static int
add_point (num_segs, x, y)
     int num_segs;
     int x, y;
{
  if (num_segs >= max_segs)
    {
      max_segs += DEFAULT_MAX_INC;

      segs = (GdkSegment *) xrealloc ((void *) segs, sizeof (GdkSegment) * max_segs);

      if (!segs)
	fatal_error ("Unable to reallocate points array in free_select.");
    }

  if (num_segs)
    {
      segs[num_segs].x1 = segs[num_segs - 1].x2;
      segs[num_segs].y1 = segs[num_segs - 1].y2;
    }
  else
    {
      segs[num_segs].x1 = first_x;
      segs[num_segs].y1 = first_y;
    }

  segs[num_segs].x2 = x;
  segs[num_segs].y2 = y;

  return 1;
}


/*  Routines to scan convert the polygon  */

static link_ptr
insert_in_list (list, x)
     link_ptr list;
     int x;
{
  link_ptr orig = list;
  link_ptr rest;

  if (!list)
    return add_to_list (list, (void *) x);

  while (list)
    {
      rest = next_item (list);
      if (x < (int) list->data)
	{
	  rest = add_to_list (rest, list->data);
	  list->next = rest;
	  list->data = (void *) x;
	  return orig;
	}
      else if (!rest)
	{
	  append_to_list (list, (void *) x);
	  return orig;
	}
      list = next_item (list);
    }

  return orig;
}


static void
convert_segment (scanlines, width, height, x1, y1, x2, y2)
     link_ptr * scanlines;
     int width;
     int height;
     int x1, y1;
     int x2, y2;
{
  int ydiff, y, tmp;
  float xinc, xstart;

  if (y1 > y2)
    { tmp = y2; y2 = y1; y1 = tmp; 
      tmp = x2; x2 = x1; x1 = tmp; }
  ydiff = (y2 - y1);

  if ( ydiff )
    {
      xinc = (float) (x2 - x1) / (float) ydiff;
      xstart = x1 + 0.5 * xinc;
      for (y = y1 ; y < y2; y++)
	{
	  if (y >= 0 && y < height)
	    scanlines[y] = insert_in_list (scanlines[y], ROUND (xstart));
	  xstart += xinc;
	}
    }
}


static GRegion *
scan_convert (num_segs, width, height, gdisp)
     int num_segs;
     int width;
     int height;
     GDisplay * gdisp;
{
  GRegion * region;
  link_ptr * scanlines;
  link_ptr list;
  unsigned char *dest;
  int * vals, val;
  int x1, y1, x2, y2;
  int x, w;
  int i, j;

  if (num_segs < 3) 
    return NULL;

  region = gregion_new (width, height);

  if (free_options->antialias)
    {
      width  *= SUPERSAMPLE;
      height *= SUPERSAMPLE;
      /* allocate value array  */
      vals = (int *) xmalloc (sizeof (int) * width);
    }

  scanlines = (link_ptr *) xmalloc (sizeof (link_ptr) * height);
  for (i = 0; i < height; i++)
    scanlines[i] = NULL;

  for (i = 0; i < num_segs; i++)
    {
      gdisplay_untransform_coords (gdisp, segs[i].x1, segs[i].y1, &x1, &y1, TRUE, 0);
      gdisplay_untransform_coords (gdisp, segs[i].x2, segs[i].y2, &x2, &y2, TRUE, 0);

      if (free_options->antialias)
	convert_segment (scanlines, width, height,
			 x1 * SUPERSAMPLE, y1 * SUPERSAMPLE,
			 x2 * SUPERSAMPLE, y2 * SUPERSAMPLE);
      else
	convert_segment (scanlines, width, height, x1, y1, x2, y2);
    }

  dest = mask_buf_data (region->mask);

  for (i = 0; i < height; i++)
    {
      list = scanlines[i];

      /*  zero the vals array  */
      if (free_options->antialias && !(i % SUPERSAMPLE))
	memset (vals, 0, width * sizeof (int));

      while (list)
	{
	  x = (int) list->data;
	  list = next_item(list);
	  if (!list)
	      warning ("Cannot properly scanline convert polygon!\n");
	  else 
	    {
	      w = (int) list->data - x;
	      if (! free_options->antialias)
		gregion_add_segment (region, x, i, w, 255);
	      else
		for (j = 0; j < w; j++)
		  vals[j + x] += 255;
	      list = next_item (list);
	    }
	}

      if (free_options->antialias && !((i+1) % SUPERSAMPLE))
	for (j = 0; j < width; j += SUPERSAMPLE)
	  {
	    val = 0;
	    for (x = 0; x < SUPERSAMPLE; x++)
	      val += vals[j + x];
	    
	    *dest++ = val / SUPERSAMPLE2;
	  }
      
      free_list (scanlines[i]);
    }

  if (free_options->antialias)
    xfree (vals);

  xfree (scanlines);
  
  return region;
}


/*************************************/
/*  Polygonal selection apparatus  */

void
free_select_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay *gdisp;
  FreeSelect *free_sel;

  gdisp = (GDisplay *) gdisp_ptr;
  free_sel = (FreeSelect *) tool->private;

  free_sel->num_segs = 0;

  gdk_pointer_grab (gdisp->canvas->window, FALSE,
		    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
		    NULL, NULL, bevent->time);

  free_sel->replace = 0;

  tool->state = ACTIVE;
  tool->gdisp_ptr = gdisp_ptr;
  first_x = bevent->x;
  first_y = bevent->y;

  if (bevent->state & GDK_SHIFT_MASK)
    free_sel->op = ADD;
  else if (bevent->state & GDK_CONTROL_MASK)
    free_sel->op = SUB;
  else
    {
      if (gdisplay_mask_value (gdisp, bevent->x, bevent->y) > HALF_WAY)
	{
	  init_edit_selection (tool, gdisp_ptr, bevent);
	  return;
	}
      free_sel->op = ADD;
      free_sel->replace = 1;
    }

  draw_core_start (free_sel->core,
		   gdisp->canvas->window,
		   tool);
}

void
free_select_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  FreeSelect *free_sel;
  GDisplay *gdisp;
  GRegion *region;

  gdisp = (GDisplay *) gdisp_ptr;
  free_sel = (FreeSelect *) tool->private;

  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  draw_core_stop (free_sel->core, tool);

  tool->state = INACTIVE;

  /*  First take care of the case where the user "cancels" the action  */
  if (! (bevent->state & GDK_BUTTON3_MASK))
    {
      /*  Add one additional segment  */
      add_point (free_sel->num_segs, first_x, first_y);
      free_sel->num_segs++;

      /*  if applicable, replace the current selection  */
      /*  or insure that a floating selection is anchored down...  */
      if (free_sel->replace)
	gimage_mask_clear (gdisp->gimage);
      else
	gimage_mask_anchor (gdisp->gimage);

      region = scan_convert (free_sel->num_segs,
			     gdisp->gimage->region->width,
			     gdisp->gimage->region->height,
			     gdisp);

      if (region)
	{
	  if (free_options->feather)
	    gregion_feather (region,
			     gdisp->gimage->region,
			     free_options->feather_radius, free_sel->op);
	  else
	    gregion_combine_region (gdisp->gimage->region,
				    free_sel->op, region);
	  gregion_free (region);
	}

      /*  show selection on all views  */
      gdisplays_selection_visibility (gdisp->gimage->ID, 1);
      gdisplays_flush ();
    }
}

void
free_select_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
  FreeSelect *free_sel;
  GDisplay *gdisp;

  if (tool->state != ACTIVE)
    return;

  gdisp = (GDisplay *) gdisp_ptr;
  free_sel = (FreeSelect *) tool->private;

  if (add_point (free_sel->num_segs, mevent->x, mevent->y))
    {
      gdk_draw_segments (free_sel->core->win, free_sel->core->gc,
			 segs + free_sel->num_segs, 1);
      free_sel->num_segs ++;
    }
}


void
free_select_control (tool, action, gdisp_ptr)
     Tool *tool;
     int action;
     gpointer gdisp_ptr;
{
  FreeSelect * free_sel;

  free_sel = (FreeSelect *) tool->private;

  switch (action)
    {
    case PAUSE : 
      draw_core_pause (free_sel->core, tool);
      break;
    case RESUME :
      draw_core_resume (free_sel->core, tool);
      break;
    case HALT :
      draw_core_stop (free_sel->core, tool);
      break;
    }
}


void
free_select_draw (tool)
     Tool * tool;
{
  FreeSelect * free_sel;

  free_sel = (FreeSelect *) tool->private;

  gdk_draw_segments (free_sel->core->win, free_sel->core->gc,
		     segs, free_sel->num_segs);
}


Tool *
tools_new_free_select ()
{
  Tool * tool;
  FreeSelect * private;

  /*  The tool options  */
  if (!free_options)
    free_options = create_selection_options (FREE_SELECT);

  tool = (Tool *) xmalloc (sizeof (Tool));
  private = (FreeSelect *) xmalloc (sizeof (FreeSelect));

  private->core = draw_core_new (free_select_draw);
  private->num_segs = 0;

  tool->type = FREE_SELECT;
  tool->state = INACTIVE;
  tool->scroll_lock = 1;   /*  Do not allow scrolling  */
  tool->private = (void *) private;
  tool->button_press_func = free_select_button_press;
  tool->button_release_func = free_select_button_release;
  tool->motion_func = free_select_motion;
  tool->arrow_keys_func = edit_sel_arrow_keys_func;
  tool->control_func = free_select_control;

  return tool;
}


void
tools_free_free_select (tool)
     Tool * tool;
{
  FreeSelect * free_sel;

  free_sel = (FreeSelect *) tool->private;

  draw_core_free (free_sel->core);
  xfree (free_sel);
}
