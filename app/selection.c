/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "boundary.h"
#include "colormaps.h"
#include "errors.h"
#include "gdisplay.h"
#include "gdisplay_ops.h"
#include "gimage_mask.h"
#include "selection.h"
#include "marching_ants.h"


/*  The possible internal drawing states...  */
#define INVISIBLE         0
#define INTRO             1
#define MARCHING          2

#define INITIAL_DELAY     15  /* in milleseconds */


/* static function prototypes */
static GdkPixmap *create_cycled_ants_pixmap (GdkWindow *, gint);
static void    cycle_ant_colors            (Selection *);

static void    selection_draw              (Selection *);
static void    selection_transform_segs    (Selection *, BoundSeg *,
					    GdkSegment *, int, int);
static void    selection_generate_segs     (Selection *);
static void    selection_free_segs         (Selection *);
static gint    selection_march_ants        (gpointer);
static gint    selection_start_marching    (gpointer);

GdkPixmap *marching_ants[9] = { NULL };
GdkPixmap *cycled_ants_pixmap_rgb = NULL;
GdkPixmap *cycled_ants_pixmap_gray = NULL;


/*********************************/
/*  Local function definitions   */
/*********************************/

static GdkPixmap *
create_cycled_ants_pixmap (win, depth)
     GdkWindow *win;
     gint depth;
{
  GdkPixmap *pixmap;
  GdkGC *gc;
  GdkColor col;
  int i, j;

  pixmap = gdk_pixmap_new (win, 8, 8, depth);
  gc = gdk_gc_new (win);

  for (i = 0; i < 8; i++)
    for (j = 0; j < 8; j++)
      {
	col.pixel = marching_ants_pixels[((i + j) % 8)];
	gdk_gc_set_foreground (gc, &col);
	gdk_draw_line (pixmap, gc, i, j, i, j);
      }

  return pixmap;
}


static void
cycle_ant_colors (select)
     Selection * select;
{
  int i;
  int index;
  int r, g, b;
  guint32 pixel;

  for (i = 0; i < 8; i++)
    {
      index = (i + (8 - select->index)) % 8;
      r = (index < 4) ? 0 : (255 << 8);
      g = (index < 4) ? 0 : (255 << 8);
      b = (index < 4) ? 0 : (255 << 8);
      pixel = marching_ants_pixels[i];

      store_color (&pixel, r, g, b);
    }
}


static void
selection_draw (select)
     Selection * select;
{
  GdkGC *gc_1st, *gc_2nd;

  if (select->hidden)
    return;

  /*  If there is a secondary boundary, use gc2 for
   *  the floating selection and the gc for the original
   *  gimage mask.  This serves to emphasize the mask,
   *  which acts as the viewport to the floating selection.
   */
  if (select->segs2)
    {
      gc_1st = select->gc2;
      gc_2nd = select->gc;
    }
  else
    gc_1st = select->gc;

  if (select->segs)
    gdk_draw_segments (select->win, gc_1st,
		       select->segs, select->num_segs);
  if (select->segs2)
    gdk_draw_segments (select->win, gc_2nd,
		       select->segs2, select->num_segs2);
}


static void
selection_transform_segs (select, src_segs, dest_segs, num_segs, secondary)
     Selection *select;
     BoundSeg *src_segs;
     GdkSegment *dest_segs;
     int num_segs;
     int secondary;
{
  GDisplay * gdisp;
  int x, y;
  int i;

  gdisp = (GDisplay *) select->gdisp;

  for (i = 0; i < num_segs; i++)
    {
      gdisplay_transform_coords (gdisp, src_segs[i].x1, src_segs[i].y1,
				 &x, &y, !secondary);

      dest_segs[i].x1 = x; dest_segs[i].y1 = y;

      gdisplay_transform_coords (gdisp, src_segs[i].x2, src_segs[i].y2,
				 &x, &y, !secondary);

      dest_segs[i].x2 = x;  dest_segs[i].y2 = y;

      /*  If this segment is a closing segment
       *   we need to transform it in one display pixel
       */
      if (src_segs[i].open == 0)
	{
	  /*  If it is vertical  */
	  if (dest_segs[i].x1 == dest_segs[i].x2)
	    {
	      dest_segs[i].x1 -= 1;
	      dest_segs[i].x2 -= 1;
	    }
	  else
	    {
	      dest_segs[i].y1 -= 1;
	      dest_segs[i].y2 -= 1;
	    }
	}
    }
}


static void
selection_generate_segs (select)
     Selection * select;
{
  GDisplay * gdisp;
  BoundSeg *segs;
  int num_segs;
  
  gdisp = (GDisplay *) select->gdisp;

  /*  Ask the gimage for the boundary of its selected region...
   *  Then transform that information into a new buffer of XSegments
   */
  segs = gimage_mask_boundary (gdisp->gimage, &num_segs);
  if (num_segs)
    {
      select->segs = (GdkSegment *) xmalloc (sizeof (GdkSegment) * num_segs);
      select->num_segs = num_segs;
      selection_transform_segs (select, segs, select->segs, num_segs, 0);
    }
  else
    {
      select->segs = NULL;
      select->num_segs = 0;
    }

  /*  Possible secondary boundary representation  */
  segs = gimage_mask_2nd_boundary (gdisp->gimage, &num_segs);
  if (num_segs)
    {
      select->segs2 = (GdkSegment *) xmalloc (sizeof (GdkSegment) * num_segs);
      select->num_segs2 = num_segs;
      selection_transform_segs (select, segs, select->segs2, num_segs, 1);
    }
  else
    {
      select->segs2 = NULL;
      select->num_segs2 = 0;
    }
}


static void
selection_free_segs (select)
     Selection * select;
{
  if (select->segs)
    xfree (select->segs);
  if (select->segs2)
    xfree (select->segs2);

  select->segs      = NULL;
  select->num_segs  = 0;
  select->segs2     = NULL;
  select->num_segs2 = 0;
}


static gint
selection_start_marching (data)
     gpointer data;
{
  Selection * select;

  select = (Selection *) data;

  /*  if the RECALC bit is set, reprocess the boundaries  */
  if (select->recalc)
    {
      selection_free_segs (select);
      selection_generate_segs (select);
      /* Toggle the RECALC flag */
      select->recalc = FALSE;
    }

  /*  increment stipple index  */
  select->index++;
  if (select->index > 7)
    select->index = 0;

  /*  Make sure the state is set to marching  */
  select->state = MARCHING;

  /*  Draw the ants  */
  if (select->cycle)
    {
      cycle_ant_colors (select);
    }
  else
    {
      gdk_gc_set_stipple (select->gc, marching_ants[select->index]);
      gdk_gc_set_stipple (select->gc2, marching_ants[select->index]);
    }

  selection_draw (select);

  /*  Reset the timer  */
  select->timer = gtk_timeout_add (select->speed,
				   selection_march_ants,
				   (gpointer) select);

  return FALSE;
}

static gint
selection_march_ants (data)
     gpointer data;
{
  Selection * select;

  select = (Selection *) data;

  /*  increment stipple index  */
  select->index++;
  if (select->index > 7)
    select->index = 0;

  /*  Draw the ants  */
  if (select->cycle)
    {
      cycle_ant_colors (select);
    }
  else
    {
      gdk_gc_set_stipple (select->gc, marching_ants[select->index]);
      gdk_gc_set_stipple (select->gc2, marching_ants[select->index]);
      selection_draw (select);
    }

  return TRUE;
}

/*********************************/
/*  Public function definitions  */
/*********************************/

Selection *
selection_create (win, gdisp_ptr, size, width, speed)
     GdkWindow *win;
     gpointer gdisp_ptr;
     int size;
     int width;
     int speed;
{
  GdkColor fg, bg;
  GDisplay *gdisp;
  Selection * new;
  int i;

  gdisp = (GDisplay *) gdisp_ptr;

  new = (Selection *) xmalloc (sizeof (Selection));

  if (gimage_color (gdisp->gimage) && color_cycled)
    {
      new->cycle = 1;
      if (!cycled_ants_pixmap_rgb)
	cycled_ants_pixmap_rgb = create_cycled_ants_pixmap (win, gdisp->depth);
							    
      new->cycle_pix = cycled_ants_pixmap_rgb;
    }
  else if (gimage_gray (gdisp->gimage) && gray_cycled)
    {
      new->cycle = 1;
      if (!cycled_ants_pixmap_gray)
	cycled_ants_pixmap_gray = create_cycled_ants_pixmap (win, gdisp->depth);
      new->cycle_pix = cycled_ants_pixmap_gray;
    }
  else
    {
      new->cycle = 0;
      if (!marching_ants[0])
	for (i = 0; i < 9; i++)
	  marching_ants[i] = gdk_bitmap_create_from_data (win, (char*) ant_data[i], 8, 8);
    }

  new->win         = win;
  new->gdisp       = gdisp_ptr;
  new->segs        = NULL;
  new->num_segs    = 0;
  new->segs2       = NULL;
  new->num_segs2   = 0;
  new->state       = INVISIBLE;
  new->paused      = 0;
  new->recalc      = TRUE;
  new->index       = 0;
  new->speed       = speed;
  new->hidden      = FALSE;

  /*  create a new graphics context  */
  new->gc = gdk_gc_new (new->win);

  if (new->cycle)
    {
      new->gc2 = new->gc;
      gdk_gc_set_fill (new->gc, GDK_TILED);
      gdk_gc_set_tile (new->gc, new->cycle_pix);
      gdk_gc_set_line_attributes (new->gc, 1, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_MITER);
    }
  else
    {
      /*  get black and white pixels for this gdisplay  */
      fg.pixel = gdisplay_black_pixel ((GDisplay *) gdisp_ptr);
      bg.pixel = gdisplay_white_pixel ((GDisplay *) gdisp_ptr);

      gdk_gc_set_foreground (new->gc, &fg);
      gdk_gc_set_background (new->gc, &bg);

      /*  get black and white pixels for this gdisplay  */
      fg.pixel = gdisplay_black_pixel ((GDisplay *) gdisp_ptr);
      bg.pixel = gdisplay_gray_pixel ((GDisplay *) gdisp_ptr);

      /*  create a new graphics context  */
      new->gc2 = gdk_gc_new (new->win);
      gdk_gc_set_foreground (new->gc2, &fg);
      gdk_gc_set_background (new->gc2, &bg);

      gdk_gc_set_fill (new->gc, GDK_OPAQUE_STIPPLED);
      gdk_gc_set_fill (new->gc2, GDK_OPAQUE_STIPPLED);

      gdk_gc_set_line_attributes (new->gc, 1, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_MITER);
      gdk_gc_set_line_attributes (new->gc2, 1, GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_MITER);
    }

  return new;
}


void
selection_pause (select)
     Selection * select;
{
  if (select->state != INVISIBLE)
    gtk_timeout_remove (select->timer);

  select->paused ++;
}


void
selection_resume (select)
     Selection * select;
{
  if (select->paused == 1)
    select->timer = gtk_timeout_add (INITIAL_DELAY, selection_start_marching,
				     (gpointer) select);

  select->paused--;
}


void
selection_start (select, recalc)
     Selection * select;
     int recalc;
{
  /*  A call to selection_start with recalc == TRUE means that 
   *  we want to recalculate the selection boundary--usually
   *  after scaling or panning the display, or modifying the
   *  selection in some way.  If recalc == FALSE, the already
   *  calculated boundary is simply redrawn.
   */
  if (recalc)
    select->recalc = TRUE;

  /*  If this selection is paused, do not start it  */
  if (select->paused > 0)
    return;

  if (select->state != INVISIBLE)
    gtk_timeout_remove (select->timer);

  select->state = INTRO;  /*  The state before the first draw  */
  select->timer = gtk_timeout_add (INITIAL_DELAY, selection_start_marching,
				   (gpointer) select);
}


void
selection_invis (select)
     Selection * select;
{
  GDisplay * gdisp;
  int x1, y1, x2, y2;

  if (select->state == INVISIBLE)
    return;

  gtk_timeout_remove (select->timer);
  select->state = INVISIBLE;

  gdisp = (GDisplay *) select->gdisp;

  /*  Find the bounds of the selection  */
  if (gdisplay_mask_bounds (gdisp, &x1, &y1, &x2, &y2))
    gdisplay_expose_area (gdisp, x1, y1, (x2 - x1), (y2 - y1));
}


void
selection_hide (select, gdisp_ptr)
     Selection * select;
     void * gdisp_ptr;
{
  selection_invis (select);

  /*  toggle the visibility  */
  select->hidden = select->hidden ? FALSE : TRUE;

  selection_start (select, TRUE);
}


void
selection_free (select)
     Selection * select;
{
  if (select->state != INVISIBLE)
     gtk_timeout_remove (select->timer);

  if (select->gc)
    gdk_gc_destroy (select->gc);
  if (select->gc2 && !select->cycle)
    gdk_gc_destroy (select->gc2);

  selection_free_segs (select);
  xfree (select);
}

