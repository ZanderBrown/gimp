/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "autodialog.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "edit_selection.h"
#include "rect_select.h"
#include "rect_selectP.h"

#define NO  0
#define YES 1

extern SelectionOptions *ellipse_options;
static SelectionOptions *rect_options = NULL;


/*  Selection options dialog--for all selection tools  */

static gint
selection_toggle_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataInt *int_data;
  int *toggle_val;
  
  toggle_val = (int *) observer->user_data;
  int_data = (GtkDataInt *) data;
  
  if (int_data->value == GTK_STATE_ACTIVATED)
    *toggle_val = TRUE;
  else if (int_data->value == GTK_STATE_DEACTIVATED)
    *toggle_val = FALSE;

  return FALSE;
}

static gint
selection_scale_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  double *scale_val;

  scale_val = (double *) observer->user_data;
  adj_data = (GtkDataAdjustment *) data;
  *scale_val = adj_data->value;

  return FALSE;
}

static void
selection_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

SelectionOptions *
create_selection_options (tool_type)
     ToolType tool_type;
{
  SelectionOptions *options;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *antialias_toggle;
  GtkWidget *feather_toggle;
  GtkWidget *feather_scale;
  GtkData *feather_scale_data;

  /*  the new options structure  */
  options = (SelectionOptions *) xmalloc (sizeof (SelectionOptions));
  options->antialias = 1;
  options->feather = 0;
  options->feather_radius = 10.0;

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  
  /*  the main label  */
  switch (tool_type)
    {
    case RECT_SELECT:
      label = gtk_label_new ("Rectangular Select Options");
      break;
    case ELLIPSE_SELECT:
      label = gtk_label_new ("Elliptical Selection Options");
      break;
    case FREE_SELECT:
      label = gtk_label_new ("Free-hand Selection Options");
      break;
    case FUZZY_SELECT:
      label = gtk_label_new ("Fuzzy Selection Options");
      break;
    case BEZIER_SELECT:
      label = gtk_label_new ("Bezier Selection Options");
      break;
    case ISCISSORS:
      label = gtk_label_new ("Intelligent Scissors Options");
      break;
    case BY_COLOR_SELECT:
      label = gtk_label_new ("\"By Color\" Select Options");
      break;
    default:
      break;
    }

  gtk_box_pack (vbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  /*  the antialias toggle button  */
  antialias_toggle = gtk_check_button_new ();
  gtk_box_pack (vbox, antialias_toggle, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Antialiasing");
  gtk_label_set_alignment (label, 0.0, 0.5);
  gtk_container_add (antialias_toggle, label);
  gtk_widget_show (label);
  options->antialias_observer.update = selection_toggle_update;
  options->antialias_observer.disconnect = selection_disconnect;
  options->antialias_observer.user_data = &options->antialias;
  gtk_data_attach (gtk_button_get_state (antialias_toggle),
		   &options->antialias_observer);
  gtk_widget_show (antialias_toggle);

  /*  the feather toggle button  */
  feather_toggle = gtk_check_button_new ();
  gtk_box_pack (vbox, feather_toggle, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Feather");
  gtk_label_set_alignment (label, 0.0, 0.5);
  gtk_container_add (feather_toggle, label);
  gtk_widget_show (label);
  options->feather_observer.update = selection_toggle_update;
  options->feather_observer.disconnect = selection_disconnect;
  options->feather_observer.user_data = &options->feather;
  gtk_data_attach (gtk_button_get_state (feather_toggle),
		   &options->feather_observer);
  gtk_widget_show (feather_toggle);
  /*  deactivate the feathering toggle  */
  gtk_widget_activate (feather_toggle);

  /*  the feather radius scale  */
  hbox = gtk_hbox_new (FALSE, 4);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  
  label = gtk_label_new ("Feather Radius");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  feather_scale_data = gtk_data_adjustment_new (10.0, 0.0, 100.0, 1.0, 1.0, 1.0);
  feather_scale = gtk_hscale_new ((GtkDataAdjustment *) feather_scale_data);
  gtk_box_pack (hbox, feather_scale, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_scale_set_value_pos (feather_scale, GTK_POS_TOP);
  options->feather_radius_observer.update = selection_scale_update;
  options->feather_radius_observer.disconnect = selection_disconnect;
  options->feather_radius_observer.user_data = &options->feather_radius;
  gtk_data_attach (feather_scale_data, &options->feather_radius_observer);
  gtk_widget_show (feather_scale);
  gtk_widget_show (hbox);

  /*  Register this selection options widget with the main tools options dialog  */
  tools_register_options (tool_type, vbox);

  return options;
}


/*************************************/
/*  Rectangular selection apparatus  */

void
rect_select_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  RectSelect * rect_sel;
  int x, y;

  gdisp = (GDisplay *) gdisp_ptr;
  rect_sel = (RectSelect *) tool->private;

  gdisplay_untransform_coords (gdisp, bevent->x, bevent->y, &x, &y, TRUE, 0);

  rect_sel->x = x;
  rect_sel->y = y;
  rect_sel->w = 0;
  rect_sel->h = 0;
  rect_sel->replace = 0;

  gdk_pointer_grab (gdisp->canvas->window, FALSE,
		    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
		    NULL, NULL, bevent->time);

  tool->state = ACTIVE;
  tool->gdisp_ptr = gdisp_ptr;

  if (bevent->state & GDK_SHIFT_MASK)
    rect_sel->op = ADD;
  else if (bevent->state & GDK_CONTROL_MASK)
    rect_sel->op = SUB;
  else
    {
      if (gdisplay_mask_value (gdisp, bevent->x, bevent->y) > HALF_WAY)
	{
	  init_edit_selection (tool, gdisp_ptr, bevent);
	  return;
	}
      rect_sel->op = ADD;
      rect_sel->replace = 1;
    }

  draw_core_start (rect_sel->core, gdisp->canvas->window, tool);
}

void
rect_select_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  RectSelect * rect_sel;
  GDisplay * gdisp;
  GRegion * new_region;
  int x1, y1, x2, y2, w, h;

  gdisp = (GDisplay *) gdisp_ptr;
  rect_sel = (RectSelect *) tool->private;

  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  draw_core_stop (rect_sel->core, tool);
  tool->state = INACTIVE;

  /*  First take care of the case where the user "cancels" the action  */
  if (! (bevent->state & GDK_BUTTON3_MASK))
    {
      /*  if applicable, replace the current selection  */
      /*  or insure that a floating selection is anchored down...  */
      if (rect_sel->replace)
	gimage_mask_clear (gdisp->gimage);
      else
	gimage_mask_anchor (gdisp->gimage);

      x1 = (rect_sel->w < 0) ? rect_sel->x + rect_sel->w : rect_sel->x;
      y1 = (rect_sel->h < 0) ? rect_sel->y + rect_sel->h : rect_sel->y;
      w = (rect_sel->w < 0) ? -rect_sel->w : rect_sel->w;
      h = (rect_sel->h < 0) ? -rect_sel->h : rect_sel->h;
      x2 = x1 + w;
      y2 = y1 + h;

      switch (tool->type)
	{
	case RECT_SELECT:
	  /*  if feathering for rect, make a new region with the
	   *  rectangle and feather that with the old region
	   */
	  if (rect_options->feather)
	    {
	      new_region = gregion_new (gdisp->gimage->width,
					gdisp->gimage->height);
	      gregion_combine_rect (new_region, ADD,
				    x1, y1,
				    (x2 - x1), (y2 - y1));
	      gregion_feather (new_region, gdisp->gimage->region,
			       rect_options->feather_radius, rect_sel->op);

	      gregion_free (new_region);
	    }
	  else
	    gregion_combine_rect (gdisp->gimage->region, rect_sel->op,
				  x1, y1,
				  (x2 - x1), (y2 - y1));
	  break;

	case ELLIPSE_SELECT:
	  /*  if feathering for ellipse, make a new region with the
	   *  ellipse and feather that with the old region
	   */
	  if (ellipse_options->feather)
	    {
	      new_region = gregion_new (gdisp->gimage->width,
					gdisp->gimage->height);
	      gregion_combine_ellipse (new_region, ADD,
				       x1, y1,
				       (x2 - x1), (y2 - y1), ellipse_options->antialias);
	      gregion_feather (new_region, gdisp->gimage->region,
			       ellipse_options->feather_radius, rect_sel->op);

	      gregion_free (new_region);
	    }
	  else
	    gregion_combine_ellipse (gdisp->gimage->region, rect_sel->op,
				     x1, y1,
				     (x2 - x1), (y2 - y1), ellipse_options->antialias);
	  break;

	default:
	  break;
	}

      /*  show selection on all views  */
      gdisplays_selection_visibility (gdisp->gimage->ID, 1);
    }
}

void
rect_select_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
  RectSelect * rect_sel;
  GDisplay * gdisp;
  int x, y;

  if (tool->state != ACTIVE)
    return;

  gdisp = (GDisplay *) gdisp_ptr;
  rect_sel = (RectSelect *) tool->private;

  draw_core_pause (rect_sel->core, tool);
      
  gdisplay_untransform_coords (gdisp, mevent->x, mevent->y, &x, &y, TRUE, 0);
  rect_sel->w = (x - rect_sel->x);
  rect_sel->h = (y - rect_sel->y);

  /*  If both the control and shift keys are down,
      then make the rectangle square (or ellipse circular) */
  if (mevent->state & GDK_SHIFT_MASK && mevent->state & GDK_CONTROL_MASK)
    rect_sel->w = rect_sel->h = MAXIMUM (rect_sel->w, rect_sel->h);

  draw_core_resume (rect_sel->core, tool);
}

void
rect_select_draw (tool)
     Tool *tool;
{
  GDisplay * gdisp;
  RectSelect * rect_sel;
  int x1, y1, x2, y2;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  rect_sel = (RectSelect *) tool->private;

  x1 = MINIMUM (rect_sel->x, rect_sel->x + rect_sel->w);
  y1 = MINIMUM (rect_sel->y, rect_sel->y + rect_sel->h);
  x2 = MAXIMUM (rect_sel->x, rect_sel->x + rect_sel->w);
  y2 = MAXIMUM (rect_sel->y, rect_sel->y + rect_sel->h);

  gdisplay_transform_coords (gdisp, x1, y1, &x1, &y1, 0);
  gdisplay_transform_coords (gdisp, x2, y2, &x2, &y2, 0);

  gdk_draw_rectangle (rect_sel->core->win,
		      rect_sel->core->gc, 0,
		      x1, y1, (x2 - x1), (y2 - y1));
}

void
rect_select_control (tool, action, gdisp_ptr)
     Tool * tool;
     int action;
     gpointer gdisp_ptr;
{
  RectSelect * rect_sel;

  rect_sel = (RectSelect *) tool->private;

  switch (action)
    {
    case PAUSE : 
      draw_core_pause (rect_sel->core, tool);
      break;
    case RESUME :
      draw_core_resume (rect_sel->core, tool);
      break;
    case HALT :
      draw_core_stop (rect_sel->core, tool);
      break;
    }
}

Tool *
tools_new_rect_select ()
{
  Tool * tool;
  RectSelect * private;

  /*  The tool options  */
  if (!rect_options)
    rect_options = create_selection_options (RECT_SELECT);

  tool = (Tool *) xmalloc (sizeof (Tool));
  private = (RectSelect *) xmalloc (sizeof (RectSelect));

  private->core = draw_core_new (rect_select_draw);
  private->x = private->y = 0;
  private->w = private->h = 0;

  tool->type = RECT_SELECT;
  tool->state = INACTIVE;
  tool->scroll_lock = 0;  /*  Allow scrolling  */
  tool->private = (void *) private;
  tool->button_press_func = rect_select_button_press;
  tool->button_release_func = rect_select_button_release;
  tool->motion_func = rect_select_motion;
  tool->arrow_keys_func = edit_sel_arrow_keys_func;
  tool->control_func = rect_select_control;

  return tool;
}

void
tools_free_rect_select (tool)
     Tool * tool;
{
  RectSelect * rect_sel;

  rect_sel = (RectSelect *) tool->private;

  draw_core_free (rect_sel->core);
  xfree (rect_sel);
}
