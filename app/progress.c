/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "actionarea.h"
#include "errors.h"
#include "gimprc.h"
#include "interface.h"
#include "progress.h"

#define PROGRESS_WIDTH 250
#define PROGRESS_HEIGHT 20

static void progress_cancel_callback (GtkWidget *, gpointer, gpointer);
static void progress_cancel_all_callback (GtkWidget *, gpointer, gpointer);
static gint progress_pbar_events (GtkWidget *, GdkEvent *);
static void progress_draw (ProgressP);

static ActionAreaItem action_items[2] =
{
  { "Cancel", progress_cancel_callback, NULL, NULL },
  { "Cancel All", progress_cancel_all_callback, NULL, NULL },
};

ProgressP
progress_new (title, label, callback, callback_data)
     char *title, *label;
     CancelCallback callback;
     void *callback_data;
{
  ProgressP progress;
  GtkWidget *main_vbox;
  GtkWidget *hbox;
  GtkWidget *label_widget;
  GtkWidget *frame;
  GtkWidget *action_area;

  progress = (ProgressP) xmalloc (sizeof (_Progress));

  progress->amount = 0;
  progress->callback = callback;
  progress->callback_data = callback_data;
  progress->pixmap = NULL;
  progress->gc = NULL;

  progress->shell = gtk_window_new (title, GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_uposition (progress->shell, progress_x, progress_y);
  main_vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (progress->shell, main_vbox);

  label_widget = gtk_label_new (label);
  gtk_label_set_alignment (label_widget, 0.5, 0.5);
  gtk_box_pack (main_vbox, label_widget, TRUE, FALSE, 0, GTK_PACK_START);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_container_set_border_width (hbox, 0);
  gtk_box_pack (main_vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_type (frame, GTK_SHADOW_IN);
  gtk_box_pack (hbox, frame, FALSE, FALSE, 0, GTK_PACK_START);

  progress->pbar = gtk_drawing_area_new (PROGRESS_WIDTH, PROGRESS_HEIGHT,
					 progress_pbar_events,
					 GDK_EXPOSURE_MASK);
  gtk_widget_set_user_data (progress->pbar, progress);
  gtk_container_add (frame, progress->pbar);

  action_items[0].user_data = progress;
  action_items[1].user_data = progress;
  action_area = build_action_area (action_items, 2);
  gtk_box_pack (main_vbox, action_area, TRUE, TRUE, 2, GTK_PACK_START);

  gtk_widget_show (action_area);
  gtk_widget_show (progress->pbar);
  gtk_widget_show (frame);
  gtk_widget_show (hbox);
  gtk_widget_show (label_widget);
  gtk_widget_show (main_vbox);
  gtk_widget_show (progress->shell);

  return progress;
}

void
progress_free (progress)
     ProgressP progress;
{
  if (progress)
    {
      if (progress->gc)
	gdk_gc_destroy (progress->gc);
      if (progress->pixmap)
	gdk_pixmap_destroy (progress->pixmap);
      gtk_widget_destroy (progress->shell);
      xfree (progress);
    }
}

void
progress_update (progress, amount)
     ProgressP progress;
     long amount;
{
  if (progress)
    {
      progress->amount = amount;
      progress_draw (progress);
    }
}

static void
progress_cancel_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  ProgressP progress;

  progress = (ProgressP) client_data;
  if (progress && progress->callback)
    (* progress->callback) (progress->callback_data, (void*) 1);
}

static void
progress_cancel_all_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  ProgressP progress;

  progress = (ProgressP) client_data;
  if (progress && progress->callback)
    (* progress->callback) (progress->callback_data, (void*) 2);
}

static gint
progress_pbar_events (widget, event)
     GtkWidget *widget;
     GdkEvent *event;
{
  ProgressP progress;

  progress = gtk_widget_get_user_data (widget);

  switch (event->type)
    {
    case GDK_EXPOSE:
      if (!progress->pixmap)
	progress->pixmap = gdk_pixmap_new (progress->pbar->window,
					   progress->pbar->window->width,
					   progress->pbar->window->height, -1);
      if (!progress->gc)
	progress->gc = gdk_gc_new (progress->pbar->window);

      progress_draw (progress);
      break;

    default:
      break;
    }

  return FALSE;
}

static void
progress_draw (progress)
     ProgressP progress;
{
  GdkColor *win_bg;
  GdkWindow *window;
  int amount;

  if (progress->gc && progress->pixmap)
    {
      window = progress->pbar->window;

      win_bg = &(progress->pbar->style->background [GTK_STATE_NORMAL]);
      gdk_gc_set_foreground (progress->gc, win_bg);

      gdk_draw_rectangle (progress->pixmap, progress->gc, 1,
			  0, 0, window->width, window->height);

      amount = (progress->amount * window->width) / 100;
      gtk_draw_shadow (progress->pixmap,
		       progress->pbar->style->highlight_gc[GTK_STATE_NORMAL],
		       progress->pbar->style->shadow_gc[GTK_STATE_NORMAL],
		       NULL, GTK_SHADOW_OUT, 0, 0, amount, window->height,
		       progress->pbar->style->shadow_thickness);

      gdk_draw_pixmap (progress->pbar->window, progress->gc, progress->pixmap,
		       0, 0, 0, 0, window->width, window->height);
    }
}
