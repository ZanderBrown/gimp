/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __CHANNEL_H__
#define __CHANNEL_H__

#include "temp_buf.h"

/* structure declarations */

typedef struct _Channel Channel;

struct _Channel
  {
    char * name;                  /*  name of the channel          */

    unsigned char * data;	  /*  raw intensity values         */
    int visible;                  /*  controls visibility          */

    int width, height;            /*  size of channel              */
    int bytes;                    /*  bytes per pixel              */

    unsigned char col[3];         /*  RGB triplet for channel color*/
    int opacity;                  /*  Channel opacity              */

    int shmid;                    /*  Shared memory ID             */
    void * shmaddr;               /*  Shared memory address        */

    int ID;                       /*  provides a unique ID         */
    int gimage_ID;                /*  ID of gimage owner           */
  };


/* function declarations */

Channel *       channel_new (int, int, int, char *, int, unsigned char *);
Channel *       channel_copy (Channel *);
Channel *       channel_get_ID (int);
void            channel_delete (Channel *);
void            channel_apply_image (Channel *, int, int, int, int, TempBuf *);

/* access functions */

unsigned char * channel_data (Channel *);
int             channel_toggle_visibility (Channel *);


#endif /* __CHANNEL_H__ */

