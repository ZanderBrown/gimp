/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __INTERFACE_H__
#define __INTERFACE_H__

/* externed variables  */
extern GtkWidget *tool_widgets[];
extern GtkWidget *popup_shell;

/* function declarations */
void    create_toolbox        (void);
void    create_display_shell  (GtkWidget **, GtkWidget **, GtkWidget **,
			       GtkWidget **, GtkWidget **, GtkWidget **,
			       GtkWidget **, GtkDataAdjustment **, GtkDataAdjustment **,
			       GtkDataAdjustment **, GtkDataAdjustment **,
			       GtkEventFunction, GdkEventMask,
			       int, int, unsigned int *, char *, int);
void    map_dialog            (GtkWidget *);
GtkWidget *query_name_box     (char *, char *, GtkCallback, gpointer);
GtkWidget *message_box        (char *, GtkCallback, gpointer);

#endif /* INTERFACE_H */
