/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "appenv.h"
#include "actionarea.h"
#include "colormaps.h"
#include "cursorutil.h"
#include "fileops.h"
#include "gconvert.h"
#include "gdisplay_ops.h"
#include "general.h"
#include "gimage.h"
#include "gximage.h"
#include "interface.h"
#include "scale.h"


static void gdisplay_close_warning_callback (GtkWidget *, gpointer, gpointer);
static void gdisplay_cancel_warning_callback (GtkWidget *, gpointer, gpointer);
static void gdisplay_close_warning_dialog   (char *, GDisplay *);

/*
 *  This file is for operations on the gdisplay object
 */   

guint32
gdisplay_white_pixel (gdisp)
     GDisplay * gdisp;
{
  switch (gdisp->gimage->type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE:
    case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
      return color_white_pixel;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      return gray_white_pixel;
      break;
    default:
      return 0;
    }
}

guint32
gdisplay_gray_pixel (gdisp)
     GDisplay * gdisp;
{
  switch (gdisp->gimage->type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE:
    case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
      return color_gray_pixel;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      return gray_gray_pixel;
      break;
    default:
      return 0;
    }
}


guint32
gdisplay_black_pixel (gdisp)
     GDisplay * gdisp;
{
  switch (gdisp->gimage->type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE:
    case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
      return color_black_pixel;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      return gray_black_pixel;
      break;
    default:
      return 0;
    }
}



void
gdisplay_new_view (gdisp)
     GDisplay *gdisp;
{
  GImage *gimage;
  GDisplay *new_gdisp;

  /* make sure the image has been fully loaded... */
  if (!gdisp->gimage->busy)
    {
      gimage = gdisp->gimage;
      
      if (gimage)
	new_gdisp = gdisplay_new (gimage, gdisp->scale);
    }
}


void
gdisplay_close_window (gdisp)
     GDisplay *gdisp;
{
  GtkWidget *shell;
  
  /*  If the image has been modified, give the user a chance to save
   *  it before nuking it--this only applies if its the last view
   *  to an image canvas.  (a gimage with ref_count = 1)
   */
  if (gdisp->gimage->ref_count == 1 && gdisp->gimage->dirty > 0)
    gdisplay_close_warning_dialog (prune_filename (gdisp->gimage->filename), gdisp);
  else
    {
      shell = gdisp->shell;
      gdisplay_remove_and_delete (gdisp);
      gtk_widget_destroy (shell);
    }
}


void
gdisplay_shrink_wrap (gdisp)
     GDisplay *gdisp;
{
  gint x, y;
  gint disp_width, disp_height;
  gint width, height;
  gint shell_x, shell_y;
  gint shell_width, shell_height;
  gint max_auto_width, max_auto_height;
  gint border_x, border_y;
  int s_width, s_height;

  s_width = gdk_screen_width ();
  s_height = gdk_screen_height ();

  width = SCALE (gdisp, gdisp->gimage->width);
  height = SCALE (gdisp, gdisp->gimage->height);

  disp_width = gdisp->disp_width;
  disp_height = gdisp->disp_height;

  shell_width = gdisp->shell->window->width;
  shell_height = gdisp->shell->window->height;

  border_x = shell_width - disp_width;
  border_y = shell_height - disp_height;

  max_auto_width = (s_width - border_x) * 0.75;
  max_auto_height = (s_height - border_y) * 0.75;

  /*  If 1) the projected width & height are smaller than screen size, &
   *     2) the current display size isn't already the desired size, expand
   */
  if (((width + border_x) < s_width && (height + border_y) < s_height) &&
      (width != disp_width || height != disp_height))
    {
      gtk_widget_set_usize (gdisp->canvas,
			    width, height);

      gtk_widget_show (gdisp->canvas);

      gdk_window_get_origin (gdisp->shell->window, &shell_x, &shell_y);

      shell_width = width + border_x;
      shell_height = height + border_y;
      
      x = HIGHPASS (shell_x, BOUNDS (s_width - shell_width, 0, s_width));
      y = HIGHPASS (shell_y, BOUNDS (s_height - shell_height, 0, s_height));

      if (x != shell_x || y != shell_y)
	gdk_window_move (gdisp->shell->window, x, y);

      /*  Set the new disp_width and disp_height values  */
      gdisp->disp_width = width;
      gdisp->disp_height = height;
    }
  /*  If the projected width is greater than current, but less than
   *  3/4 of the screen size, expand automagically
   */
  else if ((width > disp_width || height > disp_height) &&
	   (disp_width < max_auto_width && disp_height < max_auto_height))
    {
      gtk_widget_set_usize (gdisp->canvas,
			    max_auto_width, max_auto_height);

      gtk_widget_show (gdisp->canvas);

      gdk_window_get_origin (gdisp->shell->window, &shell_x, &shell_y);
      
      shell_width = width + border_x;
      shell_height = height + border_y;
      
      x = HIGHPASS (shell_x, BOUNDS (s_width - shell_width, 0, s_width));
      y = HIGHPASS (shell_y, BOUNDS (s_height - shell_height, 0, s_height));

      if (x != shell_x || y != shell_y)
	gdk_window_move (gdisp->shell->window, x, y);

      /*  Set the new disp_width and disp_height values  */
      gdisp->disp_width = max_auto_width;
      gdisp->disp_height = max_auto_height;
    }
  /*  Otherwise, reexpose by hand to reflect changes  */
  else
    gdisplay_expose_full (gdisp);

  /*  If the width or height of the display has changed, recalculate
   *  the display offsets...
   */
  if (disp_width != gdisp->disp_width ||
      disp_height != gdisp->disp_height)
    {
      gdisp->offset_x += (disp_width - gdisp->disp_width) / 2;
      gdisp->offset_y += (disp_height - gdisp->disp_height) / 2;
    }
}


int
gdisplay_resize_image (gdisp)
     GDisplay *gdisp;
{
  int sx, sy;
  int width, height;

  /*  Calculate the width and height of the new canvas  */
  sx = SCALE (gdisp, gdisp->gimage->width);
  sy = SCALE (gdisp, gdisp->gimage->height);
  width = HIGHPASS (sx, gdisp->disp_width);
  height = HIGHPASS (sy, gdisp->disp_height);

  /* if the new dimensions of the ximage are different than the old...resize */
  if (width != gdisp->disp_width || height != gdisp->disp_height)
    {
      /*  adjust the gdisplay offsets -- we need to set them so that the
       *  center of our viewport is at the center of the image.
       */
      gdisp->offset_x = (sx / 2) - (width / 2);
      gdisp->offset_y = (sy / 2) - (height / 2);

      gdisp->disp_width = width;
      gdisp->disp_height = height;

      if (GTK_WIDGET_VISIBLE (gdisp->canvas))
	gtk_widget_hide (gdisp->canvas);

      gtk_widget_set_usize (gdisp->canvas,
			    gdisp->disp_width,
			    gdisp->disp_height);

      gtk_widget_show (gdisp->canvas);
    }

  return 1;
}


/*  convert temp buffers to gdisplays & vice-versa  */


int
temp_buf_to_gdisplay (buf)
     TempBuf * buf;
{
  /*  This function creates the new gdisplay  */
  if (buf->width && buf->height)
  {
    GImage * new_gimage;
    GDisplay * new_gdisp;
    int type;
    
    switch (buf->bytes)
      {
      case 1:
	type = GRAY_GIMAGE;
	break;
      case 2:
	type = GRAYA_GIMAGE;
	break;
      case 3:
	type = RGB_GIMAGE;
	break;
      case 4:
	type = RGBA_GIMAGE;
	break;
      default:
	return 0;
      }

    new_gimage = gimage_new (buf->width, buf->height, type);
    
    /*  copy the src image to the dest image  */
    memcpy (gimage_data (new_gimage), temp_buf_data (buf), 
	    buf->height * buf->width * buf->bytes);

    /*  create the new gdisplay  */
    new_gdisp = gdisplay_new (new_gimage, 0x0101);
    
    /*  paint the new gdisplay  */
    gdisplays_update_full (new_gimage->ID);
    gdisplays_flush ();

    return 1;
  }

  return 0;
}


/*  IMPORTANT:  this takes a GImage, not a GDisplay  */

TempBuf *
gdisplay_to_temp_buf (gimage)
     GImage * gimage;
{
  TempBuf * buf;

  buf = temp_buf_load (NULL, (void *) gimage, 0, 0, 
		       gimage->width, gimage->height);

  return buf;
}



/********************************************************
 *   Routines to query before closing a dirty image     *
 ********************************************************/

static void
gdisplay_close_warning_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  GDisplay *gdisp;
  GtkWidget *shell;
  GtkWidget *mbox;

  mbox = (GtkWidget *) client_data;
  gdisp = (GDisplay *) gtk_widget_get_user_data (mbox);
  shell = gdisp->shell;
  gdisplay_remove_and_delete (gdisp);
  gtk_widget_destroy (shell);
  gtk_widget_destroy (mbox);
}


static void
gdisplay_cancel_warning_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  GtkWidget *mbox;

  mbox = (GtkWidget *) client_data;
  gtk_widget_destroy (mbox);
}


static void
gdisplay_close_warning_dialog (image_name, gdisp)
     char *image_name;
     GDisplay *gdisp;
{
  static ActionAreaItem mbox_action_items[2] =
  {
    { "Close", gdisplay_close_warning_callback, NULL, NULL },
    { "Cancel", gdisplay_cancel_warning_callback, NULL, NULL }
  };
  GtkWidget *mbox;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *action_area;
  char * warning_buf;

  warning_buf = (char *) xmalloc (strlen (image_name) + 50);
  sprintf (warning_buf, "Changes made to %s.  Close anyway?", image_name);

  mbox = gtk_window_new (image_name, GTK_WINDOW_DIALOG);
  vbox = gtk_vbox_new (FALSE, 10);
  gtk_container_add (mbox, vbox);
  label = gtk_label_new (warning_buf);
  gtk_box_pack (vbox, label, TRUE, FALSE, 0, GTK_PACK_START);

  gtk_widget_set_user_data (mbox, gdisp);
  mbox_action_items[0].user_data = mbox;
  mbox_action_items[1].user_data = mbox;
  action_area = build_action_area (mbox_action_items, 2);
  gtk_box_pack (vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);

  gtk_widget_show (label);
  gtk_widget_show (action_area);
  gtk_widget_show (vbox);
  map_dialog (mbox);

  xfree (warning_buf);
}

