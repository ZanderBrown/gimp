/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <math.h>
#include "appenv.h"
#include "autodialog.h"
#include "boundary.h"
#include "draw_core.h"
#include "edit_selection.h"
#include "fuzzy_select.h"
#include "gimage_mask.h"
#include "gimprc.h"
#include "gdisplay.h"
#include "rect_select.h"

#define NO  0
#define YES 1

typedef struct _fuzzy_select FuzzySelect;

struct _fuzzy_select
{
  DrawCore *     core;         /*  Core select object                      */
  
  int            x, y;         /*  Point from which to execute seed fill  */
  int            last_x;       /*                                         */
  int            last_y;       /*  variables to keep track of sensitivity */
  int            threshold;    /*  threshold value for soft seed fill     */

  int            op;           /*  selection operation (ADD, SUB, etc)     */
  int            replace;      /*  replace current selection?              */
};


/*  fuzzy select action functions  */

static void   fuzzy_select_button_press   (Tool *, GdkEventButton *, gpointer);
static void   fuzzy_select_button_release (Tool *, GdkEventButton *, gpointer);
static void   fuzzy_select_motion         (Tool *, GdkEventMotion *, gpointer);
static void   fuzzy_select_draw           (Tool *);
static void   fuzzy_select_control        (Tool *, int, gpointer);

/*  fuzzy select action functions  */
static GdkSegment *   fuzzy_select_calculate (Tool *, void *, int *);


/*  XSegments which make up the fuzzy selection boundary  */

static GdkSegment *segs = NULL;
static int         num_segs = 0;
static GRegion *   fuzzy_region = NULL;
static SelectionOptions *fuzzy_options = NULL;


/*************************************/
/*  Fuzzy selection apparatus  */

static int
is_pixel_sufficiently_different (col1, col2, antialias, threshold, bytes, has_alpha)
     unsigned char * col1;
     unsigned char * col2;
     int antialias;
     int threshold;
     int bytes;
     int has_alpha;
{
  int diff;
  int max;
  int b;
  int alpha;

  max = 0;
  alpha = (has_alpha) ? bytes - 1 : bytes;

  /*  if there is an alpha channel, never select transparent regions  */
  if (has_alpha && col2[alpha] == 0)
    return 0;

  for (b = 0; b < alpha; b++)
    {
      diff = col1[b] - col2[b];
      diff = abs (diff);
      if (diff > max)
	max = diff;
    }

  if (antialias)
    {
      float aa;

      aa = 1.5 - ((float) max / threshold);
      if (aa <= 0)
	return 0;
      else if (aa < 0.5)
	return (unsigned char) (aa * 512);
      else
	return 255;
    }
  else
    {
      if (max > threshold)
	return 0;
      else
	return 255;
    }
}

static int
find_contiguous_segment (col, line, mask, width, bytes, has_alpha, 
			 antialias, threshold, initial, start, end)
     unsigned char * col;
     unsigned char * line;
     unsigned char * mask;
     int width;
     int bytes;
     int has_alpha;
     int antialias;
     int threshold;
     int initial;
     int *start;
     int *end;
{
  unsigned char * col2, * m;
  unsigned char diff;

  /* check the starting pixel */
  if (! (diff = is_pixel_sufficiently_different (col, line, antialias,
						 threshold, bytes, has_alpha)))
    return FALSE;

  *mask = diff;
  col2 = line - bytes;
  m = mask - 1;
  *start = initial;

  while (*start > 0 && diff)
    {
      diff = is_pixel_sufficiently_different (col, col2, antialias,
					      threshold, bytes, has_alpha);
      if ((*m-- = diff))
	{
	  col2 -= bytes;
	  (*start)--;
	}
    }

  diff = 1;
  col2 = line + bytes;
  m = mask + 1;
  *end = initial + 1;

  while (*end < width && diff)
    {
      diff = is_pixel_sufficiently_different (col, col2, antialias,
					      threshold, bytes, has_alpha);
      if ((*m++ = diff))
	{
	  col2 += bytes;
	  (*end)++;
	}
    }

  return TRUE;
}

void
find_contiguous_region (region, gimage, antialias, threshold, x, y, col)
     GRegion * region;
     GImage * gimage;
     int antialias;
     int threshold;
     int x, y;
     unsigned char * col;
{
  int start, end, i;
  unsigned char * mask;
  unsigned char * line;

  if (threshold == 0) threshold = 1;
  if (y >= gimage_height (gimage) || y < 0) return;

  mask = mask_buf_data (region->mask) + (region->mask->width * y) + x;
  if (*mask) return;

  line = gimage_data (gimage) + gimage_bytes (gimage) * 
    (gimage_width (gimage) * y + x);
  if (!col) col = line;

  if ( ! find_contiguous_segment (col, line, mask, gimage_width (gimage),
				  gimage_bytes (gimage),
				  gimage_has_alpha (gimage),
				  antialias, threshold, x, &start, &end))
    return;

  for (i = start; i < end; i++)
    {
      find_contiguous_region (region, gimage, antialias, threshold, i, y - 1, col);
      find_contiguous_region (region, gimage, antialias, threshold, i, y + 1, col);
    }
}


/*  fuzzy select action functions  */

static void
fuzzy_select_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay *gdisp;
  FuzzySelect *fuzzy_sel;

  gdisp = (GDisplay *) gdisp_ptr;
  fuzzy_sel = (FuzzySelect *) tool->private;

  fuzzy_sel->x = bevent->x;
  fuzzy_sel->y = bevent->y;
  fuzzy_sel->last_x = fuzzy_sel->x;
  fuzzy_sel->last_y = fuzzy_sel->y;
  fuzzy_sel->threshold = default_threshold;
  fuzzy_sel->replace = 0;

  gdk_pointer_grab (gdisp->canvas->window, FALSE,
		    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
		    NULL, NULL, bevent->time);
      
  tool->state = ACTIVE;
  tool->gdisp_ptr = gdisp_ptr;

  if (bevent->state & GDK_SHIFT_MASK)
    fuzzy_sel->op = ADD;
  else if (bevent->state & GDK_CONTROL_MASK)
    fuzzy_sel->op = SUB;
  else
    {
      if (gdisplay_mask_value (gdisp, bevent->x, bevent->y) > HALF_WAY)
	{
	  init_edit_selection (tool, gdisp_ptr, bevent);
	  return;
	}
      fuzzy_sel->op = ADD;
      fuzzy_sel->replace = 1;
    }

  /*  calculate the region boundary  */
  segs = fuzzy_select_calculate (tool, gdisp_ptr, &num_segs);

  draw_core_start (fuzzy_sel->core,
		   gdisp->canvas->window,
		   tool);
}

static void
fuzzy_select_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  FuzzySelect * fuzzy_sel;
  GDisplay * gdisp;

  gdisp = (GDisplay *) gdisp_ptr;
  fuzzy_sel = (FuzzySelect *) tool->private;

  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  draw_core_stop (fuzzy_sel->core, tool);
  tool->state = INACTIVE;

  /*  First take care of the case where the user "cancels" the action  */
  if (! (bevent->state & GDK_BUTTON3_MASK))
    {
      /*  if applicable, replace the current selection  */
      /*  or insure that a floating selection is anchored down...  */
      if (fuzzy_sel->replace)
	gimage_mask_clear (gdisp->gimage);
      else
	gimage_mask_anchor (gdisp->gimage);

      if (fuzzy_options->feather)
	gregion_feather (fuzzy_region,
			 gdisp->gimage->region,
			 fuzzy_options->feather_radius, fuzzy_sel->op);
      else
	gregion_combine_region (gdisp->gimage->region, fuzzy_sel->op,
				fuzzy_region);

      /*  show selection on all views  */
      gdisplays_selection_visibility (gdisp->gimage->ID, 1);
      gdisplays_flush ();

      /*  adapt the threshold based on the final value of this use  */
      default_threshold = fuzzy_sel->threshold;
    }

  /*  free the fuzzy region struct  */
  gregion_free (fuzzy_region);
  fuzzy_region = NULL;

  /*  If the segment array is allocated, free it  */
  if (segs)
    xfree (segs);
  segs = NULL;
}

static void
fuzzy_select_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
  FuzzySelect * fuzzy_sel;
  GdkSegment * new_segs;
  int num_new_segs;
  int diff, diff_x, diff_y;

  if (tool->state != ACTIVE)
    return;

  fuzzy_sel = (FuzzySelect *) tool->private;

  diff_x = mevent->x - fuzzy_sel->last_x;
  diff_y = mevent->y - fuzzy_sel->last_y;

  diff = ((ABS (diff_x) > ABS (diff_y)) ? diff_x : diff_y) / 2;

  fuzzy_sel->last_x = mevent->x;
  fuzzy_sel->last_y = mevent->y;

  fuzzy_sel->threshold += diff;
  fuzzy_sel->threshold = BOUNDS (fuzzy_sel->threshold, 0, 255);

  /*  calculate the new fuzzy boundary  */
  new_segs = fuzzy_select_calculate (tool, gdisp_ptr, &num_new_segs);

  /*  stop the current boundary  */
  draw_core_pause (fuzzy_sel->core, tool);

  /*  make sure the XSegment array is freed before we assign the new one  */
  if (segs)
    xfree (segs);
  segs = new_segs;
  num_segs = num_new_segs;

  /*  start the new boundary  */
  draw_core_resume (fuzzy_sel->core, tool);
}

static GdkSegment *
fuzzy_select_calculate (tool, gdisp_ptr, nsegs)
     Tool *tool;
     void *gdisp_ptr;
     int *nsegs;
{
  PixelRegion maskPR;
  FuzzySelect *fuzzy_sel;
  GDisplay *gdisp;
  GRegion *new;
  GdkSegment *segs;
  BoundSeg *bsegs;
  unsigned char *start;
  int i, x, y;

  fuzzy_sel = (FuzzySelect *) tool->private;
  gdisp = (GDisplay *) gdisp_ptr;

  new = gregion_new (gimage_width (gdisp->gimage), 
		     gimage_height (gdisp->gimage));
  if (fuzzy_region)
    gregion_free (fuzzy_region);
  fuzzy_region = new;
  
  gdisplay_untransform_coords (gdisp, fuzzy_sel->x,
			       fuzzy_sel->y, &x, &y, FALSE, 1);
  
  start = gimage_data (gdisp->gimage) + 
    gimage_bytes (gdisp->gimage) * (gimage_width (gdisp->gimage) * y + x);
  find_contiguous_region (fuzzy_region, gdisp->gimage, fuzzy_options->antialias,
			  fuzzy_sel->threshold, x, y, start);
  
  /*  calculate and allocate a new XSegment array which represents the boundary
   *  of the color-contiguous region
   */
  maskPR.bytes = 1;
  maskPR.w = fuzzy_region->mask->width;
  maskPR.h = fuzzy_region->mask->height;
  maskPR.rowstride = maskPR.w * maskPR.bytes;
  maskPR.data = mask_buf_data (fuzzy_region->mask);

  bsegs = find_mask_boundary (&maskPR, nsegs, 0, 0, 
			      fuzzy_region->mask->width, 
			      fuzzy_region->mask->height);

  segs = (GdkSegment *) xmalloc (sizeof (GdkSegment) * *nsegs);

  for (i = 0; i < *nsegs; i++)
    {
      gdisplay_transform_coords (gdisp, bsegs[i].x1, bsegs[i].y1, &x, &y, 1);
      segs[i].x1 = x;  segs[i].y1 = y;
      gdisplay_transform_coords (gdisp, bsegs[i].x2, bsegs[i].y2, &x, &y, 1);
      segs[i].x2 = x;  segs[i].y2 = y;
    }

  /*  free boundary segments  */
  xfree (bsegs);

  return segs;
}

static void
fuzzy_select_draw (tool)
     Tool * tool;
{
  FuzzySelect * fuzzy_sel;

  fuzzy_sel = (FuzzySelect *) tool->private;

  if (segs)
    gdk_draw_segments (fuzzy_sel->core->win, fuzzy_sel->core->gc, segs, num_segs);
}

static void
fuzzy_select_control (tool, action, gdisp_ptr)
     Tool *tool;
     int action;
     gpointer gdisp_ptr;
{
  FuzzySelect * fuzzy_sel;

  fuzzy_sel = (FuzzySelect *) tool->private;

  switch (action)
    {
    case PAUSE : 
      draw_core_pause (fuzzy_sel->core, tool);
      break;
    case RESUME :
      draw_core_resume (fuzzy_sel->core, tool);
      break;
    case HALT :
      draw_core_stop (fuzzy_sel->core, tool);
      break;
    }
}

Tool *
tools_new_fuzzy_select ()
{
  Tool * tool;
  FuzzySelect * private;

  /*  The tool options  */
  if (!fuzzy_options)
    fuzzy_options = create_selection_options (FUZZY_SELECT);

  tool = (Tool *) xmalloc (sizeof (Tool));
  private = (FuzzySelect *) xmalloc (sizeof (FuzzySelect));

  private->core = draw_core_new (fuzzy_select_draw);

  tool->type = FUZZY_SELECT;
  tool->state = INACTIVE;
  tool->scroll_lock = 1;  /*  Disallow scrolling  */
  tool->private = (void *) private;
  tool->button_press_func = fuzzy_select_button_press;
  tool->button_release_func = fuzzy_select_button_release;
  tool->motion_func = fuzzy_select_motion;
  tool->arrow_keys_func = edit_sel_arrow_keys_func;
  tool->control_func = fuzzy_select_control;

  return tool;
}

void
tools_free_fuzzy_select (tool)
     Tool * tool;
{
  FuzzySelect * fuzzy_sel;

  fuzzy_sel = (FuzzySelect *) tool->private;
  draw_core_free (fuzzy_sel->core);
  xfree (fuzzy_sel);
}


