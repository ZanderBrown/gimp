/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdlib.h>
#include "appenv.h"
#include "colormaps.h"
#include "color_select.h"
#include "palette.h"
#include "visual.h"

#define BORDER 2
#define PADX BORDER
#define PADY BORDER

/*  Global variables  */
int active_color = 0;

/*  Static variables  */
static GdkGC *color_area_gc = NULL;
static GtkWidget *color_area = NULL;
static GdkPixmap *color_area_pixmap = NULL;
static ColorSelectP color_select = NULL;
static int color_select_active = 0;
static int edit_color;

/*  Local functions  */
static int
color_area_target (x, y)
     int x, y;
{
  int unit_x, unit_y;
  int rect_w, rect_h;
  int width, height;

  width = color_area->window->width;
  height = color_area->window->height;

  unit_x = (width - PADX * 2) / 3;
  unit_y = (height - PADY * 2) / 3;
  rect_w = unit_x * 2;
  rect_h = unit_y * 2;

  /*  foreground active  */
  if (x > PADX && x < (PADX + rect_w) &&
      y > PADY && y < (PADY + rect_h))
    return 0;
  else if (x > (PADX + unit_x) && x < (PADX + rect_w + unit_x) &&
	   y > (PADY + unit_y) && y < (PADY + rect_h + unit_y))
    return 1;
  else
    return -1;
}

static void
color_area_draw ()
{
  GdkColor *win_bg;
  GdkColor fg, bg, bd;
  int unit_x, unit_y;
  int rect_w, rect_h;
  int width, height;

  width = color_area_pixmap->width;
  height = color_area_pixmap->height;
  win_bg = &(color_area->style->background [GTK_STATE_NORMAL]);
  fg.pixel = foreground_pixel;
  bg.pixel = background_pixel;
  bd.pixel = color_black_pixel;

  unit_x = (width - PADX * 2) / 3;
  unit_y = (height - PADY * 2) / 3;
  rect_w = unit_x * 2;
  rect_h = unit_y * 2;

  gdk_gc_set_foreground (color_area_gc, win_bg);
  gdk_draw_rectangle (color_area_pixmap, color_area_gc, 1,
		      0, 0, width, height);

  /*  foreground active  */
  if (active_color == 0) 
    {
      gdk_gc_set_foreground (color_area_gc, &bg);
      gdk_draw_rectangle (color_area_pixmap, color_area_gc, 1,
			  PADX + unit_x, PADY + unit_y, rect_w, rect_h);

      gdk_gc_set_foreground (color_area_gc, &bd);
      gdk_draw_rectangle (color_area_pixmap, color_area_gc, 0,
			  PADX - BORDER, PADY - BORDER,
			  rect_w + BORDER*2 - 1, rect_h + BORDER*2 - 1);
      gdk_gc_set_foreground (color_area_gc, &fg);
      gdk_draw_rectangle (color_area_pixmap, color_area_gc, 1,
			  PADX, PADY, rect_w, rect_h);
    }
  else
    {
      gdk_gc_set_foreground (color_area_gc, &bd);
      gdk_draw_rectangle (color_area_pixmap, color_area_gc, 0,
			  PADX + unit_x - BORDER, PADY + unit_y - BORDER,
			  rect_w + BORDER*2 - 1, rect_h + BORDER*2 - 1);
      gdk_gc_set_foreground (color_area_gc, &bg);
      gdk_draw_rectangle (color_area_pixmap, color_area_gc, 1,
			  PADX + unit_x, PADY + unit_y, rect_w, rect_h);

      gdk_gc_set_foreground (color_area_gc, &fg);
      gdk_draw_rectangle (color_area_pixmap, color_area_gc, 1,
			  PADX, PADY, rect_w, rect_h);
    }

  gdk_draw_pixmap (color_area->window, color_area_gc, color_area_pixmap,
		   0, 0, 0, 0, width, height);
}

static void 
color_area_select_callback (r, g, b, cancelled)
     int r, g, b;
     int cancelled;
{
  if (color_select)
    {
      color_select_hide (color_select);
      color_select_active = 0;

      if (! cancelled)
	{
	  if (edit_color == FOREGROUND)
	    palette_set_foreground (r, g, b);
	  else
	    palette_set_background (r, g, b);
	}
    }
}

static void 
color_area_edit ()
{
  unsigned char r, g, b;

  if (active_color == FOREGROUND)
    {
      palette_get_foreground (&r, &g, &b);
      edit_color = FOREGROUND;
    }
  else
    {
      palette_get_background (&r, &g, &b);
      edit_color = BACKGROUND;
    }

  if (! color_select)
    {
      color_select = color_select_new (r, g, b, color_area_select_callback);
      color_select_active = 1;
    }
  else
    {
      if (! color_select_active)
	color_select_show (color_select);
      color_select_set_color (color_select, r, g, b, 1);
    }
}

static gint
color_area_events (GtkWidget *widget, 
		   GdkEvent  *event)
{
  GdkEventButton *bevent;
  int target;
  unsigned char fg_r, fg_g, fg_b;
  unsigned char bg_r, bg_g, bg_b;

  switch (event->type)
    {
    case GDK_EXPOSE:
      if (!color_area_pixmap)
	color_area_pixmap = gdk_pixmap_new (widget->window,
					    widget->window->width,
					    widget->window->height, -1);
      if (!color_area_gc)
	color_area_gc = gdk_gc_new (widget->window);

      color_area_draw ();
      break;

    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;
      
      if (bevent->button == 1)
	{
	  target = color_area_target (bevent->x, bevent->y);
	  if (target >= 0)
	    {
	      if (target == active_color)
		color_area_edit ();
	      else
		{
		  active_color = target;
		  color_area_draw ();
		}
	    }
	  else
	    {
	      palette_get_foreground (&fg_r, &fg_g, &fg_b);
	      palette_get_background (&bg_r, &bg_g, &bg_b);
	      
	      palette_set_foreground (bg_r, bg_g, bg_b);
	      palette_set_background (fg_r, fg_g, fg_b);
	      
	      color_area_draw ();
	    }
	}
      break;
    
    default:
      break;
    }

  return FALSE;
}

GtkWidget *
color_area_create (width, height)
     int width, height;
{
  color_area = gtk_drawing_area_new (width, height, color_area_events,
				     GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK);

  return color_area;
}

void
color_area_update (void)
{
  color_area_draw ();
}

