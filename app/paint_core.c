/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "appenv.h"
#include "brushes.h"
#include "errors.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "layers_dialog.h"
#include "paint_funcs.h"
#include "paint_core.h"
#include "selection.h"
#include "tools.h"
#include "undo.h"

#define    MIN_BLOCK_WIDTH  64
#define    MIN_BLOCK_HEIGHT 64
#define    SQR(x) ((x) * (x))
#define    EPSILON  0.00001


/*  local function prototypes  */
static void  paint_core_subsample_mask   (Tool *, MaskBuf *, MaskBuf *);
static void  paint_core_apply_brush_mask (Tool *, TempBuf *, int, int, int);
static void  paint_core_paste            (Tool *, TempBuf *, int, int, int);
static void  paint_core_interpolate      (Tool *);
static void  set_undo_blocks             (GImage *, int, int, int, int);
static void  restore_original_image      (GImage *, int, int, int, int);
static void  reconstruct_original_image  (Tool *, TempBuf *);
static void  set_canvas_blocks           (Tool *, TempBuf *, int);
static void  reconstruct_canvas_mask     (Tool *, TempBuf *);


/***********************************************************************/


/*  undo blocks variables  */
static TempBuf **  undo_blocks = NULL;
static TempBuf **  canvas_blocks = NULL;
static int         block_width;
static int         block_height;
static int         horz_blocks;
static int         vert_blocks;

/*  undo blocks utility functions  */
static TempBuf **  allocate_blocks        (TempBuf **, int, int, int, int);
static void        free_blocks            (TempBuf **);


/***********************************************************************/


/*  paint buffers variables  */
static TempBuf *   orig_buf = NULL;
static TempBuf *   canvas_buf = NULL;

/*  paint buffers utility functions  */
static void        free_paint_buffers     (void);


/***********************************************************************/


#define KERNEL_WIDTH   3
#define KERNEL_HEIGHT  3

/*  Brush pixel subsampling kernels  */
static int subsample[4][4][9] = 
{
  {
    { 16, 48, 0, 48, 144, 0, 0, 0, 0 },
    { 0, 64, 0, 0, 192, 0, 0, 0, 0 },
    { 0, 48, 16, 0, 144, 48, 0, 0, 0 },
    { 0, 32, 32, 0, 96, 96, 0, 0, 0 },
  },
  {
    { 0, 0, 0, 64, 192, 0, 0, 0, 0 },
    { 0, 0, 0, 0, 256, 0, 0, 0, 0 },
    { 0, 0, 0, 0, 192, 64, 0, 0, 0 },
    { 0, 0, 0, 0, 128, 128, 0, 0, 0 },
  },
  {
    { 0, 0, 0, 48, 144, 0, 16, 48, 0 },
    { 0, 0, 0, 0, 192, 0, 0, 64, 0 },
    { 0, 0, 0, 0, 144, 48, 0, 48, 16 },
    { 0, 0, 0, 0, 96, 96, 0, 32, 32 },
  },
  {
    { 0, 0, 0, 32, 96, 0, 32, 96, 0 },
    { 0, 0, 0, 0, 128, 0, 0, 128, 0 },
    { 0, 0, 0, 0, 96, 32, 0, 96, 32 },
    { 0, 0, 0, 0, 64, 64, 0, 64, 64 },
  },
};

void
paint_core_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  PaintCore * paint_core;
  GDisplay * gdisp;
  GBrushP brush;
  int draw_line = 0;

  gdisp = (GDisplay *) gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  gdisplay_untransform_coords_f (gdisp, (double) bevent->x, (double) bevent->y,
				 &paint_core->curx, &paint_core->cury, 1);
  paint_core->state = bevent->state;

  /*  Each buffer is the same size as the maximum bounds of the active brush... */
  if (!(brush = get_active_brush ()))
    {
      warning ("No brushes available for use with this tool.");
      return;
    }

  paint_core->spacing = 
    (double) MAXIMUM (brush->mask->width, brush->mask->height) * 
    ((double) get_brush_spacing () / 100.0);
  if (paint_core->spacing < 1.0)
    paint_core->spacing = 1.0;
  paint_core->brush_mask = brush->mask;

  /*  free the block structures  */
  if (undo_blocks)
    free_blocks (undo_blocks);
  if (canvas_blocks)
    free_blocks (canvas_blocks);

  /*  Allocate the undo structure  */
  undo_blocks = allocate_blocks (undo_blocks,
				 brush->mask->width, brush->mask->height,
				 gimage_width (gdisp->gimage),
				 gimage_height (gdisp->gimage));

  /*  Allocate the canvas blocks structure  */
  canvas_blocks = allocate_blocks (canvas_blocks,
				   brush->mask->width, brush->mask->height,
				   gimage_width (gdisp->gimage),
				   gimage_height (gdisp->gimage));

  /*  Get the initial undo extents  */
  paint_core->x1 = paint_core->x2 = paint_core->curx;
  paint_core->y1 = paint_core->y2 = paint_core->cury;
  paint_core->distance = 0.0;

  /*  if this is a new image, reinit the core vals  */
  if (gdisp_ptr != tool->gdisp_ptr ||
      ! (bevent->state & GDK_SHIFT_MASK))
    {
      /*  initialize some values  */
      paint_core->startx = paint_core->lastx = paint_core->curx;
      paint_core->starty = paint_core->lasty = paint_core->cury;
    }
  /*  If shift is down and this is not the first paint
   *  stroke, then draw a line from the last coords to the pointer
   */
  else if (bevent->state & GDK_SHIFT_MASK)
    {
      draw_line = 1;
      paint_core->startx = paint_core->lastx;
      paint_core->starty = paint_core->lasty;
    }

  tool->state = ACTIVE;
  tool->gdisp_ptr = gdisp_ptr;
  tool->paused_count = 0;

  /*  pause the current selection and grab the pointer  */
  gdisplays_selection_visibility (gdisp->gimage->ID, 2);

  gdk_pointer_grab (gdisp->canvas->window, FALSE,
		    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
		    NULL, NULL, bevent->time);

  /*  Let the specific painting function initialize itself  */
  (* paint_core->paint_func) (tool, INIT_PAINT);

  /*  Paint to the image  */
  if (draw_line)
    {
      paint_core_interpolate (tool);
      paint_core->lastx = paint_core->curx;
      paint_core->lasty = paint_core->cury;
    }
  else
    (* paint_core->paint_func) (tool, MOTION_PAINT);

  gdisplays_flush ();
}

void
paint_core_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  GImage * gimage;
  Channel * channel;
  Layer * layer;
  PaintCore * paint_core;
  PaintUndo * pu;
  TempBuf * undo_buf;

  gdisp = (GDisplay *) gdisp_ptr;
  gimage = gdisp->gimage;
  paint_core = (PaintCore *) tool->private;

  /*  resume the current selection and ungrab the pointer  */
  gdisplays_selection_visibility (gdisp->gimage->ID, 3);

  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  /*  Let the specific painting function finish up  */
  (* paint_core->paint_func) (tool, FINISH_PAINT);

  /*  Set tool state to inactive -- no longer painting */
  tool->state = INACTIVE;

  /*  Determine if any part of the image has been altered--
   *  if nothing has, then just return...
   */
  if ((paint_core->x2 == paint_core->x1) || (paint_core->y2 == paint_core->y1))
    return;

  undo_push_group_start (PAINT_CORE_UNDO);

  pu = (PaintUndo *) xmalloc (sizeof (PaintUndo));
  pu->tool_type = tool->type;
  pu->lastx = paint_core->startx;
  pu->lasty = paint_core->starty;

  /*  Push a paint undo  */
  undo_push_paint (tool->ID, pu);

  /*  Get the undo image  */
  undo_buf = temp_buf_new ((paint_core->x2 - paint_core->x1),
			   (paint_core->y2 - paint_core->y1),
			   gimage_bytes (gimage),
			   paint_core->x1, paint_core->y1, NULL);

  reconstruct_original_image (tool, undo_buf);

  /*  push an undo  */
  if (gimage->active_channel >= 0)
    {
      if ((channel = channel_get_ID (gimage->active_channel)))
	channel_apply_image (channel, 0, 0, 0, 0, undo_buf);
    }
  else if (gimage->floating_sel >= 0)
    {
      /*  do not anchor layer  (since it's floating) */
      if ((layer = layer_get_ID (gimage->floating_sel)))
	layer_apply_image (layer, 0, 0, 0, 0, undo_buf, 0);
    }
  else
    {
      /*  anchor layer  */
      if ((layer = layer_get_ID (gimage->active_layer)))
	layer_apply_image (layer, 0, 0, 0, 0, undo_buf, 1);
    }

  /*  because no further gdisplays_flush calls will be made,
   *  we need to make a call to the layers_dialog_flush to
   *  update any modifications resulting from this painting
   */
  layers_dialog_flush ();

  /*  push the group end  */
  undo_push_group_end ();
}

void
paint_core_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  PaintCore * paint_core;

  gdisp = (GDisplay *) gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  gdisplay_untransform_coords_f (gdisp, (double) mevent->x, (double) mevent->y,
				 &paint_core->curx, &paint_core->cury, 1);
  paint_core->state = mevent->state;

  paint_core_interpolate (tool);
    
  gdisplays_flush ();

  paint_core->lastx = paint_core->curx;
  paint_core->lasty = paint_core->cury;
}

void
paint_core_control (tool, action, gdisp_ptr)
     Tool * tool;
     int action;
     gpointer gdisp_ptr;
{
  PaintCore * paint_core;

  paint_core = (PaintCore *) tool->private;

  switch (action)
    {
    case PAUSE :
      draw_core_pause (paint_core->core, tool);
      break;
    case RESUME :
      draw_core_resume (paint_core->core, tool);
      break;
    case HALT :
      (* paint_core->paint_func) (tool, FINISH_PAINT);
      break;
    }
}

void
paint_core_no_draw (tool)
     Tool * tool;
{
  return;
}

Tool *
paint_core_new (type)
     int type;
{
  Tool * tool;
  PaintCore * private;

  tool = (Tool *) xmalloc (sizeof (Tool));
  private = (PaintCore *) xmalloc (sizeof (PaintCore));

  private->core = draw_core_new (paint_core_no_draw);

  tool->type = type;
  tool->state = INACTIVE;
  tool->scroll_lock = 0;  /*  Allow scrolling  */
  tool->gdisp_ptr = NULL;
  tool->private = (void *) private;

  tool->button_press_func = paint_core_button_press;
  tool->button_release_func = paint_core_button_release;
  tool->motion_func = paint_core_motion;
  tool->arrow_keys_func = standard_arrow_keys_func;
  tool->control_func = paint_core_control;

  return tool;
}

void
paint_core_free (tool)
     Tool * tool;
{
  PaintCore * paint_core;

  paint_core = (PaintCore *) tool->private;

  /*  Make sure the selection core is not visible  */
  if (tool->state == ACTIVE)
    draw_core_stop (paint_core->core, tool);

  /*  Free the selection core  */
  draw_core_free (paint_core->core);

  /*  If the undo blocks exist, nuke them  */
  if (undo_blocks)
    {
      free_blocks (undo_blocks);
      undo_blocks = NULL;
    }

  /*  If the canvas blocks exist, nuke them  */
  if (canvas_blocks)
    {
      free_blocks (canvas_blocks);
      canvas_blocks = NULL;
    }

  /*  Free the temporary buffers if they exist  */
  free_paint_buffers ();

  /*  Finally, free the paint tool itself  */
  xfree (paint_core);
}


/************************/
/*  Painting functions  */
/************************/

TempBuf *
paint_core_get_paint_area (tool, fill)
     Tool * tool;
     int fill;
{
  GDisplay * gdisp;
  PaintCore * paint_core;
  int x, y;
  int x1, y1, x2, y2;
  int bytes;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  bytes = gimage_has_alpha (gdisp->gimage) ?
    gimage_bytes (gdisp->gimage) : gimage_bytes (gdisp->gimage) + 1;

  /*  adjust the x and y coordinates to the upper left corner of the brush  */
  x = (int) paint_core->curx - (paint_core->brush_mask->width >> 1);
  y = (int) paint_core->cury - (paint_core->brush_mask->height >> 1);
  
  x1 = BOUNDS (x - 1, 0, gimage_width (gdisp->gimage));
  y1 = BOUNDS (y - 1, 0, gimage_height (gdisp->gimage));
  x2 = BOUNDS (x + paint_core->brush_mask->width + 1,
	       0, gimage_width (gdisp->gimage));
  y2 = BOUNDS (y + paint_core->brush_mask->height + 1,
	       0, gimage_height (gdisp->gimage));

  /*  configure the canvas buffer  */
  if ((x2 - x1) && (y2 - y1))
    canvas_buf = temp_buf_resize (canvas_buf, bytes, x1, y1,
				  (x2 - x1), (y2 - y1));
  else
    return NULL;

  return canvas_buf;
}

TempBuf * 
paint_core_get_orig_image (tool, x1, y1, x2, y2)
     Tool * tool;
     int x1, y1;
     int x2, y2;
{
  GDisplay * gdisp;
  PaintCore * paint_core;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  orig_buf = temp_buf_resize (orig_buf, gimage_bytes (gdisp->gimage),
			      x1, y1, (x2 - x1), (y2 - y1)); 

  /*  load the orig temp buffer with the original contents of the gimage  */
  reconstruct_original_image (tool, orig_buf);
  
  return orig_buf;
}

void
paint_core_paste_canvas (tool, brush_opacity, image_opacity, paint_mode,
			 brush_hardness, mode)
     Tool * tool;
     int brush_opacity;
     int image_opacity;
     int paint_mode;
     int brush_hardness;
     int mode;
{
  /*  apply the brush mask  */
  paint_core_apply_brush_mask (tool, canvas_buf, brush_opacity,
			       brush_hardness, mode);

  /*  paste the canvas buf  */
  paint_core_paste (tool, canvas_buf, image_opacity, paint_mode, mode);
}

/************************************************************
 *             LOCAL FUNCTION DEFINITIONS                   *
 ************************************************************/

static void
paint_core_subsample_mask (tool, mask, dest)
     Tool * tool;
     MaskBuf * mask;
     MaskBuf * dest;
{
  PaintCore * paint_core;
  double left;
  unsigned char * m, * d;
  int * k;
  int index1, index2;
  int * kernel;
  int new_val;
  double x, y;
  int i, j;
  int r, s;

  paint_core = (PaintCore *) tool->private;

  x = paint_core->curx;
  x += (x < 0) ? mask->width : 0;
  left = x - ((int) x);
  index1 = (int) (left * 4);

  y = paint_core->cury;
  y += (y < 0) ? mask->height : 0;
  left = y - ((int) y);
  index2 = (int) (left * 4);

  kernel = subsample[index2][index1];

  m = mask_buf_data (mask);
  for (i = 0; i < mask->height; i++)
    {
      for (j = 0; j < mask->width; j++)
	{
	  k = kernel;
	  for (r = 0; r < KERNEL_HEIGHT; r++)
	    {
	      d = mask_buf_data (dest) + (i+r) * dest->width + j;
	      s = KERNEL_WIDTH;
	      while (s--)
		{
		  new_val = *d + ((*m * *k++) >> 8);
		  *d++ = (new_val > 255) ? 255 : new_val;
		}
	    }
	  m++;
	}
    }
}

static void
paint_core_solidify_mask (brush_mask, bm)
     MaskBuf * brush_mask;
     MaskBuf * bm;
{
  int i, j;
  unsigned char * data, * src;

  /*  get the data and advance one line into it  */
  data = mask_buf_data (bm) + bm->width;
  src = mask_buf_data (brush_mask);

  for (i = 0; i < brush_mask->height; i++)
    {
      data++;
      for (j = 0; j < brush_mask->width; j++)
	{
	  *data++ = (*src++) ? OPAQUE : TRANSPARENT;
	}
      data++;
    }
}

static void
paint_core_apply_brush_mask (tool, paste_buf, opacity, brush_hardness, mode)
     Tool * tool;
     TempBuf * paste_buf;
     int opacity;
     int brush_hardness;
     int mode;
{
  GDisplay * gdisp;
  PaintCore * paint_core;
  PixelRegion srcPR, maskPR;
  MaskBuf * bm;
  int x, y;
  int xb, yb;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  bm = mask_buf_new (paint_core->brush_mask->width + 2,
		     paint_core->brush_mask->height + 2);

  switch (brush_hardness)
    {
    case SOFT:
      paint_core_subsample_mask (tool, paint_core->brush_mask, bm);
      break;
    case HARD:
      paint_core_solidify_mask (paint_core->brush_mask, bm);
      break;
    }

  x = (int) paint_core->curx - (bm->width >> 1);
  y = (int) paint_core->cury - (bm->height >> 1);
  xb = BOUNDS (x, 0, gimage_width (gdisp->gimage));
  yb = BOUNDS (y, 0, gimage_height (gdisp->gimage));

  bm->x = x;
  bm->y = y;

  /*  If the mode is CONSTANT, build up the canvas blocks  */
  if (mode == CONSTANT)
    {
      /*  paint the brush mask to the canvas blocks  */
      set_canvas_blocks (tool, bm, opacity);

      /*  get the final mask from the canvas blocks  */
      reconstruct_canvas_mask (tool, bm);
    }

  /*  combine the brush mask and the paste_buf  */
  srcPR.bytes = paste_buf->bytes;
  srcPR.w = paste_buf->width;
  srcPR.h = paste_buf->height;
  srcPR.rowstride = srcPR.bytes * paste_buf->width;
  srcPR.data = temp_buf_data (paste_buf);

  maskPR.bytes = 1;
  maskPR.rowstride = maskPR.bytes * bm->width;
  maskPR.data = mask_buf_data (bm) + (yb - y) * maskPR.rowstride + 
    (xb - x) * maskPR.bytes;

  /*  apply the mask  */
  apply_mask_to_region (&srcPR, &maskPR, OPAQUE);

  mask_buf_free (bm);
}

static void
paint_core_paste (tool, paste_buf, opacity, paint_mode, mode)
     Tool * tool;
     TempBuf * paste_buf;
     int opacity;
     int paint_mode;
     int mode;
{
  GDisplay * gdisp;
  PaintCore * paint_core;
  PixelRegion paintPR;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  paintPR.bytes = paste_buf->bytes;
  paintPR.x = paste_buf->x;
  paintPR.y = paste_buf->y;
  paintPR.w = paste_buf->width;
  paintPR.h = paste_buf->height;
  paintPR.rowstride = paintPR.bytes * paste_buf->width;
  paintPR.data = temp_buf_data (paste_buf);

  /*  If the mode is CONSTANT, restore the original image
   *  and then paint over it...
   */
  if (mode == CONSTANT)
    {
      /*  restore the image under the paintbrush to its original form  */
      restore_original_image (gdisp->gimage, paste_buf->x, paste_buf->y,
			      paste_buf->width, paste_buf->height);
    }

  /*  set undo blocks  */
  set_undo_blocks (gdisp->gimage, paste_buf->x, paste_buf->y,
		   paste_buf->width, paste_buf->height);

  /*  apply the paint area to the gimage  */
  gimage_apply_image (gdisp->gimage, &paintPR, 0, opacity, paint_mode);

  /*  Update the undo extents  */
  paint_core->x1 = MINIMUM (paint_core->x1, paste_buf->x);
  paint_core->y1 = MINIMUM (paint_core->y1, paste_buf->y);
  paint_core->x2 = MAXIMUM (paint_core->x2, (paste_buf->x + paste_buf->width));
  paint_core->y2 = MAXIMUM (paint_core->y2, (paste_buf->y + paste_buf->height));

  /*  Update the gimage  */
  gimage_update (gdisp->gimage, paste_buf->x, paste_buf->y,
		 paste_buf->width, paste_buf->height);
}

static void
paint_core_interpolate (tool)
     Tool * tool;
{
  PaintCore * paint_core;
  int n;
  double dx, dy;
  double left;
  double t;
  double initial;
  double dist;
  double total;

  paint_core = (PaintCore *) tool->private;

  dx = paint_core->curx - paint_core->lastx;
  dy = paint_core->cury - paint_core->lasty;

  if (!dx && !dy)
    return;

  dist = sqrt (SQR (dx) + SQR (dy));
  total = dist + paint_core->distance;
  initial = paint_core->distance;

  while (paint_core->distance < total)
    {
      n = (int) (paint_core->distance / paint_core->spacing + 1.0 + EPSILON);
      left = n * paint_core->spacing - paint_core->distance;
      paint_core->distance += left;

      if (paint_core->distance <= total)
	{
	  t = (paint_core->distance - initial) / dist;

	  paint_core->curx = paint_core->lastx + dx * t;
	  paint_core->cury = paint_core->lasty + dy * t;
	  (* paint_core->paint_func) (tool, MOTION_PAINT);
	}
    }

  paint_core->distance = total;
  paint_core->curx = paint_core->lastx + dx;
  paint_core->cury = paint_core->lasty + dy;
}

static void 
reconstruct_original_image (tool, buf)
     Tool * tool;
     TempBuf * buf;
{
  PaintCore * paint_core;
  GDisplay * gdisp;
  TempBuf * block;
  int index;
  int x, y;
  int endx, endy;
  int row, col;
  int offx, offy;
  int x2, y2;
  int width, height;
  PixelRegion srcPR, destPR;

  paint_core = (PaintCore *) tool->private;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  /*  load the buffer from the current gimage  */
  temp_buf_load (buf, gdisp->gimage, buf->x, buf->y, buf->width, buf->height);
  width = gimage_width (gdisp->gimage);
  height = gimage_height (gdisp->gimage);

  /*  init some variables  */
  srcPR.bytes = buf->bytes;
  destPR.rowstride = buf->bytes * buf->width;

  y = BOUNDS (buf->y, 0, height);
  endx = BOUNDS (buf->x + buf->width, buf->x, width);
  endy = BOUNDS (buf->y + buf->height, buf->y, height);

  row = (y / block_height);
  /*  patch the buffer with the saved portions of the image  */
  while (y < endy)
    {
      x = BOUNDS (buf->x, 0, width);
      col = (x / block_width);

      /*  calculate y offset into this row of blocks  */
      offy = (y - row * block_height);
      y2 = (row + 1) * block_height;
      if (y2 > endy) y2 = endy;
      srcPR.h = y2 - y;

      while (x < endx)
      {
	index = row * horz_blocks + col;
	block = undo_blocks [index];
	/*  If the block exists, patch it into buf  */
	if (block)
	  {
	    /* calculate x offset into the block  */
	    offx = (x - col * block_width);
	    x2 = (col + 1) * block_width;
	    if (x2 > endx) x2 = endx;
	    srcPR.w = x2 - x;
	    
	    /*  Calculate the offsets into each buffer  */
	    srcPR.rowstride = srcPR.bytes * block->width;
	    srcPR.data = temp_buf_data (block) + offy * srcPR.rowstride + offx * srcPR.bytes;
	    destPR.data = temp_buf_data (buf) + 
	      (y - buf->y) * destPR.rowstride + (x - buf->x) * buf->bytes;

	    copy_region (&srcPR, &destPR);
	  }

	col ++;
	x = col * block_width;
      }

      row ++;
      y = row * block_height;
    }
}

static void 
restore_original_image (gimage, x, y, w, h)
     GImage * gimage;
     int x, y;
     int w, h;
{
  TempBuf * block;
  int index;
  int sx, sy;
  int endx, endy;
  int row, col;
  int offx, offy;
  int x2, y2;
  int width, height;
  PixelRegion srcPR, destPR;

  /*  get gimage width and height for bounds checking  */
  width = gimage_width (gimage);
  height = gimage_height (gimage);

  /*  init some variables  */
  srcPR.bytes = gimage_bytes (gimage);
  destPR.bytes = gimage_bytes (gimage);
  destPR.rowstride = destPR.bytes * gimage_width (gimage);

  sy = BOUNDS (y, 0, height);
  endx = BOUNDS (x + w, x, width);
  endy = BOUNDS (y + h, y, height);

  row = (sy / block_height);
  /*  patch the buffer with the saved portions of the image  */
  while (sy < endy)
    {
      sx = BOUNDS (x, 0, width);
      col = (sx / block_width);

      /*  calculate y offset into this row of blocks  */
      offy = (sy - row * block_height);
      y2 = (row + 1) * block_height;
      if (y2 > endy) y2 = endy;
      srcPR.h = y2 - sy;

      while (sx < endx)
      {
	index = row * horz_blocks + col;
	block = undo_blocks [index];
	/*  If the block exists, patch it into buf  */
	if (block)
	  {
	    /* calculate x offset into the block  */
	    offx = (sx - col * block_width);
	    x2 = (col + 1) * block_width;
	    if (x2 > endx) x2 = endx;
	    srcPR.w = x2 - sx;
	    
	    /*  Calculate the offsets into each buffer  */
	    srcPR.rowstride = srcPR.bytes * block->width;
	    srcPR.data = temp_buf_data (block) +
	      offy * srcPR.rowstride + offx * srcPR.bytes;
	    destPR.data = gimage_data (gimage) + 
	      sy * destPR.rowstride + sx * destPR.bytes;

	    copy_region (&srcPR, &destPR);
	  }

	col ++;
	sx = col * block_width;
      }

      row ++;
      sy = row * block_height;
    }
}

static void
set_undo_blocks (gimage, x, y, w, h)
     GImage * gimage;
     int x, y;
     int w, h;
{
  int endx, endy;
  int startx;
  int index;
  int x1, y1;
  int x2, y2;
  int row, col;
  int width, height;

  width = gimage_width (gimage);
  height = gimage_height (gimage);

  startx = x;
  endx = x + w;
  endy = y + h;

  row = y / block_height;
  while (y < endy)
    {
      col = x / block_width;
      while (x < endx)
	{
	  index = row * horz_blocks + col;
	  
	  /*  If the block doesn't exist, create and initialize it  */
	  if (! undo_blocks [index])
	    {
	      /*  determine memory efficient width and height of block  */
	      x1 = col * block_width;
	      x2 = BOUNDS (x1 + block_width, 0, width);
	      w = (x2 - x1);
	      y1 = row * block_height;
	      y2 = BOUNDS (y1 + block_height, 0, height);
	      h = (y2 - y1);

	      /*  get the specified portion of the gimage  */
	      undo_blocks [index] = temp_buf_load (NULL, gimage, x1, y1, w, h);
	    }
	  col++;
	  x = col * block_width;
	}

      row ++;
      y = row * block_height;
      x = startx;
    }
}

static void 
set_canvas_blocks (tool, buf, opacity)
     Tool * tool;
     MaskBuf * buf;
     int opacity;
{
  PaintCore * paint_core;
  GDisplay * gdisp;
  MaskBuf * block;
  int index;
  int x, y;
  int endx, endy;
  int row, col;
  int offx, offy;
  int bx1, by1, bx2, by2;
  int x2, y2;
  int width, height;
  PixelRegion maskPR, srcPR;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  width = gimage_width (gdisp->gimage);
  height = gimage_height (gdisp->gimage);

  /*  init some variables  */
  maskPR.bytes = buf->bytes;
  maskPR.rowstride = maskPR.bytes * buf->width;
  srcPR.bytes = 1;

  y = BOUNDS (buf->y, 0, height);
  endx = BOUNDS (buf->x + buf->width, buf->x, width);
  endy = BOUNDS (buf->y + buf->height, buf->y, height);

  row = (y / block_height);
  /*  patch the buffer with the saved portions of the image  */
  while (y < endy)
    {
      x = BOUNDS (buf->x, 0, width);
      col = (x / block_width);

      /*  calculate y offset into this row of blocks  */
      offy = (y - row * block_height);
      y2 = (row + 1) * block_height;
      if (y2 > endy) y2 = endy;
      srcPR.h = y2 - y;

      while (x < endx)
      {
	index = row * horz_blocks + col;

	/*  If the block doesn't exist, create it  */
	if (! (block = canvas_blocks [index]))
	  {
	    bx1 = col * block_width;
	    bx2 = BOUNDS (bx1 + block_width, 0, width);
	    by1 = row * block_height;
	    by2 = BOUNDS (by1 + block_height, 0, height);

	    block = canvas_blocks [index] = mask_buf_new ((bx2 - bx1),
							  (by2 - by1));
	  }

	/* calculate x offset into the block  */
	offx = (x - col * block_width);
	x2 = (col + 1) * block_width;
	if (x2 > endx) x2 = endx;
	srcPR.w = x2 - x;
	
	/*  Calculate the offsets into each buffer  */
	srcPR.rowstride = srcPR.bytes * block->width;
	srcPR.data = mask_buf_data (block) +
	  offy * srcPR.rowstride + offx * srcPR.bytes;
	
	maskPR.data = mask_buf_data (buf) + 
	  (y - buf->y) * maskPR.rowstride + (x - buf->x) * maskPR.bytes;
	
	combine_mask_and_region (&srcPR, &maskPR, opacity);

	col ++;
	x = col * block_width;
      }

      row ++;
      y = row * block_height;
    }
}

static void 
reconstruct_canvas_mask (tool, buf)
     Tool * tool;
     MaskBuf * buf;
{
  PaintCore * paint_core;
  GDisplay * gdisp;
  MaskBuf * block;
  int index;
  int x, y;
  int endx, endy;
  int row, col;
  int offx, offy;
  int x2, y2;
  int width, height;
  PixelRegion srcPR, destPR;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  /*  set the buffer to transparent  */
  memset (mask_buf_data (buf), TRANSPARENT, buf->width * buf->height);

  width = gimage_width (gdisp->gimage);
  height = gimage_height (gdisp->gimage);

  /*  init some variables  */
  srcPR.bytes = buf->bytes;
  destPR.rowstride = buf->bytes * buf->width;

  y = BOUNDS (buf->y, 0, height);
  endx = BOUNDS (buf->x + buf->width, buf->x, width);
  endy = BOUNDS (buf->y + buf->height, buf->y, height);

  row = (y / block_height);
  /*  patch the buffer with the saved portions of the image  */
  while (y < endy)
    {
      x = BOUNDS (buf->x, 0, width);
      col = (x / block_width);

      /*  calculate y offset into this row of blocks  */
      offy = (y - row * block_height);
      y2 = (row + 1) * block_height;
      if (y2 > endy) y2 = endy;
      srcPR.h = y2 - y;

      while (x < endx)
      {
	index = row * horz_blocks + col;
	block = canvas_blocks [index];
	/*  If the block exists, patch it into buf  */
	if (block)
	  {
	    /* calculate x offset into the block  */
	    offx = (x - col * block_width);
	    x2 = (col + 1) * block_width;
	    if (x2 > endx) x2 = endx;
	    srcPR.w = x2 - x;
	    
	    /*  Calculate the offsets into each buffer  */
	    srcPR.rowstride = srcPR.bytes * block->width;
	    srcPR.data = temp_buf_data (block) + offy * srcPR.rowstride + offx * srcPR.bytes;
	    destPR.data = temp_buf_data (buf) + 
	      (y - buf->y) * destPR.rowstride + (x - buf->x) * buf->bytes;

	    copy_region (&srcPR, &destPR);
	  }

	col ++;
	x = col * block_width;
      }

      row ++;
      y = row * block_height;
    }
}

static TempBuf **
allocate_blocks (blocks, brush_width, brush_height, image_width, image_height)
     TempBuf ** blocks;
     int brush_width, brush_height;
     int image_width, image_height;
{
  int num_blocks;
  int i;

  /*  calculate the width and height of each undo block  */
  block_width = (brush_width*2 < MIN_BLOCK_WIDTH) ? MIN_BLOCK_WIDTH : brush_width*2;
  block_height = (brush_height*2 < MIN_BLOCK_HEIGHT) ? MIN_BLOCK_HEIGHT : brush_height*2;

  /*  calculate the number of rows and cols in the undo block grid  */
  horz_blocks = (image_width + block_width - 1) / block_width;
  vert_blocks = (image_height + block_height - 1) / block_height;

  /*  Allocate the array  */
  num_blocks = horz_blocks * vert_blocks;
  blocks = (TempBuf **) xmalloc (sizeof (TempBuf *) * num_blocks);

  /*  Initialize the array  */
  for (i = 0; i < num_blocks; i++)
    blocks [i] = NULL;

  return blocks;
}

static void
free_blocks (blocks)
     TempBuf ** blocks;
{
  int i;
  int num_blocks;

  num_blocks = vert_blocks * horz_blocks;

  for (i = 0; i < num_blocks; i++)
    if (blocks [i])
      temp_buf_free (blocks [i]);

  xfree (blocks);
}

/*****************************************************/
/*  Paint buffers utility functions                  */
/*****************************************************/


static void
free_paint_buffers ()
{
  if (orig_buf)
    temp_buf_free (orig_buf);

  orig_buf = NULL;

  if (canvas_buf)
    temp_buf_free (canvas_buf);

  canvas_buf = NULL;
}
