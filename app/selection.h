/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __SELECTION_H__
#define __SELECTION_H__

typedef struct _selection Selection;

struct _selection
{
  /*  This information is for maintaining the selection's appearance  */
  GdkWindow *   win;        /*  Window to draw to                 */
  void *        gdisp;      /*  GDisplay that owns the selection  */
  GdkGC *       gc;         /*  GC for drawing selection outline  */
  GdkGC *       gc2;        /*  2nd GC for drawing selection outline */

  /*  This information is for drawing the marching ants around the border  */
  GdkSegment *  segs;       /*  gdk segments of area boundary     */
  int           num_segs;   /*  number of segments in segs1       */
  GdkSegment *  segs2;      /*  gdk segments of area boundary     */
  int           num_segs2;  /*  number of segments in segs2       */
  int           state;      /*  internal drawing state            */
  int           paused;     /*  count of pause requests           */
  int           recalc;     /*  flag to recalculate the selection */
  int           index;      /*  index of current stipple pattern  */
  int           speed;      /*  speed of marching ants            */
  int           hidden;     /*  is the selection hidden?          */
  gint          timer;      /*  timer for successive draws        */
  int           cycle;      /*  color cycling turned on           */
  GdkPixmap *   cycle_pix;  /*  cycling pixmap                    */
};

/*  Function declarations  */

Selection *  selection_create          (GdkWindow *, gpointer, int, int, int);
void         selection_pause           (Selection *);
void         selection_resume          (Selection *);
void         selection_start           (Selection *, int);
void         selection_invis           (Selection *);
void         selection_hide            (Selection *, void *);
void         selection_free            (Selection *);

#endif  /*  __SELECTION_H__  */
