/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <math.h>
#include "appenv.h"
#include "errors.h"
#include "floating_sel.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "info_dialog.h"
#include "palette.h"
#include "transform_core.h"
#include "transform_tool.h"
#include "temp_buf.h"
#include "tools.h"
#include "undo.h"

/* define pi */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif  /*  M_PI  */

/*  variables  */
static TranInfo    old_trans_info;
InfoDialog *       transform_info = NULL;

/*  forward function declarations  */
static int         transform_core_bounds  (Tool *, void *);
static void *      transform_core_recalc  (Tool *, void *);
static TempBuf *   transform_core_do      (Tool *, void *);
static void        transform_core_paste   (Tool *, void *, TempBuf *, int);


#define BILINEAR(jk,j1k,jk1,j1k1,dx,dy) \
                ((1-dy) * ((1-dx)*jk + dx*j1k) + \
		    dy  * ((1-dx)*jk1 + dx*j1k1)) 


void
transform_core_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  TransformCore * transform_core;
  GDisplay * gdisp;
  int srw, srh;
  int x, y;
  int i;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;

  /*  Save the current transformation info  */
  for (i = 0; i < TRAN_INFO_SIZE; i++)
    old_trans_info [i] = transform_core->trans_info [i];
	  
  /*  if we have already displayed the bounding box and handles,
   *  check to make sure that the display which currently owns the
   *  tool is the one which just received the button pressed event
   */
  if ((transform_core->function >= CREATING) && (gdisp_ptr == tool->gdisp_ptr) &&
      transform_core->interactive)
    {
      srw = transform_core->srw;
      srh = transform_core->srh;
      
      x = bevent->x + (srw >> 1);
      y = bevent->y + (srh >> 1);

      /*  Find which handle the cursor has been clicked in, if any...  */
      if (x == BOUNDS (x, transform_core->sx1, transform_core->sx1 + srw) &&
	  y == BOUNDS (y, transform_core->sy1, transform_core->sy1 + srh))
	transform_core->function = HANDLE_1;
      else if (x == BOUNDS (x, transform_core->sx2, transform_core->sx2 + srw) &&
	       y == BOUNDS (y, transform_core->sy2, transform_core->sy2 + srh))
	transform_core->function = HANDLE_2;
      else if (x == BOUNDS (x, transform_core->sx3, transform_core->sx3 + srw) &&
	       y == BOUNDS (y, transform_core->sy3, transform_core->sy3 + srh))
	transform_core->function = HANDLE_3;
      else if (x == BOUNDS (x, transform_core->sx4, transform_core->sx4 + srw) &&
	       y == BOUNDS (y, transform_core->sy4, transform_core->sy4 + srh))
	transform_core->function = HANDLE_4;
      
      /*  otherwise, the new function will be creating, since we want to start anew  */
      else
	transform_core->function = CREATING;

      if (transform_core->function > CREATING)
	{
	  /*  Save the current pointer position  */
	  gdisplay_untransform_coords (gdisp, bevent->x, bevent->y,
				       &transform_core->startx,
				       &transform_core->starty, TRUE, 0);
	  transform_core->lastx = transform_core->startx;
	  transform_core->lasty = transform_core->starty;

	  gdk_pointer_grab (gdisp->canvas->window, FALSE,
			    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			    NULL, NULL, bevent->time);
	  
	  tool->state = ACTIVE;
	  
	  return;
	}
    }

  /*  if the cursor is clicked inside the current selection, show the
   *  bounding box and handles...
   */
  if (gdisplay_mask_value (gdisp, bevent->x, bevent->y) > HALF_WAY)
    {
      /*  If the tool is already active, clear the current state and reset  */
      if (tool->state == ACTIVE)
	transform_core_reset (tool, gdisp_ptr);

      /*  Set the pointer to the gdisplay that owns this tool  */
      tool->gdisp_ptr = gdisp_ptr;
      tool->state = ACTIVE;

      /*  Grab the pointer if we're in non-interactive mode  */
      if (!transform_core->interactive)
	gdk_pointer_grab (gdisp->canvas->window, FALSE,
			  GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON2_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
			  NULL, NULL, bevent->time);
	  
      /*  Find the transform bounds for some tools (like scale, perspective)
       *  that actually need the bounds for initializing
       */
      transform_core_bounds (tool, gdisp_ptr);

      /*  Initialize the transform tool  */
      (* transform_core->trans_func) (tool, gdisp_ptr, INIT);

      /*  Recalculate the transform tool  */
      transform_core_recalc (tool, gdisp_ptr);

      /*  start drawing the bounding box and handles...  */
      draw_core_start (transform_core->core, gdisp->canvas->window, tool);
    }
}

void
transform_core_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;
  TransformCore * transform_core;
  TempBuf * new_buf;
  int first_transform;
  int cut;
  int x, y;
  int i;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;

  /*  if we are creating, there is nothing to be done...exit  */
  if (transform_core->function == CREATING && transform_core->interactive)
    return;

  /*  release of the pointer grab  */
  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  /*  if the 3rd button isn't pressed, transform the selected region  */
  if (! (bevent->state & GDK_BUTTON3_MASK))
    {
      /*  Start a transform undo group  */
      undo_push_group_start (TRANSFORM_CORE_UNDO);

      /*  If original is NULL, then this is the first transformation  */
      first_transform = (transform_core->original) ? FALSE : TRUE;

      /*  If we're in interactive mode, and haven't yet done any
       *  transformations, we need to copy the current selection to
       *  the transform tool's private selection pointer, so that the
       *  original source can be repeatedly modified.
       */
      if (first_transform)
	{
	  /*  If there is a floating selection, don't "cut"  */
	  if (gdisp->gimage->floating_sel >= 0)
	    cut = 0;
	  else
	    cut = 1;

	  /*  extract the selected region  */
	  transform_core->original = gimage_mask_extract (gdisp->gimage, cut);

	  /*  update  */
	  if (transform_core->original)
	    {
	      x = transform_core->original->x;
	      y = transform_core->original->y;
	      
	      gimage_update (gdisp->gimage, x, y,
			     x + transform_core->original->width,
			     y + transform_core->original->height);
	    }
	}

      /*  first, send the request for the transformation to the tool...
       *  if the tool returns a NULL pointer, invoke a general transformation method.
       */
      if (! (new_buf = (* transform_core->trans_func) (tool, gdisp_ptr, FINISH)))
	new_buf = transform_core_do (tool, gdisp_ptr);
      
      if (new_buf)
	{
	  /*  paste the new transformed image to the gimage...also implement
	   *  undo...
	   */
	  transform_core_paste (tool, gdisp_ptr, new_buf, first_transform);

	  /*  free the new buffer  */
	  temp_buf_free (new_buf);
	}
      
      /*  push the undo group end  */
      undo_push_group_end ();
    }
  else
    {
      /*  stop the current tool drawing process  */
      draw_core_pause (transform_core->core, tool);

      /*  Restore the previous transformation info  */
      for (i = 0; i < TRAN_INFO_SIZE; i++)
	transform_core->trans_info [i] = old_trans_info [i];

      /*  recalculate the tool's transformation matrix  */
      transform_core_recalc (tool, gdisp_ptr);

      /*  resume drawing the current tool  */
      draw_core_resume (transform_core->core, tool);
    }

  /*  if this tool is non-interactive, make it inactive after use  */
  if (!transform_core->interactive)
    tool->state = INACTIVE;
}

void
transform_core_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
  GDisplay *gdisp;
  TransformCore *transform_core;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;

  /*  if we are creating or this tool is non-interactive, there is 
   *  nothing to be done so exit.
   */
  if (transform_core->function == CREATING || !transform_core->interactive)
    return;

  /*  stop the current tool drawing process  */
  draw_core_pause (transform_core->core, tool);

  gdisplay_untransform_coords (gdisp, mevent->x, mevent->y, &transform_core->curx,
			       &transform_core->cury, TRUE, 0);
  transform_core->state = mevent->state;

  /*  recalculate the tool's transformation matrix  */
  (* transform_core->trans_func) (tool, gdisp_ptr, MOTION);

  transform_core->lastx = transform_core->curx;
  transform_core->lasty = transform_core->cury;

  /*  resume drawing the current tool  */
  draw_core_resume (transform_core->core, tool);
}

void
transform_core_control (tool, action, gdisp_ptr)
     Tool *tool;
     int action;
     gpointer gdisp_ptr;
{
  TransformCore * transform_core;

  transform_core = (TransformCore *) tool->private;

  switch (action)
    {
    case PAUSE : 
      draw_core_pause (transform_core->core, tool);
      break;
    case RESUME :
      if (transform_core_recalc (tool, gdisp_ptr))
	draw_core_resume (transform_core->core, tool);
      else
	{
	  info_dialog_popdown (transform_info);
	  tool->state = INACTIVE;
	}
      break;
    case HALT :
      transform_core_reset (tool, gdisp_ptr);
      break;
    }
}

void
transform_core_no_draw (tool)
     Tool * tool;
{
  return;
}

void
transform_core_draw (tool)
     Tool * tool;
{
  int x1, y1, x2, y2, x3, y3, x4, y4;
  TransformCore * transform_core;
  GDisplay * gdisp;
  int srw, srh;

  gdisp = tool->gdisp_ptr;
  transform_core = (TransformCore *) tool->private;

  #define SRW 10
  #define SRH 10

  gdisplay_transform_coords (gdisp, transform_core->tx1, transform_core->ty1,
			     &transform_core->sx1, &transform_core->sy1, 0);
  gdisplay_transform_coords (gdisp, transform_core->tx2, transform_core->ty2,
			     &transform_core->sx2, &transform_core->sy2, 0);
  gdisplay_transform_coords (gdisp, transform_core->tx3, transform_core->ty3,
			     &transform_core->sx3, &transform_core->sy3, 0);
  gdisplay_transform_coords (gdisp, transform_core->tx4, transform_core->ty4,
			     &transform_core->sx4, &transform_core->sy4, 0);

  x1 = transform_core->sx1;  y1 = transform_core->sy1;
  x2 = transform_core->sx2;  y2 = transform_core->sy2;
  x3 = transform_core->sx3;  y3 = transform_core->sy3;
  x4 = transform_core->sx4;  y4 = transform_core->sy4;

  /*  find the handles' width and height  */
  transform_core->srw = srw = SRW;
  transform_core->srh = srh = SRH;

  /*  draw the bounding box  */
  gdk_draw_line (transform_core->core->win, transform_core->core->gc,
		 x1, y1, x2, y2);
  gdk_draw_line (transform_core->core->win, transform_core->core->gc,
		 x2, y2, x4, y4);
  gdk_draw_line (transform_core->core->win, transform_core->core->gc,
		 x3, y3, x4, y4);
  gdk_draw_line (transform_core->core->win, transform_core->core->gc,
		 x3, y3, x1, y1);

  /*  draw the tool handles  */
  gdk_draw_rectangle (transform_core->core->win, transform_core->core->gc, 0,
		      x1 - (srw >> 1), y1 - (srh >> 1), srw, srh);
  gdk_draw_rectangle (transform_core->core->win, transform_core->core->gc, 0,
		      x2 - (srw >> 1), y2 - (srh >> 1), srw, srh);
  gdk_draw_rectangle (transform_core->core->win, transform_core->core->gc, 0,
		      x3 - (srw >> 1), y3 - (srh >> 1), srw, srh);
  gdk_draw_rectangle (transform_core->core->win, transform_core->core->gc, 0,
		      x4 - (srw >> 1), y4 - (srh >> 1), srw, srh);
}

Tool *
transform_core_new (type, interactive)
     int type;
     int interactive;
{
  Tool * tool;
  TransformCore * private;
  int i;

  tool = (Tool *) xmalloc (sizeof (Tool));
  private = (TransformCore *) xmalloc (sizeof (TransformCore));

  private->interactive = interactive;

  if (interactive)
    private->core = draw_core_new (transform_core_draw);
  else
    private->core = draw_core_new (transform_core_no_draw);

  private->function = CREATING;
  private->original = NULL;

  for (i = 0; i < TRAN_INFO_SIZE; i++)
    private->trans_info[i] = 0;

  tool->type = type;
  tool->state = INACTIVE;
  tool->scroll_lock = 1;    /*  Do not allow scrolling  */
  tool->gdisp_ptr = NULL;
  tool->private = (void *) private;

  tool->button_press_func = transform_core_button_press;
  tool->button_release_func = transform_core_button_release;
  tool->motion_func = transform_core_motion;
  tool->arrow_keys_func = standard_arrow_keys_func;
  tool->control_func = transform_core_control;

  return tool;
}

void
transform_core_free (tool)
     Tool *tool;
{
  TransformCore * transform_core;

  transform_core = (TransformCore *) tool->private;

  /*  Make sure the selection core is not visible  */
  if (tool->state == ACTIVE)
    draw_core_stop (transform_core->core, tool);

  /*  Free the selection core  */
  draw_core_free (transform_core->core);

  /*  Free up the original selection if it exists  */
  if (transform_core->original)
    temp_buf_free (transform_core->original);

  /*  If there is an information dialog, free it up  */
  if (transform_info)
    info_dialog_free (transform_info);
  transform_info = NULL;

  /*  Finally, free the transform tool itself  */
  xfree (transform_core);
}

void
transform_bounding_box (tool)
     Tool * tool;
{
  TransformCore * transform_core;

  transform_core = (TransformCore *) tool->private;

  transform_point (transform_core->transform,
		   transform_core->x1, transform_core->y1,
		   &transform_core->tx1, &transform_core->ty1);
  transform_point (transform_core->transform,
		   transform_core->x2, transform_core->y1,
		   &transform_core->tx2, &transform_core->ty2);
  transform_point (transform_core->transform,
		   transform_core->x1, transform_core->y2,
		   &transform_core->tx3, &transform_core->ty3);
  transform_point (transform_core->transform,
		   transform_core->x2, transform_core->y2,
		   &transform_core->tx4, &transform_core->ty4);
}

void
transform_point (m, x, y, nx, ny)
     Matrix m;
     double x, y;
     double *nx, *ny;
{
  double xx, yy, ww;

  xx = m[0][0] * x + m[0][1] * y + m[0][2];
  yy = m[1][0] * x + m[1][1] * y + m[1][2];
  ww = m[2][0] * x + m[2][1] * y + m[2][2];
  
  if (!ww)
    ww = 1.0;

  *nx = xx / ww;
  *ny = yy / ww;
}

void
mult_matrix (m1, m2)
     Matrix m1, m2;
{
  Matrix result;
  int i, j, k;

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      {
	result [i][j] = 0.0;
	for (k = 0; k < 3; k++)
	  result [i][j] += m1 [i][k] * m2[k][j];
      }

  /*  copy the result into matrix 2  */
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      m2 [i][j] = result [i][j];
}

void
identity_matrix (m)
     Matrix m;
{
  int i, j;

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      m[i][j] = (i == j) ? 1 : 0;

}

void
translate_matrix (m, x, y)
     Matrix m;
     double x, y;
{
  Matrix trans;

  identity_matrix (trans);
  trans[0][2] = x;
  trans[1][2] = y;
  mult_matrix (trans, m);
}

void
scale_matrix (m, x, y)
     Matrix m;
     double x, y;
{
  Matrix scale;

  identity_matrix (scale);
  scale[0][0] = x;
  scale[1][1] = y;
  mult_matrix (scale, m);
}

void 
rotate_matrix (m, theta)
     Matrix m;
     double theta;
{
  Matrix rotate;
  double cos_theta, sin_theta;

  cos_theta = cos (theta);
  sin_theta = sin (theta);

  identity_matrix (rotate);
  rotate[0][0] = cos_theta;
  rotate[0][1] = -sin_theta;
  rotate[1][0] = sin_theta;
  rotate[1][1] = cos_theta;
  mult_matrix (rotate, m);
}

void
xshear_matrix (m, shear)
     Matrix m;
     double shear;
{
  Matrix shear_m;

  identity_matrix (shear_m);
  shear_m[0][1] = shear;
  mult_matrix (shear_m, m);
}

void
yshear_matrix (m, shear)
     Matrix m;
     double shear;
{
  Matrix shear_m;

  identity_matrix (shear_m);
  shear_m[1][0] = shear;
  mult_matrix (shear_m, m);
}

/*  find the determinate for a 3x3 matrix  */
double
determinate (m)
     Matrix m;
{
  int i;
  double det = 0;

  for (i = 0; i < 3; i ++)
    {
      det += m[0][i] * m[1][(i+1)%3] * m[2][(i+2)%3];
      det -= m[2][i] * m[1][(i+1)%3] * m[0][(i+2)%3];
    }

  return det;
}

/*  find the cofactor matrix of a matrix  */
void
cofactor (m, m_cof)
     Matrix m;
     Matrix m_cof;
{
  int i, j;
  int x1, y1;
  int x2, y2;
  
  for (i = 0; i < 3; i++)
    {
      switch (i)
	{
	case 0 : y1 = 1; y2 = 2; break;
	case 1 : y1 = 0; y2 = 2; break;
	case 2 : y1 = 0; y2 = 1; break;
	}
      for (j = 0; j < 3; j++)
	{
	  switch (j)
	    {
	    case 0 : x1 = 1; x2 = 2; break;
	    case 1 : x1 = 0; x2 = 2; break;
	    case 2 : x1 = 0; x2 = 1; break;
	    }
	  m_cof[i][j] = (m[x1][y1] * m[x2][y2] - m[x1][y2] * m[x2][y1]) * 
	    (((i+j) % 2) ? -1 : 1);
	}
    }
}

/*  find the inverse of a 3x3 matrix  */
void
invert (m, m_inv)
     Matrix m;
     Matrix m_inv;
{
  double det = determinate (m);
  int i, j;

  if (det == 0.0)
    return;

  /*  Find the cofactor matrix of m, store it in m_inv  */
  cofactor (m, m_inv);

  /*  divide by the determinate  */
  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++)
      m_inv[i][j] = m_inv[i][j] / det;
}

void
transform_core_reset(tool, gdisp_ptr)
     Tool * tool;
     void * gdisp_ptr;
{
  TransformCore * transform_core;
  GDisplay * gdisp;

  transform_core = (TransformCore *) tool->private;
  gdisp = (GDisplay *) gdisp_ptr;

  if (transform_core->original)
    temp_buf_free (transform_core->original);
  transform_core->original = NULL;

  /*  inactivate the tool  */
  transform_core->function = CREATING;
  draw_core_stop (transform_core->core, tool);
  info_dialog_popdown (transform_info);
  tool->state = INACTIVE;
}

static int
transform_core_bounds (tool, gdisp_ptr)
     Tool *tool;
     void *gdisp_ptr;
{
  GDisplay * gdisp;
  TransformCore * transform_core;
  TempBuf * buf;
  int valid;
  int offset_x, offset_y;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;
  buf = transform_core->original;

  /*  find the boundaries  */
  if (buf)
    {
      transform_core->x1 = buf->x;
      transform_core->y1 = buf->y;
      transform_core->x2 = buf->x + buf->width;
      transform_core->y2 = buf->y + buf->height;
      valid = TRUE;
    }
  else
    {
      gimage_offsets (gdisp->gimage, &offset_x, &offset_y);
      valid = gimage_mask_bounds (gdisp->gimage,
				  &transform_core->x1, &transform_core->y1,
				  &transform_core->x2, &transform_core->y2);
      transform_core->x1 += offset_x;
      transform_core->y1 += offset_y;
      transform_core->x2 += offset_x;
      transform_core->y2 += offset_y;
    }
  
  return valid;
}

static void *
transform_core_recalc (tool, gdisp_ptr)
     Tool * tool;
     void * gdisp_ptr;
{
  TransformCore * transform_core;

  transform_core = (TransformCore *) tool->private;

  if (transform_core_bounds (tool, gdisp_ptr))
    return (* transform_core->trans_func) (tool, gdisp_ptr, RECALC);
  else
    return (void *) NULL;
}

/*  Actually carry out a transformation  */
static TempBuf *
transform_core_do (tool, gdisp_ptr)
     Tool * tool;
     void * gdisp_ptr;
{
  GDisplay * gdisp;
  TransformCore * transform_core;
  TempBuf * buf, * float_buf;
  Matrix m;
  int interpolation;
  int itx, ity;
  int offset;
  int x1, y1, x2, y2;
  int offx, offy;
  int tx1, ty1, tx2, ty2;
  int width, height;
  int alpha;
  int bytes, b;
  int rowstride;
  int x, y;
  double xinc, yinc, winc;
  double tx, ty, tw;
  double ttx, tty;
  double dx, dy;
  unsigned char empty = 0;
  unsigned char * data_src;
  unsigned char * data_dest;
  unsigned char * p1, * p2, * p3, * p4;
  unsigned char * a1, * a2, * a3, * a4;
  unsigned char bg_col[MAX_CHANNELS];

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;
  float_buf = transform_core->original;

  /*  determine if interpolation is turned on...  */
  interpolation = transform_tool_smoothing ();

  /*  Get the background color  */
  gimage_get_background (gdisp->gimage, bg_col);

  switch (gimage_type (gdisp->gimage))
    {
    case RGB_GIMAGE: case RGBA_GIMAGE:
      bg_col[ALPHA_PIX] = TRANSPARENT;
      alpha = 3;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      bg_col[ALPHA_G_PIX] = TRANSPARENT;
      alpha = 1;
      break;
    case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
      bg_col[ALPHA_G_PIX] = TRANSPARENT;
      alpha = 1;
      /*  If the gimage is indexed color, ignore smoothing value  */
      interpolation = 0;
      break;
    }

  /*  Find the inverse of the transformation matrix  */
  invert (transform_core->transform, m);

  /*  The original bounding box  */
  x1 = transform_core->x1;
  y1 = transform_core->y1;
  x2 = transform_core->x2;
  y2 = transform_core->y2;

  /*  Find the bounding coordinates  */
  tx1 = MINIMUM (transform_core->tx1, transform_core->tx2);
  tx1 = MINIMUM (tx1, transform_core->tx3);
  tx1 = MINIMUM (tx1, transform_core->tx4);
  ty1 = MINIMUM (transform_core->ty1, transform_core->ty2);
  ty1 = MINIMUM (ty1, transform_core->ty3);
  ty1 = MINIMUM (ty1, transform_core->ty4);
  tx2 = MAXIMUM (transform_core->tx1, transform_core->tx2);
  tx2 = MAXIMUM (tx2, transform_core->tx3);
  tx2 = MAXIMUM (tx2, transform_core->tx4);
  ty2 = MAXIMUM (transform_core->ty1, transform_core->ty2);
  ty2 = MAXIMUM (ty2, transform_core->ty3);
  ty2 = MAXIMUM (ty2, transform_core->ty4);

  /*  Get the new temporary buffer for the transformed result  */
  buf = temp_buf_new ((tx2 - tx1), (ty2 - ty1), float_buf->bytes, tx1, ty1, NULL);

  width = float_buf->width;
  height = float_buf->height;
  bytes = float_buf->bytes;
  rowstride = width * bytes;
  data_src = temp_buf_data (float_buf);
  data_dest = temp_buf_data (buf);

  xinc = m[0][0];
  yinc = m[1][0];
  winc = m[2][0];

  /*  If we're interpolating, set x1 and y1 back one pixel so that
   *  we can correctly interpolate pixels on the leftmost and topmost borders
   */
  offx = x1;
  offy = y1;
  if (interpolation)
    {
      x1 --;  y1 --;
    }

  for (y = ty1; y < ty2; y++)
    {
      /*  When we calculate the inverse transformation, we should transform
       *  the center of each destination pixel...
       */
      tx = xinc * (tx1 + 0.5) + m[0][1] * (y + 0.5) + m[0][2];
      ty = yinc * (tx1 + 0.5) + m[1][1] * (y + 0.5) + m[1][2];
      tw = winc * (tx1 + 0.5) + m[2][1] * (y + 0.5) + m[2][2];
      for (x = tx1; x < tx2; x++)
	{
	  /*  normalize homogeneous coords  */
	  if (tw == 0.0)
	    warning ("homogeneous coordinate = 0...\n");
	  else if (tw != 1.0)
	    {
	      ttx = tx / tw;
	      tty = ty / tw;
	    }
	  else
	    {
	      ttx = tx;
	      tty = ty;
	    }

	  /*  tx & ty are the coordinates of the point in the original
	   *  selection's floating buffer.  Make sure they're within bounds
	   */
	  if (ttx < 0)
	    itx = (int) (ttx - 0.999999);
	  else
	    itx = (int) ttx;

	  if (tty < 0)
	    ity = (int) (tty - 0.999999);
	  else
	    ity = (int) tty;

	  /*  if interpolation is on, get the fractional error  */
	  if (interpolation)
	    {
	      dx = ttx - itx;
	      dy = tty - ity;
	    }

	  if (itx >= x1 && itx < x2 && ity >= y1 && ity < y2)
	    {
	      itx -= offx;
	      ity -= offy;

	      offset = itx + ity * width;
	      /*  Set the destination pixels  */
	      if (interpolation)
		{
		  /*  Calculate the offsets into the image buffer  */
		  p1 = p3 = data_src + offset*bytes;
		  p4 = p2 = p1 + bytes;
		  p3 += rowstride;
		  p4 += rowstride;

		  a1 = p1 + alpha;
		  a2 = p2 + alpha;
		  a3 = p3 + alpha;
		  a4 = p4 + alpha;

		  /*  Enforce bounds on transformed point  */
		  if (itx + 1 >= width)
		    {
		      p2 = p1;  a2 = &empty;
		      p4 = p3;  a4 = &empty;
		    }
		  else if (itx < 0)
		    {
		      p1 = p2;  a1 = &empty;
		      p3 = p4;  a3 = &empty;
		    }
		  if (ity + 1 >= height)
		    {
		      p3 = p1;  a3 = &empty;
		      p4 = p2;  a4 = &empty;
		    }
		  else if (ity < 0)
		    {
		      p1 = p3;  a1 = &empty;
		      p2 = p4;  a2 = &empty;
		    }

		  for (b = 0; b < alpha; b++)
		    *data_dest++ = BILINEAR (*p1++, *p2++, *p3++, *p4++, dx, dy);
		  /*  The alpha channel  */
		  *data_dest++ = BILINEAR (*a1, *a2, *a3, *a4, dx, dy);
		}
	      else
		{
		  offset *= bytes;
		  for (b = 0; b < bytes; b++)
		    *data_dest++ = data_src [offset++];
		}
	    }
	  else
	    {
	      /*  increment the destination pointers  */
	      for (b = 0; b < bytes; b++)
		*data_dest ++ = bg_col[b];
	    }
	  /*  increment the transformed coordinates  */
	  tx += xinc;
	  ty += yinc;
	  tw += winc;
	}
    }

  return buf;
}

/*  Paste a transform to the gdisplay  */

static void
transform_core_paste (tool, gdisp_ptr, buf, first)
     Tool * tool;
     void * gdisp_ptr;
     TempBuf * buf;
     int first;
{
  Layer * layer;
  TransformCore * transform_core;
  TransformUndo * tu;
  GDisplay * gdisp;
  int i;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;

  /*  Create a new layer from the buffer  */
  layer = layer_from_buf (gdisp->gimage, buf, 0, 0, "Floating Selection",
			  OPAQUE, NORMAL);

  /*  Set layer offsets  */
  layer->offset_x = buf->x;
  layer->offset_y = buf->y;

  /*  Add the new layer  */
  floating_sel_swap (gdisp->gimage, layer);

  /*  Flush the gdisplays  */
  gdisplays_flush ();

  /*  create and initialize the transform_undo structure  */
  tu = (TransformUndo *) xmalloc (sizeof (TransformUndo));
  tu->tool_type = tool->type;
  for (i = 0; i < TRAN_INFO_SIZE; i++)
    tu->trans_info[i] = old_trans_info[i];
  tu->first = first;
  tu->original = NULL;

  undo_push_transform (tool->ID, (void *) tu);
}
