/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __UNDO_H__
#define __UNDO_H__

/*  Undo types  */
#define      IMAGE_UNDO              1
#define      IMAGE_MOD_UNDO          2
#define      MASK_UNDO               3
#define      FS_SWAP_UNDO            4
#define      FS_TRANSLATE_UNDO       5
#define      FS_MASK_UNDO            6
#define      LAYER_DISPLACE_UNDO     7
#define      GLOBAL_EDIT_UNDO        8
#define      TRANSFORM_UNDO          9
#define      PAINT_UNDO              10

/*  Aggregate undo types  */
#define      FS_REPLACE_UNDO         21
#define      FS_ANCHOR_UNDO          22
#define      FS_CLEAR_UNDO           23
#define      LAYER_ANCHOR_UNDO       24
#define      FLOAT_MASK_UNDO         25
#define      EDIT_CUT_UNDO           26
#define      TRANSFORM_CORE_UNDO     27
#define      PAINT_CORE_UNDO         28

/*  Undo interface functions  */

int      undo_push_group_start   (int);
int      undo_push_group_end     (void);
int      undo_push_image         (int, int, int, int, int, int, int);
int      undo_push_image_mod     (int, int, int, void *);
int      undo_push_mask          (int, void *);
int      undo_push_fs_swap       (int);
int      undo_push_fs_translate  (int);
int      undo_push_fs_mask       (int);
int      undo_push_layer_displace(int, int);
int      undo_push_global_edit   (void *);
int      undo_push_transform     (int, void *);
int      undo_push_paint         (int, void *);

int      undo_pop                (void);
int      undo_redo               (void);
void     undo_clean_stack        (void);
void     undo_free               (void);


#endif  /* __UNDO_H__ */


