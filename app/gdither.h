/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GDITHER_H__
#define __GDITHER_H__

#define FIRSTBYTE(l)   (l & 0x000000FF)
#define SECONDBYTE(l)  ((l & 0x0000FF00)>>8)
#define THIRDBYTE(l)   ((l & 0x00FF0000)>>16)
#define SECONDWORD(l)  (l >> 16)

#define DITHER_LEVEL 8
#define NUM_DITHER_MATRICES (DITHER_LEVEL*DITHER_LEVEL)

typedef unsigned char vector[DITHER_LEVEL];
typedef vector matrix[DITHER_LEVEL];

/*
 *  Data items which need to be available to scale.c
 */

extern long *red_ordered_dither;	/* array of information for ordered dithering */
extern long *green_ordered_dither;	/* array of information for ordered dithering */
extern long *blue_ordered_dither;	/* array of information for ordered dithering */
extern matrix ordered_dither_matrices[NUM_DITHER_MATRICES+1];

/*
 *  Function declarations...
 *
 */

void gdither_init ();
void gdither_free ();

void dither_segment (unsigned char *, unsigned char *, int, int, int);

#endif /* __GDITHER_H__ */


