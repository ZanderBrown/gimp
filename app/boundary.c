/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <values.h>
#include "appenv.h"
#include "errors.h"
#include "boundary.h"

/* half intensity for mask */
#define HALF_WAY 127

/* BoundSeg array growth parameter */
#define MAX_SEGS_INC  2048

/*  The array of vertical segments  */
static int *      vert_segs = NULL;

/*  The array of segments  */
static BoundSeg * tmp_segs = NULL;
static int        num_segs = 0;
static int        max_segs = 0;

/* static empty segment arrays */
static int *      empty_segs_n = NULL;
static int        num_empty_n = 0;
static int *      empty_segs_c = NULL;
static int        num_empty_c = 0;
static int *      empty_segs_l = NULL;
static int        num_empty_l = 0;
static int        max_empty_segs = 0;

/* global state variables--improve parameter efficiency */
static PixelRegion *  cur_PR;


/*  local function prototypes  */
static void find_empty_segs (PixelRegion *, int, int *, int, int *);
static void make_seg (int, int, int, int, int);
static void allocate_vert_segs ();
static void allocate_empty_segs ();
static void process_horiz_seg (int, int, int, int, int);
static void make_horiz_segs (int, int, int, int *, int, int);
static void generate_boundary (int, int, int, int);



/*  Function definitions  */

static void
find_empty_segs (maskPR, scanline, empty_segs, max_empty, num_empty)
     PixelRegion * maskPR;
     int scanline;
     int empty_segs[];
     int max_empty;
     int *num_empty;
{
  unsigned char * data;
  int x;
  int val, last;
  
  *num_empty = 0;

  if (scanline < 0 || scanline >= maskPR->h)
    {
      empty_segs[(*num_empty)++] = 0;
      empty_segs[(*num_empty)++] = MAXINT;
      return;
    }
  else
    data = maskPR->data + scanline * maskPR->rowstride + 
      (maskPR->bytes - 1);

  empty_segs[(*num_empty)++] = 0;
  last = -1;

  for (x = 0; x < maskPR->w; x++, data += maskPR->bytes)
    {
      empty_segs[*num_empty] = x;
      val = (*data > HALF_WAY) ? 1 : -1;
      if (last * val < 0)
	(*num_empty)++;
      last = val;
    }

  if (last > 0)
    empty_segs[(*num_empty)++] = x;

  empty_segs[(*num_empty)++] = MAXINT;
}


static void
make_seg (x1, y1, x2, y2, open)
     int x1, y1;
     int x2, y2;
     int open;
{
  if (num_segs >= max_segs) 
    {
      max_segs += MAX_SEGS_INC;

      tmp_segs = (BoundSeg *) xrealloc ((void *) tmp_segs,
					sizeof (BoundSeg) * max_segs);

      if (!tmp_segs)
	fatal_error ("Unable to reallocate segments array for region boundary.");
    }

  tmp_segs[num_segs].x1 = x1;
  tmp_segs[num_segs].y1 = y1;
  tmp_segs[num_segs].x2 = x2;
  tmp_segs[num_segs].y2 = y2;
  tmp_segs[num_segs].open = open;
  num_segs ++;
}


static void
allocate_vert_segs ()
{
  int i;

  /*  allocate and initialize the vert_segs array  */
  vert_segs = (int *) xrealloc ((void *) vert_segs, (cur_PR->w + 1) * sizeof (int));

  for (i = 0; i <= cur_PR->w ; i++)
    vert_segs[i] = -1;
}


static void
allocate_empty_segs ()
{
  int need_num_segs;

  /*  find the maximum possible number of empty segments given the current gregion  */
  need_num_segs = cur_PR->w + 2;

  if (need_num_segs > max_empty_segs)
    {
      max_empty_segs = need_num_segs;

      empty_segs_n = (int *) xrealloc (empty_segs_n, sizeof (int) * max_empty_segs);
      empty_segs_c = (int *) xrealloc (empty_segs_c, sizeof (int) * max_empty_segs);
      empty_segs_l = (int *) xrealloc (empty_segs_l, sizeof (int) * max_empty_segs);

      if (!empty_segs_n || !empty_segs_l || !empty_segs_c)
	fatal_error ("Unable to reallocate empty segments array for region boundary.");
    }

}


static void
process_horiz_seg (x1, y1, x2, y2, open)
     int x1, y1;
     int x2, y2;
     int open;
{
  /*  This procedure accounts for any vertical segments that must be
      drawn to close in the horizontal segments.                     */

  if (vert_segs [x1] >= 0)
    {
      make_seg (x1, vert_segs[x1], x1, y1, 1);
      vert_segs[x1] = -1;
    }
  else
    vert_segs[x1] = y1;

  if (vert_segs [x2] >= 0)
    {
      make_seg (x2, vert_segs[x2], x2, y2, 0);
      vert_segs[x2] = -1;
    }
  else
    vert_segs[x2] = y2;

  make_seg (x1, y1, x2, y2, open);
}
    

static void
make_horiz_segs (start, end, scanline, empty, num_empty, top)
     int start, end;
     int scanline;
     int empty[];
     int num_empty;
     int top;
{
  int empty_index;
  int e_s, e_e;    /* empty segment start and end values */

  for (empty_index = 0; empty_index < num_empty; empty_index += 2)
    {
      e_s = *empty++;
      e_e = *empty++;
      if (e_s <= start && e_e >= end)
	process_horiz_seg (start, scanline, end, scanline, top);
      else if ((e_s > start && e_s < end) ||
	       (e_e < end && e_e > start))
	process_horiz_seg (MAXIMUM (e_s, start), scanline,
			   MINIMUM (e_e, end), scanline, top);
    }

}


static void
generate_boundary (x1, y1, x2, y2)
     int x1, y1, x2, y2;
{
  int scanline;
  int i;
  int * tmp_segs;

  /*  array for determining the vertical line segments which must be drawn  */
  allocate_vert_segs ();

  /*  make sure there is enough space for the empty segment array  */
  allocate_empty_segs ();

  num_segs = 0;

  /*  Find the empty segments for the previous and current scanlines  */
  find_empty_segs (cur_PR, y1 - 1, empty_segs_l,
		   max_empty_segs, &num_empty_l);
  find_empty_segs (cur_PR, y1, empty_segs_c,
		   max_empty_segs, &num_empty_c);

  for (scanline = y1 ; scanline < y2; scanline++)
    {
      /*  find the empty segment list for the next scanline  */
      find_empty_segs (cur_PR, scanline + 1, empty_segs_n,
		       max_empty_segs, &num_empty_n);

      /*  process the segments on the current scanline  */
      for (i = 1; i < num_empty_c - 1; i += 2)
	{
	  make_horiz_segs (empty_segs_c [i], empty_segs_c [i+1],
			   scanline, empty_segs_l, num_empty_l, 1);
	  make_horiz_segs (empty_segs_c [i], empty_segs_c [i+1],
			   scanline+1, empty_segs_n, num_empty_n, 0);
	}

      /*  get the next scanline of empty segments, swap others  */
      tmp_segs = empty_segs_l;
      empty_segs_l = empty_segs_c;
      num_empty_l = num_empty_c;
      empty_segs_c = empty_segs_n;
      num_empty_c = num_empty_n;
      empty_segs_n = tmp_segs;
    }
}


BoundSeg *
find_mask_boundary (maskPR, num_elems, x1, y1, x2, y2)
     PixelRegion * maskPR;
     int * num_elems;
     int x1, y1, x2, y2;
{
  BoundSeg * new_segs = NULL;

  /*  The mask paramater can be any PixelRegion.  If the region
   *  has more than 1 bytes/pixel, the last byte of each pixel is
   *  used to determine the boundary outline.
   */
  cur_PR = maskPR;

  /*  Calculate the boundary  */
  generate_boundary (x1, y1, x2, y2);

  /*  Set the number of X segments  */
  *num_elems = num_segs;

  /*  Make a copy of the boundary  */
  if (num_segs)
    {
      new_segs = (BoundSeg *) xmalloc (sizeof (BoundSeg) * num_segs);
      memcpy (new_segs, tmp_segs, (sizeof (BoundSeg) * num_segs));
    }

  /*  Return the new boundary  */
  return new_segs;
}
