/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <string.h>
#include "appenv.h"
#include "gimprc.h"
#include "info_dialog.h"
#include "interface.h"

/*  static functions  */
static InfoField * info_field_new (InfoDialog *, char *, char *);
static void        update_field (InfoField *);


static InfoField *
info_field_new (idialog, title, text_ptr)
     InfoDialog *idialog;
     char * title;
     char * text_ptr;
{
  GtkWidget *label;
  InfoField *field;

  field = (InfoField *) xmalloc (sizeof (InfoField));

  label = gtk_label_new (title);
  gtk_label_set_alignment (label, 0.0, 0.5);
  gtk_box_pack (idialog->labels, label, FALSE, FALSE, 0, GTK_PACK_START);

  field->w = gtk_label_new (text_ptr);
  gtk_label_set_alignment (field->w, 0.0, 0.5);
  gtk_box_pack (idialog->values, field->w, FALSE, FALSE, 0, GTK_PACK_START);

  field->text_ptr = text_ptr;

  gtk_widget_show (field->w);
  gtk_widget_show (label);

  return field;
}

static void
update_field (field)
     InfoField * field;
{
  gchar *old_text;

  /*  only update the field if its new value differs from the old  */
  gtk_label_get (field->w, &old_text);

  if (strcmp (old_text, field->text_ptr))
    {
      /* set the new value and update somehow */
      gtk_label_set (field->w, field->text_ptr);
    }
}

/*  function definitions  */

InfoDialog *
info_dialog_new (title)
     char * title;
{
  InfoDialog * idialog;
  GtkWidget *shell;
  GtkWidget *vbox;
  GtkWidget *labels, *values;
  GtkWidget *info_area, *info_frame;
  
  idialog = (InfoDialog *) xmalloc (sizeof (InfoDialog));
  idialog->field_list = NULL;

  /*  Create the main dialog shell  */
  shell = gtk_window_new (title, GTK_WINDOW_DIALOG);
  gtk_widget_set_uposition (shell, info_x, info_y);
  
  vbox = gtk_vbox_new (FALSE, 10);
  gtk_container_set_border_width (vbox, 10);
  gtk_container_add (shell, vbox);

  info_frame = gtk_frame_new (NULL);
  gtk_frame_set_type (info_frame, GTK_SHADOW_ETCHED_IN);
  gtk_box_pack (vbox, info_frame, TRUE, TRUE, 0, GTK_PACK_START);

  info_area = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (info_area, 5);
  gtk_container_add (info_frame, info_area);

  labels = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (labels, 0);
  gtk_box_pack (info_area, labels, TRUE, TRUE, 0, GTK_PACK_START);
  
  values = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (values, 0);
  gtk_box_pack (info_area, values, TRUE, TRUE, 0, GTK_PACK_START);
  
  idialog->shell = shell;
  idialog->vbox = vbox;
  idialog->info_area = info_area;
  idialog->labels = labels;
  idialog->values = values;
  idialog->action_area = NULL;

  gtk_widget_show (idialog->labels);
  gtk_widget_show (idialog->values);
  gtk_widget_show (idialog->info_area);
  gtk_widget_show (info_frame);
  gtk_widget_show (idialog->vbox);
  
  return idialog;
}

void
info_dialog_free (idialog)
     InfoDialog * idialog;
{
  link_ptr list;

  if (!idialog)
    return;

  /*  Free each item in the field list  */
  list = idialog->field_list;

  while (list)
    {
      xfree (list->data);
      list = next_item (list);
    }
  
  /*  Free the actual field linked list  */
  free_list (idialog->field_list);

  /*  Destroy the associated widgets  */
  gtk_widget_destroy (idialog->shell);

  /*  Free the info dialog memory  */
  xfree (idialog);
}

void
info_dialog_add_field (idialog, title, text_ptr)
     InfoDialog * idialog;
     char * title;
     char * text_ptr;
{
  InfoField * new_field;

  if (!idialog)
    return;

  new_field = info_field_new (idialog, title, text_ptr);
  idialog->field_list = add_to_list (idialog->field_list, (void *) new_field);
}

void
info_dialog_popup (idialog)
     InfoDialog * idialog;
{
  if (!idialog)
    return;

  if (!GTK_WIDGET_VISIBLE (idialog->shell))
    gtk_widget_show (idialog->shell);
}

void
info_dialog_popdown (idialog)
     InfoDialog * idialog;
{
  if (!idialog)
    return;

  if (GTK_WIDGET_VISIBLE (idialog->shell))
    gtk_widget_hide (idialog->shell);
}

void
info_dialog_update (idialog)
     InfoDialog * idialog;
{
  link_ptr list;

  if (!idialog)
    return;

  list = idialog->field_list;

  while (list)
    {
      update_field ((InfoField *) list->data);
      list = next_item (list);
    }
}




