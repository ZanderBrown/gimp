/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "appenv.h"
#include "actionarea.h"
#include "buildmenu.h"
#include "colormaps.h"
#include "color_area.h"
#include "color_select.h"
#include "errors.h"
#include "general.h"
#include "gimprc.h"
#include "linked.h"
#include "image_buf.h"
#include "interface.h"
#include "palette.h"
#include "visual.h"

#define ENTRY_WIDTH  24
#define ENTRY_HEIGHT 20
#define SPACING 1
#define COLUMNS 8
#define ROWS 5

#define PREVIEW_WIDTH ((ENTRY_WIDTH * COLUMNS) + (SPACING * (COLUMNS + 1)))
#define PREVIEW_HEIGHT ((ENTRY_HEIGHT * ROWS) + (SPACING * (ROWS + 1)))

#define PALETTE_EVENT_MASK GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | GDK_ENTER_NOTIFY_MASK

typedef struct _Palette _Palette, *PaletteP;
typedef struct _PaletteEntries _PaletteEntries, *PaletteEntriesP;
typedef struct _PaletteEntry _PaletteEntry, *PaletteEntryP;

struct _Palette {
  GtkWidget *shell;
  GtkWidget *vbox;
  GtkWidget *frame;
  GtkWidget *menu;
  GtkWidget *option_menu;
  GtkWidget *color_area;
  GtkWidget *color_name;
  GtkWidget *palette_ops;
  GdkGC *gc;
  GtkDataAdjustment *sbar_data;
  GtkObserver sbar_observer;
  PaletteEntriesP entries;
  PaletteEntryP color;
  ImageBuf color_image;
  ColorSelectP color_select;
  int color_select_active;
  int scroll_offset;
  int updating;
};

struct _PaletteEntries {
  char *name;
  char *filename;
  char *working_filename;
  link_ptr colors;
  int n_colors;
  int changed;
  int working;
};

struct _PaletteEntry {
  unsigned char color[3];
  char *name;
  int position;
};


static void palette_init_palettes (void);
static void palette_free_palettes (int);
static void palette_create_palette_menu (PaletteP);
static void palette_delete_working_palettes ();
static PaletteEntriesP palette_standard_entries ();
static PaletteEntryP palette_add_entry (PaletteEntriesP, char *, int, int, int);
static void palette_delete_entry (PaletteP);
static void palette_calc_scrollbar (PaletteP);

static PaletteEntriesP palette_entries_load (char *);
static link_ptr palette_entries_insert_list (link_ptr, PaletteEntriesP);
static void palette_entries_delete (char *);
static void palette_entries_save (PaletteEntriesP, char *);
static void palette_entries_free (PaletteEntriesP);
static void palette_entry_free (PaletteEntryP);
static void palette_entries_set_callback (GtkWidget *, gpointer, gpointer);

static void palette_change_color (int, int, int, int);
static gint palette_color_area_events (GtkWidget *, GdkEvent *);
static gint palette_scroll_update (GtkObserver *, GtkData *);
static void palette_scroll_disconnect (GtkObserver *, GtkData *);
static void palette_new_callback (GtkWidget *, gpointer, gpointer);
static void palette_delete_callback (GtkWidget *, gpointer, gpointer);
static void palette_edit_callback (GtkWidget *, gpointer, gpointer);
static void palette_close_callback (GtkWidget *, gpointer, gpointer);
static void palette_new_entries_callback (GtkWidget *, gpointer, gpointer);
static void palette_add_entries_callback (GtkWidget *, gpointer, gpointer);
static void palette_merge_entries_callback (GtkWidget *, gpointer, gpointer);
static void palette_reset_entries_callback (GtkWidget *, gpointer, gpointer);
static void palette_delete_entries_callback (GtkWidget *, gpointer, gpointer);
static void palette_select_callback (int, int, int, int);

static void palette_draw_entries (PaletteP);
static void palette_draw_current_entry (PaletteP);
static void palette_update_current_entry (PaletteP);

static PaletteP         palette = NULL;
static link_ptr         palette_entries_list = NULL;
static PaletteEntriesP  default_palette_entries = NULL;
static int              num_palette_entries = 0;
static unsigned char    foreground[3] = { 0, 0, 0 };
static unsigned char    background[3] = { 255, 255, 255 };


static ActionAreaItem action_items[] =
{
  { "New", palette_new_callback, NULL, NULL },
  { "Edit", palette_edit_callback, NULL, NULL },
  { "Delete", palette_delete_callback, NULL, NULL },
  { "Close", palette_close_callback, NULL, NULL },
};

static MenuItem palette_ops[] =
{
  { "New Palette", 0, 0, palette_new_entries_callback, NULL, NULL, NULL },
  { "Merge Palette", 0, 0, palette_merge_entries_callback, NULL, NULL, NULL },
  { "Save Palette", 0, 0, NULL, NULL, NULL, NULL },
  { "Delete Palette", 0, 0, palette_delete_entries_callback, NULL, NULL, NULL },
  { "Reset Palettes", 0, 0, palette_reset_entries_callback, NULL, NULL, NULL },
  { "Close", 0, 0, palette_close_callback, NULL, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};

void
palettes_init ()
{
  palette_init_palettes ();
}

void
palettes_free ()
{
  palette_free_palettes (1);
}

void
palette_create ()
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *sbar;
  GtkWidget *frame;
  GtkWidget *options_box;
  GtkWidget *action_area;
  GtkWidget *arrow_hbox;
  GtkWidget *label;
  GtkWidget *arrow;
  GtkWidget *menu_bar;
  GtkWidget *menu_bar_item;

  if (!palette)
    {
      palette = xmalloc (sizeof (_Palette));

      palette->entries = default_palette_entries;
      palette->color = NULL;
      palette->color_select = NULL;
      palette->color_select_active = 0;
      palette->scroll_offset = 0;
      palette->gc = NULL;

      /*  The shell and main vbox  */
      palette->shell = gtk_window_new ("Color Palette", GTK_WINDOW_TOPLEVEL);
      vbox = gtk_vbox_new (FALSE, 5);
      gtk_container_add (palette->shell, vbox);

      /*  The palette options box  */
      options_box = gtk_hbox_new (FALSE, 2);
      gtk_container_set_border_width (options_box, 0);
      gtk_box_pack (vbox, options_box, FALSE, FALSE, 0, GTK_PACK_START);

      /*  The popup menu -- palette_ops  */
      palette_ops[0].user_data = palette;
      palette_ops[1].user_data = palette;
      palette_ops[2].user_data = palette;
      palette_ops[3].user_data = palette;
      palette_ops[4].user_data = palette;
      palette_ops[5].user_data = palette;
      palette_ops[6].user_data = palette;
      palette->palette_ops = build_menu (palette_ops, NULL);

      /*  The palette commands pulldown menu  */
      menu_bar = gtk_menu_bar_new ();
      gtk_box_pack (options_box, menu_bar, FALSE, FALSE, 0, GTK_PACK_START);
      menu_bar_item = gtk_menu_item_new ();
      gtk_container_add (menu_bar, menu_bar_item);
      gtk_menu_item_set_submenu (menu_bar_item, palette->palette_ops);
      arrow_hbox = gtk_hbox_new (FALSE, 0);
      gtk_container_set_border_width (arrow_hbox, 0);
      gtk_container_add (menu_bar_item, arrow_hbox);
      label = gtk_label_new ("Ops");
      arrow = gtk_arrow_new (GTK_ARROW_DOWN, GTK_SHADOW_OUT);
      gtk_box_pack (arrow_hbox, arrow, FALSE, FALSE, 0, GTK_PACK_START);
      gtk_box_pack (arrow_hbox, label, FALSE, FALSE, 4, GTK_PACK_START);
      gtk_label_set_alignment (label, 0.5, 0.5);
      gtk_arrow_set_alignment (arrow, 0.5, 0.5);

      gtk_widget_show (arrow);
      gtk_widget_show (label);
      gtk_widget_show (arrow_hbox);
      gtk_widget_show (menu_bar_item);
      gtk_widget_show (menu_bar);

      palette->option_menu = gtk_option_menu_new ();
      gtk_box_pack (options_box, palette->option_menu, TRUE, TRUE, 0, GTK_PACK_START);
      gtk_widget_show (palette->option_menu);

      palette_create_palette_menu (palette);

      gtk_widget_show (options_box);

      /*  The active color name  */
      palette->color_name = gtk_text_entry_new ();
      gtk_text_entry_set_text (palette->color_name, "Active Color Name");
      gtk_box_pack (vbox, palette->color_name, FALSE, FALSE, 0, GTK_PACK_START);

      gtk_widget_show (palette->color_name);

      /*  The horizontal box containing preview & scrollbar  */
      hbox = gtk_hbox_new (FALSE, 2);
      gtk_container_set_border_width (hbox, 0);
      gtk_box_pack (vbox, hbox, TRUE, TRUE, 0, GTK_PACK_START);
      frame = gtk_frame_new (NULL);
      gtk_frame_set_type (frame, GTK_SHADOW_IN);
      gtk_box_pack (hbox, frame, FALSE, FALSE, 0, GTK_PACK_START);
      palette->sbar_data = (GtkDataAdjustment *)
	gtk_data_adjustment_new (0, 0, PREVIEW_HEIGHT, 1, 1, PREVIEW_HEIGHT);
      sbar = gtk_vscrollbar_new (palette->sbar_data);
      gtk_box_pack (hbox, sbar, FALSE, FALSE, 0, GTK_PACK_START);

      /*  Create the color area window and the underlying image  */
      palette->color_area = gtk_drawing_area_new (PREVIEW_WIDTH, PREVIEW_HEIGHT,
						  palette_color_area_events,
						  PALETTE_EVENT_MASK);
      gtk_widget_set_user_data (palette->color_area, (gpointer) palette);
      gtk_container_add (frame, palette->color_area);

      gtk_widget_show (palette->color_area);
      gtk_widget_show (sbar);
      gtk_widget_show (frame);
      gtk_widget_show (hbox);

      /*  The action area  */
      action_items[0].user_data = palette;
      action_items[1].user_data = palette;
      action_items[2].user_data = palette;
      action_items[3].user_data = palette;
      action_area = build_action_area (action_items, 4);
      gtk_box_pack (vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);

      gtk_widget_show (action_area);
      gtk_widget_show (vbox);
      gtk_widget_show (palette->shell);
      install_colormap (RGB);

      palette->color_image = image_buf_create (COLOR_BUF, PREVIEW_WIDTH, PREVIEW_HEIGHT);
    }
  else
    {
      if (!GTK_WIDGET_VISIBLE (palette->shell))
	{
	  gtk_widget_show (palette->shell);
	  install_colormap (RGB);
	}
    }
}

void
palette_free ()
{
  if (palette)
    {
      gdk_gc_destroy (palette->gc);
      image_buf_destroy (palette->color_image);

      if (palette->color_select)
	color_select_free (palette->color_select);

      xfree (palette);

      palette = NULL;
    }
}

void
palette_get_foreground (r, g, b)
     unsigned char *r, *g, *b;
{
  *r = foreground[0];
  *g = foreground[1];
  *b = foreground[2];
}

void
palette_get_background (r, g, b)
     unsigned char *r, *g, *b;
{
  *r = background[0];
  *g = background[1];
  *b = background[2];
}

void
palette_set_foreground (r, g, b)
     int r, g, b;
{
  unsigned char rr, gg, bb;

  /*  Foreground  */
  foreground[0] = r;
  foreground[1] = g;
  foreground[2] = b;

  palette_get_foreground (&rr, &gg, &bb);
  store_color (&foreground_pixel, rr, gg, bb);
  color_area_update ();
}

void
palette_set_background (r, g, b)
     int r, g, b;
{
  unsigned char rr, gg, bb;

  /*  Background  */
  background[0] = r;
  background[1] = g;
  background[2] = b;

  palette_get_background (&rr, &gg, &bb);
  store_color (&background_pixel, rr, gg, bb);
  color_area_update ();
}


/*****************************************/
/*         Local functions               */
/*****************************************/

static void
palette_init_palettes ()
{
  DIR *dir;
  PaletteEntriesP pal;
  char filename[256];
  char *home = NULL;
  char *path = NULL;
  char *local_path, *token;
  struct stat buf;
  int err;
  struct dirent * dir_ent;
  link_ptr path_list, list;

  if (!palette_path)
    return;

  home = getenv ("HOME");

  /*  configure the local_path so that it includes the temp_path, which
   *  will be the location of any temporary palette working files
   */
  local_path = xmalloc (strlen (palette_path) + strlen (temp_path) + 2);
  sprintf (local_path, "%s:%s", palette_path, temp_path);
  token = strtok (local_path, ":");

  path_list = NULL;
  while (token)
    {
      if (*token == '~')
	{
	  /*  malloc the path to one extra character for appending a '/' character  */
	  path = xmalloc (strlen (home) + strlen (token) + 1);
	  sprintf (path, "%s%s", home, token + 1);
	}
      else
	path = xstrdup (token);

      path_list = append_to_list (path_list, path);
      token = strtok (NULL, ":");
    }

  list = path_list;
  while (list)
    {
      path = (char *) list->data;

      /*  see if the directory exists and if it has any items in it  */
      err = stat (path, &buf);
      if (!err && S_ISDIR (buf.st_mode))
	{
	  if (path[strlen (path) - 1] != '/')
	    strcat (path, "/");

	  /*  open the palette directory  */
	  if (! (dir= opendir (path)))
	    warning ("error reading palettes from directory \"%s\"", path);
	  else
	    {
	      while ( (dir_ent = readdir (dir)) )
		{
		  sprintf (filename, "%s%s", path, dir_ent->d_name);
		  /*  double check the filename--especially that it is not a sub-dir  */
		  err = stat (filename, &buf);
		  if (!err && S_ISREG (buf.st_mode))
		    {
		      pal = palette_entries_load (filename);
		      if (pal)
			{
			  palette_entries_list =
			    palette_entries_insert_list (palette_entries_list, pal);

			  /*  Check if the current palette is the default one  */
			  if (strcmp (default_palette, pal->name) == 0)
			    default_palette_entries = pal;
			}
		    }
		}
	      closedir (dir);
	    }
	}

      xfree (path);
      list = next_item (list);
    }

  free_list (path_list);
  xfree (local_path);
}

static void
palette_free_palettes (save_working)
     int save_working;
{
  link_ptr list;
  PaletteEntriesP entries;
  char *filename;

  list = palette_entries_list;

  while (list)
    {
      entries = (PaletteEntriesP) list->data;

      /*  If the palette has been changed, save it as a working palette  */
      if (entries->changed && save_working)
	{
	  /*  set the working flag  */
	  entries->working = 1;
	  /*  set the filename to be in the temp_path  */
	  filename = xmalloc (strlen (entries->name) + strlen (temp_path) + 10);
	  sprintf (filename, "%s/%s.working", temp_path, entries->name);
	  /*  save the palette  */
	  palette_entries_save (entries, filename);

	  xfree (filename);
	}

      palette_entries_free (entries);
      list = next_item (list);
    }
  free_list (palette_entries_list);

  if (palette)
    {
      palette->entries = NULL;
      palette->color = NULL;
      palette->color_select = NULL;
      palette->color_select_active = 0;
      palette->scroll_offset = 0;
    }
  num_palette_entries = 0;
  palette_entries_list = NULL;
}

static void
palette_create_palette_menu (palette)
     PaletteP palette;
{
  MenuItem  *palettes;
  link_ptr   list;
  PaletteEntriesP p_entries;
  int i = 0;
  int default_index = -1;

  /*  The palettes option menu  */
  palettes = (MenuItem *) xmalloc (sizeof (MenuItem) * (num_palette_entries + 1));
  list = palette_entries_list;
  while (list)
    {
      p_entries = (PaletteEntriesP) list->data;

      if (p_entries == default_palette_entries)
	default_index = i;

      palettes[i].label = p_entries->name;
      palettes[i].accelerator_key = 0;
      palettes[i].accelerator_mods = 0;
      palettes[i].callback = palette_entries_set_callback;
      palettes[i].user_data = p_entries;
      palettes[i].subitems = NULL;
      i++;
      list = next_item (list);
    }
  palettes[i].label = NULL;

  palette->menu = build_menu (palettes, NULL);
  gtk_option_menu_set_menu (palette->option_menu, palette->menu);

  /*  Set the current item of the option menu to reflect the default palette  */
  if (default_index != -1)
    gtk_option_menu_set_history (palette->option_menu, default_index);

  xfree (palettes);
}

static void
palette_delete_working_palettes ()
{
  link_ptr list;
  PaletteEntriesP entries;

  list = palette_entries_list;
  while (list)
    {
      entries = (PaletteEntriesP) list->data;
      if (entries->working)
	palette_entries_delete (entries->working_filename);

      list = next_item (list);
    }
}

static PaletteEntriesP
palette_entries_load (filename)
     char *filename;
{
  PaletteEntriesP entries;
  char str[512];
  char * tok;
  FILE * fp;
  int r, g, b;

  entries = (PaletteEntriesP) xmalloc (sizeof (_PaletteEntries));

  entries->filename = xstrdup (filename);
  entries->name = xstrdup (prune_filename (filename));
  entries->working_filename = NULL;
  entries->colors = NULL;
  entries->n_colors = 0;
  entries->working = 0;

  /*  Open the requested file  */
  if (! (fp = fopen (filename, "r")))
    {
      warning ("can't load palette \"%s\"\n", filename);
      palette_entries_free (entries);
      return NULL;
    }

  fread (str, 13, 1, fp);
  str[13] = '\0';
  if (strcmp (str, "GIMP Palette\n"))
    {
      fclose (fp);
      return NULL;
    }

  while (!feof (fp))
    {
      if (!fgets (str, 512, fp))
	continue;

      if (str[0] != '#')
	{
	  tok = strtok (str, " \t");

	  /*  If the first token is "Working", then this is a working palette...
	   *  read in the correct filename and reset the name field
	   */
	  if (! strcmp (tok, "Working"))
	    {
	      tok = strtok (NULL, "\n");
	      entries->working_filename = entries->filename;
	      xfree (entries->name);
	      entries->filename = xstrdup (tok);
	      entries->name = xstrdup (prune_filename (entries->filename));
	      entries->working = 1;
	      entries->changed = 1;
	    }
	  else
	    {
	      if (tok)
		r = atoi (tok);
	      tok = strtok (NULL, " \t");
	      if (tok)
		g = atoi (tok);
	      tok = strtok (NULL, " \t");
	      if (tok)
		b = atoi (tok);

	      tok = strtok (NULL, "\n");

	      palette_add_entry (entries, tok, r, g, b);
	    }
	}
    }

  /*  Clean up  */
  fclose (fp);

  entries->changed = 0;

  return entries;
}

static void
palette_entries_delete (filename)
     char *filename;
{
  unlink (filename);
}

static link_ptr
palette_entries_insert_list (list, entries)
     link_ptr list;
     PaletteEntriesP entries;
{
  link_ptr tmp;
  PaletteEntriesP p;

  /*  A working palette is one that has been
   *  modified during the course of a gimp session and
   *  expected to load back to it's previous stage without
   *  an explicit save command.
   */

  /*  If it's not a working palette, just add it to the list  */
  if (!entries->working)
    {
      num_palette_entries++;
      return append_to_list (list, (void *) entries);
    }

  /*  Find the palette it is supposed to replace, and replace it  */
  tmp = list;

  while (tmp)
    {
      p = (PaletteEntriesP) tmp->data;
      if (! strcmp (p->filename, entries->filename))
	{
	  /*  replace the entries and free the old one  */
	  tmp->data = entries;
	  palette_entries_free (p);
	  return list;
	}
      tmp = next_item (tmp);
    }

  /*  If we get here, there was no matching palette --
   *  add the working palette to the list
   */
  num_palette_entries++;
  return append_to_list (list, (void *) entries);
}

static void
palette_entries_save (palette, filename)
     PaletteEntriesP palette;
     char *filename;
{
  FILE * fp;
  link_ptr list;
  PaletteEntryP entry;

  /*  Open the requested file  */
  if (! (fp = fopen (filename, "w")))
    {
      warning ("can't save palette \"%s\"\n", filename);
      return;
    }

  fprintf (fp, "GIMP Palette\n");
  if (palette->working)
    fprintf (fp, "Working\t%s\n", palette->filename);
  fprintf (fp, "# %s -- GIMP Palette file\n", palette->name);

  list = palette->colors;
  while (list)
    {
      entry = (PaletteEntryP) list->data;
      fprintf (fp, "%d %d %d\t%s\n", entry->color[0], entry->color[1],
	       entry->color[2], entry->name);
      list = next_item (list);
    }

  /*  Clean up  */
  fclose (fp);
}

static void
palette_entries_free (entries)
     PaletteEntriesP entries;
{
  PaletteEntryP entry;
  link_ptr list;

  list = entries->colors;
  while (list)
    {
      entry = (PaletteEntryP) list->data;
      palette_entry_free (entry);
      list = list->next;
    }

  xfree (entries->name);
  xfree (entries->filename);
  if (entries->working_filename)
    xfree (entries->working_filename);
  xfree (entries);
}

static void
palette_entry_free (entry)
     PaletteEntryP entry;
{
  if (entry->name)
    xfree (entry->name);

  xfree (entry);
}

static void
palette_entries_set_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PaletteEntriesP pal;

  if (palette)
    {
      pal = (PaletteEntriesP) client_data;

      palette->entries = pal;
      palette->color = NULL;
      if (palette->color_select_active)
	{
	  palette->color_select_active = 0;
	  color_select_hide (palette->color_select);
	}
      palette->color_select = NULL;
      palette->scroll_offset = 0;

      palette_calc_scrollbar (palette);
      palette_draw_entries (palette);
      palette_draw_current_entry (palette);
    }
}

static void
palette_change_color (r, g, b, state)
     int r, g, b;
     int state;
{
  if (palette)
    {
      switch (state)
	{
	case COLOR_NEW:
	  palette->color = palette_add_entry (palette->entries, "Untitled", r, g, b);

	  palette_calc_scrollbar (palette);
	  palette_draw_entries (palette);
	  palette_draw_current_entry (palette);
	  break;

	case COLOR_UPDATE:
	  break;

	case COLOR_FINISH:
	  break;
	}
    }

  if (active_color == FOREGROUND)
    palette_set_foreground (r, g, b);
  else if (active_color == BACKGROUND)
    palette_set_background (r, g, b);
}

void
palette_set_active_color (r, g, b, state)
     int r, g, b;
     int state;
{
  palette_change_color (r, g, b, state);
}

static gint
palette_color_area_events (widget, event)
     GtkWidget *widget;
     GdkEvent *event;
{
  GdkEventButton *bevent;
  PaletteP palette;
  link_ptr tmp_link;
  int r, g, b;
  int width, height;
  int entry_width;
  int entry_height;
  int row, col;
  int pos;

  palette = (PaletteP) gtk_widget_get_user_data (widget);
  if (!palette)
    return FALSE;

  switch (event->type)
    {
    case GDK_EXPOSE:
      /*  If this is the first exposure  */
      if (!palette->gc)
	{
	  palette->gc = gdk_gc_new (palette->color_area->window);

	  /*  Setup the scrollbar observer  */
	  palette->sbar_observer.update = palette_scroll_update;
	  palette->sbar_observer.disconnect = palette_scroll_disconnect;
	  palette->sbar_observer.user_data = (gpointer) palette;
	  gtk_data_attach ((GtkData *) palette->sbar_data, &palette->sbar_observer);

	  palette->updating = 1;
	  if (!palette->entries)
	    {
	      default_palette_entries = palette_standard_entries ();
	      palette->entries = default_palette_entries;
	    }

	  palette_calc_scrollbar (palette);
	  palette->updating = 0;
	}

      palette_draw_entries (palette);
      palette_draw_current_entry (palette);
      break;

    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;

      if (bevent->button == 1)
	{
	  width = image_buf_width (palette->color_image);
	  height = image_buf_height (palette->color_image);
	  entry_width = ((width - (SPACING * (COLUMNS + 1))) / COLUMNS) + SPACING;
	  entry_height = ((height - (SPACING * (ROWS + 1))) / ROWS) + SPACING;

	  col = (bevent->x - 1) / entry_width;
	  row = (palette->scroll_offset + bevent->y - 1) / entry_height;
	  pos = row * COLUMNS + col;

	  tmp_link = nth_item (palette->entries->colors, pos);
	  if (tmp_link)
	    {
	      palette_draw_current_entry (palette);
	      palette->color = tmp_link->data;

	      /*  Update either foreground or background colors  */
	      r = palette->color->color[0];
	      g = palette->color->color[1];
	      b = palette->color->color[2];
	      if (active_color == FOREGROUND)
		palette_set_foreground (r, g, b);
	      else if (active_color == BACKGROUND)
		palette_set_background (r, g, b);

	      palette_update_current_entry (palette);
	    }
	}
      break;

    case GDK_ENTER_NOTIFY:
      install_colormap (RGB);
      break;

    default:
      break;
    }

  return FALSE;
}

static gint
palette_scroll_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  PaletteP palette;

  adj_data = (GtkDataAdjustment *) data;
  palette = (PaletteP) observer->user_data;

  if (palette)
    {
      palette->scroll_offset = adj_data->value;
      palette_draw_entries (palette);
      palette_draw_current_entry (palette);
    }

  return FALSE;
}

static void
palette_scroll_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

static void
palette_new_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PaletteP palette;

  palette = client_data;
  if (palette)
    {
      if (active_color == FOREGROUND)
	palette->color =
	  palette_add_entry (palette->entries, "Untitled",
			     foreground[0], foreground[1], foreground[2]);
      else if (active_color == BACKGROUND)
	palette->color =
	  palette_add_entry (palette->entries, "Untitled",
			     background[0], background[1], background[2]);

      palette_calc_scrollbar (palette);
      palette_draw_entries (palette);
      palette_draw_current_entry (palette);
    }
}

static void
palette_delete_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PaletteP palette;

  palette = client_data;
  if (palette)
    palette_delete_entry (palette);
}

static void
palette_edit_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PaletteP palette;
  unsigned char *color;

  palette = client_data;
  if (palette && palette->color)
    {
      color = palette->color->color;

      if (!palette->color_select)
	{
	  palette->color_select = color_select_new (color[0], color[1], color[2],
						    palette_select_callback);
	  palette->color_select_active = 1;
	}
      else
	{
	  if (!palette->color_select_active)
	    {
	      color_select_show (palette->color_select);
	      palette->color_select_active = 1;
	    }

	  color_select_set_color (palette->color_select, color[0], color[1], color[2], 1);
	}
    }
}

static void
palette_close_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PaletteP palette;

  palette = client_data;
  if (palette)
    {
      if (palette->color_select_active)
	{
	  palette->color_select_active = 0;
	  color_select_hide (palette->color_select);
	}

      if (GTK_WIDGET_VISIBLE (palette->shell))
	gtk_widget_hide (palette->shell);
    }
}

static void
palette_new_entries_callback (w, client_data, call_data)
     GtkWidget * w;
     gpointer client_data;
     gpointer call_data;
{
  query_name_box ("New Palette", "Enter a name for new palette",
		  palette_add_entries_callback, NULL);
}

static void
palette_add_entries_callback (w, client_data, call_data)
     GtkWidget * w;
     gpointer client_data;
     gpointer call_data;
{
  char * palette_name;
  PaletteEntriesP entries;

  palette_name = call_data;
  if (palette)
    {
      entries = xmalloc (sizeof (_PaletteEntries));
      entries->filename = xmalloc (strlen (temp_path) + strlen (palette_name) + 2);
      sprintf (entries->filename, "%s/%s", temp_path, palette_name);
      entries->name = palette_name;  /*  don't need to copy because this memory is ours  */
      entries->colors = NULL;
      entries->n_colors = 0;
      entries->working = 0;
      entries->changed = 1;

      palette_entries_list = palette_entries_insert_list (palette_entries_list, entries);

      gtk_widget_destroy (palette->menu);
      palette_create_palette_menu (palette);
    }
}

static void
palette_merge_entries_callback (w, client_data, call_data)
     GtkWidget * w;
     gpointer client_data;
     gpointer call_data;
{
}

static void
palette_delete_entries_callback (w, client_data, call_data)
     GtkWidget * w;
     gpointer client_data;
     gpointer call_data;
{
  PaletteP palette;
  PaletteEntriesP entries;

  palette = client_data;
  if (palette)
    {
      /*  If a color selection dialog is up, hide it  */
      if (palette->color_select_active)
	{
	  palette->color_select_active = 0;
	  color_select_hide (palette->color_select);
	}

      gtk_widget_destroy (palette->menu);

      entries = palette->entries;
      if (entries)
	{
	  if (entries->filename)
	    palette_entries_delete (entries->filename);
	  if (entries->working)
	    palette_entries_delete (entries->working_filename);
	}

      palette_free_palettes (0);  /*  free palettes, don't save any modified versions  */
      palette_init_palettes ();   /*  load in brand new palettes  */

      palette_create_palette_menu (palette);
      palette->entries = default_palette_entries;
      palette_calc_scrollbar (palette);
      palette_draw_entries (palette);
      palette_draw_current_entry (palette);
    }
}

static void
palette_reset_entries_callback (w, client_data, call_data)
     GtkWidget * w;
     gpointer client_data;
     gpointer call_data;
{
  PaletteP palette;

  palette = client_data;
  if (palette)
    {
      /*  If a color selection dialog is up, hide it  */
      if (palette->color_select_active)
	{
	  palette->color_select_active = 0;
	  color_select_hide (palette->color_select);
	}

      gtk_widget_destroy (palette->menu);

      palette_delete_working_palettes ();
      palette_free_palettes (0);  /*  free palettes, don't save any modified versions  */
      palette_init_palettes ();   /*  load in brand new palettes  */

      palette_create_palette_menu (palette);
      palette->entries = default_palette_entries;
      palette_calc_scrollbar (palette);
      palette_draw_entries (palette);
      palette_draw_current_entry (palette);
    }
}

static void
palette_select_callback (r, g, b, cancelled)
     int r, g, b;
     int cancelled;
{
  unsigned char * color;

  if (palette)
    {
      color_select_hide (palette->color_select);
      palette->color_select_active = 0;

      if (!cancelled && palette->color)
	{
	  color = palette->color->color;

	  color[0] = r;
	  color[1] = g;
	  color[2] = b;

	  /*  Update either foreground or background colors  */
	  if (active_color == FOREGROUND)
	    palette_set_foreground (r, g, b);
	  else if (active_color == BACKGROUND)
	    palette_set_background (r, g, b);

	  palette_calc_scrollbar (palette);
	  palette_draw_entries (palette);
	  palette_draw_current_entry (palette);
	}
    }
}

static int
palette_draw_color_row (colors, ncolors, y, buffer, image)
     unsigned char **colors;
     int ncolors, y;
     unsigned char *buffer;
     ImageBuf image;
{
  unsigned char *p;
  unsigned char bcolor;
  int width, height;
  int entry_width;
  int entry_height;
  int vsize;
  int vspacing;
  int i, j;

  bcolor = 0;

  width = image_buf_width (image);
  height = image_buf_height (image);
  entry_width = (width - (SPACING * (COLUMNS + 1))) / COLUMNS;
  entry_height = (height - (SPACING * (ROWS + 1))) / ROWS;

  if ((y >= 0) && ((y + SPACING) < height))
    vspacing = SPACING;
  else if (y < 0)
    vspacing = SPACING + y;
  else
    vspacing = height - y;

  if (vspacing > 0)
    {
      if (y < 0)
	y += SPACING - vspacing;

      for (i = SPACING - vspacing; i < SPACING; i++, y++)
	{
	  p = buffer;
	  for (j = 0; j < width; j++)
	    {
	      *p++ = bcolor;
	      *p++ = bcolor;
	      *p++ = bcolor;
	    }

	  image_buf_draw_row (image, buffer, 0, y, image_buf_width (image));
	}

      if (y > SPACING)
	y += SPACING - vspacing;
    }
  else
    y += SPACING;

  vsize = (y >= 0) ? (entry_height) : (entry_height + y);

  if ((y >= 0) && ((y + entry_height) < height))
    vsize = entry_height;
  else if (y < 0)
    vsize = entry_height + y;
  else
    vsize = height - y;

  if (vsize > 0)
    {
      p = buffer;
      for (i = 0; i < ncolors; i++)
	{
	  for (j = 0; j < SPACING; j++)
	    {
	      *p++ = bcolor;
	      *p++ = bcolor;
	      *p++ = bcolor;
	    }

	  for (j = 0; j < entry_width; j++)
	    {
	      *p++ = colors[i][0];
	      *p++ = colors[i][1];
	      *p++ = colors[i][2];
	    }
	}

      for (i = 0; i < (COLUMNS - ncolors); i++)
	{
	  for (j = 0; j < (SPACING + entry_width); j++)
	    {
	      *p++ = 0;
	      *p++ = 0;
	      *p++ = 0;
	    }
	}

      for (j = 0; j < SPACING; j++)
	{
	  *p++ = bcolor;
	  *p++ = bcolor;
	  *p++ = bcolor;
	}

      if (y < 0)
	y += entry_height - vsize;
      for (i = 0; i < vsize; i++, y++)
	image_buf_draw_row (image, buffer, 0, y, image_buf_width (image));
      if (y > entry_height)
	y += entry_height - vsize;
    }
  else
    y += entry_height;

  return y;
}

static void
palette_draw_entries (palette)
     PaletteP palette;
{
  PaletteEntryP entry;
  unsigned char *buffer;
  unsigned char *colors[COLUMNS];
  link_ptr tmp_link;
  int width, height;
  int entry_width;
  int entry_height;
  int row_vsize;
  int index, y;

  if (palette && palette->entries && !palette->updating)
    {
      width = image_buf_width (palette->color_image);
      height = image_buf_height (palette->color_image);
      entry_width = (width - (SPACING * (COLUMNS + 1))) / COLUMNS;
      entry_height = (height - (SPACING * (ROWS + 1))) / ROWS;

      buffer = xmalloc (width * 3);

      y = -palette->scroll_offset;
      row_vsize = SPACING + entry_height;
      tmp_link = palette->entries->colors;
      index = 0;

      while ((tmp_link) && (y < -row_vsize))
	{
	  tmp_link = tmp_link->next;

	  if (++index == COLUMNS)
	    {
	      index = 0;
	      y += row_vsize;
	    }
	}

      index = 0;
      while (tmp_link)
	{
	  entry = tmp_link->data;
	  tmp_link = tmp_link->next;

	  colors[index] = entry->color;
	  index++;

	  if (index == COLUMNS)
	    {
	      index = 0;
	      y = palette_draw_color_row (colors, COLUMNS, y, buffer, palette->color_image);
	      if (y >= height)
		break;
	    }
	}

      while (y < height)
	{
	  y = palette_draw_color_row (colors, index, y, buffer, palette->color_image);
	  index = 0;
	}

      image_buf_put (palette->color_image, palette->color_area->window, palette->gc);

      xfree (buffer);
    }
}

static void
palette_draw_current_entry (palette)
     PaletteP palette;
{
  PaletteEntryP entry;
  int width, height;
  int entry_width;
  int entry_height;
  int row, col;
  int x, y;

  if (palette && !palette->updating && palette->color)
    {
      gdk_gc_set_function (palette->gc, GDK_INVERT);

      entry = palette->color;

      row = entry->position / COLUMNS;
      col = entry->position % COLUMNS;

      entry_width = (image_buf_width (palette->color_image) -
		     (SPACING * (COLUMNS + 1))) / COLUMNS;
      entry_height = (image_buf_height (palette->color_image) -
		      (SPACING * (ROWS + 1))) / ROWS;

      x = col * (entry_width + SPACING);
      y = row * (entry_height + SPACING);
      y -= palette->scroll_offset;

      width = entry_width + SPACING;
      height = entry_height + SPACING;

      gdk_draw_rectangle (palette->color_area->window, palette->gc,
			  0, x, y, width, height);

      gdk_gc_set_function (palette->gc, GDK_COPY);
    }
}

static void
palette_update_current_entry (palette)
     PaletteP palette;
{
  /*  Draw the current entry  */
  palette_draw_current_entry (palette);

  /*  Update the active color name  */
  gtk_text_entry_set_text (palette->color_name, palette->color->name);
}

static PaletteEntryP
palette_add_entry (palette, name, r, g, b)
     PaletteEntriesP palette;
     char *name;
     int r, g, b;
{
  PaletteEntryP entry;

  if (palette)
    {
      entry = xmalloc (sizeof (_PaletteEntry));

      entry->color[0] = r;
      entry->color[1] = g;
      entry->color[2] = b;
      if (name)
	entry->name = xstrdup (name);
      else
	entry->name = xstrdup ("Untitled");
      entry->position = palette->n_colors;

      palette->colors = append_to_list (palette->colors, entry);
      palette->n_colors += 1;

      palette->changed = 1;

      return entry;
    }

  return NULL;
}

static PaletteEntriesP
palette_standard_entries ()
{
#define STEPS 20
  PaletteEntriesP new_entries;
  char buf[32];
  int i, r, g, b;

  new_entries = (PaletteEntriesP) xmalloc (sizeof (_PaletteEntries));
  new_entries->filename = NULL;
  new_entries->name = NULL;
  new_entries->n_colors = 0;
  new_entries->colors = NULL;

  for (i = 0; i < STEPS; i++)
    {
      r = g = b = (i * 255) / (STEPS - 1);
      sprintf (buf, "Gray %f", (float) i / (float) (STEPS - 1));
      palette_add_entry (new_entries, buf, r, g, b);
    }

  for (i = 0; i < STEPS; i++)
    {
      r = (i * 255) / (STEPS - 1);
      g = b = 0;
      sprintf (buf, "Red %f", (float) i / (float) (STEPS - 1));
      palette_add_entry (new_entries, buf, r, g, b);
    }

  for (i = 0; i < STEPS; i++)
    {
      g = (i * 255) / (STEPS - 1);
      r = b = 0;
      sprintf (buf, "Green %f", (float) i / (float) (STEPS - 1));
      palette_add_entry (new_entries, buf, r, g, b);
    }

  for (i = 0; i < STEPS; i++)
    {
      b = (i * 255) / (STEPS - 1);
      r = g = 0;
      sprintf (buf, "Blue %f", (float) i / (float) (STEPS - 1));
      palette_add_entry (new_entries, buf, r, g, b);
    }

  new_entries->changed = 0;

  return new_entries;
}

static void
palette_delete_entry (palette)
     PaletteP palette;
{
  PaletteEntryP entry;
  link_ptr tmp_link;
  int pos;

  if (palette && palette->color)
    {
      entry = palette->color;
      palette->entries->colors = remove_from_list (palette->entries->colors, entry);
      palette->entries->n_colors--;
      palette->entries->changed = 1;

      pos = entry->position;
      palette_entry_free (entry);

      tmp_link = nth_item (palette->entries->colors, pos);

      if (tmp_link)
	{
	  palette->color = tmp_link->data;

	  while (tmp_link)
	    {
	      entry = tmp_link->data;
	      tmp_link = tmp_link->next;
	      entry->position = pos++;
	    }
	}
      else
	{
	  tmp_link = nth_item (palette->entries->colors, pos - 1);
	  if (tmp_link)
	    palette->color = tmp_link->data;
	}

      if (palette->entries->n_colors == 0)
	palette->color = palette_add_entry (palette->entries, "Black", 0, 0, 0);

      palette_calc_scrollbar (palette);
      palette_draw_entries (palette);
      palette_draw_current_entry (palette);
    }
}

static void
palette_calc_scrollbar (palette)
     PaletteP palette;
{
  int n_entries;
  int cur_entry_row;
  int nrows;
  int row_vsize;
  int vsize;
  int page_size;
  int new_offset;

  if (palette)
    {
      n_entries = palette->entries->n_colors;
      nrows = n_entries / COLUMNS;
      if (n_entries % COLUMNS)
	nrows += 1;
      row_vsize = SPACING + ((image_buf_height (palette->color_image) -
			      (SPACING * (ROWS + 1))) / ROWS);
      vsize = row_vsize * nrows;
      page_size = row_vsize * ROWS;

      if (palette->color)
	cur_entry_row = palette->color->position / COLUMNS;
      else
	cur_entry_row = 0;

      new_offset = cur_entry_row * row_vsize;

      /* scroll only if necessary */
      if (new_offset < palette->scroll_offset)
	{
	  palette->scroll_offset = new_offset;
	}
      else if (new_offset > palette->scroll_offset)
	{
	  /* only scroll the minimum amount to bring the current color into view */
	  if ((palette->scroll_offset + page_size - row_vsize) < new_offset)
	    palette->scroll_offset = new_offset - (page_size - row_vsize);
	}

      /* sanity check to make sure the scrollbar offset is valid */
      if (vsize > page_size)
	if (palette->scroll_offset > (vsize - page_size))
	  palette->scroll_offset = vsize - page_size;

      palette->sbar_data->value = palette->scroll_offset;
      palette->sbar_data->upper = vsize;
      palette->sbar_data->page_size = (page_size < vsize) ? page_size : vsize;
      palette->sbar_data->page_increment = page_size;
      palette->sbar_data->step_increment = row_vsize;

      gtk_data_notify ((GtkData *) palette->sbar_data);
    }
}
