/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>

#include "appenv.h"
#include "errors.h"
#include "fileops.h"
#include "general.h"
#include "gimprc.h"
#include "plug_in.h"

/*  global gimprc variables  */
char *    plug_in_path = NULL;
link_ptr  plug_ins = NULL;
char *    temp_path = NULL;
int       temp_path_set = 0;
char *    brush_path = NULL;
char *    default_brush = NULL;
char *    pattern_path = NULL;
char *    default_pattern = NULL;
char *    palette_path = NULL;
char *    default_palette = NULL;
int       marching_speed = 150;   /* 150 ms */
int       gamma_set = 0;
double    gamma_val = 1.0;
int       transparency_type = 1;  /* Mid-Tone Checks */
int       transparency_size = 1;  /* Medium sized */
int       levels_of_undo = 1;     /* 1 level of undo default */
int       color_cube_shades[3] = {6, 7, 4};
int       install_cmap = 0;
int       toolbox_x = 5, toolbox_y = 5;
int       progress_x = 170, progress_y = 5;
int       info_x = 170, info_y = 5;
int       color_select_x = 160, color_select_y = 190;
int       tool_options_x = 5, tool_options_y = 545;
int       cycled_marching_ants = 0;
int       default_threshold = 15;
int       stingy_memory_use = 0;

/*  static function prototypes  */
static void  issue_warnings  (void);
static char* get_token       (char *, int);
static void  parse_accelerator (char *, char *, int *);
static void  parse_position (char *, int *, int *);

void
parse_gimprc ()
{
  FILE *fp;
  char str[512];
  char *token;
  int i;
  struct stat stat_buf;
  char *path;
  char *search_path;
  char *gimp_dir;

  /* Find the gimprc file.
   */
  gimp_dir = getenv ("GIMP_DIRECTORY");
  if (gimp_dir)
    {
      search_path = (char*) xmalloc (strlen (gimp_dir) + 4);
      sprintf (path, "~/:%s", gimp_dir);
    }
  else
    search_path = xstrdup ("~/");

  path = search_in_path (search_path, ".gimprc");
  if (path)
    {
      fp = fopen (path, "rt");
      if (!fp)
	fatal_error ("Unable to open \".gimprc\"");
    }
  else
    fatal_error ("\".gimprc\" file not found");

  xfree (search_path);

  /* Parse the file.
     This is a very basic parsing mechanism.
     It parses one line at a time. */

  plug_ins = NULL;
  while (!feof (fp))
    {
      if (!fgets (str, 512, fp))
	continue;

      if (str[0] != '#')
        {
	  token = strtok (str, " \t\n");
	  if (!token)
	    continue;

          if (strcmp (token, "temp-path") == 0)
	    {
	      temp_path = get_token (strtok (NULL, "\n"), '\"');
	      if (temp_path)
		{
		  temp_path = expand_path (temp_path);

		  if (!(stat (temp_path, &stat_buf)))
		    {
		      /*  do some consistency checks...  */
		      if (! S_ISDIR (stat_buf.st_mode))
			warning ("\"%s\" is not a directory.", temp_path);
		      else
			{
			  temp_path_set = 1;
			}
		    }
		  else
		    warning ("GIMP temp path \"%s\" could not be found.",
			     temp_path);
		}
	      else
		warning ("no temp directory specified.");
	    }
          else if (strcmp (token, "brush-path") == 0)
	    {
	      brush_path = get_token (strtok (NULL, "\n"), '\"');
	      if (brush_path)
		brush_path = xstrdup (brush_path);
	    }
	  else if (strcmp (token, "default-brush") == 0)
	    {
	      default_brush = get_token (strtok (NULL, "\n"), '\"');
	      if (default_brush)
		default_brush = xstrdup (default_brush);
	    }
          else if (strcmp (token, "pattern-path") == 0)
	    {
	      pattern_path = get_token (strtok (NULL, "\n"), '\"');
	      if (pattern_path)
	        pattern_path = xstrdup (pattern_path);
	    }
	  else if (strcmp (token, "default-pattern") == 0)
	    {
	      default_pattern = get_token (strtok (NULL, "\n"), '\"');
	      if (default_pattern)
		default_pattern = xstrdup (default_pattern);
	    }
          else if (strcmp (token, "palette-path") == 0)
	    {
	      palette_path = get_token (strtok (NULL, "\n"), '\"');
	      if (palette_path)
	        palette_path = xstrdup (palette_path);
	    }
	  else if (strcmp (token, "default-palette") == 0)
	    {
	      default_palette = get_token (strtok (NULL, "\n"), '\"');
	      if (default_palette)
		default_palette = xstrdup (default_palette);
	    }
	  else if (strcmp (token, "gamma-correction") == 0)
	    {
	      token = strtok (NULL, "\n");
	      gamma_val = atof (token);
	      gamma_set = 1;
	    }
	  else if (strcmp (token, "color-cube") == 0)
	    {
	      for (i = 0; i < 3; i++)
		{
		  token = strtok (NULL, ",\n");
		  color_cube_shades[i] = atoi (token);
		}
	    }
	  else if (strcmp (token, "marching-ants-speed") == 0)
	    {
	      token = strtok (NULL, "\n");
	      marching_speed = atoi (token);
	    }
	  else if (strcmp (token, "undo-levels") == 0)
	    {
	      token = strtok (NULL, "\n");
	      levels_of_undo = atoi (token);
	    }
	  else if (strcmp (token, "transparency-type") == 0)
	    {
	      token = strtok (NULL, "\n");
	      transparency_type = atoi (token);
	    }
	  else if (strcmp (token, "transparency-size") == 0)
	    {
	      token = strtok (NULL, "\n");
	      transparency_size = atoi (token);
	    }
	  else if (strcmp (token, "install-colormap") == 0)
	    {
	      install_cmap = 1;
	    }
	  else if (strcmp (token, "toolbox-position") == 0)
	    {
	      token = strtok (NULL, "\n");
	      parse_position (token, &toolbox_x, &toolbox_y);
	    }
	  else if (strcmp (token, "progress-position") == 0)
	    {
	      token = strtok (NULL, "\n");
	      parse_position (token, &progress_x, &progress_y);
	    }
	  else if (strcmp (token, "info-position") == 0)
	    {
	      token = strtok (NULL, "\n");
	      parse_position (token, &info_x, &info_y);
	    }
	  else if (strcmp (token, "color-select-position") == 0)
	    {
	      token = strtok (NULL, "\n");
	      parse_position (token, &color_select_x, &color_select_y);
	    }
	  else if (strcmp (token, "tool-options-position") == 0)
	    {
	      token = strtok (NULL, "\n");
	      parse_position (token, &tool_options_x, &tool_options_y);
	    }
	  else if (strcmp (token, "colormap-cycling") == 0)
	    {
	      cycled_marching_ants = 1;
	    }
	  else if (strcmp (token, "default-threshold") == 0)
	    {
	      token = strtok (NULL, "\n");
	      default_threshold = atoi (token);
	    }
	  else if (strcmp (token, "stingy-memory-use") == 0)
	    {
	      stingy_memory_use = 1;
	    }
          else if (strcmp (token, "plug-in-path") == 0)
	    {
	      plug_in_path = get_token (strtok (NULL, "\n"), '\"');
	      if (plug_in_path)
		plug_in_path = xstrdup (plug_in_path);
	    }
	  else if (strcmp (token, "file-plug-in") == 0)
	    {
	      char *prog, *types, *ext, *title;

	      prog = strtok (NULL, " \t\n");
	      types = strtok (NULL, " \t");
	      title = get_token (strtok (NULL, "\n"), '\"');
	      ext = get_token (NULL, '\"');

	      add_file_filter (ext, prog, types, title);
	    }
	  else if (strcmp (token, "plug-in") == 0)
	    {
	      char *prog, *title;
	      char *accel_text;
	      char accelerator_key = 0;
	      int accelerator_mods = 0;

	      prog = strtok (NULL, " \t\n");
	      title = get_token (strtok (NULL, "\n"), '\"');
	      accel_text = get_token (NULL, '\"');


	      if (prog && title)
		{
		  if (accel_text)
		    parse_accelerator (accel_text, &accelerator_key, &accelerator_mods);

		  plug_ins = plug_in_add_item (plug_ins, prog, title,
					       accelerator_key, accelerator_mods);
		}
	    }
	  else
	    {
	      warning ("unknown token: %s", token);
	    }
        }
    }

  fclose (fp);

  issue_warnings ();
}


static void
issue_warnings ()
{
  if (!gamma_set)
    {
      warning ("Gamma correction values unset.");
      warning ("Run the utility \"gamma_correct\" to determine");
      warning ("appropriate values for this display.");
      warning ("By default, the GIMP will run with no gamma correction.");
    }

  if (!temp_path_set)
    {
      warning ("Using /tmp for temporary GIMP files.");
      temp_path = xstrdup ("/tmp");
    }
}


static char*
get_token (str, delim)
     char *str;
     int delim;
{
  static char *the_str = NULL;
  char *val;

  if (str)
    the_str = str;

  while (*the_str && (*the_str++ != delim))
    ;

  if (*the_str)
    val = the_str;
  else
    return NULL;

  while (*the_str && (*the_str++ != delim))
    ;

  if (*(the_str - 1) == delim)
    *(the_str - 1) = 0;

  return val;
}


static void
parse_accelerator (text, key, mods)
     char * text;
     char * key;
     int * mods;
{
  char * mods_text;
  char * mod_text;
  char * key_text;

  mods_text = strtok (text, "<\n");
  strtok (NULL, ">\n");
  key_text = strtok (NULL, "\n");

  if (key_text)
    *key = key_text[0];

  *mods = 0;
  mod_text = strtok (mods_text, " ");
  while (mod_text)
    {
      if (!strcasecmp (mod_text, "shift"))
	*mods |= GDK_SHIFT_MASK;
      else if (!strcasecmp (mod_text, "control"))
	*mods |= GDK_CONTROL_MASK;
      else if (!strcasecmp (mod_text, "alt"))
	*mods |= GDK_MOD1_MASK;

      mod_text = strtok (NULL, " ");
    }
}


static void
parse_position (text, x, y)
     char * text;
     int * x, * y;
{
  char * x_text;
  char * y_text;

  x_text = strtok (text, "+-");
  y_text = strtok (NULL, "");
  if (x_text)
    *x = atoi (x_text);
  if (y_text)
    *y = atoi (y_text);
}
