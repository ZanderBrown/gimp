/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "autodialog.h"
#include "brushes.h"
#include "errors.h"
#include "gdisplay.h"
#include "paint_funcs.h"
#include "paint_core.h"
#include "palette.h"
#include "paintbrush.h"
#include "selection.h"
#include "tools.h"

/*  forward function declarations  */
static void    pencil_motion      (Tool *);

static void *  pencil_options = NULL;  

void *
pencil_paint_func (tool, state)
     Tool * tool;
     int state;
{
  PaintCore * paint_core;

  paint_core = (PaintCore *) tool->private;

  switch (state)
    {
    case INIT_PAINT :
      break;

    case MOTION_PAINT :
      pencil_motion (tool);
      break;

    case FINISH_PAINT :
      break;

    default : 
      break;
    }

  return NULL;
}


Tool *
tools_new_pencil ()
{
  Tool * tool;
  PaintCore * private;

  if (!pencil_options)
    pencil_options = tools_register_no_options (PENCIL, "Pencil Options");

  tool = paint_core_new (PENCIL);

  private = (PaintCore *) tool->private;
  private->paint_func = pencil_paint_func;

  return tool;
}


void
tools_free_pencil (tool)
     Tool * tool;
{
  paint_core_free (tool);
}


void
pencil_motion (tool)
     Tool * tool;
{
  GDisplay * gdisp;
  PaintCore * paint_core;
  TempBuf * area;
  unsigned char col[MAX_CHANNELS];

  gdisp = (GDisplay *) tool->gdisp_ptr;
  paint_core = (PaintCore *) tool->private;

  gimage_get_foreground (gdisp->gimage, col);

  /*  Get a region which can be used to paint to  */
  if (! (area = paint_core_get_paint_area (tool, 0)))
    return;

  /*  set the alpha channel  */
  col[area->bytes - 1] = OPAQUE;

  /*  color the pixels  */
  color_pixels (temp_buf_data (area), col,
		area->width * area->height, area->bytes);

  /*  paste the newly painted canvas to the gimage which is being worked on  */
  paint_core_paste_canvas (tool, OPAQUE, (int) (get_brush_opacity () * 255),
			   get_brush_paint_mode (), HARD, CONSTANT);
}
