/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "gconvert.h"
#include "colormaps.h"
#include "visual.h"


void 
grayscale_8 (src, dest, width)
     unsigned char * src;
     unsigned char * dest;
     long width;
{
  while (width--)
    *dest++ = grays [*src++];
}


void 
grayscale_16 (src, dest, width)
     unsigned char * src;
     unsigned short * dest;
     long width;
{
  while (width--)
    *dest++ = COLOR_COMPOSE (*src, *src, *src++);
}


void 
grayscale_24 (src, dest, width)
     unsigned char * src;
     unsigned long * dest;
     long width;
{
  while (width--)
    *dest++ = COLOR_COMPOSE (*src, *src, *src++);
}

