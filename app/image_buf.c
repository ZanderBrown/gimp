/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "errors.h"
#include "gconvert.h"
#include "gdither.h"
#include "image_buf.h"
#include "visual.h"

typedef struct _ImageBuf {
  int color;
  GdkImage *gdk_image;
} _ImageBuf;

static void image_buf_draw_row_8 (ImageBuf, unsigned char *, int, int, int);
static void image_buf_draw_row_16 (ImageBuf, unsigned char *, int, int, int);
static void image_buf_draw_row_24 (ImageBuf, unsigned char *, int, int, int);


ImageBuf
image_buf_create (color, width, height)
     int color, width, height;
{
  _ImageBuf *image;
  GdkVisual * visual;

  if (color == COLOR_BUF)
    visual = color_visual;
  else if (color == GRAY_BUF)
    visual = gray_visual;

  image = xmalloc (sizeof (_ImageBuf));

  image->color = color;
  image->gdk_image = gdk_image_new (GDK_IMAGE_FASTEST, visual, width, height);

  return (ImageBuf) image;
}

void
image_buf_destroy (image)
     ImageBuf image;
{
  _ImageBuf *_image;

  _image = (_ImageBuf *) image;

  gdk_image_destroy (_image->gdk_image);
  xfree (_image);
}

void
image_buf_put (image, window, gc)
     ImageBuf image;
     GdkWindow *window;
     GdkGC *gc;
{
  image_buf_put_area (image, window, gc, 0, 0,
		      image_buf_width (image),
		      image_buf_height (image));
}

void
image_buf_put_area (image, window, gc, x, y, w, h)
     ImageBuf image;
     GdkWindow *window;
     GdkGC *gc;
     int x, y, w, h;
{
  _ImageBuf *_image;

  if (image && window && gc)
    {
      _image = (_ImageBuf *) image;

      gdk_draw_image (window, gc, _image->gdk_image, x, y, x, y, w, h);
    }
}

void
image_buf_draw_row (image, row, x, y, w)
     ImageBuf image;
     unsigned char *row;
     int x, y;
     int w;
{
  if (image)
    {
       switch (image_buf_depth (image))
	 {
	 case 8:
	   image_buf_draw_row_8 (image, row, x, y, w);
	   break;
	 case 12:
	 case 15:
	 case 16:
	   image_buf_draw_row_16 (image, row, x, y, w);
	   break;
	 case 24:
	 case 32:
	   image_buf_draw_row_24 (image, row, x, y, w);
	   break;
	 default:
	   fatal_error ("unable to handle %d bit in color selections", image_buf_depth (image));
	   break;
	 }
    }
}

void *
image_buf_data (image)
     ImageBuf image;
{
  return ((_ImageBuf*) image)->gdk_image->mem;
}

int
image_buf_width (image)
     ImageBuf image;
{
  return ((_ImageBuf*) image)->gdk_image->width;
}

int
image_buf_height (image)
     ImageBuf image;
{
  return ((_ImageBuf*) image)->gdk_image->height;
}

int
image_buf_depth (image)
     ImageBuf image;
{
  return ((_ImageBuf*) image)->gdk_image->depth;
}

int
image_buf_color (image)
     ImageBuf image;
{
  return ((_ImageBuf*) image)->color;
}

int
image_buf_bytes (image)
     ImageBuf image;
{
  return (((_ImageBuf*) image)->gdk_image->bpp >> 3);
}

int
image_buf_row_bytes (image)
     ImageBuf image;
{
  return ((_ImageBuf*) image)->gdk_image->bpl;
}

static void
image_buf_draw_row_8 (image, row, x, y, w)
     ImageBuf image;
     unsigned char *row;
     int x, y;
     int w;
{
  unsigned char *src;
  unsigned char *dest;

  src = row;
  dest = image_buf_data (image);
  dest += image_buf_row_bytes (image) * y + x;

  switch (image_buf_color (image))
    {
    case COLOR_BUF :
      dither_segment (src, dest, 0, y, w);
      break;
    case GRAY_BUF :
      grayscale_8 (src, dest, w);
      break;
    }
}

static void
image_buf_draw_row_16 (image, row, x, y, w)
     ImageBuf image;
     unsigned char *row;
     int x, y;
     int w;
{
  unsigned char *src;
  unsigned short *dest;
  int i, r, g, b;

  src = row;
  dest = image_buf_data (image);
  dest += ((image_buf_row_bytes (image) * y) >> 1) + x;

  switch (image_buf_color (image))
    {
    case COLOR_BUF :
      for (i = 0; i < w; i++)
	{
	  r = *src++;
	  g = *src++;
	  b = *src++;

	  *dest++ = COLOR_COMPOSE (r, g, b);
	}
      break;
    case GRAY_BUF :
      grayscale_16 (src, dest, w);
      break;
    }
}

static void
image_buf_draw_row_24 (image, row, x, y, w)
     ImageBuf image;
     unsigned char *row;
     int x, y;
     int w;
{
  unsigned char *src;
  unsigned long *dest;
  int i, r, g, b;

  src = row;
  dest = image_buf_data (image);
  dest += ((image_buf_row_bytes (image) * y) >> 2) + x;

  switch (image_buf_color (image))
    {
    case COLOR_BUF :
      for (i = 0; i < w; i++)
	{
	  r = *src++;
	  g = *src++;
	  b = *src++;

	  *dest++ = COLOR_COMPOSE (r, g, b);
	}
      break;
    case GRAY_BUF :
      grayscale_24 (src, dest, w);
      break;
    }
}
