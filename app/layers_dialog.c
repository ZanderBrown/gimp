/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include "appenv.h"
#include "actionarea.h"
#include "brush_select.h"
#include "buildmenu.h"
#include "colormaps.h"
#include "errors.h"
#include "gdisplay.h"
#include "gimage.h"
#include "general.h"
#include "image_buf.h"
#include "interface.h"
#include "layers_dialog.h"
#include "paint_funcs.h"
#include "palette.h"
#include "visual.h"


#define PREVIEW_EVENT_MASK  GDK_EXPOSURE_MASK | GDK_ENTER_NOTIFY_MASK

#define LAYER_LIST_WIDTH 200
#define LAYER_LIST_HEIGHT 200

typedef enum
{
  NoPreview,
  SmallPreview,
  MediumPreview,
  LargePreview
} PreviewSize;

typedef struct _LayersDialog LayersDialog;

struct _LayersDialog {
  GtkWidget *shell;
  GtkWidget *image_menu;
  GtkWidget *image_option_menu;
  GtkWidget *layer_list;
  GtkDataAdjustment *opacity_data;
  GtkObserver opacity_observer;
  GtkDataAdjustment *sbar_data;
  GtkObserver sbar_observer;
  int scroll_offset;

  ImageBuf layer_image;
  ImageBuf mask_image;
  int image_width, image_height;

  int gimage_id;
};

typedef struct _LayerWidget LayerWidget;

struct _LayerWidget {
  GtkWidget *layer_preview;
  GtkWidget *mask_preview;
  GtkWidget *list_item;
  GtkObserver visibility_observer;
  GtkObserver linkage_observer;
  GtkObserver list_item_observer;

  GImage *gimage;
  Layer *layer;
  GdkGC *gc;
  GdkPixmap *layer_pixmap;
  GdkPixmap *mask_pixmap;
  int width, height;
  int dirty;
};

/*  layer widget function prototypes  */
static gint layer_widget_visibility_update (GtkObserver *, GtkData *);
static gint layer_widget_linkage_update (GtkObserver *, GtkData *);
static gint layer_widget_select_update (GtkObserver *, GtkData *);
static gint layer_widget_layer_events (GtkWidget *, GdkEvent *);
static void layer_widget_layer_redraw (LayerWidget *);
static void layer_widget_layer_flush (GtkWidget *, gpointer, gpointer);

/*  local function prototypes  */
static GtkWidget *create_image_menu (void);
static GtkWidget *create_layer_widget (GImage *, Layer *);
static void layers_dialog_close_callback (GtkWidget *, gpointer, gpointer);
static void paint_mode_menu_callback (GtkWidget *, gpointer, gpointer);
static void image_menu_callback (GtkWidget *, gpointer, gpointer);
static gint opacity_scale_update (GtkObserver *, GtkData *);
static void layers_dialog_disconnect (GtkObserver *, GtkData *);
static void layers_dialog_update (void);
static Layer *get_active_layer (GImage *);
static void layers_dialog_new_layer_callback (GtkWidget *, gpointer, gpointer);
static void layers_dialog_new_layer_query (int);

/*  the image preview sizes  */
static int image_preview_sizes [] =
{
  0,
  32,
  64,
  128,
};

/*  Only one layers dialog  */
static LayersDialog *layersD;
static PreviewSize preview_size = MediumPreview;

/*  the action area structure  */
static ActionAreaItem action_items[] =
{
  { "Close", layers_dialog_close_callback, NULL, NULL },
};

static MenuItem layers_ops[] =
{
  { "New Layer", 0, 0, layers_dialog_new_layer_callback, NULL, NULL, NULL },
  { "Delete Layer", 0, 0, NULL, NULL, NULL, NULL },
  { "Close", 0, 0, layers_dialog_close_callback, NULL, NULL, NULL },
  { NULL, 0, 0, NULL, NULL, NULL, NULL },
};


/************************************/
/*  Public layers dialog functions  */
/************************************/

void
layers_dialog_create ()
{
  GtkWidget *vbox;
  GtkWidget *util_box;
  GtkWidget *label;
  GtkWidget *menu;
  GtkWidget *option_menu;
  GtkWidget *slider;
  GtkWidget *action_area;
  GtkWidget *arrow_hbox;
  GtkWidget *arrow;
  GtkWidget *ops_menu;
  GtkWidget *menu_bar;
  GtkWidget *menu_bar_item;
  GtkWidget *listbox;

  if (!layersD)
    {
      layersD = xmalloc (sizeof (LayersDialog));
      layersD->layer_image = NULL;
      layersD->mask_image = NULL;
      layersD->gimage_id = -1;

      /*  The shell and main vbox  */
      layersD->shell = gtk_window_new ("Layers", GTK_WINDOW_TOPLEVEL);
      vbox = gtk_vbox_new (FALSE, 5);
      gtk_container_add (layersD->shell, vbox);

      /*  The hbox to hold the command menu and image option menu box  */
      util_box = gtk_hbox_new (FALSE, 2);
      gtk_container_set_border_width (util_box, 0);
      gtk_box_pack (vbox, util_box, FALSE, FALSE, 0, GTK_PACK_START);

      /*  The GIMP image option menu  */
      label = gtk_label_new ("Image:");
      gtk_box_pack (util_box, label, FALSE, FALSE, 2, GTK_PACK_START);
      layersD->image_option_menu = gtk_option_menu_new ();
      layersD->image_menu = create_image_menu ();
      gtk_box_pack (util_box, layersD->image_option_menu, TRUE, TRUE, 2, GTK_PACK_START);

      gtk_widget_show (layersD->image_option_menu);
      gtk_option_menu_set_menu (layersD->image_option_menu, layersD->image_menu);
      gtk_widget_show (label);

      /*  The layers commands pulldown menu  */
      layers_ops[0].user_data = layersD;
      layers_ops[1].user_data = layersD;
      layers_ops[2].user_data = layersD;
      ops_menu = build_menu (layers_ops, NULL);

      menu_bar = gtk_menu_bar_new ();
      gtk_box_pack (util_box, menu_bar, FALSE, FALSE, 2, GTK_PACK_END);
      menu_bar_item = gtk_menu_item_new ();
      gtk_container_add (menu_bar, menu_bar_item);
      gtk_menu_item_set_submenu (menu_bar_item, ops_menu);
      arrow_hbox = gtk_hbox_new (FALSE, 0);
      gtk_container_set_border_width (arrow_hbox, 0);
      gtk_container_add (menu_bar_item, arrow_hbox);
      label = gtk_label_new ("Ops");
      arrow = gtk_arrow_new (GTK_ARROW_DOWN, GTK_SHADOW_OUT);
      gtk_box_pack (arrow_hbox, arrow, FALSE, FALSE, 0, GTK_PACK_START);
      gtk_box_pack (arrow_hbox, label, FALSE, FALSE, 4, GTK_PACK_START);
      gtk_label_set_alignment (label, 0.5, 0.5);
      gtk_arrow_set_alignment (arrow, 0.5, 0.5);

      gtk_widget_show (arrow);
      gtk_widget_show (label);
      gtk_widget_show (arrow_hbox);
      gtk_widget_show (menu_bar_item);
      gtk_widget_show (menu_bar);
      gtk_widget_show (util_box);


      /*  The Mode option menu, and the Opacity scale  */
      util_box = gtk_hbox_new (FALSE, 2);
      gtk_container_set_border_width (util_box, 0);
      gtk_box_pack (vbox, util_box, FALSE, FALSE, 0, GTK_PACK_START);

      label = gtk_label_new ("Mode:");
      gtk_box_pack (util_box, label, FALSE, FALSE, 2, GTK_PACK_START);

      menu = create_paint_mode_menu (paint_mode_menu_callback);
      option_menu = gtk_option_menu_new ();
      gtk_box_pack (util_box, option_menu, FALSE, FALSE, 2, GTK_PACK_START);

      gtk_widget_show (label);
      gtk_widget_show (option_menu);
      gtk_option_menu_set_menu (option_menu, menu);

      label = gtk_label_new ("Opacity:");
      gtk_box_pack (util_box, label, FALSE, FALSE, 2, GTK_PACK_START);
      layersD->opacity_data = (GtkDataAdjustment *)
	gtk_data_adjustment_new (100.0, 0.0, 100.0, 1.0, 1.0, 1.0);
      slider = gtk_hscale_new (layersD->opacity_data);
      gtk_box_pack (util_box, slider, TRUE, TRUE, 0, GTK_PACK_START);
      gtk_scale_set_value_pos (slider, GTK_POS_TOP);
      layersD->opacity_observer.update = opacity_scale_update;
      layersD->opacity_observer.disconnect = layers_dialog_disconnect;
      layersD->opacity_observer.user_data = layersD;
      gtk_data_attach ((GtkData *) layersD->opacity_data, &layersD->opacity_observer);

      gtk_widget_show (label);
      gtk_widget_show (slider);
      gtk_widget_show (util_box);


      /*  The layers listbox  */
      listbox = gtk_listbox_new ();
      gtk_listbox_set_shadow_type (listbox, GTK_SHADOW_IN);
      gtk_box_pack (vbox, listbox, TRUE, TRUE, 2, GTK_PACK_START);
      layersD->layer_list = gtk_listbox_get_list (listbox);
      gtk_widget_set_usize (layersD->layer_list, LAYER_LIST_WIDTH, LAYER_LIST_HEIGHT);
      gtk_list_set_selection_mode (layersD->layer_list, GTK_SELECTION_BROWSE);
      gtk_widget_show (listbox);


      /*  The action area  */
      action_items[0].user_data = layersD;
      action_area = build_action_area (action_items, 1);
      gtk_box_pack (vbox, action_area, FALSE, FALSE, 2, GTK_PACK_START);

      gtk_widget_show (action_area);
      gtk_widget_show (vbox);
      gtk_widget_show (layersD->shell);
      install_colormap (RGB);
    }
  else
    {
      if (!GTK_WIDGET_VISIBLE (layersD->shell))
	gtk_widget_show (layersD->shell);
      install_colormap (RGB);
    }
}


void
layers_dialog_update_image_list ()
{
  if (! layersD)
    return;

  gtk_widget_destroy (layersD->image_menu);

  layersD->image_menu = create_image_menu ();
  gtk_option_menu_set_menu (layersD->image_option_menu, layersD->image_menu);
}


void
layers_dialog_dirty_layer (layer_widget_ptr)
     void *layer_widget_ptr;
{
  LayerWidget *layer_widget;
  GImage *gimage;

  if (!layer_widget_ptr)
    return;

  layer_widget = (LayerWidget *) layer_widget_ptr;

  /*  All of the cases where this notification can be ignored  */
  if (!layersD)
    return;
  if (! (gimage = gimage_get_ID (layersD->gimage_id)))
    return;
  if (gimage != layer_widget->gimage)
    return;

  /*  Set the dirty bit in the requested layer widget--an update will be done  */
  layer_widget->dirty = 1;
}


void
layers_dialog_remove_layer (layer_widget_ptr)
     void *layer_widget_ptr;
{
  LayerWidget *layer_widget;
  GImage *gimage;
  GList *list = NULL;

  if (!layer_widget_ptr)
    return;

  layer_widget = (LayerWidget *) layer_widget_ptr;

  /*  All of the cases where this notification can be ignored  */
  if (!layersD)
    return;
  if (! (gimage = gimage_get_ID (layersD->gimage_id)))
    return;
  if (gimage != layer_widget->gimage)
    return;

  /*  Remove the requested layer from the dialog  */
  list = g_list_append (list, layer_widget->list_item);
  gtk_list_remove_items (layersD->layer_list, list);

  gtk_widget_destroy (layer_widget->list_item);
  if (layer_widget->gc)
    gdk_gc_destroy (layer_widget->gc);
  if (layer_widget->layer_pixmap)
    gdk_pixmap_destroy (layer_widget->layer_pixmap);
  if (layer_widget->mask_pixmap)
    gdk_pixmap_destroy (layer_widget->mask_pixmap);

  xfree (layer_widget);
}


void
layers_dialog_add_layer (gimage_id, layer_id)
     int gimage_id;
     int layer_id;
{
  GImage *gimage;
  Layer *layer;
  GList *item_list;
  GtkWidget *list_item;

  gimage = gimage_get_ID (gimage_id);
  layer = layer_get_ID (layer_id);

  if (!layersD || (gimage_id != layersD->gimage_id))
    return;
  if (!gimage || !layer)
    return;

  item_list = NULL;

  list_item = create_layer_widget (gimage, layer);
  item_list = g_list_append (item_list, list_item);

  if (layersD->layer_image)
    image_buf_destroy (layersD->layer_image);

  layersD->layer_image = image_buf_create (COLOR_BUF,
					   layersD->image_width,
					   layersD->image_height);

  /*  if there is a floating selection, and this layer isn't it,
   *  we need to insert this layer_widget at index 1 in the list
   */
  if (gimage->floating_sel != -1 && layer->ID != gimage->floating_sel)
    gtk_list_insert_items (layersD->layer_list, item_list, 1);
  /*  otherwise, just prepend this list item  */
  else
    gtk_list_prepend_items (layersD->layer_list, item_list);
}


void
layers_dialog_flush ()
{
  if (!layersD)
    return;

  gtk_container_foreach ((GtkContainer*) layersD->layer_list,
			 layer_widget_layer_flush, NULL);
}


/****************************/
/*  layer widget functions  */
/****************************/

static int
layer_widget_visibility_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataInt *int_data;
  LayerWidget *layer_widget;

  layer_widget = (LayerWidget *) observer->user_data;
  int_data = (GtkDataInt *) data;

  if (int_data->value == GTK_STATE_ACTIVATED ||
      int_data->value == GTK_STATE_DEACTIVATED)
    {
      layer_toggle_visibility (layer_widget->layer);
      gdisplays_update_area (layer_widget->gimage->ID,
			     layer_widget->layer->offset_x, layer_widget->layer->offset_y,
			     layer_widget->layer->width, layer_widget->layer->height);
      gdisplays_flush ();
    }

  return FALSE;
}


static int
layer_widget_linkage_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataInt *int_data;
  LayerWidget *layer_widget;

  layer_widget = (LayerWidget *) observer->user_data;
  int_data = (GtkDataInt *) data;

  if (int_data->value == GTK_STATE_ACTIVATED ||
      int_data->value == GTK_STATE_DEACTIVATED)
    layer_toggle_linkage (layer_widget->layer);

  return FALSE;
}


static int
layer_widget_select_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  LayerWidget *layer_widget;
  GtkDataInt *int_data;

  int_data = (GtkDataInt*) data;
  layer_widget = (LayerWidget *) observer->user_data;

  /*  Is the list item being selected?  */
  if (int_data->value != GTK_STATE_SELECTED)
    return FALSE;

  /*  set the gimage's active layer to be this layer  */
  layer_widget->gimage->active_layer = layer_widget->layer->ID;

  return FALSE;
}


static gint
layer_widget_layer_events (widget, event)
     GtkWidget *widget;
     GdkEvent *event;
{
  GdkEventExpose *eevent;
  LayerWidget *layer_widget;

  layer_widget = (LayerWidget *) gtk_widget_get_user_data (widget);

  switch (event->type)
    {
    case GDK_EXPOSE:
      /*  If this is the first exposure  */
      if (!layer_widget->gc)
	{
	  layer_widget->gc = gdk_gc_new (layer_widget->layer_preview->window);
	  layer_widget->layer_pixmap = gdk_pixmap_new (layer_widget->layer_preview->window,
						       layer_widget->width,
						       layer_widget->height,
						       -1);
	}

      if (layer_widget->dirty)
	layer_widget_layer_redraw (layer_widget);
      else
	{
	  eevent = (GdkEventExpose *) event;
	  gdk_draw_pixmap (layer_widget->layer_preview->window,
			   layer_widget->gc,
			   layer_widget->layer_pixmap,
			   eevent->area.x, eevent->area.y,
			   eevent->area.x, eevent->area.y,
			   eevent->area.width, eevent->area.height);
	}
      break;

    case GDK_ENTER_NOTIFY:
      install_colormap (RGB);
      break;

    default:
      break;
    }

  return FALSE;
}


static void
layer_widget_layer_redraw (layer_widget)
     LayerWidget *layer_widget;
{
  TempBuf * preview_buf;
  PixelRegion srcPR, destPR;
  unsigned char * src, *s;
  unsigned char * buf, *b;
  int has_alpha;
  int is_gray;
  int alpha;
  int val;
  int i, j, k;

  /* do not attempt to redraw before the widget has been fully realized and mapped */
  if (!layer_widget->gc)
    return;

  layer_widget->dirty = 0;
  has_alpha = (layer_widget->layer->type == RGBA_GIMAGE ||
	       layer_widget->layer->type == GRAYA_GIMAGE ||
	       layer_widget->layer->type == INDEXEDA_GIMAGE);
  is_gray = (layer_widget->layer->type == GRAYA_GIMAGE ||
	     layer_widget->layer->type == GRAY_GIMAGE);
  alpha = has_alpha ? layer_widget->layer->bytes - 1 : layer_widget->layer->bytes;

  if (has_alpha || is_gray)
    buf = (unsigned char *) xmalloc (sizeof (char) * layer_widget->width * 3);


  srcPR.bytes = layer_widget->layer->bytes;
  srcPR.w = layer_widget->layer->width;
  srcPR.h = layer_widget->layer->height;
  srcPR.x = srcPR.y = 0;
  srcPR.rowstride = srcPR.bytes * layer_widget->layer->width;
  srcPR.data = layer_data (layer_widget->layer);

  preview_buf = temp_buf_new (layer_widget->width, layer_widget->height,
			      srcPR.bytes, 0, 0, NULL);
  destPR.x = 0;
  destPR.y = 0;
  destPR.w = layer_widget->width;
  destPR.h = layer_widget->height;
  destPR.rowstride = srcPR.bytes * layer_widget->width;
  destPR.data = temp_buf_data (preview_buf);

  scale_region (&srcPR, &destPR);

  src = destPR.data;

  for (i = 0; i < layer_widget->height; i++)
    {
      s = src;
      b = buf;

      if (is_gray)
	{
	  for (j = 0; j < layer_widget->width; j++)
	    {
	      val = has_alpha ? (s[0] * s[1]) / 255 : s[0];
	      *b++ = val;
	      *b++ = val;
	      *b++ = val;
	      s += srcPR.bytes;
	    }

	  image_buf_draw_row (layersD->layer_image, buf, 0, i, layer_widget->width);
	}
      else
	{
	  if (has_alpha)
	    {
	      for (j = 0; j < layer_widget->width; j++)
		{
		  for (k = 0; k < alpha; k++)
		    b[k] = (s[k] * s[alpha]) / 255;
		  s += srcPR.bytes;
		  b += alpha;
		}

	      image_buf_draw_row (layersD->layer_image, buf, 0, i, layer_widget->width);
	    }
	  else
	    image_buf_draw_row (layersD->layer_image, src, 0, i, layer_widget->width);
	}

      src += destPR.rowstride;
    }

  image_buf_put_area (layersD->layer_image,
		      layer_widget->layer_pixmap,
		      layer_widget->gc,
		      0, 0, layer_widget->width, layer_widget->height);

  /*  make sure the image has been transfered completely to the pixmap before
   *  we use it again...
   */
  gdk_flush ();

  gdk_draw_pixmap (layer_widget->layer_preview->window,
		   layer_widget->gc,
		   layer_widget->layer_pixmap,
		   0, 0, 0, 0,
		   layer_widget->width, layer_widget->height);

  if (has_alpha)
    xfree (buf);
  temp_buf_free (preview_buf);
}


static void
layer_widget_layer_flush (widget, client_data, call_data)
     GtkWidget *widget;
     gpointer client_data;
     gpointer call_data;
{
  LayerWidget *layer_widget;

  layer_widget = (LayerWidget *) gtk_widget_get_user_data (widget);
  if (layer_widget->dirty)
    layer_widget_layer_redraw (layer_widget);
}


/***********************/
/*  widget routines    */
/***********************/

static GtkWidget *
create_image_menu ()
{
  extern link_ptr image_list;

  GImage *gimage;
  GtkWidget *menu_item;
  GtkWidget *menu;
  char *menu_item_label;
  char *image_name;
  link_ptr tmp;
  int num_items = 0;

  menu = gtk_menu_new ();

  tmp = image_list;
  while (tmp)
    {
      gimage = tmp->data;
      tmp = next_item (tmp);

      image_name = prune_filename (gimage->filename);
      menu_item_label = (char *) xmalloc (strlen (image_name) + 15);
      sprintf (menu_item_label, "%s-%d", image_name, gimage->ID);
      menu_item = gtk_menu_item_new_with_label (menu_item_label);
      gtk_callback_add (gtk_menu_item_get_state (menu_item),
			image_menu_callback, (gpointer) gimage->ID);
      gtk_container_add (menu, menu_item);
      gtk_widget_show (menu_item);

      xfree (menu_item_label);
      num_items ++;
    }

  if (!num_items)
    {
      menu_item = gtk_menu_item_new_with_label ("none");
      gtk_container_add (menu, menu_item);
      gtk_widget_show (menu_item);
    }

  return menu;
}

static GtkWidget *
create_layer_widget (gimage, layer)
     GImage *gimage;
     Layer *layer;
{
  LayerWidget *layer_widget;
  GtkWidget *list_item;
  GtkWidget *hbox;
  GtkWidget *preview_box;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *item_frame;
  GtkWidget *frame;
  GtkWidget *arrow;
  GtkWidget *check_button;
  GtkWidget *push_button;
  GtkData *item_state;

  list_item = gtk_list_item_new ();

  layer_widget = (LayerWidget *) xmalloc (sizeof (LayerWidget));
  layer_widget->gimage = gimage;
  layer_widget->layer = layer;
  layer_widget->layer_preview = NULL;
  layer_widget->mask_preview = NULL;
  layer_widget->gc = NULL;
  layer_widget->layer_pixmap = NULL;
  layer_widget->mask_pixmap = NULL;
  layer_widget->dirty = 1;
  layer_widget->list_item = list_item;

  /*  Need to let the list item know about the layer_widget  */
  gtk_widget_set_user_data (list_item, layer_widget);

  /*  set up the list item observer  */
  item_state = gtk_list_item_get_state (list_item);
  layer_widget->list_item_observer.update = layer_widget_select_update;
  layer_widget->list_item_observer.disconnect = layers_dialog_disconnect;
  layer_widget->list_item_observer.user_data = (gpointer) layer_widget;
  gtk_data_attach (item_state, &layer_widget->list_item_observer);

  item_frame = gtk_frame_new (NULL);
  gtk_frame_set_type (item_frame, GTK_SHADOW_ETCHED_IN);
  gtk_container_set_border_width (item_frame, 0);
  gtk_container_add (list_item, item_frame);

  hbox = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (hbox, 0);
  gtk_container_add (item_frame, hbox);

  /*  The layer reordering buttons  */
  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (vbox, 0);
  gtk_box_pack (hbox, vbox, FALSE, FALSE, 2, GTK_PACK_START);

  arrow = gtk_arrow_new (GTK_ARROW_UP, GTK_SHADOW_OUT);
  push_button = gtk_push_button_new ();
  gtk_container_add (push_button, arrow);
  gtk_box_pack (vbox, push_button, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_widget_show (arrow);
  gtk_widget_show (push_button);

  arrow = gtk_arrow_new (GTK_ARROW_DOWN, GTK_SHADOW_OUT);
  push_button = gtk_push_button_new ();
  gtk_container_add (push_button, arrow);
  gtk_box_pack (vbox, push_button, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_widget_show (arrow);
  gtk_widget_show (push_button);

  gtk_widget_show (vbox);


  /*  determine width and height  */
  if (gimage->width > gimage->height)
    {
      layer_widget->width = (image_preview_sizes[preview_size] * layer->width) /
	gimage->width;
      layer_widget->height = (layer_widget->width * layer->height) / layer->width;
    }
  else
    {
      layer_widget->height = (image_preview_sizes[preview_size] * layer->height) /
	gimage->height;
      layer_widget->width = (layer_widget->height * layer->width) / layer->height;
    }

  /*  Make sure to get the maximum image extents  */
  if (layersD->image_width < layer_widget->width)
    layersD->image_width = layer_widget->width;
  if (layersD->image_height < layer_widget->height)
    layersD->image_height = layer_widget->height;

  /*  The previews  */
  preview_box = gtk_hbox_new (TRUE, 5);
  gtk_container_set_border_width (preview_box, 0);
  gtk_box_pack (hbox, preview_box, FALSE, FALSE, 2, GTK_PACK_START);

  if (layer_widget->width && layer_widget->height)
    {
      frame = gtk_frame_new (NULL);
      gtk_frame_set_type (frame, GTK_SHADOW_IN);
      gtk_container_set_border_width (frame, 2);
      gtk_box_pack (preview_box, frame, FALSE, FALSE, 2, GTK_PACK_START);

      layer_widget->layer_preview = gtk_drawing_area_new (layer_widget->width,
							  layer_widget->height,
							  layer_widget_layer_events,
							  PREVIEW_EVENT_MASK);
      gtk_widget_set_user_data (layer_widget->layer_preview, layer_widget);

      gtk_container_add (frame, layer_widget->layer_preview);
      gtk_widget_show (layer_widget->layer_preview);
      gtk_widget_show (frame);
    }

  gtk_widget_show (preview_box);


  /*  The visible and linked check buttons  */
  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (vbox, 0);
  gtk_box_pack (hbox, vbox, FALSE, FALSE, 2, GTK_PACK_START);

  check_button = gtk_check_button_new ();
  gtk_box_pack (vbox, check_button, FALSE, FALSE, 2, GTK_PACK_END);
  label = gtk_label_new ("linked");
  gtk_container_add (check_button, label);
  gtk_widget_show (label);
  layer_widget->linkage_observer.update = layer_widget_linkage_update;
  layer_widget->linkage_observer.disconnect = layers_dialog_disconnect;
  layer_widget->linkage_observer.user_data = (gpointer) layer_widget;
  gtk_data_attach (gtk_button_get_state (check_button), &layer_widget->linkage_observer);
  gtk_widget_show (check_button);

  check_button = gtk_check_button_new ();
  gtk_box_pack (vbox, check_button, FALSE, FALSE, 2, GTK_PACK_END);
  label = gtk_label_new ("visible");
  gtk_container_add (check_button, label);
  gtk_widget_show (label);
  layer_widget->visibility_observer.update = layer_widget_visibility_update;
  layer_widget->visibility_observer.disconnect = layers_dialog_disconnect;
  layer_widget->visibility_observer.user_data = (gpointer) layer_widget;
  gtk_data_attach (gtk_button_get_state (check_button), &layer_widget->visibility_observer);
  gtk_widget_show (check_button);

  label = gtk_label_new (layer->name);
  gtk_box_pack (vbox, label, FALSE, FALSE, 2, GTK_PACK_END);
  gtk_widget_show (label);

  gtk_widget_show (vbox);
  gtk_widget_show (hbox);
  gtk_widget_show (item_frame);
  gtk_widget_show (list_item);

  /*  Let the layer know about its corresponding layer widget  */
  layer->layer_widget = (void *) layer_widget;

  return list_item;
}


static void
layers_dialog_new_layer_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  /*  if there is a currently selected gimage, request a new layer
   */
  if (!layersD)
    return;
  if (layersD->gimage_id == -1)
    return;

  layers_dialog_new_layer_query (layersD->gimage_id);
}


static void
layers_dialog_close_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  LayersDialog *ld;

  ld = (LayersDialog *) client_data;

  if (GTK_WIDGET_VISIBLE (ld->shell))
    gtk_widget_hide (ld->shell);
}


static void
paint_mode_menu_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  GImage *gimage;
  Layer *layer;

  if (! (gimage = gimage_get_ID (layersD->gimage_id)))
    return;
  if (! (layer = get_active_layer (gimage)))
    return;

  /*  If the layer is not a background layer, set the transparency and redraw  */
  if (layer->type == RGBA_GIMAGE ||
      layer->type == GRAYA_GIMAGE ||
      layer->type == INDEXEDA_GIMAGE)
    {
      layer->mode = (int) client_data;
      gdisplays_update_area (gimage->ID,
			     layer->offset_x, layer->offset_y,
			     layer->width, layer->height);
      gdisplays_flush ();
    }
}


static void
image_menu_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  if (!layersD)
    return;
  if (gimage_get_ID ((int) client_data) != NULL)
    {
      layersD->gimage_id = (int) client_data;
      layers_dialog_update ();
    }
}


static gint
opacity_scale_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataAdjustment *adj_data;
  GImage *gimage;
  Layer *layer;

  if (! (gimage = gimage_get_ID (layersD->gimage_id)))
    return FALSE;

  adj_data = (GtkDataAdjustment *) data;
  if (! (layer = get_active_layer (gimage)))
    return FALSE;

  /*  If the layer is not a background layer, set the transparency and redraw  */
  if (layer->type == RGBA_GIMAGE ||
      layer->type == GRAYA_GIMAGE ||
      layer->type == INDEXEDA_GIMAGE)
    {
      layer->opacity = (int) (adj_data->value * 2.55);
      gdisplays_update_area (gimage->ID,
			     layer->offset_x, layer->offset_y,
			     layer->width, layer->height);
      gdisplays_flush ();
    }

  return FALSE;
}


static void
layers_dialog_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}


static void
layers_dialog_update ()
{
  GtkWidget *list_item;
  GImage *gimage;
  Layer *layer;
  link_ptr list;
  GList *item_list;

  if (!layersD)
    return;

  /*  Free all elements in the layers listbox  */
  gtk_list_clear_items (layersD->layer_list, 0, -1);

  if (! (gimage = gimage_get_ID (layersD->gimage_id)))
    return;

  list = gimage->layers;
  item_list = NULL;

  /*  Set the maximum image extents in advance  */
  layersD->image_width = layersD->image_height = 0;

  while (list)
    {
      /*  create a layer list item  */
      layer = (Layer *) list->data;
      list_item = create_layer_widget (gimage, layer);
      item_list = g_list_append (item_list, list_item);

      list = next_item (list);
    }

  if (layersD->layer_image)
    image_buf_destroy (layersD->layer_image);

  layersD->layer_image = image_buf_create (COLOR_BUF,
					   layersD->image_width,
					   layersD->image_height);

  gtk_list_insert_items (layersD->layer_list, item_list, 0);
}


static Layer *
get_active_layer (gimage)
     GImage *gimage;
{
  Layer *layer;

  /*  return the active layer  */
  if (gimage->floating_sel >= 0)
    {
      layer = layer_get_ID (gimage->floating_sel);
      return layer;
    }
  else
    {
      layer = layer_get_ID (gimage->active_layer);
      return layer;
    }
}

/*
 *  The new layer query dialog
 */

typedef struct _NewLayerOptions NewLayerOptions;

struct _NewLayerOptions {
  GtkWidget *query_box;
  GtkWidget *entry;
  int fill_type;

  int gimage_id;
};

static int fill_type = TRANSPARENT_FILL;
static char *layer_name = NULL;

static void
new_layer_query_ok_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  NewLayerOptions *options;
  GImage *gimage;

  options = (NewLayerOptions *) client_data;
  if (layer_name)
    xfree (layer_name);
  layer_name = xstrdup (gtk_text_entry_get_text (options->entry));
  fill_type = options->fill_type;

  if ((gimage = gimage_get_ID (options->gimage_id)))
    {
      gimage_add_layer (gimage, layer_name,
			gimage_type_with_alpha (gimage),
			OPAQUE, NORMAL_MODE);
      gimage_fill (gimage, fill_type);
      gdisplays_update_full (gimage->ID);
      gdisplays_flush ();
    }

  gtk_widget_destroy (options->query_box);
  xfree (options);
}

static void
new_layer_query_cancel_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  NewLayerOptions *options;

  options = (NewLayerOptions *) client_data;
  gtk_widget_destroy (options->query_box);
  xfree (options);
}

static void
new_layer_background_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  NewLayerOptions *options;

  options = (NewLayerOptions *) client_data;
  options->fill_type = BACKGROUND_FILL;
}

static void
new_layer_white_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  NewLayerOptions *options;

  options = (NewLayerOptions *) client_data;
  options->fill_type = WHITE_FILL;
}

static void
new_layer_transparent_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  NewLayerOptions *options;

  options = (NewLayerOptions *) client_data;
  options->fill_type = TRANSPARENT_FILL;
}

static void
layers_dialog_new_layer_query (gimage_id)
     int gimage_id;
{
  static ActionAreaItem action_items[2] =
  {
    { "Ok", new_layer_query_ok_callback, NULL, NULL },
    { "Cancel", new_layer_query_cancel_callback, NULL, NULL }
  };
  NewLayerOptions *options;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *radio_frame;
  GtkWidget *radio_box;
  GtkWidget *radio_button;
  GtkWidget *action_area;
  GtkData *owner = NULL;
  int i;
  char *button_names[3] =
  {
    "Background",
    "White",
    "Transparent"
  };
  GtkCallback button_callbacks[3] =
  {
    new_layer_background_callback,
    new_layer_white_callback,
    new_layer_transparent_callback
  };

  /*  the new options structure  */
  options = (NewLayerOptions *) xmalloc (sizeof (NewLayerOptions));
  options->fill_type = fill_type;
  options->gimage_id = gimage_id;

  /*  the dialog  */
  options->query_box = gtk_window_new ("New Layer Options", GTK_WINDOW_DIALOG);

  /*  the main vbox  */
  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_add (options->query_box, vbox);

  /*  the entry hbox, label and entry  */
  hbox = gtk_hbox_new (FALSE, 5);
  gtk_box_pack (vbox, hbox, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_container_set_border_width (hbox, 0);
  label = gtk_label_new ("Layer name:");
  gtk_box_pack (hbox, label, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);
  options->entry = gtk_text_entry_new ();
  gtk_box_pack (hbox, options->entry, TRUE, TRUE, 0, GTK_PACK_START);
  gtk_text_entry_set_text (options->entry, (layer_name ? layer_name : "New Layer"));
  gtk_widget_show (options->entry);
  gtk_widget_show (hbox);

  /*  the radio frame and box  */
  radio_frame = gtk_frame_new ("Layer Fill Type");
  gtk_box_pack (vbox, radio_frame, FALSE, FALSE, 0, GTK_PACK_START);

  radio_box = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (radio_box, 0);
  gtk_container_add (radio_frame, radio_box);

  /*  the radio buttons  */
  for (i = 0; i < 3; i++)
    {
      radio_button = gtk_radio_button_new (owner);
      if (!i)
	owner = gtk_button_get_owner (radio_button);
      gtk_box_pack (radio_box, radio_button, FALSE, FALSE, 0, GTK_PACK_START);
      gtk_callback_add (gtk_button_get_state (radio_button),
			button_callbacks[i],
			options);

      /*  set the correct radio button  */
      if (i == options->fill_type)
	{
	  ((GtkDataWidget *) owner)->widget = radio_button;
	  gtk_data_notify (owner);
	}

      label = gtk_label_new (button_names[i]);
      gtk_label_set_alignment (label, 0.0, 0.5);
      gtk_container_add (radio_button, label);
      gtk_widget_show (label);
      gtk_widget_show (radio_button);
    }
  gtk_widget_show (radio_box);
  gtk_widget_show (radio_frame);

  action_items[0].user_data = options;
  action_items[1].user_data = options;
  action_area = build_action_area (action_items, 2);
  gtk_box_pack (vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);

  gtk_widget_show (action_area);
  gtk_widget_show (vbox);
  map_dialog (options->query_box);
}
