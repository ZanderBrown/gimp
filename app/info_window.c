/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include "appenv.h"
#include "actionarea.h"
#include "colormaps.h"
#include "info_dialog.h"
#include "info_window.h"
#include "gdisplay.h"
#include "general.h"
#include "gximage.h"
#include "interface.h"
#include "visual.h"

#define MAX_BUF 256

typedef struct _InfoWinData InfoWinData;
struct _InfoWinData
{
  char dimensions_str[MAX_BUF];
  char scale_str[MAX_BUF];
  char color_type_str[MAX_BUF];
  char visual_class_str[MAX_BUF];
  char visual_depth_str[MAX_BUF];
  char shades_str[MAX_BUF];
};

/*  The different classes of visuals  */
static char *visual_classes[] =
{
  "Static Gray",
  "Grayscale",
  "Static Color",
  "Pseudo Color",
  "True Color",
  "Direct Color",
};


static void
get_shades (gdisp, buf)
     GDisplay * gdisp;
     char * buf;
{
  switch (gdisp->gimage->type)
    {
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      if (emulate_gray)
	switch (gdisp->depth)
	  {
	  case 15 : case 16 :
	    sprintf (buf, "%d",
		     MINIMUM (MINIMUM ((1 << (8 - gray_visual->red_prec)),
				       (1 << (8 - gray_visual->green_prec))),
			      (1 << (8 - gray_visual->blue_prec))));
	    break;
	  case 24 :
	    sprintf (buf, "256");
	    break;
	  }
      else
	sprintf (buf, "%d", shades_gray);
      break;
    case RGB_GIMAGE: case RGBA_GIMAGE:
      switch (gdisp->depth)
	{
	case 8 :
	  sprintf (buf, "%d / %d / %d", shades_r, shades_g, shades_b);
	  break;
	case 15 : case 16 :
	  sprintf (buf, "%d / %d / %d",
		   (1 << (8 - color_visual->red_prec)),
		   (1 << (8 - color_visual->green_prec)),
		   (1 << (8 - color_visual->blue_prec)));
	  break;
	case 24 :
	  sprintf (buf, "256 / 256 / 256");
	  break;
	}
      break;

    case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
      sprintf (buf, "%d", gdisp->gimage->num_cols);
      break;
    }
}

static void
info_window_close_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  info_dialog_popdown ((InfoDialog *) client_data);
}

  /*  displays information:
   *    image name
   *    image width, height
   *    zoom ratio
   *    image color type
   *    Display info:
   *      visual class
   *      visual depth
   *      shades of color/gray
   */

static ActionAreaItem action_items[] = 
{
  { "Close", info_window_close_callback, NULL, NULL },
};

InfoDialog *
info_window_create (gdisp_ptr)
     void * gdisp_ptr;
{
  InfoDialog *info_win;
  GDisplay *gdisp;
  GtkWidget *action_area;
  InfoWinData *iwd;
  char * title, * title_buf;
  
  gdisp = (GDisplay *) gdisp_ptr;

  title = prune_filename (gdisp->gimage->filename);

  /*  allocate the title buffer  */
  title_buf = (char *) xmalloc (sizeof (char) * (strlen (title) + 15));
  sprintf (title_buf, "%s: Window Info", title);
  
  /*  create the info dialog  */
  info_win = info_dialog_new (title_buf);
  xfree (title_buf);

  iwd = (InfoWinData *) xmalloc (sizeof (InfoWinData));
  info_win->user_data = iwd;

  /*  add the information fields  */
  info_dialog_add_field (info_win, "Dimensions (w x h): ", iwd->dimensions_str);
  info_dialog_add_field (info_win, "Scale Ratio: ", iwd->scale_str);
  info_dialog_add_field (info_win, "Display Type: ", iwd->color_type_str);
  info_dialog_add_field (info_win, "Visual Class: ", iwd->visual_class_str);
  info_dialog_add_field (info_win, "Visual Depth: ", iwd->visual_depth_str);
  if (gdisp->gimage->type == RGB_GIMAGE ||
      gdisp->gimage->type == RGBA_GIMAGE)
    info_dialog_add_field (info_win, "Shades of Color: ", iwd->shades_str);
  else if (gdisp->gimage->type == INDEXED_GIMAGE)
    info_dialog_add_field (info_win, "Shades: ", iwd->shades_str);
  else if (gdisp->gimage->type == GRAY_GIMAGE ||
	   gdisp->gimage->type == GRAY_GIMAGE)
    info_dialog_add_field (info_win, "Shades of Gray: ", iwd->shades_str);

  /*  update the fields  */
  info_window_update (info_win, gdisp_ptr);

  /* Create the action area  */
  action_items[0].user_data = info_win;
  action_area = build_action_area (action_items, 1);
  gtk_box_pack (info_win->vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (action_area);

  return info_win;
}

void
info_window_free (info_win)
     InfoDialog *info_win;
{
  xfree (info_win->user_data);
  info_dialog_free (info_win);
}

void
info_window_update (info_win, gdisp_ptr)
     InfoDialog *info_win;
     void * gdisp_ptr;
{
  GDisplay *gdisp;
  InfoWinData *iwd;

  gdisp = (GDisplay *) gdisp_ptr;
  iwd = (InfoWinData *) info_win->user_data;

  /*  width and height  */
  sprintf (iwd->dimensions_str, "%d x %d",
	   (int) gdisp->gimage->width, (int) gdisp->gimage->height);

  /*  zoom ratio  */
  sprintf (iwd->scale_str, "%d:%d",
	   SCALEDEST (gdisp), SCALESRC (gdisp));

  /*  color type  */
  if (gdisp->gimage->type == RGB_GIMAGE)
    sprintf (iwd->color_type_str, "%s", "RGB Color");
  else if (gdisp->gimage->type == GRAY_GIMAGE)
    sprintf (iwd->color_type_str, "%s", "Grayscale");
  if (gdisp->gimage->type == RGBA_GIMAGE)
    sprintf (iwd->color_type_str, "%s", "RGB-alpha Color");
  else if (gdisp->gimage->type == GRAYA_GIMAGE)
    sprintf (iwd->color_type_str, "%s", "Grayscale-alpha");
  else if (gdisp->gimage->type == INDEXED_GIMAGE)
    sprintf (iwd->color_type_str, "%s", "Indexed Color");

  /*  visual class  */
  if (gdisp->gimage->type == RGB_GIMAGE ||
      gdisp->gimage->type == RGBA_GIMAGE ||
      gdisp->gimage->type == INDEXED_GIMAGE)
    sprintf (iwd->visual_class_str, "%s", visual_classes[color_visual->type]);
  else if (gdisp->gimage->type == GRAY_GIMAGE ||
	   gdisp->gimage->type == GRAYA_GIMAGE)
    sprintf (iwd->visual_class_str, "%s", visual_classes[gray_visual->type]);

  /*  visual depth  */
  sprintf (iwd->visual_depth_str, "%d", gdisp->depth);

  /*  pure color shades  */
  get_shades (gdisp, iwd->shades_str);

  info_dialog_update (info_win);
}

