/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __COLORMAPS_H__
#define __COLORMAPS_H__

#include "gimage.h"               /* For the image types  */

extern GdkColormap *rgbcmap;	  /* RGB colormap */
extern guint32 color_cube[256];   /* pixels of rgbpal */

extern GdkColormap *graycmap;	  /* grayscale colormap */
extern guint32 grays[256];        /* pixels of graypal */

extern unsigned int shades_r;     /*  shades of red in the RGB colormap  */
extern unsigned int shades_g;     /*  shades of green in the RGB colormap  */
extern unsigned int shades_b;     /*  shades of blue in the RGB colormap  */
extern unsigned int shades_gray;  /*  shades of gray in the grayscale colormap  */

/*  Pixel values of black and white in the color and gray visuals  */
extern guint32 color_black_pixel;
extern guint32 color_gray_pixel;
extern guint32 color_white_pixel;
extern guint32 gray_black_pixel;
extern guint32 gray_gray_pixel;
extern guint32 gray_white_pixel;

/*  Foreground and Background colors  */
extern guint32 foreground_pixel;
extern guint32 background_pixel;

/*  Old and New colors  */
extern guint32 old_color_pixel;
extern guint32 new_color_pixel;

/*  Colormap entries reserved for color cycled marching ants--optional  */
extern guint32 marching_ants_pixels[8];
extern int     color_cycled;
extern int     gray_cycled;

GdkColormap *	get_colormap (int);
void            store_color (guint32 *, int, int, int);
void            install_colormap (int);
void		create_standard_colormaps ();
void		free_standard_colormaps ();


#endif  /*  __COLORMAPS_H__  */
