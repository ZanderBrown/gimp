/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "appenv.h"
#include "errors.h"
#include "gimage.h"
#include "gimage_mask.h"
#include "layer.h"
#include "layers_dialog.h"
#include "linked.h"
#include "paint_funcs.h"
#include "temp_buf.h"
#include "undo.h"

/*  static functions  */

static void layer_allocate (Layer *, int);
static void transform_color(GImage *, PixelRegion *,
			    PixelRegion *, int);

/*
 *  Static variables
 */
static int global_layer_ID = 1;
static link_ptr layer_list = NULL;


/********************************/
/*  Local function definitions  */

static void
layer_allocate (layer, size)
     Layer * layer;
     int size;
{
  layer->shmid = shmget (IPC_PRIVATE,
			 size,
			 IPC_CREAT|0777);
  
  if (layer->shmid < 0)
    {
      perror ("shmget failed");
      fatal_error ("shmget failed!");
    }

  layer->shmaddr = layer->data = 
    (unsigned char *) shmat (layer->shmid, 0, 0);

  if (layer->shmaddr < (void*) 0)
    fatal_error ("shmat failed!");
}


static void
transform_color (gimage, layerPR, bufPR, type)
     GImage * gimage;
     PixelRegion * layerPR;
     PixelRegion * bufPR;
     int type;
{
  int i, h;
  unsigned char * s, * d;

  h = layerPR->h;
  s = bufPR->data;
  d = layerPR->data;

  while (h--)
    {
      for (i = 0; i < layerPR->w; i++)
	{
	  gimage_transform_color (gimage, s + (i * bufPR->bytes),
				  d + (i * layerPR->bytes), type);
	  /*  copy alpha channel  */
	  d[(i + 1) * layerPR->bytes - 1] = s[(i + 1) * bufPR->bytes - 1];
	}
      
      s += bufPR->rowstride;
      d += layerPR->rowstride;
    }
}


/**************************/
/*  Function definitions  */


Layer *
layer_new (gimage_ID, width, height, type, name, opacity, mode)
     int gimage_ID;
     int width, height;
     int type;
     char * name;
     int opacity;
     int mode;
{
  Layer * layer;

  layer = (Layer *) xmalloc (sizeof (Layer));

  if (!name)
    name = "unnamed";

  layer->name = (char *) xmalloc (strlen (name) + 1);
  strcpy (layer->name, name);

  /*  set size information  */
  layer->offset_x = 0;
  layer->offset_y = 0;
  layer->width = width;
  layer->height = height;
  layer->type = type;

  switch (type)
    {
    case RGB_GIMAGE:
      layer->bytes = 3; break;
    case GRAY_GIMAGE:
      layer->bytes = 1; break;
    case RGBA_GIMAGE:
      layer->bytes = 4; break;
    case GRAYA_GIMAGE:
      layer->bytes = 2; break;
    case INDEXED_GIMAGE:
      layer->bytes = 1; break;
    case INDEXEDA_GIMAGE:
      layer->bytes = 2; break;
    default:
      warning ("Layer type not supported.\n");
      break;
    }

  /*  allocate the memory for this layer  (shm)  */
  layer_allocate (layer, width * height * layer->bytes);
  layer->visible = 1;
  layer->linked = 0;
  layer->preserve_trans = 0;

  /*  no layer mask is present at start  */
  layer->mask = NULL;
  layer->apply_mask = 0;

  /*  mode and opacity  */
  layer->mode = mode;
  layer->opacity = opacity;

  /*  give this layer an ID  */
  layer->ID = global_layer_ID++;
  layer->gimage_ID = gimage_ID;

  layer->layer_widget = NULL;

  /*  add the new layer to the global list  */
  layer_list = append_to_list (layer_list, (void *) layer);

  return layer;
}


Layer *
layer_copy (layer)
     Layer * layer;
{
  char * layer_name;
  Layer * new_layer;

  /*  formulate the new layer name  */
  layer_name = (char *) xmalloc (strlen (layer->name) + 6);
  sprintf (layer_name, "%s copy", layer->name);

  /*  allocate a new layer object  */
  new_layer = layer_new (layer->gimage_ID, layer->width, layer->height,
			 layer->type, layer_name, layer->opacity, layer->mode);

  /*  copy the contents across layers  */
  memcpy (new_layer->data, layer->data, layer->width * layer->height * layer->bytes);

  /*  duplicate the layer mask if necessary  */
  if (layer->mask)
    {
      new_layer->mask = channel_copy (layer->mask);
      new_layer->apply_mask = 1;
    }

  /*  free up the layer_name memory  */
  xfree (layer_name);

  return new_layer;
}


Layer *
layer_from_buf (gimage_ptr, buf, width, height, name, opacity, mode)
     void * gimage_ptr;
     TempBuf * buf;
     int width, height;
     char * name;
     int opacity;
     int mode;
{
  GImage * gimage;
  Layer * new_layer;
  int layer_type;
  int lx, ly, bx, by;
  unsigned char col[MAX_CHANNELS] = { 0, 0, 0, 0 };
  PixelRegion layerPR, bufPR;

  /*  Function copies buffer to a layer
   *  taking into consideration the possibility of transforming
   *  the contents to meet the requirements of the target image type
   */

  /*  If no buffer, return NULL  */
  if (!buf)
    return NULL;

  gimage = (GImage *) gimage_ptr;

  /*  If width or height is 0, use the global buffer's width and height  */
  if (width == 0 || height == 0)
    {
      width = buf->width;
      height = buf->height;
    }

  layer_type = gimage_type_with_alpha (gimage);

  /*  Create the new layer  */
  new_layer = layer_new (0, width, height, layer_type,
			 name, opacity, mode);

  /*  If the buf width or height is less than the layer's
   *  give the layer an initial coloring
   */
  if (buf->width < new_layer->width ||
      buf->height < new_layer->height)
    color_pixels (layer_data (new_layer), col, new_layer->bytes,
		  new_layer->width * new_layer->height);

  /*  coordinates into the layer  */
  if (new_layer->width >= buf->width)
    {
      lx = (new_layer->width - buf->width) >> 1;
      bx = 0;
    }
  else
    {
      lx = 0;
      bx = (buf->width - new_layer->width) >> 1;
    }

  if (new_layer->height >= buf->height)
    {
      ly = (new_layer->height - buf->height) >> 1;
      by = 0;
    }
  else
    {
      ly = 0;
      by = (buf->height - new_layer->height) >> 1;
    }

  /*  Configure the pixel regions  */
  layerPR.bytes = new_layer->bytes;
  layerPR.w = MINIMUM (new_layer->width, buf->width);
  layerPR.h = MINIMUM (new_layer->height, buf->height);
  layerPR.rowstride = layerPR.bytes * new_layer->width;
  layerPR.data = layer_data (new_layer) + ly * layerPR.rowstride +
    lx * layerPR.bytes;

  bufPR.bytes = buf->bytes;
  bufPR.w = layerPR.w;
  bufPR.h = layerPR.h;
  bufPR.rowstride = bufPR.bytes * buf->width;
  bufPR.data = temp_buf_data (buf) + by * bufPR.rowstride +
    bx * bufPR.bytes;

  if ((buf->bytes == 4 && new_layer->type == RGBA_GIMAGE) ||
      (buf->bytes == 2 && new_layer->type == GRAYA_GIMAGE))
    /*  If we want a layer the same type as the buffer  */
    copy_region (&bufPR, &layerPR);
  else
    /*  Transform the contents of the buf to the new_layer  */
    transform_color (gimage, &layerPR, &bufPR,
		     (buf->bytes == 4) ? RGB : GRAY);

  return new_layer;
}


Channel *
layer_add_mask (layer, name, opacity, col)
     Layer * layer;
     char * name;
     int opacity;
     unsigned char * col;
{
  char * mask_name;

  if (layer->mask)
    return NULL;

  mask_name = (char *) xmalloc (strlen (name) + 1);
  strcpy (mask_name, name);

  layer->mask = channel_new (layer->gimage_ID, layer->width, layer->height,
			     mask_name, opacity, col);
  layer->apply_mask = 1;

  xfree (mask_name);

  return layer->mask;
}


Layer *
layer_get_ID (ID)
     int ID;
{
  link_ptr tmp = layer_list;
  Layer * layer;

  while (tmp)
    {
      layer = (Layer *) tmp->data;
      if (layer->ID == ID)
	return layer;
      
      tmp = next_item (tmp);
    }

  return NULL;
}


void
layer_delete (layer)
     Layer * layer;
{
  /*  remove this image from the global list  */
  layer_list = remove_from_list (layer_list, (void *) layer);

  /*  free the shared memory  */
  if (layer->shmaddr >= (void*) 0)
    {
      shmdt (layer->shmaddr);
      shmctl (layer->shmid, IPC_RMID, 0);
    }

  /*  if a layer mask exists, free it  */
  if (layer->mask)
    channel_delete (layer->mask);
      
  /*  free the layer name buffer  */
  xfree (layer->name);

  xfree (layer);
}


void
layer_apply_mask (layer)
     Layer * layer;
{
  PixelRegion srcPR, maskPR;

  if (!layer->mask)
    return;

  /*  this operation can only be done to layers with an alpha channel  */
  if (layer->type != RGBA_GIMAGE && layer->type != GRAYA_GIMAGE)
    return;

  /*  Might want to optimize this later by saving only layer mask...  */
  /*  Put this apply mask operation on the undo stack
   *  No need to anchor the layer.
   */
  layer_apply_image (layer, 0, 0, layer->width, layer->height, NULL, 0);
  
  /*  Combine the current layer's alpha channel and the mask  */
  srcPR.bytes = layer->bytes;
  srcPR.w = layer->width;
  srcPR.h = layer->height;
  srcPR.rowstride = layer->width * layer->bytes;
  srcPR.data = layer->data;

  maskPR.rowstride = layer->mask->width * layer->mask->bytes;
  maskPR.data = layer->mask->data;

  apply_mask_to_region (&srcPR, &maskPR, OPAQUE);

  layer->mask = NULL;
}


int
layer_anchor (layer)
     Layer * layer;
{
  TempBuf * old_layer;
  PixelRegion srcPR, destPR;
  int srcx, srcy;
  int destx, desty;

  /*  if there are non-zero offsets for this layer,
   *  translate the contents by the offsets and fill
   *  in the remainder of the image with transparent
   */
  if (layer->offset_x || layer->offset_y)
    {
      /*  save the old layer  */
      old_layer = temp_buf_new (layer->width, layer->height, layer->bytes,
				layer->offset_x, layer->offset_y, NULL);
      memcpy (temp_buf_data (old_layer), layer->data,
	      layer->width * layer->height * layer->bytes);

      /*  make the undo call here  */
      undo_push_group_start (LAYER_ANCHOR_UNDO);
      undo_push_layer_displace (layer->gimage_ID, layer->ID);;
      undo_push_image (layer->gimage_ID, layer->ID, -1,
		       0, 0, layer->width, layer->height);
      undo_push_group_end ();

      /*  reposition the layer contents  */
      srcx = (layer->offset_x > 0) ? 0 : layer->offset_x;
      srcy = (layer->offset_y > 0) ? 0 : layer->offset_y;
      destx = (layer->offset_x > 0) ? layer->offset_x : 0;
      desty = (layer->offset_y > 0) ? layer->offset_y : 0;

      srcPR.bytes = layer->bytes;
      srcPR.w = (layer->offset_x > 0) ? layer->width - layer->offset_x :
	layer->width + layer->offset_x;
      srcPR.h = (layer->offset_y > 0) ? layer->height - layer->offset_y :
	layer->height + layer->offset_y;
      srcPR.rowstride = layer->width * layer->bytes;
      srcPR.data = temp_buf_data (old_layer) + 
	srcPR.rowstride * srcy + srcPR.bytes * srcx;

      destPR.rowstride = srcPR.rowstride;
      destPR.data = layer->data + 
	srcPR.rowstride * desty + srcPR.bytes * destx;

      /*  set background of anchored region to transparent  */
      memset (layer->data, 0, layer->width * layer->height * layer->bytes);
      /*  copy the appropriate data back in...  */
      if (srcPR.w > 0 && srcPR.h > 0)
	copy_region (&srcPR, &destPR);

      layer->offset_x = 0;
      layer->offset_y = 0;

      /*  return 1 to indicate that layer WAS anchored  */
      return 1;
    }
  else
    /*  layer was not anchored  */
    return 0;
}


void
layer_displace (layer, off_x, off_y)
     Layer * layer;
     int off_x, off_y;
{
  /*  the undo call goes here  */
  undo_push_layer_displace (layer->gimage_ID, layer->ID);
  
  /*  update the layer offsets  */
  layer->offset_x = off_x;
  layer->offset_y = off_y;
}


void
layer_apply_image (layer, x1, y1, x2, y2, buf, anchor)
     Layer * layer;
     int x1, y1, x2, y2;
     TempBuf * buf;
     int anchor;
{
  int undo = 0;

  if (anchor)
    undo = layer_anchor (layer);

  if (! undo)
    {
      if (! buf)
	/*  Need to push an undo operation  */
	undo_push_image (layer->gimage_ID, layer->ID, -1, x1, y1, x2, y2);
      else
	undo_push_image_mod (layer->gimage_ID, layer->ID, -1, buf);
    }

  layers_dialog_dirty_layer (layer->layer_widget);
}


/********************/
/* access functions */

unsigned char *
layer_data (layer)
     Layer * layer;
{
  return layer->data;
}


Channel *
layer_mask (layer)
     Layer * layer;
{
  return layer->mask;
}


int
layer_toggle_visibility (layer)
     Layer * layer;
{
  layer->visible = !layer->visible;

  return layer->visible;
}


int
layer_toggle_linkage (layer)
     Layer * layer;
{
  layer->linked = !layer->linked;

  return layer->linked;
}


int
layer_toggle_preserve_trans (layer)
     Layer * layer;
{
  layer->preserve_trans = !layer->preserve_trans;

  return layer->preserve_trans;
}


int
layer_toggle_mask_application (layer)
     Layer * layer;
{
  layer->apply_mask = !layer->apply_mask;

  return layer->apply_mask;
}


