/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __FLOATING_SEL_H__
#define __FLOATING_SEL_H__

#include "boundary.h"
#include "gimage.h"

/*  floating selection functions  */
BoundSeg *     floating_sel_boundary   (GImage *, int *);
int            floating_sel_bounds     (GImage *, int *, int *, int *, int *);
unsigned char  floating_sel_value      (GImage *, int, int);
void           floating_sel_translate  (GImage *, int, int);
void           floating_sel_replace    (GImage *, Layer *, int);
void           floating_sel_swap       (GImage *, Layer *);
void           floating_sel_anchor     (GImage *);
TempBuf *      floating_sel_extract    (GImage *, int);
void           floating_sel_clear      (GImage *);
void           floating_sel_all        (GImage *);
void           floating_sel_sharpen    (GImage *);
void           floating_sel_invert     (GImage *);
void           floating_sel_feather    (GImage *, double);
void           floating_sel_border     (GImage *, int);

#endif  /*  __FLOATING_SEL_H__  */
