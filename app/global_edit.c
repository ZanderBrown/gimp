/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include "appenv.h"
#include "actionarea.h"
#include "autodialog.h"
#include "floating_sel.h"
#include "gdisplay.h"
#include "gimage.h"
#include "gimage_mask.h"
#include "general.h"
#include "global_edit.h"
#include "interface.h"
#include "layer.h"
#include "linked.h"
#include "paint_funcs.h"
#include "temp_buf.h"
#include "tools.h"
#include "undo.h"


/*  The named paste dialog  */
typedef struct _PasteNamedDlg PasteNamedDlg;
struct _PasteNamedDlg
{
  GtkWidget   *shell;
  GtkWidget   *listbox;
  GtkWidget   *list;
  GtkObserver  observer;
  int          paste_into;
  GDisplay    *gdisp;
};

/*  The named buffer structure...  */
typedef struct _named_buffer NamedBuffer;

struct _named_buffer
{
  TempBuf *    buf;
  char *       name;
};


/*  The named buffer list  */
link_ptr named_buffers = NULL;

/*  The global edit buffer  */
TempBuf * global_buf = NULL;


/*  Crop the buffer to the size of pixels with non-zero transparency */

TempBuf *
crop_buffer (buf)
     TempBuf * buf;
{
  TempBuf * new_buf;
  unsigned char * data;
  int bytes, alpha;
  int x, y;
  int x1, y1, x2, y2;
  int empty = 1;

  /*  Find the size of the new buffer  */
  x1 = buf->width;
  y1 = buf->height;
  x2 = 0;
  y2 = 0;
      
  data = temp_buf_data (buf);
  bytes = buf->bytes;
  alpha = bytes - 1;
      
  for (y = 0; y < buf->height; y++)
    for (x = 0; x < buf->width; x++, data += bytes)
      if (data[alpha])
	{
	  if (x < x1)
	    x1 = x;
	  if (x > x2)
	    x2 = x;
	  if (y < y1)
	    y1 = y;
	  if (y > y2)
	    y2 = y;
	  empty = 0;
	}
  
  x2 = BOUNDS (x2 + 1, 0, buf->width);
  y2 = BOUNDS (y2 + 1, 0, buf->height);

  /*  If there are no visible pixels, return NULL */
  if (empty)
    new_buf = NULL;
  /*  If no cropping, return original buffer  */
  else if (x1 == 0 && y1 == 0 && x2 == buf->width && y2 == buf->height)
    new_buf = buf;
  /*  Otherwise, crop the original area  */
  else
    new_buf = temp_buf_copy_area (buf, NULL, x1, y1, (x2 - x1), (y2 - y1));

  return new_buf;
}

TempBuf *
edit_cut (gdisp_ptr)
     void * gdisp_ptr;
{
  TempBuf * cut;
  TempBuf * cropped_cut;
  GImage * gimage;

  gimage = ((GDisplay *) gdisp_ptr)->gimage;

  /*  stop any active tool  */
  active_tool_control (HALT, gdisp_ptr);

  /*  Start an undo group--use a group because the extract call will
   *  also push an undo of some sort  (depending on floating sel)
   */
  undo_push_group_start (EDIT_CUT_UNDO);
  undo_push_global_edit (global_buf);

  /*  Next, cut the mask portion from the gimage  */
  cut = gimage_mask_extract (gimage, 1);

  if (cut)
    {
      /*  Update the gimage  */
      gimage_update (gimage, cut->x, cut->y, cut->width, cut->height);
      gdisplays_flush ();

      cropped_cut = crop_buffer (cut);

      if (cropped_cut != cut)
	temp_buf_free (cut);
    }
  else
    cropped_cut = NULL;

  /*  End the undo group  */
  undo_push_group_end ();

  if (cropped_cut)
    {
      /*  Set the global edit buffer  */
      global_buf = cropped_cut;

      return cropped_cut;
    }
  else
    return NULL;
}

TempBuf *
edit_copy (gdisp_ptr)
     void * gdisp_ptr;
{
  TempBuf * copy;
  TempBuf * cropped_copy;
  GImage * gimage;

  gimage = ((GDisplay *) gdisp_ptr)->gimage;

  /*  Push an undo  */
  undo_push_global_edit (global_buf);

  /*  First, copy the masked portion of the gimage  */
  copy = gimage_mask_extract (gimage, 0);

  if (copy)
    {
      cropped_copy = crop_buffer (copy);

      if (cropped_copy != copy)
	temp_buf_free (copy);
    }
  else
    cropped_copy = NULL;

  if (cropped_copy)
    {
      /*  Set the global edit buffer  */
      global_buf = cropped_copy;

      return cropped_copy;
    }
  else
    return NULL;
}

int
edit_paste (gdisp_ptr, paste, paste_into)
     void * gdisp_ptr;
     TempBuf * paste;
     int paste_into;
{
  GImage * gimage;
  Layer * cur_float;
  Layer * float_layer;
  int x1, y1, x2, y2;
  int cx, cy;

  gimage = ((GDisplay *) gdisp_ptr)->gimage;
  
  /*  stop any active tool  */
  active_tool_control (HALT, gdisp_ptr);

  /*  Make a new floating layer  */
  float_layer = layer_from_buf (gimage, paste, 0, 0, "Floating Selection",
				OPAQUE, NORMAL);

  if (float_layer)
    {
      /*  Set the offsets to the center of the current floating selection
       *  or to the center of the image if there is no current selection
       */
      if ((cur_float = layer_get_ID (gimage->floating_sel)))
	{
	  cx = (cur_float->width >> 1) + cur_float->offset_x;
	  cy = (cur_float->height >> 1) + cur_float->offset_y;
	}
      else
	{
	  gimage_mask_bounds (gimage, &x1, &y1, &x2, &y2);
	  cx = (x1 + x2) >> 1;
	  cy = (y1 + y2) >> 1;
	}

      float_layer->offset_x = cx - (float_layer->width >> 1);
      float_layer->offset_y = cy - (float_layer->height >> 1);

      /*  Replace the current floating selection with the new floating layer  */
      floating_sel_replace (gimage, float_layer, paste_into);
      gdisplays_flush ();

      return 1;
    }
  else
    return 0;
}

int
global_edit_cut (gdisp_ptr)
     void * gdisp_ptr;
{
  if (!edit_cut (gdisp_ptr))
    return FALSE;
  else
    return TRUE;
}

int
global_edit_copy (gdisp_ptr)
     void * gdisp_ptr;
{
  if (!edit_copy (gdisp_ptr))
    return FALSE;
  else
    return TRUE;
}

int
global_edit_paste (gdisp_ptr, paste_into)
     void * gdisp_ptr;
     int paste_into;
{
  if (!edit_paste (gdisp_ptr, global_buf, paste_into))
    return FALSE;
  else
    return TRUE;
}

void
global_edit_free ()
{
  if (global_buf)
    temp_buf_free (global_buf);

  global_buf = NULL;
}

/*********************************************/
/*        Named buffer operations            */

static void
set_list_of_named_buffers (list_widget)
     GtkWidget *list_widget;
{
  link_ptr list;
  NamedBuffer *nb;
  GtkWidget *list_item;

  list = named_buffers;

  while (list)
    {
      nb = (NamedBuffer *) list->data;
      list = next_item (list);

      list_item = gtk_list_item_new_with_label (nb->name);
      gtk_container_add (list_widget, list_item);
      gtk_widget_show (list_item);
      gtk_widget_set_user_data (list_item, (gpointer) nb);
    }
}

static void
named_buffer_paste_foreach (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PasteNamedDlg *pn_dlg;
  NamedBuffer *nb;

  pn_dlg = (PasteNamedDlg *) client_data;
  nb = (NamedBuffer *) gtk_widget_get_user_data (w);
  temp_buf_unswap (nb->buf);
  edit_paste (pn_dlg->gdisp, nb->buf, pn_dlg->paste_into);
  temp_buf_swap (nb->buf);
}

static void
named_buffer_paste_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PasteNamedDlg *pn_dlg;

  pn_dlg = (PasteNamedDlg *) client_data;

  gtk_container_foreach ((GtkContainer*) pn_dlg->list,
			 named_buffer_paste_foreach, client_data);

  /*  Destroy the box  */
  /*gtk_widget_destroy (pn_dlg->shell);*/
  gtk_widget_hide (pn_dlg->shell);

  xfree (pn_dlg);
}

static void
named_buffer_delete_foreach (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PasteNamedDlg *pn_dlg;
  NamedBuffer * nb;

  pn_dlg = (PasteNamedDlg *) client_data;
  nb = (NamedBuffer *) gtk_widget_get_user_data (w);
  named_buffers = remove_from_list (named_buffers, (void *) nb);
  xfree (nb->name);
  temp_buf_free (nb->buf);
  xfree (nb);
  set_list_of_named_buffers (pn_dlg->list);
}

static void
named_buffer_delete_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PasteNamedDlg *pn_dlg;

  pn_dlg = (PasteNamedDlg *) client_data;
  gtk_container_foreach ((GtkContainer*) pn_dlg->list,
			 named_buffer_delete_foreach, client_data);
}

static void
named_buffer_cancel_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  PasteNamedDlg *pn_dlg;

  pn_dlg = (PasteNamedDlg *) client_data;

  /*  Destroy the box  */
  /*gtk_widget_destroy (pn_dlg->shell);*/
  gtk_widget_hide (pn_dlg->shell);

  xfree (pn_dlg);
}

static gint
named_buffer_paste_into_update (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
  GtkDataInt *int_data;
  PasteNamedDlg *pn_dlg;
  
  pn_dlg = (PasteNamedDlg *) observer->user_data;
  int_data = (GtkDataInt *) data;

  if (int_data->value == GTK_STATE_ACTIVATED)
    pn_dlg->paste_into = FALSE;
  else if (int_data->value == GTK_STATE_DEACTIVATED)
    pn_dlg->paste_into = TRUE;

  return FALSE;
}

static void
named_buffer_disconnect (observer, data)
     GtkObserver *observer;
     GtkData *data;
{
}

static void
paste_named_buffer (gdisp)
     GDisplay *gdisp;
{
  static ActionAreaItem action_items[3] =
  {
    { "Paste", named_buffer_paste_callback, NULL, NULL },
    { "Delete", named_buffer_delete_callback, NULL, NULL },
    { "Cancel", named_buffer_cancel_callback, NULL, NULL }
  };
  PasteNamedDlg *pn_dlg;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *paste_into;
  GtkWidget *action_area;

  pn_dlg = (PasteNamedDlg *) xmalloc (sizeof (PasteNamedDlg));
  pn_dlg->gdisp = gdisp;
  pn_dlg->shell = gtk_window_new ("Paste Named Buffer", GTK_WINDOW_DIALOG);

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_add (pn_dlg->shell, vbox);
  label = gtk_label_new ("Select a buffer to paste:");
  gtk_box_pack (vbox, label, TRUE, FALSE, 0, GTK_PACK_START);
  gtk_widget_show (label);

  pn_dlg->listbox = gtk_listbox_new ();
  gtk_listbox_set_shadow_type (pn_dlg->listbox, GTK_SHADOW_IN);
  gtk_box_pack (vbox, pn_dlg->listbox, TRUE, TRUE, 0, GTK_PACK_START);
  pn_dlg->list = gtk_listbox_get_list (pn_dlg->listbox);
  gtk_widget_set_usize (pn_dlg->list, 125, 200);
  gtk_list_set_selection_mode (pn_dlg->list, GTK_SELECTION_BROWSE);
  set_list_of_named_buffers (pn_dlg->list);
      
  paste_into = gtk_check_button_new ();
  gtk_box_pack (vbox, paste_into, FALSE, FALSE, 0, GTK_PACK_START);
  label = gtk_label_new ("Replace Current Selection");
  gtk_label_set_alignment (label, 0.0, 0.5);
  gtk_container_add (paste_into, label);
  gtk_widget_show (label);
  pn_dlg->observer.update = named_buffer_paste_into_update;
  pn_dlg->observer.disconnect = named_buffer_disconnect;
  pn_dlg->observer.user_data = pn_dlg;
  gtk_data_attach (gtk_button_get_state (paste_into), &pn_dlg->observer);

  action_items[0].user_data = pn_dlg;
  action_items[1].user_data = pn_dlg;
  action_items[2].user_data = pn_dlg;
  action_area = build_action_area (action_items, 3);
  gtk_box_pack (vbox, action_area, FALSE, FALSE, 0, GTK_PACK_START);
      
  gtk_widget_show (paste_into);
  gtk_widget_show (pn_dlg->listbox);
  gtk_widget_show (action_area);
  gtk_widget_show (vbox);
  map_dialog (pn_dlg->shell);
}

static void
new_named_buffer_callback (w, client_data, call_data)
     GtkWidget *w;
     gpointer client_data;
     gpointer call_data;
{
  TempBuf *new_buf;
  NamedBuffer *nb;

  new_buf = (TempBuf *) client_data;
  nb = (NamedBuffer *) xmalloc (sizeof (NamedBuffer));

  nb->buf = temp_buf_copy (new_buf, NULL);
  nb->name = xstrdup ((char *) call_data);
  temp_buf_swap (nb->buf);
  named_buffers = append_to_list (named_buffers, (void *) nb);
}

static void
new_named_buffer (new_buf)
     TempBuf *new_buf;
{
  /*  Create the dialog box to ask for a name  */
  query_name_box ("Named Buffer", "Enter a name for this buffer",
		  new_named_buffer_callback, new_buf);
}

int
named_edit_cut (gdisp_ptr)
     void * gdisp_ptr;
{
  TempBuf * new_buf;

  new_buf = edit_cut (gdisp_ptr);

  if (! new_buf)
    return FALSE;
  else
    new_named_buffer (new_buf);

  return TRUE;
}

int
named_edit_copy (gdisp_ptr)
     void * gdisp_ptr;
{
  TempBuf * new_buf;

  new_buf = edit_copy (gdisp_ptr);

  if (! new_buf)
    return FALSE;
  else
    new_named_buffer (new_buf);

  return TRUE;
}

int
named_edit_paste (gdisp_ptr)
     void * gdisp_ptr;
{
  paste_named_buffer ((GDisplay *) gdisp_ptr);

  return TRUE;
}

void
named_buffers_free ()
{
  link_ptr list;
  NamedBuffer * nb;

  list = named_buffers;

  while (list)
    {
      nb = (NamedBuffer *) list->data;
      temp_buf_free (nb->buf);
      xfree (nb->name);
      xfree (nb);
      list = next_item (list);
    }

  free_list (named_buffers);
  named_buffers = NULL;
}






