/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <math.h>
#include "appenv.h"
#include "errors.h"
#include "floating_sel.h"
#include "gdisplay.h"
#include "general.h"
#include "gimage.h"
#include "gimage_mask.h"
#include "interface.h"
#include "layers_dialog.h"
#include "paint_funcs.h"
#include "palette.h"
#include "plug_in.h"
#include "undo.h"

/*  Local function declarations  */
static void     gimage_plug_in_callback      (void *, void *);
static GImage * gimage_create                (void);
static void     gimage_allocate_projection   (GImage *);
static void     gimage_free_projection       (GImage *);
static void     gimage_allocate_shadow       (GImage *);
static void     gimage_free_layers           (GImage *);
static void     gimage_free_channels         (GImage *);
static void     gimage_construct_layers      (GImage *, link_ptr, int, int, int, int);
static void     gimage_construct_channels    (GImage *, link_ptr, int, int, int, int);
static void     gimage_initialize_projection (GImage *, int, int, int, int);

/*  projection functions  */
static void     project_intensity            (GImage *, Layer *, PixelRegion *,
					      PixelRegion *, PixelRegion *);
static void     project_intensity_alpha      (GImage *, Layer *, PixelRegion *,
					      PixelRegion *, PixelRegion *);
static void     project_indexed              (GImage *, Layer *, PixelRegion *,
					      PixelRegion *);
static void     project_channel              (GImage *, Channel *, 
					      PixelRegion *, PixelRegion *);


/*
 *  Static variables
 */
static int global_gimage_ID = 1;
static char basename[] = "Untitled";
link_ptr image_list = NULL;

static int valid_combinations[][MAX_CHANNELS + 1] =
{
  /* RGB GIMAGE */
  { -1, -1, -1, COMBINE_INTEN_INTEN, COMBINE_INTEN_INTEN_A },
  /* RGBA GIMAGE */
  { -1, -1, -1, COMBINE_INTEN_A_INTEN, COMBINE_INTEN_A_INTEN_A },
  /* GRAY GIMAGE */
  { -1, COMBINE_INTEN_INTEN, COMBINE_INTEN_INTEN_A, -1, -1 },
  /* GRAYA GIMAGE */
  { -1, COMBINE_INTEN_A_INTEN, COMBINE_INTEN_A_INTEN_A, -1, -1 },
  /* INDEXED GIMAGE */
  { -1, COMBINE_INDEXED_INDEXED, COMBINE_INDEXED_INDEXED_A, -1, -1 },
  /* INDEXEDA GIMAGE */
  { -1, -1, COMBINE_INDEXED_A_INDEXED_A, -1, -1 },
};

/* static functions */

static GImage *
gimage_create ()
{
  GImage *gimage;

  gimage = (GImage *) xmalloc (sizeof (GImage));

  gimage->has_filename = 0;
  gimage->num_cols = 0;
  gimage->cmap = NULL;
  gimage->ID = global_gimage_ID++;
  gimage->ref_count = 0;
  gimage->instance_count = 0;
  gimage->shadow_shmaddr = (void *) -1;
  gimage->shadow_shmid = 0;
  gimage->plug_ins = NULL;
  gimage->busy = FALSE;
  gimage->dirty = 1;
  gimage->region = NULL;
  gimage->flat = 1;
  gimage->construct_flag = -1;
  gimage->projection = NULL;
  gimage->layers = NULL;
  gimage->channels = NULL;
  gimage->floating_sel = -1;
  gimage->boundary_known = 0;
  gimage->boundary = NULL;
  gimage->num_segments = 0;

  image_list = append_to_list (image_list, (void *) gimage);

  return gimage;
}

static void
gimage_allocate_projection (gimage)
     GImage *gimage;
{
  unsigned int size;

  if (gimage->projection)
    gimage_free_projection (gimage);

  /*  Find the number of bytes required for the projection.
   *  This includes the intensity channels and an alpha channel
   *  if one doesn't exist.
   */
  switch (gimage->type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE: case INDEXED_GIMAGE:
      gimage->proj_bytes = 4;
      gimage->proj_type = RGBA_GIMAGE;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      gimage->proj_bytes = 2;
      gimage->proj_type = GRAYA_GIMAGE;
      break;
    default:
      warning ("gimage type unsupported.\n");
      break;
    }

  size = gimage->width * gimage->height * gimage->proj_bytes;

  /*  allocate the new projection  */
  gimage->projection = (unsigned char *) xmalloc (size);
}  

static void
gimage_free_projection (gimage)
     GImage * gimage;
{
  if (gimage->projection)
    xfree (gimage->projection);
}

static void
gimage_allocate_shadow (gimage)
     GImage * gimage;
{
  long size;

  size = gimage_width (gimage) * gimage_height (gimage) * gimage_bytes (gimage);

  gimage->shadow_shmid = shmget (IPC_PRIVATE,
				 size,
				 IPC_CREAT|0777);
  
  if (gimage->shadow_shmid < 0)
    {
      perror ("shadow shmget failed");
      fatal_error ("shadow shmget failed!");
    }

  gimage->shadow_shmaddr = gimage->shadow_buf = 
    (unsigned char *) shmat (gimage->shadow_shmid, 0, 0);

  if (gimage->shadow_shmaddr < (void*) 0)
    fatal_error ("shadow shmat failed!");
}


/* function definitions */

GImage *
gimage_new (width, height, type)
     long width, height;
     int type;
{
  GImage *gimage;
  Layer *new_layer;
  char *layer_name;
  int i;

  gimage = gimage_create ();

  gimage->filename = xstrdup (basename);
  gimage->width = width;
  gimage->height = height;
  gimage->type = type;

  switch (gimage->type)
    {
    case RGB_GIMAGE:
      gimage->bpp = 3; break;
    case GRAY_GIMAGE:
      gimage->bpp = 1; break;
    case RGBA_GIMAGE:
      gimage->bpp = 4; break;
    case GRAYA_GIMAGE:
      gimage->bpp = 2; break;
    case INDEXED_GIMAGE:
      gimage->bpp = 1; break;
    default:
      warning ("GImage type not supported.\n");
      break;
    }

  switch (gimage->type)
    {
    case RGB_GIMAGE:
    case GRAY_GIMAGE:
      layer_name = "Background";
      break;
    case RGBA_GIMAGE:
    case GRAYA_GIMAGE:
      layer_name = "Layer 1";
      break;
    case INDEXED_GIMAGE:
      /* always allocate 256 colors for the colormap */
      gimage->num_cols = 0;
      gimage->cmap = (unsigned char *) xmalloc (COLORMAP_SIZE);
      memset (gimage->cmap, 0, COLORMAP_SIZE);
      layer_name = "Background";
      break;
    default:
      break;
    }

  /*  Make the background (or first) layer  */
  new_layer = layer_new (gimage->ID, gimage->width, gimage->height,
			 gimage->type, layer_name, OPAQUE, NORMAL);
  /*  add the layer to the list  */
  gimage->layers = add_to_list (gimage->layers, new_layer);
  /*  configure the active pointers  */
  gimage->active_layer = new_layer->ID;

  gimage->active_channel = -1;  /* no default active channel */

  /*  set all color channels visible and active  */
  for (i = 0; i < MAX_CHANNELS; i++)
    {
      gimage->visible[i] = 1;
      gimage->active[i] = 1;
    }

  /* create the selection mask */
  gimage->region = gregion_new (width, height);

  layers_dialog_update_image_list ();
  return gimage;
}


GImage *
gimage_get_named (name)
     char *name;
{
  link_ptr tmp = image_list;
  GImage *gimage;
  char *str;

  while (tmp)
    {
      gimage = tmp->data;
      str = prune_filename (gimage->filename);
      if (strcmp (str, name) == 0)
	return gimage;
      
      tmp = next_item (tmp);
    }

  return NULL;
}


GImage *
gimage_get_ID (ID)
     int ID;
{
  link_ptr tmp = image_list;
  GImage *gimage;

  while (tmp)
    {
      gimage = (GImage *) tmp->data;
      if (gimage->ID == ID)
	return gimage;
      
      tmp = next_item (tmp);
    }

  return NULL;
}


int
gimage_get_shadow_ID (gimage)
     GImage * gimage;
{
  if ((gimage->shadow_shmaddr < (void*) 0) || (gimage->shadow_shmid == 0))
    gimage_allocate_shadow (gimage);

  return gimage->shadow_shmid;
}


unsigned char *
gimage_get_shadow_addr (gimage)
     GImage * gimage;
{
  if ((gimage->shadow_shmaddr < (void*) 0) || (gimage->shadow_shmid == 0))
    gimage_allocate_shadow (gimage);

  return gimage->shadow_buf;
}


void
gimage_free_shadow (gimage)
     GImage * gimage;
{
  /*  Free the shadow buffer from the specified gimage if it exists  */
  
  if (gimage->shadow_shmaddr >= (void*) 0)
    {
      shmdt (gimage->shadow_shmaddr);
      shmctl (gimage->shadow_shmid, IPC_RMID, 0);
    }

  gimage->shadow_shmaddr = (void *) -1;
  gimage->shadow_shmid = 0;
}


void
gimage_delete (gimage)
     GImage *gimage;
{
  gimage->ref_count--;

  if (!gimage->ref_count)
    {
      /*  remove this image from the global list  */
      image_list = remove_from_list (image_list, (void *) gimage);

      gimage_free_projection (gimage);
      gimage_free_shadow (gimage);

      if (gimage->cmap)
	xfree (gimage->cmap);

      xfree (gimage->filename);

      if (gimage->region)
	gregion_free (gimage->region);

      gimage_free_layers (gimage);
      gimage_free_channels (gimage);

      if (!gimage->busy)
	xfree (gimage);

      layers_dialog_update_image_list ();
    }
}


void
gimage_add_plug_in (gimage, data)
     GImage *gimage;
     void *data;
{
  gimage->plug_ins = append_to_list (gimage->plug_ins, data);
  
  if (!gimage->busy)
    gimage_plug_in_callback (gimage, 0);
}


void
gimage_plug_in_callback (client_data, call_data)
     void *client_data;
     void *call_data;
{
  GImage *gimage;
  link_ptr tmp_link;
  PlugInP plug_in;

  gimage = client_data;

  if (((int) call_data == 2) || (gimage->ref_count == 0))
    {
      tmp_link = gimage->plug_ins;
      while (tmp_link)
	{
	  free_plug_in (tmp_link->data);
	  tmp_link = tmp_link->next;
	}

      gimage->plug_ins = free_list (gimage->plug_ins);

      if (gimage->ref_count == 0)
	xfree (gimage);
    }

  if (gimage->plug_ins)
    {
      tmp_link = gimage->plug_ins;
      gimage->plug_ins = gimage->plug_ins->next;
      
      plug_in = tmp_link->data;
      tmp_link->next = NULL;
      free_list (tmp_link);

      plug_in_set_callback (plug_in, gimage_plug_in_callback, gimage);
      if (plug_in_open (plug_in))
	gimage->busy = 1;
      else
	gimage_plug_in_callback (client_data, call_data);
    }
  else
    gimage->busy = 0;
}


void
gimage_update (gimage, x, y, w, h)
     GImage * gimage;
     int x, y;
     int w, h;
{
  int offset_x, offset_y;

  gimage_offsets (gimage, &offset_x, &offset_y);
  x += offset_x;
  y += offset_y;
  gdisplays_update_area (gimage->ID, x, y, w, h);
}


void
gimage_fill (gimage, fill_type)
     GImage *gimage;
     int fill_type;
{
  Layer *layer;
  unsigned char *p;
  unsigned char c[MAX_CHANNELS];
  unsigned char r, g, b, a;
  int bpp;
  int j, pixels;

  /*  a gimage_fill affects the active layer, explicitly ignoring
   *  any possible floating selection.
   */
  if (! (layer = layer_get_ID (gimage->active_layer)))
    return;

  switch (fill_type)
    {
    case BACKGROUND_FILL:
      palette_get_background (&r, &g, &b);
      a = 255;
      break;
    case WHITE_FILL:
      a = r = g = b = 255;
      break;
    case TRANSPARENT_FILL:
      r = g = b = 255;
      a = 0;
      break;
    }

  switch (layer->type)
    {
    case RGB_GIMAGE: case RGBA_GIMAGE:
      c[RED_PIX] = r;
      c[GREEN_PIX] = g;
      c[BLUE_PIX] = b;
      if (layer->type == RGBA_GIMAGE)
	c[ALPHA_PIX] = a;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      c[GRAY_PIX] = r;
      if (layer->type == GRAYA_GIMAGE)
	c[ALPHA_G_PIX] = a;
      break;
    case INDEXED_GIMAGE:
      fatal_error ("we don't handle filling of indexed color images yet");
      break;
    }

  pixels = layer->width * layer->height;
  
  p = layer_data (layer);
  bpp = layer->bytes;
  while (pixels--)
    for (j = 0; j < bpp; j++)
      *p++ = c[j];
}


void
gimage_apply_image (gimage, src2PR, undo, opacity, mode)
     GImage * gimage;
     PixelRegion * src2PR;
     int undo;
     int opacity;
     int mode;
{
  Channel * channel;
  Layer * layer;
  MaskBuf * sel_mask;
  int x1, y1, x2, y2;
  int offset_x, offset_y;
  PixelRegion src1PR, maskPR;
  int operation;
  int i, floating;
  int active [MAX_CHANNELS];

  /*  get the selection mask if one exists  */
  sel_mask = (gregion_is_empty (gimage->region)) ?
    NULL : gimage->region->mask;

  /*  configure the active channel array  */
  for (i = 0; i < MAX_CHANNELS; i++)
    active[i] = gimage->active[i];

  floating = (gimage->floating_sel >= 0) ? 1 : 0;

  /*  determine what sort of operation is being attempted and
   *  if it's actually legal...
   */
  operation = valid_combinations [gimage_type (gimage)][src2PR->bytes];
  if (operation == -1)
    {
      warning ("gimage_apply_image sent illegal parameters\n");
      return;
    }

  /*  if there is a floating selection, preserve trans  */
  if (floating)
    active [gimage_bytes (gimage) - 1] = 0;

  gimage_offsets (gimage, &offset_x, &offset_y);

  /*  make sure the image application coordinates are within gimage bounds  */
  x1 = BOUNDS (src2PR->x, 0, gimage_width (gimage));
  y1 = BOUNDS (src2PR->y, 0, gimage_height (gimage));
  x2 = BOUNDS (src2PR->x + src2PR->w, 0, gimage_width (gimage));
  y2 = BOUNDS (src2PR->y + src2PR->h, 0, gimage_height (gimage));

  if (sel_mask)
    {
      /*  make sure coordinates are in mask bounds ...
       *  we need to add the layer offset to transform coords
       *  into the mask coordinate system
       */
      x1 = BOUNDS (x1, -offset_x, sel_mask->width - offset_x);
      y1 = BOUNDS (y1, -offset_y, sel_mask->height - offset_y);
      x2 = BOUNDS (x2, -offset_x, sel_mask->width - offset_x);
      y2 = BOUNDS (y2, -offset_y, sel_mask->height - offset_y);
    }

  /*  If the calling procedure specified an undo step...  */
  if (undo)
    {
      if (gimage->active_channel >= 0)
	{
	  if ((channel = channel_get_ID (gimage->active_channel)))
	    channel_apply_image (channel, x1, y1, x2, y2, NULL);
	}
      else if (gimage->floating_sel >= 0)
	{
	  /*  do not anchor layer  (since it's floating) */
	  if ((layer = layer_get_ID (gimage->floating_sel)))
	    layer_apply_image (layer, x1, y1, x2, y2, NULL, 0);
	}
      else
	{
	  /*  anchor layer  */
	  if ((layer = layer_get_ID (gimage->active_layer)))
	    layer_apply_image (layer, x1, y1, x2, y2, NULL, 1);
	}
    }

  /* configure the pixel regions  */
  src1PR.bytes = gimage_bytes (gimage);
  src1PR.w = (x2 - x1);
  src1PR.h = (y2 - y1);
  src1PR.rowstride = gimage_width (gimage) * src1PR.bytes;
  src1PR.data = gimage_data (gimage) + y1 * src1PR.rowstride + x1 * src1PR.bytes;
  
  src2PR->data += (y1 - src2PR->y) * src2PR->rowstride + 
    (x1 - src2PR->x) * src2PR->bytes;

  if (sel_mask)
    {
      int mx, my;

      /*  configure the mask pixel region
       *  don't use x1 and y1 because they are in layer
       *  coordinate system.  Need mask coordinate system
       */
      mx = x1 + offset_x;
      my = y1 + offset_y;

      maskPR.bytes = sel_mask->bytes;
      maskPR.rowstride = sel_mask->width;
      maskPR.data = mask_buf_data (sel_mask) + my * maskPR.rowstride + 
	mx * maskPR.bytes;
      combine_regions (&src1PR, src2PR, &src1PR, &maskPR, NULL,
		       opacity, mode, active, operation);
    }
  else
    combine_regions (&src1PR, src2PR, &src1PR, NULL, NULL,
		     opacity, mode, active, operation);
}


void
gimage_get_foreground (gimage, fg)
     GImage * gimage;
     unsigned char * fg;
{
  unsigned char pfg[3];

  /*  Get the palette color  */
  palette_get_foreground (&pfg[0], &pfg[1], &pfg[2]);

  gimage_transform_color (gimage, pfg, fg, RGB);
}


void
gimage_get_background (gimage, bg)
     GImage * gimage;
     unsigned char * bg;
{
  unsigned char pbg[3];

  /*  Get the palette color  */
  palette_get_background (&pbg[0], &pbg[1], &pbg[2]);

  gimage_transform_color (gimage, pbg, bg, RGB);
}


void
gimage_get_color (gimage, rgb, src)
     GImage * gimage;
     unsigned char * rgb;
     unsigned char * src;
{
  switch (gimage_type (gimage))
    {
    case RGB_GIMAGE: case RGBA_GIMAGE:
      /*  Straight copy  */
      *rgb++ = *src++;
      *rgb++ = *src++;
      *rgb   = *src;
      break;
    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      *rgb++ = *src;
      *rgb++ = *src;
      *rgb   = *src;
      break;
    case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
      {
	int index = *src * 3;
	*rgb++ = gimage->cmap [index++];
	*rgb++ = gimage->cmap [index++];
	*rgb   = gimage->cmap [index++];
      }
      break;
    }
}


void
gimage_transform_color (gimage, src, dest, type)
     GImage * gimage;
     unsigned char * src, * dest;
     int type;
{
#define INTENSITY(r,g,b) (r * 0.30 + g * 0.59 + b * 0.11)

  switch (type)
    {
    case RGB:
      switch (gimage->type)
	{
	case RGB_GIMAGE: case RGBA_GIMAGE:
	  /*  Straight copy  */
	  *dest++ = *src++;
	  *dest++ = *src++;
	  *dest++ = *src++;
	  break;
	case GRAY_GIMAGE: case GRAYA_GIMAGE:
	  /*  NTSC conversion  */
	  *dest = INTENSITY (src[RED_PIX],
			     src[GREEN_PIX],
			     src[BLUE_PIX]);
	  break;
	case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
	  /*  Least squares method  */
	  *dest = map_rgb_to_indexed (gimage->cmap,
				      gimage->num_cols,
				      src[RED_PIX],
				      src[GREEN_PIX],
				      src[BLUE_PIX]);
	  break;
	}
      break;
    case GRAY:
      switch (gimage->type)
	{
	case RGB_GIMAGE: case RGBA_GIMAGE:
	  /*  Gray to RG&B */
	  *dest++ = *src;
	  *dest++ = *src;
	  *dest++ = *src;
	  break;
	case GRAY_GIMAGE: case GRAYA_GIMAGE:
	  /*  Straight copy  */
	  *dest = *src;
	  break;
	case INDEXED_GIMAGE: case INDEXEDA_GIMAGE:
	  /*  Least squares method  */
	  *dest = map_rgb_to_indexed (gimage->cmap,
				      gimage->num_cols,
				      src[GRAY_PIX],
				      src[GRAY_PIX],
				      src[GRAY_PIX]);
	  break;
	}
      break;
      break;
    default:
      break;
    }
}


/************************************************************/
/*  Projection functions                                    */
/************************************************************/

static void
project_intensity (gimage, layer, src, dest, mask)
     GImage * gimage;
     Layer * layer;
     PixelRegion *src, *dest, *mask;
{
  if (! gimage->construct_flag)
    initial_region (src, dest, mask, NULL, layer->opacity,
		    layer->mode, gimage->visible, INITIAL_INTENSITY);
  else
    combine_regions (dest, src, dest, mask, NULL, layer->opacity,
		     layer->mode, gimage->visible, COMBINE_INTEN_A_INTEN);
}


static void
project_intensity_alpha (gimage, layer, src, dest, mask)
     GImage * gimage; 
     Layer * layer;
     PixelRegion *src, *dest, *mask;
{
  if (! gimage->construct_flag)
    initial_region (src, dest, mask, NULL, layer->opacity,
		    layer->mode, gimage->visible, INITIAL_INTENSITY_ALPHA);
  else
    combine_regions (dest, src, dest, mask, NULL, layer->opacity,
		     layer->mode, gimage->visible, COMBINE_INTEN_A_INTEN_A);
}


static void
project_indexed (gimage, layer, src, dest)
     GImage * gimage;
     Layer * layer;
     PixelRegion *src, *dest;
{
  if (! gimage->construct_flag)
    initial_region (src, dest, NULL, gimage->cmap, layer->opacity,
		    layer->mode, gimage->visible, INITIAL_INDEXED);
  else
    warning ("Unable to project indexed image.");
}


static void
project_indexed_alpha (gimage, layer, src, dest, mask)
     GImage * gimage; 
     Layer * layer;
     PixelRegion *src, *dest, *mask;
{
  if (! gimage->construct_flag)
    warning ("Invalid construction: indexed-alpha cannot be base layer");
  else
    combine_regions (dest, src, dest, mask, gimage->cmap, layer->opacity,
		     layer->mode, gimage->visible, COMBINE_INTEN_A_INDEXED_A);
}


static void
project_channel (gimage, channel, src, dest)
     GImage * gimage;
     Channel * channel;
     PixelRegion *src, *dest;
{
  if (! gimage->construct_flag)
    initial_region (src, dest, NULL, channel->col, channel->opacity,
		    NORMAL, NULL, INITIAL_CHANNEL);
  else
    combine_regions (dest, src, dest, NULL, channel->col, channel->opacity,
		     NORMAL, NULL, COMBINE_INTEN_A_CHANNEL);
}


/************************************************************/
/*  Layer/Channel functions                                 */
/************************************************************/


static void
gimage_free_layers (gimage)
     GImage * gimage;
{
  link_ptr list = gimage->layers;
  Layer * layer;

  while (list)
    {
      layer = (Layer *) list->data;
      layer_delete (layer);
      list = next_item (list);
    }
  free_list (gimage->layers);
}


static void
gimage_free_channels (gimage)
     GImage * gimage;
{ 
  link_ptr list = gimage->channels;
  Channel * channel;

  while (list)
    {
      channel = (Channel *) list->data;
      channel_delete (channel);
      list = next_item (list);
    }
  free_list (gimage->channels);
}


static void
gimage_construct_layers (gimage, layers, x, y, w, h)
     GImage * gimage;
     link_ptr layers;
     int x, y;
     int w, h;
{
  Layer * layer;
  int x1, y1, x2, y2;
  PixelRegion src1PR, src2PR, maskPR;
  PixelRegion * mask;
  MaskBuf * maskbuf;

  if (layers)
    gimage_construct_layers (gimage, next_item (layers), x, y, w, h);
  else
    return;

  layer = (Layer *) layers->data;

  if (layer->visible)
    {
      x1 = BOUNDS (layer->offset_x, x, x + w);
      y1 = BOUNDS (layer->offset_y, y, y + h);
      x2 = BOUNDS (layer->offset_x + layer->width, x, x + w);
      y2 = BOUNDS (layer->offset_y + layer->height, y, y + h);
      
      /* configure the pixel regions  */
      src1PR.bytes = gimage->proj_bytes;
      src1PR.w = (x2 - x1);
      src1PR.h = (y2 - y1);
      src1PR.rowstride = gimage->width * src1PR.bytes;
      src1PR.data = gimage->projection + y1 * src1PR.rowstride + x1 * src1PR.bytes;
      
      src2PR.bytes = layer->bytes;
      src2PR.w = src1PR.w;  src2PR.h = src1PR.h;
      src2PR.rowstride = layer->width * layer->bytes;
      src2PR.data = layer->data + (y1 - layer->offset_y) * src2PR.rowstride +
	(x1 - layer->offset_x) * src2PR.bytes;
      
      /*  If the layer is a floating selection, then the
       *  the gimage mask can act as the layer mask
       */
      if (layer->ID == gimage->floating_sel)
	{
	  if (! gregion_is_empty (gimage->region))
	    {
	      maskbuf = gimage_mask (gimage);

	      /*  configure the mask pixel region  */
	      maskPR.bytes = maskbuf->bytes;
	      maskPR.rowstride = maskbuf->width;
	      maskPR.data = mask_buf_data (maskbuf) + y1 * maskPR.rowstride + 
		x1 * maskPR.bytes;
	      mask = &maskPR;
	    }
	  else
	    mask = NULL;
	}
      else if (layer->mask)
	{
	  maskPR.bytes = layer->mask->bytes;
	  maskPR.rowstride = layer->mask->width;
	  maskPR.data = layer->mask->data + 
	    (y1 - layer->offset_y) * maskPR.rowstride +
	    (x1 - layer->offset_x) * maskPR.bytes;
	  mask = &maskPR;
	}
      else
	mask = NULL;

      /*  Based on the type of the layer, project the layer onto the
       *  projection image...
       */
      switch (layer->type)
	{
	case RGB_GIMAGE: case GRAY_GIMAGE:
	  /* no mask possible */
	  project_intensity (gimage, layer, &src2PR, &src1PR, mask);
	  break;
	case RGBA_GIMAGE: case GRAYA_GIMAGE:
	  project_intensity_alpha (gimage, layer, &src2PR, &src1PR, mask);
	  break;
	case INDEXED_GIMAGE: 
	  /* no mask possible */
	  project_indexed (gimage, layer, &src2PR, &src1PR);
	  break;
	case INDEXEDA_GIMAGE:
	  project_indexed_alpha (gimage, layer, &src2PR, &src1PR, mask);
	  break;
	default:
	  break;
	}

      gimage->construct_flag = 1;  /*  something was projected  */
    }
}


static void
gimage_construct_channels (gimage, channels, x, y, w, h)
     GImage * gimage;
     link_ptr channels;
     int x, y;
     int w, h;
{
  Channel * channel;
  PixelRegion src1PR, src2PR;
  
  if (channels)
    gimage_construct_channels (gimage, next_item (channels), x, y, w, h);
  else
    return;

  channel = (Channel *) channels->data;

  if (channel->visible)
    {
      /* configure the pixel regions  */
      src1PR.bytes = gimage->proj_bytes;
      src1PR.w = w;
      src1PR.h = h;
      src1PR.rowstride = gimage->width * src1PR.bytes;
      src1PR.data = gimage->projection + y * src1PR.rowstride +
	x * src1PR.bytes;

      src2PR.bytes = channel->bytes;
      src2PR.rowstride = channel->width * channel->bytes;
      src2PR.data = channel->data + y * src2PR.rowstride + 
	x * src2PR.bytes;

      project_channel (gimage, channel, &src1PR, &src2PR);

      gimage->construct_flag = 1;
    }
}


static void
gimage_initialize_projection (gimage, x, y, w, h)
     GImage * gimage;
     int x, y;
     int w, h;
{
  link_ptr list;
  Layer *layer;
  int coverage = 0;
  int rowstride;
  int length;
  unsigned char *data;

  /*  this function determines whether a visible layer
   *  provides complete coverage over the image.  If not,
   *  the projection is initialized to transparent
   */
  list = gimage->layers;
  while (list)
    {
      layer = (Layer *) list->data;
      if (layer->visible &&
	  (layer->width == gimage->width) &&
	  (layer->height == gimage->height) &&
	  (layer->offset_x == 0) &&
	  (layer->offset_y == 0))
	coverage = 1;

      list = next_item (list);
    }

  if (!coverage)
    {
      rowstride = gimage->width * gimage->proj_bytes;
      length = w * gimage->proj_bytes;
      data = gimage->projection + y * rowstride + x * gimage->proj_bytes;
      while (h--)
	{
	  memset (data, 0, length);
	  data += rowstride;
	}
    }
}


void
gimage_inflate (gimage)
     GImage * gimage;
{
  /*  Make sure the projection image is allocated  */
  gimage_allocate_projection (gimage);

  /*  signal that the entire gimage must be projected  */
  gimage->construct_flag = -1;

  gimage->flat = 0;
}


void
gimage_construct (gimage, x, y, w, h)
     GImage * gimage;
     int x, y;
     int w, h;
{
  /*  if the gimage is flat, construction is unnecessary.
   *  Otherwise, we need to make sure there is a projection
   *  image, and then construct into that...
   */
  if (gimage->flat)
    {
      return;
    }
  else
    {
      /*  If the construct flag is set to -1, the entire gimage
       *  needs to be constructed (projected)
       */
      if (gimage->construct_flag == -1)
	{
	  x = y = 0;
	  w = gimage->width;
	  h = gimage->height;
	}

      /*  set the construct flag, used to determine if anything
       *  has been written to the gimage raw image yet.
       */
      gimage->construct_flag = 0;

      /*  First, determine if the projection image needs to be
       *  initialized--this is the case when there are no visible
       *  layers that cover the entire canvas--either because layers
       *  are offset or only a floating selection is visible
       */
      gimage_initialize_projection (gimage, x, y, w, h);

      /*  call recursive functions which process the list of layers
       *  and then the list of extra channels...
       */
      gimage_construct_layers (gimage, gimage->layers, x, y, w, h);
      gimage_construct_channels (gimage, gimage->channels, x, y, w, h);
    }
}


Layer *
gimage_add_layer (gimage, name, type, opacity, mode)
     GImage * gimage;
     char * name;
     int type;
     int opacity;
     int mode;
{
  Layer * new_layer;

  /*  if the gimage is "flat", we need to inflate it,
   *  and then create the requested layer
   */
  if (gimage->flat)
    gimage_inflate (gimage);

  new_layer = layer_new (gimage->ID, gimage->width, gimage->height,
			 type, name, opacity, mode);

  /*  add the layer to the list  */
  gimage->layers = add_to_list (gimage->layers, new_layer);
  /*  configure the active pointers  */
  gimage->active_layer = new_layer->ID;

  /*  add the layer to the layer dialog, if applicable  */
  layers_dialog_add_layer (gimage->ID, new_layer->ID);

  return new_layer;
}


Layer *
gimage_add_floating_layer (gimage, float_layer)
     GImage *gimage;
     Layer *float_layer;
{
  if (gimage->flat)
    gimage_inflate (gimage);
  /*  add the new floating selection  */
  gimage->floating_sel = float_layer->ID;
  /*  let the layer know about the gimage  */
  float_layer->gimage_ID = gimage->ID;
  /*  add the layer to the list  */
  gimage->layers = add_to_list (gimage->layers, float_layer);
      
  /*  add the layer to the layer dialog, if applicable  */
  layers_dialog_add_layer (gimage->ID, float_layer->ID);

  return float_layer;
}


Layer *
gimage_remove_layer (gimage, layer_id)
     GImage * gimage;
     int layer_id;
{
  Layer * layer;

  layer = layer_get_ID (layer_id);

  if (layer)
    {
      if (gimage->active_layer == layer_id)
	gimage->active_layer = -1;
      if (gimage->floating_sel == layer_id)
	gimage->floating_sel = -1;
      
      gimage->layers = remove_from_list (gimage->layers, layer);
      
      /*  remove the layer from the layer dialog, if applicable  */
      layers_dialog_remove_layer (layer->layer_widget);

      return layer;
    }
  else
    return NULL;
}


Channel *
gimage_add_channel (gimage, name, opacity, col)
     GImage * gimage;
     char * name;
     int opacity;
     unsigned char * col;
{
  Channel * new_channel;

  /*  if the gimage is "flat", we need to inflate it,
   *  and then create the requested channel
   */
  if (gimage->flat)
    gimage_inflate (gimage);

  new_channel = channel_new (gimage->ID, gimage->width, gimage->height,
			     name, opacity, col);

  /*  add the channel to the list  */
  gimage->channels = add_to_list (gimage->channels, new_channel);

  return new_channel;
}


Channel *
gimage_remove_channel (gimage, channel_id)
     GImage * gimage;
     int channel_id;
{
  Channel * channel;

  channel = channel_get_ID (channel_id);

  if (channel)
    {
      if (gimage->active_channel == channel_id)
	gimage->active_channel = -1;
      
      gimage->channels = remove_from_list (gimage->channels, channel);

      return channel;
    }
  else
    return NULL;
}


/************************************************************/
/*  Access functions                                        */
/************************************************************/


int
gimage_type (gimage)
     GImage * gimage;
{
  Layer * layer;

  /*  If there is an active channel (a layer mask or
   *  a saved selection, etc.)  We ignore the active layer
   */
  if (gimage->active_channel >= 0)
    return GRAY_GIMAGE;
  else if (gimage->floating_sel >= 0)
    {
      if ((layer = layer_get_ID (gimage->floating_sel)))
	return layer->type;
    }
  else
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	return layer->type;
    }

  return -1;
}


int
gimage_has_alpha (gimage)
     GImage * gimage;
{
  if (gimage_type (gimage) == RGBA_GIMAGE ||
      gimage_type (gimage) == GRAYA_GIMAGE ||
      gimage_type (gimage) == INDEXEDA_GIMAGE)
    return 1;
  else
    return 0;
}


int
gimage_type_with_alpha (gimage)
     GImage * gimage;
{
  int type = gimage_type (gimage);
  int has_alpha = gimage_has_alpha (gimage);

  if (has_alpha)
    return type;
  else
    switch (type)
      {
      case RGB_GIMAGE:
	return RGBA_GIMAGE; break;
      case GRAY_GIMAGE:
	return GRAYA_GIMAGE; break;
      case INDEXED_GIMAGE:
	return INDEXEDA_GIMAGE; break;
      }
  return 0;
}


int
gimage_color (gimage)
     GImage * gimage;
{
  if (gimage_type (gimage) == RGBA_GIMAGE ||
      gimage_type (gimage) == RGB_GIMAGE)
    return 1;
  else
    return 0;
}


int
gimage_gray (gimage)
     GImage * gimage;
{
  if (gimage_type (gimage) == GRAYA_GIMAGE ||
      gimage_type (gimage) == GRAY_GIMAGE)
    return 1;
  else
    return 0;
}


int
gimage_indexed (gimage)
     GImage * gimage;
{
  if (gimage_type (gimage) == INDEXEDA_GIMAGE ||
      gimage_type (gimage) == INDEXED_GIMAGE)
    return 1;
  else
    return 0;
}


unsigned char *
gimage_data (gimage)
     GImage * gimage;
{
  Channel * channel;
  Layer * layer;

  /*  return the active channel or layer data  */
  if (gimage->active_channel >= 0)
    {
      if ((channel = channel_get_ID (gimage->active_channel)))
	return channel->data;
    }
  else if (gimage->floating_sel >= 0)
    {
      if ((layer = layer_get_ID (gimage->floating_sel)))
	return layer->data;
    }
  else
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	return layer->data;
    }

  return NULL;
}


int
gimage_bytes (gimage)
     GImage * gimage;
{
  Channel * channel;
  Layer * layer;

  /*  return the active channel or layer bytes  */
  if (gimage->active_channel >= 0)
    {
      if ((channel = channel_get_ID (gimage->active_channel)))
	return channel->bytes;
    }
  else if (gimage->floating_sel >= 0)
    {
      if ((layer = layer_get_ID (gimage->floating_sel)))
	return layer->bytes;
    }
  else
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	return layer->bytes;
    }

  return -1;
}


int
gimage_width (gimage)
     GImage * gimage;
{
  Channel * channel;
  Layer * layer;

  /*  return the active channel or layer width  */
  if (gimage->active_channel >= 0)
    {
      if ((channel = channel_get_ID (gimage->active_channel)))
	return channel->width;
    }
  else if (gimage->floating_sel >= 0)
    {
      if ((layer = layer_get_ID (gimage->floating_sel)))
	return layer->width;
    }
  else
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	return layer->width;
    }

  return -1;
}


int
gimage_height (gimage)
     GImage * gimage;
{
  Channel * channel;
  Layer * layer;

  /*  return the active channel or layer height  */
  if (gimage->active_channel >= 0)
    {
      if ((channel = channel_get_ID (gimage->active_channel)))
	return channel->height;
    }
  else if (gimage->floating_sel >= 0)
    {
      if ((layer = layer_get_ID (gimage->floating_sel)))
	return layer->height;
    }
  else
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	return layer->height;
    }

  return -1;
}


void
gimage_offsets (gimage, off_x, off_y)
     GImage * gimage;
     int * off_x, * off_y;
{
  Layer * layer;

  /*  return the active layer offsets  */
  if (gimage->floating_sel >= 0)
    {
      if ((layer = layer_get_ID (gimage->floating_sel)))
	{
	  *off_x = layer->offset_x;
	  *off_y = layer->offset_y;
	  return;
	}
    }
  else
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	{
	  *off_x = layer->offset_x;
	  *off_y = layer->offset_y;
	  return;
	}
    }

  *off_x = 0;
  *off_y = 0;
}


int
gimage_shm_ID (gimage)
     GImage * gimage;
{
  Channel * channel;
  Layer * layer;

  /* return the active channel or layer id */
  if (gimage->active_channel >= 0)
    {
      if ((channel = channel_get_ID (gimage->active_channel)))
	return channel->shmid;
    }
  else if (gimage->floating_sel >= 0)
    {
      if ((layer = layer_get_ID (gimage->floating_sel)))
	return layer->shmid;
    }
  else
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	return layer->shmid;
    }

  return -1;
}


unsigned char *
gimage_cmap (gimage)
     GImage * gimage;
{
  return gimage->cmap;
}



/************************************************************/
/*  Projection access functions                             */
/************************************************************/


unsigned char *
gimage_projection (gimage)
     GImage * gimage;
{
  Layer * layer;

  /*  If the gimage is flat, we simply want the data of the
   *  first layer...Otherwise, we'll pass back the projection
   */
  if (gimage->flat)
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	return layer_data (layer);
      else
	return NULL;
    }
  else
    return gimage->projection;
}

int
gimage_projection_type (gimage)
     GImage * gimage;
{
  Layer * layer;

  /*  If the gimage is flat, we simply want the type of the
   *  first layer...Otherwise, we'll pass back the proj_type
   */
  if (gimage->flat)
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	return layer->type;
      else
	return -1;
    }
  else
    return gimage->proj_type;
}

int
gimage_projection_bytes (gimage)
     GImage * gimage;
{
  Layer * layer;

  /*  If the gimage is flat, we simply want the bytes of the
   *  first layer...Otherwise, we'll pass back the proj_bytes
   */
  if (gimage->flat)
    {
      if ((layer = layer_get_ID (gimage->active_layer)))
	return layer->bytes;
      else
	return 0;
    }
  else
    return gimage->proj_bytes;
}
