/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#ifndef  WAIT_ANY
#define  WAIT_ANY -1
#endif   /*  WAIT_ANY  */

#include "appenv.h"
#include "app_procs.h"
#include "brushes.h"
#include "commands.h"
#include "fileops.h"
#include "interface.h"
#include "gimage.h"
#include "gimprc.h"
#include "gdisplay.h"
#include "cursorutil.h"
#include "palette.h"
#include "patterns.h"
#include "plug_in.h"
#include "errors.h"

static void on_signal (int);
static void on_sig_child (int);

/* GLOBAL data */
char *prog_name;		/* The path name we are invoked with */

void
main (int argc, char **argv)
{
  /* Initialize variables */
  prog_name = argv[0];

  /* Initialize G toolkit */
  gdk_set_debug_level (0);
  gdk_set_show_events (0);
  gtk_init (&argc, &argv);

  /* Handle some signals */
  signal (SIGHUP, on_signal);
  signal (SIGINT, on_signal);
  signal (SIGQUIT, on_signal);
  signal (SIGABRT, on_signal);
  signal (SIGBUS, on_signal);
  signal (SIGSEGV, on_signal);
  signal (SIGPIPE, on_signal);
  signal (SIGTERM, on_signal);
  signal (SIGFPE, on_signal);

  /* Handle child exits */
  signal (SIGCHLD, on_sig_child);

  parse_gimprc ();  /*  parse the local GIMP configuration file  */
  brushes_init ();  /*  initialize the list of gimp brushes  */
  patterns_init (); /*  initialize the list of gimp patterns  */
  palettes_init (); /*  initialize the list of gimp palettes  */
  plug_in_init ();  /*  initialize the plug in structures  */
  file_io_init ();  /*  initialize the file types  */

  /* Pre-init the application */
  app_pre_init ();
  /* Create the toolbox */
  create_toolbox ();
  /* Initialize the application */
  app_init ();

  argc--; argv++;
  if (argc > 0)
    while (argc--)
      file_open (*argv++, NULL);

  /* Main application loop */
  gtk_main ();
}

static int caught_fatal_sig = 0;

static void
on_signal (sig_num)
     int sig_num;
{
  char *sig;
  
  if (caught_fatal_sig)
/*    raise (sig_num);*/
    kill (getpid (), sig_num);
  caught_fatal_sig = 1;

  switch (sig_num)
    {
    case SIGHUP:
      sig = "sighup";
      break;
    case SIGINT:
      sig = "sigint";
      break;
    case SIGQUIT:
      sig = "sigquit";
      break;
    case SIGABRT:
      sig = "sigabrt";
      break;
    case SIGBUS:
      sig = "sigbus";
      break;
    case SIGSEGV:
      sig = "sigsegv";
      break;
    case SIGPIPE:
      sig = "sigpipe";
      break;
    case SIGTERM:
      sig = "sigterm";
      break;
    case SIGFPE:
      sig = "sigfpe";
      break;
    default:
      sig = "unknown signal";
      break;
    }

  fatal_error ("%s caught", sig);
}

static void
on_sig_child (sig_num)
     int sig_num;
{
  int pid;
  int status;
  
  while (1)
    {
      pid = waitpid (WAIT_ANY, &status, WNOHANG);
      if (pid <= 0)
	break;
    }
}

