/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include "appenv.h"
#include "gdisplay.h"
#include "gimage_mask.h"
#include "info_dialog.h"
#include "scale_tool.h"
#include "selection.h"
#include "tools.h"
#include "transform_core.h"

#define X1 0
#define Y1 1
#define X2 2
#define Y2 3 

/*  storage for information dialog fields  */
char          orig_width_buf  [MAX_INFO_BUF];
char          orig_height_buf [MAX_INFO_BUF];
char          width_buf       [MAX_INFO_BUF];
char          height_buf      [MAX_INFO_BUF];
char          x_ratio_buf     [MAX_INFO_BUF];
char          y_ratio_buf     [MAX_INFO_BUF];

/*  forward function declarations  */
static void *      scale_tool_scale       (Tool *, void *);
static void *      scale_tool_recalc      (Tool *, void *);
static void        scale_tool_motion      (Tool *, void *);
static void        scale_info_update      (Tool *);


void *
scale_tool_transform (tool, gdisp_ptr, state)
     Tool * tool;
     gpointer gdisp_ptr;
     int state;
{
  GDisplay * gdisp;
  TransformCore * transform_core;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;

  switch (state)
    {
    case INIT :
      if (!transform_info)
	{
	  transform_info = info_dialog_new ("Scaling Information");
	  info_dialog_add_field (transform_info, "Original Width: ", orig_width_buf);
	  info_dialog_add_field (transform_info, "Original Height: ", orig_height_buf);
	  info_dialog_add_field (transform_info, "Current Width: ", width_buf);
	  info_dialog_add_field (transform_info, "Current Height: ", height_buf);
	  info_dialog_add_field (transform_info, "X Scale Ratio: ", x_ratio_buf);
	  info_dialog_add_field (transform_info, "Y Scale Ratio: ", y_ratio_buf);
	}

      transform_core->trans_info [X1] = (double) transform_core->x1;
      transform_core->trans_info [Y1] = (double) transform_core->y1;
      transform_core->trans_info [X2] = (double) transform_core->x2;
      transform_core->trans_info [Y2] = (double) transform_core->y2;

      return NULL;
      break;

    case MOTION :
      scale_tool_motion (tool, gdisp_ptr);

      return (scale_tool_recalc (tool, gdisp_ptr));
      break;

    case RECALC :
      return (scale_tool_recalc (tool, gdisp_ptr));
      break;

    case FINISH :
      /*  If we're in indexed color, let the transform core take care of scaling
       *  since we can't assume that colors won't change otherwise...
       */
      if (gdisp->gimage->type == INDEXED_GIMAGE)
	return NULL;
      else
	return (scale_tool_scale (tool, gdisp_ptr));
      break;
    }

  return NULL;
}


Tool *
tools_new_scale_tool ()
{
  Tool * tool;
  TransformCore * private;

  tool = transform_core_new (SCALE, INTERACTIVE);

  private = tool->private;

  /*  set the rotation specific transformation attributes  */
  private->trans_func = scale_tool_transform;
  private->trans_info[X1] = 0;
  private->trans_info[Y1] = 0;
  private->trans_info[X2] = 0;
  private->trans_info[Y2] = 0;

  /*  assemble the transformation matrix  */
  identity_matrix (private->transform);

  return tool;
}


void
tools_free_scale_tool (tool)
     Tool * tool;
{
  transform_core_free (tool);
}


static void
scale_info_update (tool)
     Tool * tool;
{
  GDisplay * gdisp;
  TransformCore * transform_core;
  double ratio_x, ratio_y;
  int x1, y1, x2, y2, x3, y3, x4, y4;

  gdisp = (GDisplay *) tool->gdisp_ptr;
  transform_core = (TransformCore *) tool->private;

  /*  Find original sizes  */
  x1 = transform_core->x1;
  y1 = transform_core->y1;
  x2 = transform_core->x2;
  y2 = transform_core->y2;
  sprintf (orig_width_buf, "%d", x2 - x1);
  sprintf (orig_height_buf, "%d", y2 - y1);

  /*  Find current sizes  */
  x3 = (int) transform_core->trans_info [X1];
  y3 = (int) transform_core->trans_info [Y1];
  x4 = (int) transform_core->trans_info [X2];
  y4 = (int) transform_core->trans_info [Y2];

  sprintf (width_buf, "%d", x4 - x3);
  sprintf (height_buf, "%d", y4 - y3);

  ratio_x = ratio_y = 0.0;

  if (x2 - x1)
    ratio_x = (double) (x4 - x3) / (double) (x2 - x1);
  if (y2 - y1)
    ratio_y = (double) (y4 - y3) / (double) (y2 - y1);

  sprintf (x_ratio_buf, "%0.2f", ratio_x);
  sprintf (y_ratio_buf, "%0.2f", ratio_y);

  info_dialog_update (transform_info);
  info_dialog_popup (transform_info);
}

void
scale_tool_motion (tool, gdisp_ptr)
     Tool * tool;
     void * gdisp_ptr;
{
  GDisplay * gdisp;
  TransformCore * transform_core;
  double ratio;
  double *x1, *y1;
  double *x2, *y2;
  int w, h;
  int dir_x, dir_y;
  int diff_x, diff_y;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;

  diff_x = transform_core->curx - transform_core->lastx;
  diff_y = transform_core->cury - transform_core->lasty;

  switch (transform_core->function)
    {
    case HANDLE_1 :
      x1 = &transform_core->trans_info [X1];
      y1 = &transform_core->trans_info [Y1];
      x2 = &transform_core->trans_info [X2];
      y2 = &transform_core->trans_info [Y2];
      dir_x = dir_y = 1;
      break;
    case HANDLE_2 :
      x1 = &transform_core->trans_info [X2];
      y1 = &transform_core->trans_info [Y1];
      x2 = &transform_core->trans_info [X1];
      y2 = &transform_core->trans_info [Y2];
      dir_x = -1;
      dir_y = 1;
      break;
    case HANDLE_3 :
      x1 = &transform_core->trans_info [X1];
      y1 = &transform_core->trans_info [Y2];
      x2 = &transform_core->trans_info [X2];
      y2 = &transform_core->trans_info [Y1];
      dir_x = 1;
      dir_y = -1;
      break;
    case HANDLE_4 :
      x1 = &transform_core->trans_info [X2];
      y1 = &transform_core->trans_info [Y2];
      x2 = &transform_core->trans_info [X1];
      y2 = &transform_core->trans_info [Y1];
      dir_x = dir_y = -1;
      break;
    default :
      return;
    }

  /*  if just the control key is down, affect only the height  */
  if (transform_core->state & ControlMask && ! (transform_core->state & ShiftMask))
    diff_x = 0;
  /*  if just the shift key is down, affect only the width  */
  else if (transform_core->state & ShiftMask && ! (transform_core->state & ControlMask))
    diff_y = 0;

  *x1 += diff_x;
  *y1 += diff_y;

  if (dir_x > 0)
    {
      if (*x1 >= *x2) *x1 = *x2 - 1;
    }
  else
    {
      if (*x1 <= *x2) *x1 = *x2 + 1;
    }

  if (dir_y > 0)
    {
      if (*y1 >= *y2) *y1 = *y2 - 1;
    }
  else
    {
      if (*y1 <= *y2) *y1 = *y2 + 1;
    }

  /*  if both the control key & shift keys are down, keep the aspect ratio intac
t  */
  if (transform_core->state & GDK_CONTROL_MASK && transform_core->state & GDK_SHIFT_MASK)
    {
      ratio = (double) (transform_core->x2 - transform_core->x1) / 
        (double) (transform_core->y2 - transform_core->y1);

      w = ABS ((*x2 - *x1));
      h = ABS ((*y2 - *y1));

      if (w > h * ratio)
        h = w / ratio;
      else
        w = h * ratio;

      *y1 = *y2 - dir_y * h;
      *x1 = *x2 - dir_x * w;
    }
}

static void *
scale_tool_recalc (tool, gdisp_ptr)
     Tool * tool;
     void * gdisp_ptr;
{
  TransformCore * transform_core;
  GDisplay * gdisp;
  int x1, y1, x2, y2;
  int diffx, diffy;
  int cx, cy;
  double scalex, scaley;
  
  gdisp = (GDisplay *) tool->gdisp_ptr;
  transform_core = (TransformCore *) tool->private;
  x1 = (int) transform_core->trans_info [X1];
  y1 = (int) transform_core->trans_info [Y1];
  x2 = (int) transform_core->trans_info [X2];
  y2 = (int) transform_core->trans_info [Y2];
  
  scalex = scaley = 1.0;
  if (transform_core->x2 - transform_core->x1)
    scalex = (double) (x2 - x1) / (double) (transform_core->x2 - transform_core->x1);
  if (transform_core->y2 - transform_core->y1)
    scaley = (double) (y2 - y1) / (double) (transform_core->y2 - transform_core->y1);
  
  switch (transform_core->function)
    {
    case HANDLE_1 :
      cx = x2;  cy = y2;
      diffx = x2 - transform_core->x2;
      diffy = y2 - transform_core->y2;
      break;
    case HANDLE_2 :
      cx = x1;  cy = y2;
      diffx = x1 - transform_core->x1;
      diffy = y2 - transform_core->y2;
      break;
    case HANDLE_3 :
      cx = x2;  cy = y1;
      diffx = x2 - transform_core->x2;
      diffy = y1 - transform_core->y1;
      break;
    case HANDLE_4 :
      cx = x1;  cy = y1;
      diffx = x1 - transform_core->x1;
      diffy = y1 - transform_core->y1;
      break;
    default :
      cx = x1; cy = y1;
      diffx = diffy = 0;
      break;
    }
  
  /*  assemble the transformation matrix  */
  identity_matrix  (transform_core->transform);
  translate_matrix (transform_core->transform, (double) -cx + diffx, (double) -cy + diffy);
  scale_matrix     (transform_core->transform, scalex, scaley);
  translate_matrix (transform_core->transform, (double) cx, (double) cy);
  
  /*  transform the bounding box  */
  transform_bounding_box (tool);
  
  /*  update the information dialog  */
  scale_info_update (tool);
  
  return (void *) 1;
}

static void *
scale_tool_scale (tool, gdisp_ptr)
     Tool * tool;
     void * gdisp_ptr;
{
  GDisplay * gdisp;
  TransformCore * transform_core;
  TempBuf * new_buf, * float_buf;
  int x1, y1, x2, y2;
  int x3, y3, x4, y4;
  PixelRegion srcPR, destPR;

  gdisp = (GDisplay *) gdisp_ptr;
  transform_core = (TransformCore *) tool->private;
  float_buf = transform_core->original;
  
  x1 = transform_core->x1;
  y1 = transform_core->y1;
  x2 = transform_core->x2;
  y2 = transform_core->y2;
  srcPR.bytes = float_buf->bytes;
  srcPR.x = x1;
  srcPR.y = y1;
  srcPR.w = (x2 - x1);
  srcPR.h = (y2 - y1);
  srcPR.rowstride = float_buf->width * srcPR.bytes;
  srcPR.data = temp_buf_data (float_buf);

  x3 = transform_core->trans_info[X1];
  y3 = transform_core->trans_info[Y1];
  x4 = transform_core->trans_info[X2];
  y4 = transform_core->trans_info[Y2];
  destPR.bytes = srcPR.bytes;
  destPR.x = x3;
  destPR.y = y3;
  destPR.w = x4 - x3;
  destPR.h = y4 - y3;

  /*  Create the new selection object  */
  new_buf = temp_buf_new (destPR.w, destPR.h, destPR.bytes, x3, y3, NULL);

  destPR.rowstride = new_buf->width * destPR.bytes;
  destPR.data = temp_buf_data (new_buf);

  scale_region (&srcPR, &destPR);

  return (void *) new_buf;
}


