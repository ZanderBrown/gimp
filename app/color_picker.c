/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdlib.h>
#include <stdio.h>
#include "appenv.h"
#include "autodialog.h"
#include "color_picker.h"
#include "gdisplay.h"
#include "info_dialog.h"
#include "palette.h"
#include "tools.h"


/* maximum information buffer size */

#define MAX_INFO_BUF    4


/*  local function prototypes  */

static void  color_picker_button_press     (Tool *, GdkEventButton *, gpointer);
static void  color_picker_button_release   (Tool *, GdkEventButton *, gpointer);
static void  color_picker_motion           (Tool *, GdkEventMotion *, gpointer);
static void  color_picker_control          (Tool *, int, void *);

static void  get_color                     (Tool *, void *, int, int, int);
static void  color_picker_info_update      (Tool *);


/*  local variables  */

static unsigned char  col_value [4] = { 0, 0, 0, 0 };
static InfoDialog *   color_picker_info = NULL;
static char           red_buf   [MAX_INFO_BUF];
static char           green_buf [MAX_INFO_BUF];
static char           blue_buf  [MAX_INFO_BUF];
static char           alpha_buf [MAX_INFO_BUF];
static char           index_buf [MAX_INFO_BUF];
static char           gray_buf  [MAX_INFO_BUF];

static void *         color_picker_options = NULL;


static void
color_picker_button_press (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  GDisplay * gdisp;

  gdisp = (GDisplay *) gdisp_ptr;

  /*  If this is the first invocation of the tool, or a different gdisplay,
   *  create (or recreate) the info dialog...
   */
  if (tool->state == INACTIVE || gdisp_ptr != tool->gdisp_ptr)
    {
      /*  if the dialog exists, free it  */
      if (color_picker_info)
	{
	  /*XtRemoveCallback (color_picker_info->dialog, XmNdestroyCallback, 
			    color_picker_destroy_callback, NULL);*/
	  info_dialog_free (color_picker_info);
	}
      
      color_picker_info = info_dialog_new ("Color Picker");

      /*  if the gdisplay is for a color image, the dialog must have RGB  */
      switch (gimage_type (gdisp->gimage))
	{
	case RGB_GIMAGE: case RGBA_GIMAGE:
	  info_dialog_add_field (color_picker_info, "Red", red_buf);
	  info_dialog_add_field (color_picker_info, "Green", green_buf);
	  info_dialog_add_field (color_picker_info, "Blue", blue_buf);
	  if (gimage_type (gdisp->gimage) == RGBA_GIMAGE)
	    info_dialog_add_field (color_picker_info, "Alpha", alpha_buf);
	  break;

	case INDEXED_GIMAGE:
	  info_dialog_add_field (color_picker_info, "Red", red_buf);
	  info_dialog_add_field (color_picker_info, "Green", green_buf);
	  info_dialog_add_field (color_picker_info, "Blue", blue_buf);
	  info_dialog_add_field (color_picker_info, "Index", index_buf);
	  break;

	case GRAY_GIMAGE: case GRAYA_GIMAGE:
	  info_dialog_add_field (color_picker_info, "Intensity", gray_buf);
	  if (gimage_type (gdisp->gimage) == GRAYA_GIMAGE)
	    info_dialog_add_field (color_picker_info, "Alpha", alpha_buf);
	  break;
	  
	default :
	  break;
	}
    }

  gdk_pointer_grab (gdisp->canvas->window, FALSE,
		    GDK_POINTER_MOTION_HINT_MASK | GDK_BUTTON1_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
		    NULL, NULL, bevent->time);
      
  /*  Make the tool active and set the gdisplay which owns it  */
  tool->gdisp_ptr = gdisp_ptr;
  tool->state = ACTIVE;

  /*  if the shift key is down, create a new color.
   *  otherwise, modify the current color.
   */
  if (bevent->state & GDK_SHIFT_MASK)
    get_color (tool, gdisp_ptr, bevent->x, bevent->y, COLOR_NEW);
  else
    get_color (tool, gdisp_ptr, bevent->x, bevent->y, COLOR_UPDATE);
}

static void
color_picker_button_release (tool, bevent, gdisp_ptr)
     Tool *tool;
     GdkEventButton *bevent;
     gpointer gdisp_ptr;
{
  gdk_pointer_ungrab (bevent->time);
  gdk_flush ();

  get_color (tool, gdisp_ptr, bevent->x, bevent->y, COLOR_FINISH);
}

static void
color_picker_motion (tool, mevent, gdisp_ptr)
     Tool *tool;
     GdkEventMotion *mevent;
     gpointer gdisp_ptr;
{
  get_color (tool, gdisp_ptr, mevent->x, mevent->y, COLOR_UPDATE);
}

static void
color_picker_control (tool, action, gdisp_ptr)
     Tool * tool;
     int action;
     gpointer gdisp_ptr;
{
}


static void
get_color (tool, gdisp_ptr, x, y, final)
     Tool *tool;
     void *gdisp_ptr;
     int x, y;
     int final;
{
  GDisplay * gdisp;
  GImage * gimage;
  unsigned char * src, * cmap, alpha;
  int out_of_bounds = 0;
  int index;
  
  gdisp = (GDisplay *) gdisp_ptr;
  gimage = gdisp->gimage;

  /*  First, transform the coordinates to gimp image space  */
  gdisplay_untransform_coords (gdisp, x, y, &x, &y, FALSE, 1);

  /*  if x or y is outside the gimage, out of bounds  */
  if (x < 0 || x >= gimage_width (gimage) ||
      y < 0 || y >= gimage_height (gimage))
    out_of_bounds = 1;

  /*  if the alpha channel (if one exists) is 0, out of bounds  */
  if (gimage_has_alpha (gimage) && !out_of_bounds)
    {
      alpha = *(gimage_data (gimage) + gimage_bytes (gimage) *
		(gimage_width (gimage) * y + x) +
		gimage_bytes (gimage) - 1);

      col_value [ALPHA_PIX] = alpha;
      if (!alpha)
	out_of_bounds = 1;
    }

  if (!out_of_bounds)
    /*  If the image is color, get RGB  */
    switch (gimage_type (gimage))
      {
      case RGB_GIMAGE: case RGBA_GIMAGE:
	src = gimage_data (gimage) + gimage_bytes (gimage) *
	  (gimage_width (gimage) * y + x);
	col_value [RED_PIX] = src [RED_PIX];
	col_value [GREEN_PIX] = src [GREEN_PIX];
	col_value [BLUE_PIX] = src [BLUE_PIX];
	break;
	
      case INDEXED_GIMAGE:
	src = gimage_data (gimage) + (gimage_width (gimage) * y + x);
	col_value [3] = src [0];
	index = src [0] * 3;
	cmap = gimage_cmap (gimage);
	col_value [RED_PIX] = cmap [index];
	col_value [GREEN_PIX] = cmap [index + 1];
	col_value [BLUE_PIX] = cmap [index + 2];
	break;
	
      case GRAY_GIMAGE: case GRAYA_GIMAGE:
	src = gimage_data (gimage) + gimage_bytes (gimage) *
	  (gimage_width (gimage) * y + x);
	col_value [RED_PIX] = src [GRAY_PIX];
	col_value [GREEN_PIX] = src [GRAY_PIX];
	col_value [BLUE_PIX] = src [GRAY_PIX];
	break;
	
      default :
	break;
      }

  palette_set_active_color (col_value [RED_PIX], col_value [GREEN_PIX],
			    col_value [BLUE_PIX], final);
  color_picker_info_update (tool);
}

static void
color_picker_info_update (tool)
     Tool *tool;
{
  GDisplay * gdisp;

  gdisp = (GDisplay *) tool->gdisp_ptr;

  switch (gimage_type (gdisp->gimage))
    {
    case RGB_GIMAGE: case RGBA_GIMAGE:
      sprintf (red_buf, "%d", col_value [RED_PIX]);
      sprintf (green_buf, "%d", col_value [GREEN_PIX]);
      sprintf (blue_buf, "%d", col_value [BLUE_PIX]);
      if (gimage_type (gdisp->gimage) == RGBA_GIMAGE)
	sprintf (alpha_buf, "%d", col_value [ALPHA_PIX]);
      break;

    case INDEXED_GIMAGE:
      sprintf (red_buf, "%d", col_value [RED_PIX]);
      sprintf (green_buf, "%d", col_value [GREEN_PIX]);
      sprintf (blue_buf, "%d", col_value [BLUE_PIX]);
      sprintf (index_buf, "%d", col_value [3]);
      break;

    case GRAY_GIMAGE: case GRAYA_GIMAGE:
      sprintf (gray_buf, "%d", col_value [GRAY_PIX]);
      if (gimage_type (gdisp->gimage) == GRAYA_GIMAGE)
	sprintf (alpha_buf, "%d", col_value [ALPHA_PIX]);
      break;
    }

  info_dialog_update (color_picker_info);
  info_dialog_popup (color_picker_info);
}

Tool *
tools_new_color_picker ()
{
  Tool * tool;

  if (! color_picker_options)
    color_picker_options = tools_register_no_options (COLOR_PICKER, "Color Picker Options");

  tool = (Tool *) xmalloc (sizeof (Tool));

  tool->type = COLOR_PICKER;
  tool->state = INACTIVE;
  tool->scroll_lock = 0;  /*  Allow scrolling  */
  tool->private = NULL;
  tool->button_press_func = color_picker_button_press;
  tool->button_release_func = color_picker_button_release;
  tool->motion_func = color_picker_motion;
  tool->arrow_keys_func = standard_arrow_keys_func;
  tool->control_func = color_picker_control;

  return tool;
}

void
tools_free_color_picker (tool)
     Tool * tool;
{
  if (color_picker_info)
    {
      info_dialog_free (color_picker_info);
      color_picker_info = NULL;
    }
}

