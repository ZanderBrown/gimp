/* Displace --- image filter plug-in for The Gimp image manipulation program
 * Copyright (C) 1996 Stephen Robert Norris
 * Much of the code taken from the pinch plug-in by 1996 Federico Mena Quintero
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * You can contact me at srn@flibble.cs.su.oz.au.
 * Please send me any patches or enhancements to this code.
 * You can contact the original The Gimp authors at gimp@xcf.berkeley.edu
 *
 * Extensive modifications to the dialog box, parameters, and some
 * legibility stuff in displace() by Federico Mena Quintero ---
 * quartic@polloux.fciencias.unam.mx.  If there are any bugs in these
 * changes, they are my fault and not Stephen's.
 */

/* Version 1.11. */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "gimp.h"

/* Some useful macros */

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define WITHIN(a, b, c) ((((a) <= (b)) && ((b) <= (c))) ? 1 : 0)

#ifndef _AIX
typedef unsigned char uchar;
#endif /* AIX */

/*
 * Function prototypes.
 */

static void ok_callback(int item_id, void *client_data, void *call_data);
static void cancel_callback(int item_id, void *client_data, void *call_data);
static void image_menu_callback (int item_id, void *client_data, void *call_data);
static void text_callback(int item_id, void *client_data, void *call_data);
static void toggle_callback (int, void *, void *);
static void radio_callback (int, void *, void *);
static void displace(Image input, Image output,
	long do_x, double amount_x, Image displaceMap1,
	long do_y, double amount_y, Image displaceMap2,
	long wrap, long smear);

static uchar bilinear(double x, double y, uchar *v);

static int	dialog_id;

/*
 * This structure is used to store the persistent data of the plug-in.
 */

typedef struct {
        long	do_x;
	double	displace_x;

	long	do_y;
	double	displace_y;
 
	long	wrap;
	long	smear;
	long	black;
} Data;

int
main(int argc, char **argv)
{
	Image		input, output;
	Image		src1, src2;
	void		*data;
	int		group_id;
	int		image_menu1_id, image_menu2_id;
	int		do_x_id, do_y_id, text_x_id, text_y_id;
	int		wrap_id, black_id, smear_id;
	static long	src1_id = 0, src2_id = 0;
	char		buf[100];
	Data		tmp;

	/* Initialize filter and continue if success */

	if (!gimp_init(argc, argv))
		return 0;

	/* Get default input and output images */

	input  = gimp_get_input_image(0);
	output = gimp_get_output_image(0);

	/* If both images were available, continue */

	if (input && output) {
	  switch (gimp_image_type (input))
	    {
	    case RGB_IMAGE: case RGBA_IMAGE:
	    case GRAY_IMAGE: case GRAYA_IMAGE:
	      data = gimp_get_params();

	      if (data) {
		tmp = *((Data *) data);
	      } else {
		/* Note: these are arbitrary default values! */
		
		tmp.do_x = tmp.do_y = 1;
		tmp.displace_x = tmp.displace_y = 20.0;
		
		tmp.wrap = 1;
		tmp.smear = 0;
		tmp.black = 0;
	      }

	      dialog_id = gimp_new_dialog("Displace");
	      
	      group_id = gimp_new_row_group(dialog_id, DEFAULT, NORMAL, "");
	      /* Displace X */
  
	      do_x_id = gimp_new_check_button(dialog_id, group_id, "Displace X");
	      gimp_change_item(dialog_id, do_x_id, sizeof(tmp.do_x), &tmp.do_x);
	      gimp_add_callback(dialog_id, do_x_id, toggle_callback, &tmp.do_x);

	      sprintf(buf, "%0.1f", tmp.displace_x);
	      text_x_id = gimp_new_text(dialog_id, group_id, buf);
	      gimp_add_callback(dialog_id, text_x_id, text_callback, &tmp.displace_x);

	      image_menu1_id = gimp_new_image_menu (dialog_id, group_id, 
						    IMAGE_CONSTRAIN_RGB
						    | IMAGE_CONSTRAIN_GRAY,
						    "Displace X map.");
	      gimp_add_callback (dialog_id, image_menu1_id, image_menu_callback,
				 &src1_id);

	      /* Displace Y */
	      
	      do_y_id = gimp_new_check_button(dialog_id, group_id, "Displace Y");
	      gimp_change_item(dialog_id, do_y_id, sizeof(tmp.do_y), &tmp.do_y);
	      gimp_add_callback(dialog_id, do_y_id, toggle_callback, &tmp.do_y);
	      
	      sprintf(buf, "%0.1f", tmp.displace_y);
	      text_y_id = gimp_new_text(dialog_id, group_id, buf);
	      gimp_add_callback(dialog_id, text_y_id, text_callback, &tmp.displace_y);
	      
	      image_menu2_id = gimp_new_image_menu (dialog_id, group_id, 
						    IMAGE_CONSTRAIN_RGB
						    | IMAGE_CONSTRAIN_GRAY,
						    "Displace Y map.");
	      gimp_add_callback (dialog_id, image_menu2_id, image_menu_callback,
				 &src2_id);
	      
	      /* What happens at the edge of the image. */
	      
	      group_id = gimp_new_row_group (dialog_id, group_id, RADIO, "");
	      
	      /* Wrap around. */
	      
	      wrap_id = gimp_new_radio_button (dialog_id, group_id,
					       "Wrapped");
	      gimp_change_item (dialog_id, wrap_id, sizeof (tmp.wrap),
				&tmp.wrap);
	      gimp_add_callback (dialog_id, wrap_id, radio_callback,
				 &tmp.wrap);
	      
	      /* Fill with black. */
	      
	      black_id = gimp_new_radio_button (dialog_id, group_id,
						"Black");
	      gimp_change_item (dialog_id, black_id, sizeof (tmp.black),
				&tmp.black);
	      gimp_add_callback (dialog_id, black_id, radio_callback,
				 &tmp.black);
	      
	      /* Smear the edges out. */
	      
	      smear_id = gimp_new_radio_button (dialog_id, group_id,
						"Smeared");
	      gimp_change_item (dialog_id, smear_id, sizeof (tmp.smear),
				&tmp.smear);
	      gimp_add_callback (dialog_id, smear_id, radio_callback,
				 &tmp.smear);
	      
	      gimp_add_callback(dialog_id, gimp_ok_item_id(dialog_id),
				ok_callback, NULL);
	      gimp_add_callback(dialog_id, gimp_cancel_item_id(dialog_id),
				cancel_callback, NULL);
	      
	      /* Dialog. */
	      
	      if (gimp_show_dialog(dialog_id)) {
		src1 = gimp_get_input_image (src1_id);
		src2 = gimp_get_input_image (src2_id);
		
		gimp_set_params(sizeof(tmp), &tmp);
		gimp_init_progress("Displace");
		displace(input, output,
			 tmp.do_x, tmp.displace_x, src1, 
			 tmp.do_y, tmp.displace_y, src2,
			 tmp.wrap, tmp.smear);
		
		gimp_update_image(output);
	      } /* if */
	      break;
	    default:
	      gimp_message("Displace: can only operate on RGB or grayscale images");
	      break;
	    }

	  gimp_free_image(input);
	  gimp_free_image(output);

	  gimp_quit();
	} /* if */

	return 0;
} /* main */

static void
text_callback(int item_id, void *client_data, void *call_data)
{
	*((double *) client_data) = atof(call_data);
} /* text_callback */

static void
ok_callback(int item_id, void *client_data, void *call_data)
{
	gimp_close_dialog(dialog_id, 1);
} /* ok_callback */


static void
cancel_callback(int item_id, void *client_data, void *call_data)
{
	gimp_close_dialog(dialog_id, 0);
} /* cancel_callback */

static void
image_menu_callback (int item_id, void *client_data, void *call_data)
{
	*((long*) client_data) = *((long*) call_data);
}

static void
toggle_callback (int item_id, void *client_data, void *call_data)
{
	*((long*) client_data) = *((long*) call_data);
}

static void
radio_callback (int item_id, void *client_data, void *call_data)
{
	*((long*) client_data) = *((long*) call_data);
}

/* The displacement is done here. */

static void
displace(Image input, Image output,
	long do_x, double amount_x, Image displaceMap1,
	long do_y, double amount_y, Image displaceMap2,
	long wrap, long smear)
{
	long	width, height;
	long	channels, rowsiz;
	long	map1_channels, map2_channels, map1_rowsize, map2_rowsize;
	uchar	*src, *p;
	uchar	*destrow;
	uchar	*dest;
	int	x1, y1, x2, y2;
	int	x, y;
	int	progress, max_progress;
	int     x1_in, y1_in, x2_in, y2_in;

	uchar	*map1, *map2;

	double	amnt;
	double	needx, needy;
	int	xi, yi;

	uchar	values[4];
	uchar	val;

	int k;

	/* Get selection area */

	gimp_image_area(input, &x1, &y1, &x2, &y2);

	width    = gimp_image_width(input);
	height   = gimp_image_height(input);
	channels = gimp_image_channels(input);
	rowsiz   = width * channels;

	map1_channels = gimp_image_channels(displaceMap1);
	map1_rowsize = map1_channels * width;
	map2_channels = gimp_image_channels(displaceMap2);
	map2_rowsize = map2_channels * width;

	progress     = 0;
	max_progress = y2 - y1;

	src     = gimp_image_data(input);
	destrow = gimp_image_data(output);
	map1	= gimp_image_data(displaceMap1);
	map2	= gimp_image_data(displaceMap2);

	/*
	 * The algorithm used here is simple - see 
	 * http://the-tech.mit.edu/KPT/Tips/KPT7/KPT7.html for a description.
	 */

	destrow += y1 * rowsiz + x1 * channels;
	for (y = y1; y < y2; y++) {
		dest = destrow;

		/*
		 * We could move the displacement image address calculation out of here,
		 * but when we can have different sized displacement and destination
		 * images we'd have to move it back anyway.
		 */

		for (x = x1; x < x2; x++) {
			if (do_x) {
				amnt = amount_x * ((((float)*(map1 + map1_rowsize * y
						 + x * map1_channels)) - 128.0)/128.0);
				needx = x + amnt;
			} else {
				needx = x;
			}

			if (do_y){
				amnt = amount_y * ((((float)*(map2 + map2_rowsize * y
						 + x * map2_channels)) - 128.0)/128.0);
				needy = y + amnt;
			} else {
				needy = y;
			}

			/* Calculations complete; now copy the proper pixel */

			xi = needx;
			yi = needy;

			/* Tile the image. */

			if (wrap){
				if (xi < 0)
					xi = width - (-xi % width);
				else
					xi %= width;
				
				if (yi < 0)
					yi = height - (-yi % height);
				else
					yi %= height;
			}

			/* Smear out the edges of the image by repeating pixels. */

			if (smear) {
				if (xi < 0)
					xi = 0;
				else if (xi > width - 1)
					xi = width - 1;
				
				if (yi < 0)
					yi = 0;
				else if (yi > height - 1) 
					yi = height - 1;
			}

			p = src + rowsiz * yi + xi * channels;

			x1_in = WITHIN(0, xi, width - 1);
			y1_in = WITHIN(0, yi, height - 1);
			x2_in = WITHIN(0, xi + 1, width - 1);
			y2_in = WITHIN(0, yi + 1, height - 1);

			for (k = 0; k < channels; k++) {
					if (x1_in && y1_in)
						values[0] = *(p + k);
					else{
						values[0] = 0;
					}

					if (x2_in && y1_in)
						values[1] = *(p + channels + k);
					else if (wrap){
						values[1] = *(p + channels + k - rowsiz);
					} else {
						values[1] = 0;
					}

					if (x1_in && y2_in)
						values[2] = *(p + rowsiz + k);
					else if (wrap){
						values[2] = *(p + rowsiz - (height - 1) * rowsiz + k);
					} else {
						values[2] = 0;
					}
					/* Lower right pixel */

					if (x2_in) {
					       if (y2_in)
						       values[3] = *(p + channels + k + rowsiz);
					       else if (wrap)
						       values[3] = *(p + channels + k + rowsiz
								     - height * rowsiz);
					       else
						       values[3] = 0;
					} else if (wrap) {
					       if (y2_in)
						       values[3] = *(p + channels + k);
					       else
						       values[3] = *(p + channels + k
								     - height * rowsiz);
					} else {
					       values[3] = 0;
					}

					val = bilinear(needx, needy, values);
					*dest++ = val;
				}
			} /* for */

		destrow += rowsiz;

		progress++;
		if (progress % 5 == 0)
			gimp_do_progress(progress, max_progress);
	} /* for */
} /* displace */


/*****/

static uchar
bilinear(double x, double y, uchar *v)
{
	double m0, m1;

	x = fmod(x, 1.0);
	y = fmod(y, 1.0);

	if (x < 0)
		x += 1.0;
	if (y < 0)
		y += 1.0;

	m0 = (1.0 - x) * v[0] + x * v[1];
	m1 = (1.0 - x) * v[2] + x * v[3];

	return (uchar) ((1.0 - y) * m0 + y * m1);
} /* bilinear */
