/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 * Texture1
 */

#include <stdio.h>
#include <stdlib.h>
#include "gimp.h"

#include <math.h>

/* Noise material */

typedef long VECTOR[2];
#define FIX 12
#define UNIT ( 1<<FIX )

#define TAB_LEN 2048

static int    *Permut;
static VECTOR *Gradients;

#define SetUp(i,b0,b1,r0,r1)                    \
        t = Point[i] + 01244L;                  \
        r0 = t & (UNIT-1);                      \
        r1 = r0-(UNIT-1);                       \
        b0 = ( ( t-r0 )>>FIX ) % TAB_LEN;       \
        b1 = ( b0+1 ) % TAB_LEN;


#define at(rx,ry,i) ( ( (rx)*Gradients[(i)][0]+(ry)*Gradients[(i)][1] ) >> FIX )

#define Sigmoid(t) ( ( t*(( t*( 3*UNIT - 2*t ) ) >> FIX ) ) >> FIX )

#define L_Interpolate(t,a,b) ( a + ( ( t*(b-a) ) >> FIX ) )


#define TRUE  1
#define FALSE 0

typedef struct {
  long Scale_x, Scale_y;
  long Opacity;
  long Highlight;
  long Seed;
} Text_Values;

/* Declare local functions.
 */
static void Vals_callback( int, void *, void * );
static void toggle_callback( int, void *, void * );
static void ok_callback( int, void *, void * );
static void cancel_callback( int, void *, void * );
static void Text1( Image );

static int Init_Texture( int );
static long Noise( VECTOR );
static long Noise2( VECTOR );
/*
static void DNoise( VECTOR, VECTOR );
static void DNoise2( VECTOR, VECTOR );
*/
static int    *Permut = NULL;
static VECTOR *Gradients = NULL;

static char *prog_name;
static int dialog_ID;
static Text_Values vals;

int
main (argc, argv)
     int argc;
     char **argv;
{
  Image input;
  char buf[16];
  int group_ID;
  int temp_ID;
  int Scale_x_ID;
  int Scale_y_ID;
  int Opacity_ID;
  int Highlight_ID;
  void *data;
  
  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if ( gimp_init( argc, argv ) )
  {
    input = gimp_get_input_image( 0 );

    data = gimp_get_params( );
    if ( data )
      vals = *( ( Text_Values* ) data );
    else
    {
       vals.Scale_x = 50;
       vals.Scale_y = 50;
       vals.Opacity = 100;
       vals.Highlight = 0;
       vals.Seed = 125;
    }
      
    dialog_ID = gimp_new_dialog( "texture1" );
    group_ID = gimp_new_row_group( dialog_ID, DEFAULT, NORMAL, "" );

    temp_ID = gimp_new_column_group( dialog_ID, group_ID, NORMAL, "" );
    gimp_new_label( dialog_ID, temp_ID, "Scale x:" );
    sprintf ( buf, "%d", (int) vals.Scale_x );
    Scale_x_ID = gimp_new_text( dialog_ID, temp_ID, buf );

    temp_ID = gimp_new_column_group( dialog_ID, group_ID, NORMAL, "" );
    gimp_new_label( dialog_ID, temp_ID, "Scale y:" );
    sprintf ( buf, "%d", (int) vals.Scale_y );
    Scale_y_ID = gimp_new_text( dialog_ID, temp_ID, buf );
     
    temp_ID = gimp_new_column_group( dialog_ID, group_ID, NORMAL, "" );
    gimp_new_label( dialog_ID, temp_ID, "Opacity:" );
    sprintf ( buf, "%d", (int) vals.Opacity );
    Opacity_ID = gimp_new_text( dialog_ID, temp_ID, buf );

    temp_ID = gimp_new_column_group( dialog_ID, group_ID, NORMAL, "" );
    gimp_new_label( dialog_ID, temp_ID, "Highlight.:" );
    sprintf ( buf, "%d", (int) vals.Highlight );
    Highlight_ID = gimp_new_text( dialog_ID, temp_ID, buf );

/*
    switch (gimp_image_type (input))
    {
       case RGB_IMAGE:
       case GRAY_IMAGE:
          wrap_ID = gimp_new_check_button (dialog_ID, group_ID, "Wrap Around");
          gimp_change_item (dialog_ID, wrap_ID, sizeof (vals.wraparound), &vals.
wraparound);
          gimp_add_callback (dialog_ID, wrap_ID, toggle_callback, &vals.wraparou
nd);
       break;
       default:
          vals.Seed = 1216;
       break;
    }
*/     
    gimp_add_callback ( dialog_ID, Scale_x_ID, Vals_callback, &vals.Scale_x );
    gimp_add_callback ( dialog_ID, Scale_y_ID, Vals_callback, &vals.Scale_y );
    gimp_add_callback ( dialog_ID, Opacity_ID, Vals_callback, &vals.Opacity );
    gimp_add_callback ( dialog_ID, Highlight_ID, Vals_callback, &vals.Highlight 
);
    gimp_add_callback ( dialog_ID, gimp_ok_item_id( dialog_ID ), ok_callback, 0 
);
    gimp_add_callback ( dialog_ID, gimp_cancel_item_id( dialog_ID ), cancel_callback, 0 );

    if ( gimp_show_dialog( dialog_ID ) )
    {
       gimp_set_params( sizeof( Text_Values ), &vals );
     
       if ( input )
       {
          gimp_init_progress( "Texture1" );
          if ( !Init_Texture( vals.Seed ) )
             Text1( input );
       }
    }

    if ( Permut != NULL ) free( Permut );
    if ( Gradients != NULL ) free( Gradients );

      /* Free the image.
       */
    if ( input ) gimp_free_image( input );

      /* Quit
       */
    gimp_quit( );
  }

  return 0;
}

static void Vals_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *( ( long* ) client_data ) = ( long )atoi( call_data );
}

static void toggle_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *((long*) client_data) = *((long*) call_data);
}

static void ok_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog( dialog_ID, 1 );
}

static void cancel_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 0);
}

static void Text1( input )
     Image input;
{
  Image output;
  unsigned char *src, *dest;
  unsigned char r1, g1, b1;
  unsigned char r2, g2, b2;
  long c[3];
  VECTOR Where;
  long Noise_Val, U_Noise_Val, Tmp;
  long Opacity, Highlight, Scale_x, Scale_y;
  long width, height;
  long Channels;
  int i, j, k;
  int Count, Scale;
  long Offset_x, Offset_y, Weight;

  Opacity = vals.Opacity;
  if ( Opacity > 100 ) Opacity = 100;
  if ( Opacity <= 0 ) return;

  Scale_x = vals.Scale_x;
  if ( Scale_x > 100 ) Opacity = 100;
  if ( Scale_x < 0) Opacity = 0;
  Scale_x = ( Scale_x*UNIT ) / 222;

  Scale_y = vals.Scale_y;
  if ( Scale_y > 100 ) Opacity = 100;
  if ( Scale_y < 0) Opacity = 0;
  Scale_y = ( Scale_y*UNIT ) / 222;

  Highlight = vals.Highlight;
  if ( Highlight > 100 ) Highlight = 100;
  if ( Highlight < 0) Highlight = 0;
  Highlight = ( Highlight*UNIT ) / 100;

  output = gimp_get_output_image( 0 );
  if ( !output ) return;

  width = gimp_image_width( input );
  height = gimp_image_height( input );
  Channels = gimp_image_channels( input );

  printf ("hola\n");
  gimp_background_color( &r1, &g1, &b1 );
  printf ("fucker\n");
  gimp_foreground_color( &r2, &g2, &b2 );
  printf ("eat it\n");

   /* First pass. Compute texture */

  Weight = 0; Count = 1;
  for( Scale=1; ( Scale<width || Scale<height ); Scale*=2 )
  {
     Weight += Count*Count;
     Count++;
  }
  Weight <<= FIX;

  dest = gimp_image_data( output );   
  for ( i=0; i<height; i++ )
  {
     for ( j=0; j<width; j++ )
     {

        Where[ 0 ] = i*Scale_x;
        Where[ 1 ] = j*Scale_y;
        Noise_Val = Noise( Where );
        for( k=1, Scale=2; k<Count; k++, Scale *= 2 )
        {
           Offset_x = i-(i%Scale);
           Where[ 0 ] = Offset_x*Scale_x;

           Offset_y = j-(j%Scale);
           Where[ 1 ] = Offset_y*Scale_y;

           Tmp = ( Count-k );
           Noise_Val += Noise( Where )*Tmp*Tmp;
        }
        Noise_Val = ( Noise_Val << FIX ) / Weight;
        if ( Highlight )
        {
           Tmp = Sigmoid( Noise_Val );
           Noise_Val = ( Noise_Val*( UNIT-Highlight) + Tmp*Highlight*2 ) >> FIX;
           if ( Noise_Val>UNIT ) Noise_Val = UNIT;
        }
        U_Noise_Val = UNIT - Noise_Val;
        c[0] = ( r1*Noise_Val + r2*U_Noise_Val );
        c[1] = ( g1*Noise_Val + g2*U_Noise_Val );
        c[2] = ( b1*Noise_Val + b2*U_Noise_Val );

        if ( Channels==1 ) *dest++ = ( c[0]>>FIX ) & 255;
        else for ( k=0; k<Channels; k++ ) *dest++ = ( c[k]>>FIX ) & 255;
     }

     if ( ( i%10 ) == 0)
       gimp_do_progress( i, height );
  }

   /* Last pass to update w/ opacity */

  src = gimp_image_data( input );
  dest = gimp_image_data( output );

  for ( i=0; i<height; i++ )
     for ( j=0; j<width; j++ )
     {
        for ( k=0; k<Channels; k++ )
        {
           Tmp = *src++;
           Tmp = ( Tmp*(100-Opacity ) + Opacity*(*dest) ) / 100;
           *dest++ = Tmp & 255;
        }
     }

  gimp_update_image( output );
  if ( output )
  {
     gimp_update_image( output );
     gimp_free_image( output );
  }
}

/*****************************************************************/

long Noise( VECTOR Point )
{
   long Val;

      /* Result is in [0, 1] */   

   Val = Noise2( Point );
   Val = (Val+UNIT)/2;

/*   fprintf( stderr, "%ld ", Val ); */
   return( Val );
}

long Noise2( VECTOR Point )
{
   int bx0, bx1, by0, by1;
   int b00, b10, b01, b11;
   long rx0, rx1, ry0, ry1;
   long sx, sy;
   long a, b, t, u, v;
   int i, j;
   long Noise;

      /* Result is in [-1, 1] */   

   SetUp( 0, bx0, bx1, rx0, rx1 );
   SetUp( 1, by0, by1, ry0, ry1 );

   i = Permut[ bx0 ];
   j = Permut[ bx1 ];

   b00 = Permut[ i + by0 ];
   b10 = Permut[ j + by0 ];
   b01 = Permut[ i + by1 ];
   b11 = Permut[ j + by1 ];

   sx = Sigmoid(rx0);
   sy = Sigmoid(ry0);

   u = at( rx0, ry0, b00 );
   v = at( rx1, ry0, b10 );
   a = L_Interpolate(sx, u, v);

   u = at( rx0, ry1, b01 );
   v = at( rx1, ry1, b11 );
   b = L_Interpolate(sx, u, v);

   Noise = L_Interpolate(sy, a, b);
   
   return( Noise );
}

static int Init_Texture( Seed )
   int Seed;
{
   long random();
   int i, j, k;
   VECTOR Pt;
   long s;

   srandom( Seed );

   Permut = ( int * )malloc( ( 2*TAB_LEN + 2 ) * sizeof( int ) );
   Gradients = ( VECTOR * )malloc( ( 2*TAB_LEN + 2 ) * sizeof( VECTOR ) );
   
   if ( Permut == NULL || Gradients == NULL )
   {
      gimp_message( "texture1: Memory error -- cannot allocate _Noise() tables" 
);
      return( TRUE );
   }
      
   for ( i=0; i<TAB_LEN; i++ )
   {
      do {                  
         for ( j=0; j<2; j++ )
            Pt[j] = ( long )( random( )%(2*UNIT) -  UNIT );
         s = ( Pt[0]*Pt[0] + Pt[1]*Pt[1] ) >> FIX;
      } while ( s>UNIT ); 
      s = 1.0*UNIT*sqrt( 1.0*s/UNIT );
      for ( j=0; j<2; j++ )
         Gradients[i][j] = ( Pt[j] << FIX ) / s;

   }

   for ( i=0 ; i<TAB_LEN; i++ )
      Permut[i] = i;
   for ( i=TAB_LEN; i>0; i-=2 )
   {
      k = Permut[ i ];
      Permut[ i ] = Permut[ j = random() %  TAB_LEN ];
      Permut[ j ] = k;
   }

   for ( i=0; i<TAB_LEN+2; i++ )
   {
      Permut[ TAB_LEN + i ] = Permut[ i ];
      for ( j=0; j<2; j++ )
         Gradients[ TAB_LEN + i ][ j ] = Gradients[ i ][ j ];
   }
                           
   return( FALSE );
}


/***************************************************************/
