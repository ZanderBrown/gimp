/*
 * This is a plugin for the GIMP.
 *
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 * Copyright (C) 1996 Torsten Martinsen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: noisify.c,v 1.1 1996/04/26 09:47:07 torsten Exp $
 */

/*
 * This filter adds random noise to an image.
 * The amount of noise can be set individually for each RGB channel.
 * This filter does not operate on indexed images.
 */

#include <stdlib.h>
#include "gimp.h"

static void noisify(Image, Image);
static void scale_callback(int, void *, void *);
static void ok_callback(int, void *, void *);
static void cancel_callback(int, void *, void *);
static double gauss(void);

static long amount[3] = { 20, 20, 20 };
static int dialog_ID;

int
main(int argc, char **argv)
{
    Image input, output;
    void *data;
    int group_ID;
    int scaler_ID;
    int scaleg_ID;
    int scaleb_ID;


    if (gimp_init(argc, argv)) {
	input = 0;
	output = 0;

	input = gimp_get_input_image(0);

	if (input)
	    switch (gimp_image_type(input)) {
	    case RGB_IMAGE:
	    case GRAY_IMAGE:
		data = gimp_get_params();
		if (data)
		    {
			amount[0] = ((long*) data)[0];
			amount[1] = ((long*) data)[1];
			amount[2] = ((long*) data)[2];
		    }

		dialog_ID = gimp_new_dialog("Add Noise");
		group_ID = gimp_new_row_group(dialog_ID, DEFAULT, NORMAL, "");
		if (gimp_image_type(input) == GRAY_IMAGE)
		    {
			scaler_ID = gimp_new_scale(dialog_ID, group_ID,
						   1, 100, amount[0], 0);
			gimp_add_callback(dialog_ID, scaler_ID,
					  scale_callback, &amount[0]);
		    }
		else
		    {
			scaler_ID = gimp_new_scale(dialog_ID, group_ID,
						   1, 100, amount[0], 0);
			scaleg_ID = gimp_new_scale(dialog_ID, group_ID,
						   1, 100, amount[1], 0);
			scaleb_ID = gimp_new_scale(dialog_ID, group_ID,
						   1, 100, amount[2], 0);
			gimp_add_callback(dialog_ID, scaler_ID,
					  scale_callback, &amount[0]);
			gimp_add_callback(dialog_ID, scaleg_ID,
					  scale_callback, &amount[1]);
			gimp_add_callback(dialog_ID, scaleb_ID,
					  scale_callback, &amount[2]);
		    }
		gimp_add_callback(dialog_ID, scaler_ID,
				  scale_callback, &amount);
		gimp_add_callback(dialog_ID, gimp_ok_item_id(dialog_ID),
				  ok_callback, 0);
		gimp_add_callback(dialog_ID, gimp_cancel_item_id(dialog_ID),
				  cancel_callback, 0);

		if (gimp_show_dialog(dialog_ID)) {
		    gimp_set_params(sizeof(long)*3, &amount);

		    output = gimp_get_output_image(0);
		    if (output) {
			gimp_init_progress("Add Noise");
			noisify(input, output);
			gimp_update_image(output);
		    }
		}
		break;
	    case INDEXED_IMAGE:
		gimp_message("Add Noise: cannot operate on indexed color images");
		break;
	    default:
		gimp_message("Add Noise: cannot operate on unknown image types");
		break;
	    }
	if (input)
	    gimp_free_image(input);
	if (output)
	    gimp_free_image(output);

	gimp_quit();
    }
    return 0;
}

static void
noisify(Image input, Image output)
{
    long width, height;
    long channels, rowstride;
    unsigned char *src_row, *dest_row;
    unsigned char *src, *dest;
    short row, col;
    int x1, y1, x2, y2, p;

    gimp_image_area(input, &x1, &y1, &x2, &y2);

    width = gimp_image_width(input);
    height = gimp_image_height(input);
    channels = gimp_image_channels(input);
    rowstride = width * channels;

    src_row = gimp_image_data(input);
    dest_row = gimp_image_data(output);

    x1 *= channels;
    x2 *= channels;

    src_row += rowstride * y1 + x1;
    dest_row += rowstride * y1 + x1;

    for (row = y1; row < y2; row++) {
	src = src_row;
	dest = dest_row;

	for (col = x1; col < x2; col += channels) {
	    p = *src++ + amount[0] * gauss();
	    if (p < 0)
		p = 0;
	    else if (p > 255)
		p = 255;
	    *dest++ = p;
	    if (channels > 1) {
		p = *src++ + amount[1] * gauss();
		if (p < 0)
		    p = 0;
		else if (p > 255)
		    p = 255;
		*dest++ = p;
		p = *src++ + amount[2] * gauss();
		if (p < 0)
		    p = 0;
		else if (p > 255)
		    p = 255;
		*dest++ = p;
	    }
	}
	src_row += rowstride;
	dest_row += rowstride;
	if ((row % 5) == 0)
	    gimp_do_progress(row, y2 - y1);
    }
}

static void
scale_callback(int item_ID, void *client_data, void *call_data)
{
    *((int *) client_data) = *((long *) call_data);
}

static void
ok_callback(int item_ID, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_ID, 1);
}

static void
cancel_callback(int item_ID, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_ID, 0);
}

/*
 * Return a Gaussian (aka normal) random variable.
 *
 * Adapted from ppmforge.c, which is part of PBMPLUS.
 * The algorithm comes from:
 * 'The Science Of Fractal Images'. Peitgen, H.-O., and Saupe, D. eds.
 * Springer Verlag, New York, 1988.
 */
static double 
gauss(void)
{
    int i;
    double sum = 0.0;

    for (i = 0; i < 4; i++)
	sum += random() & 0x7FFF;

    return sum * 5.28596089837e-5 - 3.46410161514;
}
