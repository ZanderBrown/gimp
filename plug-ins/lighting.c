/*********************************************************************************/
/* Lighting 0.23 -- image filter plug-in for The Gimp program                    */
/* Copyright (C) 1996 Tom Bech                                                   */
/*===============================================================================*/
/* You can contact me at tomb@ii.uib.no                                          */
/* You can contact the original The Gimp authors at gimp@xcf.berkeley.edu        */
/*===============================================================================*/
/* This program is free software; you can redistribute it and/or modify it under */
/* the terms of the GNU General Public License as published by the Free Software */
/* Foundation; either version 2 of the License, or (at your option) any later    */
/* version.                                                                      */
/*===============================================================================*/
/* This program is distributed in the hope that it will be useful, but WITHOUT   */
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS */
/* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.*/
/*===============================================================================*/
/* You should have received a copy of the GNU General Public License along with  */
/* this program; if not, write to the Free Software Foundation, Inc., 675 Mass   */
/* Ave, Cambridge, MA 02139, USA.                                                */
/*===============================================================================*/
/* In other words, you can't sue me for whatever happens while using this ;)     */
/*********************************************************************************/
/* Changes (post 0.20)                                                           */
/* -> 0.23: Fixed a bug in main_ok/cancel_callback(), float->doubles etc.        */
/*          Added a patch from Quartic to fix some prototype problems and some   */
/*          other minor stuff. Fixed a bug in Shade().                           */
/*********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gimp.h"

/************/
/* Typedefs */
/************/

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define MAX_CLEANUP_STACK 10

#define DIRECTIONAL_LIGHT 0
#define POINT_LIGHT 1
#define SPOT_LIGHT 2

#define FLAT_SURFACE 0
#define WAVY_SURFACE 1
#define MAPPED_SURFACE 2

#define REFRACTION_DISABLED 0
#define REFRACTION_ENABLED 1

#define MAPPED_CONSTANT 0
#define MAPPED_WAVE 1
#define MAPPED_IMAGE 2

#define MAP_LINEAR 0
#define MAP_LOG 1
#define MAP_SINE 2

#define WITHOUT_COLOR 0
#define WITH_COLOR 1

/***********************/
/* Some useful structs */
/***********************/

typedef struct
{
  double x,y,z;
} Vector;

typedef struct
{
  double r,g,b;
} RGBPixel;

typedef struct
{
  double AmbientInt;
  double DiffuseInt;
  double AmbientRef;
  double DiffuseRef;
  double SpecularRef;
  double Highlight;
  RGBPixel Color;
} MaterialSettings;

typedef struct
{
  long Imagemap;
  long FitToRange;
  long MapToggles[3];
  double MaxValue;
  double MinValue;
} ImageMapSettings;

typedef struct
{
  double IOR_Air;
  double IOR_Material;
  long IOR_MapConstant;
  int IOR_MapType;
  ImageMapSettings IOR_Imagemap;
  double Thickness;
  long Thickness_MapConstant;
  int Thickness_MapType;
  ImageMapSettings Thickness_Imagemap;
  long IOR_Toggles[3];
  long Thickness_Toggles[3];
  Image IOR_Image;
  Image Thickness_Image;
} RefractionSettings;

typedef struct
{
  Vector Position;
  Vector Direction;
  RGBPixel Color;
  double Radius;
  double Intensity;
} LightSettings;

typedef struct
{
  double XAmpl,YAmpl;
  double XShift,YShift;
  double XPeriod,YPeriod;
} WaveSettings;

/*****************************/
/* Global variables and such */
/*****************************/

Vector ZeroVec = {x:0.0,y:0.0,z:0.0};
Vector Unit_X = {x:1.0,y:0.0,z:0.0};
Vector Unit_Y = {x:0.0,y:1.0,z:0.0};
Vector Unit_Z = {x:0.0,y:0.0,z:1.0};
Vector ViewPoint = {x:0.5,y:0.5,z:2.0};

RGBPixel White = {r:1.0,g:1.0,b:1.0};
RGBPixel Black = {r:0.0,g:0.0,b:0.0};
RGBPixel OldLightColor,LightColor = {r:1.0,g:1.0,b:1.0};
RGBPixel BoxColor =  {r:1.0,g:1.0,b:1.0};
RGBPixel BackGround;

LightSettings CurrentLight,OldLight;
MaterialSettings CurrentMaterial,OldMaterial;
MaterialSettings CurrentRefMaterial,OldRefMaterial;
RefractionSettings CurrentRefraction,OldRefraction;
WaveSettings CurrentSurfaceWave,OldSurfaceWave;
WaveSettings CurrentLayerIORWave,OldLayerIORWave;
WaveSettings CurrentLayerThicknessWave,OldLayerThicknessWave;
ImageMapSettings CurrentSurfaceImageMap,OldSurfaceImageMap;

Image input,output;
unsigned char *dinput=NULL,*doutput=NULL,SineMap[256],LogMap[256];
unsigned char *SurfaceMap=NULL,*LayerIORMap=NULL,*LayerThicknessMap=NULL;

int width,height,channels,modulo;
int MainDialogID=-1,ColorDialogID=-1,PointLightDialogID=-1,MaterialDialogID=-1;
int WaveDialogID=-1,BumpMapDialogID=-1,RefractionDialogID=-1,DirectionalLightDialogID=-1;
int ImageMapDialogID=-1;

long light_toggles[3] = { 1,0,0 };
long apply_light_toggles[2] = { 1,1 };
long surface_toggles[3] = { 1,0,0 };
long refractionmode = 0;
long maxcounter;

int CleanupStackCount=0;
void *CleanupStack[MAX_CLEANUP_STACK];

/**************************************/
/* Some neccesary function prototypes */
/**************************************/

void KillAllDialogs(int mode);
void CreateImageMapDialog(ImageMapSettings *map);
void CreateRefractionDialog(RefractionSettings *ref);
void CreateColorDialog(RGBPixel *color);
void CreatePointLightDialog(LightSettings *light);
void CreateDirectionalLightDialog(LightSettings *light);
void CreateMaterialDialog(MaterialSettings *material, int mode);
void CreateWaveDialog(WaveSettings *wave);
void CreateBumpMapDialog(void);

/************************/
/* Convenience routines */
/************************/

void InitCleanup(void)
{
  int cnt;
  for (cnt=0;cnt<MAX_CLEANUP_STACK;cnt++) CleanupStack[cnt]=NULL;
}

void Cleanup(void)
{
  int cnt;
  for (cnt=0;cnt<MAX_CLEANUP_STACK;cnt++)
	 {
		if (CleanupStack[cnt]!=NULL) free(CleanupStack[cnt]);
	 }
}

void AddToCleanupStack(void *element)
{
  if (CleanupStackCount>MAX_CLEANUP_STACK)
	 {
		gimp_message("Lighting: CleanupStack overflow! Terminating..\n");
		Cleanup();
		KillAllDialogs(0);
		exit(1);
	 }
  CleanupStack[CleanupStackCount++]=element;
}

void RGBAdd(RGBPixel *a,RGBPixel *b)
{
  a->r=a->r+b->r;
  a->g=a->g+b->g;
  a->b=a->b+b->b;
}

void RGBMul(RGBPixel *a,double b)
{
  a->r=a->r*b;
  a->g=a->g*b;
  a->b=a->b*b;
}

void RGBClamp(RGBPixel *a)
{
  if (a->r>1.0) a->r=1.0;
  if (a->g>1.0) a->g=1.0;
  if (a->b>1.0) a->b=1.0;
  if (a->r<0.0) a->r=0.0;
  if (a->g<0.0) a->g=0.0;
  if (a->b<0.0) a->b=0.0;
}

double InnerProduct(Vector *a,Vector *b)
{
  return(a->x*b->x+a->y*b->y+a->z*b->z);
}

Vector CrossProduct(Vector *a,Vector *b)
{
  Vector normal;

  normal.x=a->y*b->z-a->z*b->y;
  normal.y=a->z*b->x-a->x*b->z;
  normal.z=a->x*b->y-a->y*b->x;

  return(normal);
}

void Normalize(Vector *a)
{
  double len;
  
  len=sqrt(a->x*a->x+a->y*a->y+a->z*a->z);

  if (len!=0.0)
	 {
		len=1.0/len;
		a->x=a->x*len;
		a->y=a->y*len;
		a->z=a->z*len;
	 }
  else *a=ZeroVec;
}

void MulVector(Vector *a,double b)
{
  a->x=a->x*b;
  a->y=a->y*b;
  a->z=a->z*b;
}

void SubVector(Vector *c,Vector *a,Vector *b)
{
  c->x=a->x-b->x;
  c->y=a->y-b->y;
  c->z=a->z-b->z;
}

void SetVector(Vector *a, double x,double y,double z)
{
  a->x=x;
  a->y=y;
  a->z=z;
}

void AddVector(Vector *c,Vector *a,Vector *b)
{
  c->x=a->x+b->x;
  c->y=a->y+b->y;
  c->z=a->z+b->z;
}

/******************/
/* Implementation */
/******************/

int GetIORMapping(void)
{
  if (CurrentRefraction.IOR_Toggles[0]==1) return(MAPPED_CONSTANT);
  else if (CurrentRefraction.IOR_Toggles[1]==1) return(MAPPED_WAVE);
  else return(MAPPED_IMAGE);
}

int GetThicknessMapping(void)
{
  if (CurrentRefraction.Thickness_Toggles[0]==1) return(MAPPED_CONSTANT);
  else if (CurrentRefraction.Thickness_Toggles[1]==1) return(MAPPED_WAVE);
  else return(MAPPED_IMAGE);
}

int GetLightType(void)
{
  if (light_toggles[0]==1) return(DIRECTIONAL_LIGHT);
  else if (light_toggles[1]==1) return(POINT_LIGHT);
  else return(SPOT_LIGHT);
}

int GetSurfaceType(void)
{
  if (surface_toggles[0]==1) return(FLAT_SURFACE);
  else if (surface_toggles[1]==1) return(WAVY_SURFACE);
  else return(MAPPED_SURFACE);
}

int GetRefractionMode(void)
{
  if (refractionmode==0) return(REFRACTION_DISABLED);
  else return(REFRACTION_ENABLED);
}

int GetMapType(ImageMapSettings *map)
{
  if (map->MapToggles[0]==1) return(MAP_LINEAR);
  else if (map->MapToggles[1]==1) return(MAP_LOG);
  else return(MAP_SINE);
}

void SetDefaultSettings(void)
{
  SetVector(&CurrentLight.Position, 0.2,0.2,2.0);
  SetVector(&CurrentLight.Direction, -1.0,-1.0,1.0);
  CurrentLight.Intensity = 1.0;
  CurrentLight.Color=White;

  CurrentMaterial.AmbientInt = 1.0;
  CurrentMaterial.DiffuseInt = 1.0;
  CurrentMaterial.AmbientRef = 0.1;
  CurrentMaterial.DiffuseRef = 0.45;
  CurrentMaterial.SpecularRef = 0.5;
  CurrentMaterial.Highlight = 27.0;

  CurrentRefMaterial = CurrentMaterial;
  CurrentRefMaterial.AmbientInt = 0.0;
  CurrentRefMaterial.Highlight = 200.0;
  CurrentRefMaterial.Color=Black;

  CurrentSurfaceImageMap.Imagemap=0;
  CurrentSurfaceImageMap.MapToggles[0]=0;
  CurrentSurfaceImageMap.MapToggles[1]=0;
  CurrentSurfaceImageMap.MapToggles[2]=1;
  CurrentSurfaceImageMap.MinValue=0.0;
  CurrentSurfaceImageMap.MaxValue=0.1;
  CurrentRefraction.IOR_Imagemap.FitToRange=0;

  CurrentRefraction.IOR_Air = 1.0;
  CurrentRefraction.IOR_Material = 1.52;
  CurrentRefraction.IOR_MapConstant=1;
  CurrentRefraction.IOR_MapType=MAPPED_CONSTANT;
  CurrentRefraction.IOR_Imagemap.Imagemap=0;
  CurrentRefraction.IOR_Imagemap.MinValue=1.0;
  CurrentRefraction.IOR_Imagemap.MaxValue=1.65;
  CurrentRefraction.IOR_Imagemap.FitToRange=0;

  CurrentRefraction.Thickness = 0.0;
  CurrentRefraction.Thickness_MapConstant=1;
  CurrentRefraction.Thickness_MapType=MAPPED_CONSTANT;
  CurrentRefraction.Thickness_Imagemap.Imagemap=0;
  CurrentRefraction.Thickness_Imagemap.MinValue=0.0;
  CurrentRefraction.Thickness_Imagemap.MaxValue=0.2;
  CurrentRefraction.Thickness_Imagemap.FitToRange=0;
  CurrentRefraction.IOR_Toggles[0]=1;
  CurrentRefraction.IOR_Toggles[1]=0;
  CurrentRefraction.IOR_Toggles[2]=0;
  CurrentRefraction.Thickness_Toggles[0]=0;
  CurrentRefraction.Thickness_Toggles[1]=0;
  CurrentRefraction.Thickness_Toggles[2]=1;

  CurrentSurfaceWave.XAmpl=0.1;
  CurrentSurfaceWave.YAmpl=0.1;
  CurrentSurfaceWave.XShift=M_PI/2.0;
  CurrentSurfaceWave.YShift=-M_PI/2.0;
  CurrentSurfaceWave.XPeriod=6.0;
  CurrentSurfaceWave.YPeriod=6.0;
  CurrentLayerIORWave=CurrentSurfaceWave;
  CurrentLayerThicknessWave=CurrentSurfaceWave;
}

/****************/
/* Main section */
/****************/

/***************************************/
/* Directional and point light variant */
/***************************************/

RGBPixel PhongShade1(Vector *pos,Vector *viewpoint,Vector *normal,Vector *light,
						  RGBPixel *diff_col,RGBPixel *spec_col,int type)
{
  RGBPixel ambientcolor,diffusecolor,specularcolor;
  double NL,RV;
  Vector L,NN,V,Normal=*normal;

  /* Compute ambient color contribution */
  /* ================================== */

  ambientcolor=*diff_col;
  RGBMul(&ambientcolor,CurrentMaterial.AmbientInt);
  RGBMul(&ambientcolor,CurrentMaterial.AmbientRef);

  /* Compute (N*L) term of Phong's equation */
  /* ====================================== */

  if (type==POINT_LIGHT) SubVector(&L,light,pos);
  else L=*light;
  Normalize(&L);

  NL=2.0*InnerProduct(&Normal,&L);
 
  /* Compute (R*V)^alpha term of Phong's equation */
  /* ============================================ */

  SubVector(&V,viewpoint,pos);
  Normalize(&V);

  MulVector(&Normal,NL);
  SubVector(&NN,&Normal,&L);
  RV=InnerProduct(&NN,&V);
  RV=pow(RV,CurrentMaterial.Highlight);

  /* Compute diffuse and specular color contribution */
  /* =============================================== */

  diffusecolor=*diff_col;
  RGBMul(&diffusecolor,CurrentMaterial.DiffuseRef);
  RGBMul(&diffusecolor,NL);

  specularcolor=*spec_col;
  RGBMul(&specularcolor,CurrentMaterial.SpecularRef);
  RGBMul(&specularcolor,RV);

  RGBAdd(&diffusecolor,&specularcolor);
  RGBMul(&diffusecolor,CurrentMaterial.DiffuseInt);

  RGBAdd(&ambientcolor,&diffusecolor);

  return(ambientcolor);
}

/**********************/
/* Spot light variant */
/**********************/

RGBPixel PhongShade2(Vector *pos,Vector *viewpoint,Vector *normal,Vector *light,
						  RGBPixel *diff_col,RGBPixel *spec_col)
{
  RGBPixel ambientcolor,diffusecolor,specularcolor;
  double NL,RV;
  Vector L,NN,V,Normal=*normal;

/*   printf("Normal = [%f %f %f]\n",normal->x,normal->y,normal->z); */

  /* Compute ambient color contribution */
  /* ================================== */

  ambientcolor=*diff_col;
  RGBMul(&ambientcolor,CurrentMaterial.AmbientInt);
  RGBMul(&ambientcolor,CurrentMaterial.AmbientRef);

  /* Compute (N*L) term of Phong's equation */
  /* ====================================== */

  SubVector(&L,light,pos);
  Normalize(&L);

  NL=2.0*InnerProduct(&Normal,&L);
 
  /* Compute (R*V)^alpha term of Phong's equation */
  /* ============================================ */

  SubVector(&V,viewpoint,pos);
  Normalize(&V);

  MulVector(&Normal,NL);
  SubVector(&NN,&Normal,&L);
  RV=InnerProduct(&NN,&V);
  RV=pow(RV,CurrentMaterial.Highlight);
/*   printf("RV: %f\n",RV); */

  /* Compute diffuse and specular color contribution */
  /* =============================================== */

  diffusecolor=*diff_col;
  RGBMul(&diffusecolor,CurrentMaterial.DiffuseRef);
  RGBMul(&diffusecolor,NL);

  specularcolor=*spec_col;
  RGBMul(&specularcolor,CurrentMaterial.SpecularRef);
  RGBMul(&specularcolor,RV);

  RGBAdd(&diffusecolor,&specularcolor);
  RGBMul(&diffusecolor,CurrentMaterial.DiffuseInt);

  RGBAdd(&ambientcolor,&diffusecolor);

  return(ambientcolor);
}

long int XYToIndex(int x,int y)
{
  return((long int)x*(long int)channels+(long int)y*(long int)modulo);
}

int CheckBounds(int x,int y)
{
  if (x<0 || y<0 || x>width-1 || y>height-1) return(1);
  else return(0);
}

unsigned char PeekMap(unsigned char *DispMap,int x,int y)
{
  long int index;

  index=(long int)x+(long int)width*(long int)y;

  if (index<0 || index>maxcounter)
	 {
		printf("Oops! This is going to cause a SIGSEGV! PeekMap(%d %d) Max(%d %d)\n",x,y,width-1,height-1);
	 }
  return(DispMap[index]);
}

void PokeMap(unsigned char *DispMap,int x,int y,unsigned char value)
{
  long int index;

  index=(long int)x+(long int)width*(long int)y;

  DispMap[index]=value;
}

RGBPixel Peek(unsigned char *data,int x,int y)
{
  long int index=XYToIndex(x,y);
  RGBPixel color;
  
  color.r=(double)data[index]/255.0;
  color.g=(double)data[index+1]/255.0;
  color.b=(double)data[index+2]/255.0;
  return(color);
}

void Poke(unsigned char *data,int x,int y,RGBPixel *color)
{ 
  long int index=XYToIndex(x,y);

  data[index]=(unsigned char)(255.0*color->r);
  data[index+1]=(unsigned char)(255.0*color->g);
  data[index+2]=(unsigned char)(255.0*color->b);
}

Vector IntToPos(int x,int y)
{
  Vector pos;
  
  pos.x=(double)x/(double)width;
  pos.y=(double)y/(double)height;
  pos.z=CurrentRefraction.Thickness;

  return(pos);
}

void PosToInt(double x,double y,int *scr_x,int *scr_y)
{
  *scr_x=(int)(x*(double)width);
  *scr_y=(int)(y*(double)height);
}

void RGBToGray(Image image,unsigned char **map)
{
  unsigned char *data,dval;
  int xcnt,ycnt;
  RGBPixel color;
  double val;

  data=gimp_image_data(image);
  
  *map=(unsigned char *)malloc(maxcounter*sizeof(unsigned char));
  AddToCleanupStack((void *)*map);
  for (ycnt=0;ycnt<height;ycnt++)
	 {
		for (xcnt=0;xcnt<width;xcnt++)
		  {
			 color=Peek(data,xcnt,ycnt);
			 
			 /* The weights were shamelessly ripped from the grayify plug-in :) */
			 /* =============================================================== */

			 val=(0.30*color.r+0.59*color.g+0.11*color.b)/3;
			 dval=(unsigned char)(255.0*val);
			 PokeMap(*map,xcnt,ycnt,dval);
		  }
	 }
}

/******************************/
/* Rendering stuff below here */
/******************************/

double GetThickness(Vector *pos,unsigned char *map,ImageMapSettings *MapSettings)
{
  int x,y,MapType=GetMapType(MapSettings);
  double val,addfac,scalefac;
  unsigned char cval;

  /* Map position to [0..width,0..height]. Map value to [-1.0..1.0] */
  /* ============================================================== */

  PosToInt(pos->x,pos->y,&x,&y);
  cval=PeekMap(map,x,y);
/*   printf("%.3d ",cval); */

  /* Map value according to chosen map type */
  /* ====================================== */

  if (MapType==MAP_LOG) cval=LogMap[cval];
  else if (MapType==MAP_SINE) cval=SineMap[cval];
/*   printf("%.3d\n",cval); */

  val=(2.0*(double)cval/255.0)-1.0;
  
  /* Map this value to [RangeStart..RangeEnd] */
  /* ======================================== */

  addfac=(MapSettings->MaxValue+MapSettings->MinValue)/2.0;
  scalefac=MapSettings->MaxValue+MapSettings->MinValue;

  val=((addfac-MapSettings->MinValue)*val)+addfac;
/*   printf("%.3d -> %.5f\n",cval,val); */

  return(val);
}

double GetIOR(Vector *pos,unsigned char *map,ImageMapSettings *MapSettings)
{
  return(GetThickness(pos,map,MapSettings));
}

void TriNormal(Vector *normal,Vector *v,Vector *v1,Vector *v2)
{
  Vector u,w;

  SubVector(&u,v,v1);
  SubVector(&w,v,v2);
  
  *normal=CrossProduct(&u,&w);
  Normalize(normal);
  if (normal->z<0) normal->z=normal->z*-1.0;
}

/***********************************************/
/* Compute the normal vector at <pos> based on */
/* the map given.                              */
/***********************************************/

Vector GetNormal(Vector *pos,unsigned char *map,ImageMapSettings *MapSettings)
{
  Vector normal=ZeroVec,v,v1,v2,N,P;
  int x,y,numvecs=0;
  double xval,yval,val,val1=-1.0,val2=-1.0,val3=-1.0,val4=-1.0;
  
  /* Map position to [0..width,0..height] */
  /* ==================================== */

  PosToInt(pos->x,pos->y,&x,&y);

  /* Compute surface normal */
  /* ====================== */

  val=GetThickness(pos,map,MapSettings);

  if (CheckBounds(x-1,y)==0) { P=IntToPos(x-1,y); val1=GetThickness(&P,map,MapSettings); }
  if (CheckBounds(x,y-1)==0) { P=IntToPos(x,y-1); val2=GetThickness(&P,map,MapSettings); }
  if (CheckBounds(x+1,y)==0) { P=IntToPos(x+1,y); val3=GetThickness(&P,map,MapSettings); }
  if (CheckBounds(x,y+1)==0) { P=IntToPos(x,y+1); val4=GetThickness(&P,map,MapSettings); }

  v=ZeroVec;v.z=val;
  xval=1.0/width;
  yval=1.0/height;

/*   printf("<Val=%f Val1=%f Val2=%f Val3=%f Val4=%f> ",val,val1,val2,val3,val4); */

  if (val1!=-1.0 && val4!=-1.0)
	 {
		v1=ZeroVec; v1.x=-1.0*xval; v1.z=val1;
		v2=ZeroVec; v2.y=1.0*yval; v2.z=val4;
		TriNormal(&N,&v,&v1,&v2);
		AddVector(&normal,&normal,&N);
		numvecs++;
	 }
  if (val1!=-1.0 && val2!=-1.0)
	 {
		v1=ZeroVec; v1.x=-1.0*xval; v1.z=val1;
		v2=ZeroVec; v2.y=-1.0*yval; v2.z=val2;
		TriNormal(&N,&v,&v1,&v2);
		AddVector(&normal,&normal,&N);
		numvecs++;
	 }
  if (val2!=-1.0 && val3!=-1.0)
	 {
		v1=ZeroVec; v1.y=-1.0*yval; v1.z=val2;
		v2=ZeroVec; v2.x=1.0*xval; v2.z=val3;
		TriNormal(&N,&v,&v1,&v2);
		AddVector(&normal,&normal,&N);
		numvecs++;
	 }
  if (val3!=-1.0 && val4!=-1.0)
	 {
		v1=ZeroVec; v1.x=1.0*xval; v1.z=val3;
		v2=ZeroVec; v2.y=1.0*yval; v2.z=val4;
		TriNormal(&N,&v,&v1,&v2);
		AddVector(&normal,&normal,&N);
		numvecs++;
	 }

  MulVector(&normal,1.0/(double)numvecs);
  Normalize(&normal);

  return(normal);
}

/*****************************************************/
/* Compute refracted ray direction. Uses Snells law. */
/*****************************************************/

Vector Refract(Vector *pos,Vector *viewpoint,Vector *normal)
{
  double NI,NInr,a,nr;
  Vector ray,I;

  SubVector(&I,viewpoint,pos);

  nr=CurrentRefraction.IOR_Air/CurrentRefraction.IOR_Material;

  NI=InnerProduct(normal,&I);
  NInr=NI*nr;
  a=NInr-(double)sqrt(1.0-(nr*nr)*(1.0-NI*NI));

  ray=*normal;
  MulVector(&ray,a);
  MulVector(&I,nr);

  SubVector(&ray,&ray,&I);

  Normalize(&ray);

  return(ray);
}

/*******************************************************************/
/* This routine computes the color of the surface at a given point */
/* (no refraction here). It's a part of the larger Shade() below.  */
/*******************************************************************/

RGBPixel ShadePicture(Vector *pos)
{
  Vector normal;
  RGBPixel SurfaceColor,color;
  int x,y,SurfaceType=GetSurfaceType(),LightType=GetLightType();
  
  /* Get the unaltered color at this point */
  /* ===================================== */

  PosToInt(pos->x,pos->y,&x,&y);
  SurfaceColor=Peek(dinput,x,y);

  /* Check if we're supposed to apply lightling here.. */
  /* ================================================= */

  if (apply_light_toggles[0]==0)
	 {
		/* Nope, just return the plain surface color */
		/* ========================================= */

		return(SurfaceColor);
	 }

  /* Yes! (This is gonna be fun) Do normal stuff; */
  /* compute normal vector at this point.         */
  /* ============================================ */
  
  if (SurfaceType!=FLAT_SURFACE)
	 {
		normal=GetNormal(pos,SurfaceMap,&CurrentSurfaceImageMap);
		pos->z=GetThickness(pos,SurfaceMap,&CurrentSurfaceImageMap);
/* 		if (pos->z!=0.0) printf("PosZ=%f ",pos->z); */
	 }
  else normal=Unit_Z;

  /* Compute color using Phong's illumination equation */
  /* ================================================= */

  if (LightType==DIRECTIONAL_LIGHT)
	 color=PhongShade1(pos,&ViewPoint,&normal,&CurrentLight.Direction,
		&SurfaceColor,&CurrentLight.Color,LightType);
  else if (LightType==POINT_LIGHT)
	 color=PhongShade1(pos,&ViewPoint,&normal,&CurrentLight.Position,
		&SurfaceColor,&CurrentLight.Color,LightType);
  else
	 {
		/* Spot light */
		/* ========== */
	 }
  RGBClamp(&color);
  return(color);
}

/***************************************************************/
/* Check if a line starting at 'pos' in the direction of 'dir' */
/* will hit our rectangle at (0.0 .. 1.0,0.0 .. 1.0). If yes,  */
/* return the color at that point. If not, return the back-    */
/* ground color.                                               */
/***************************************************************/

RGBPixel GetRayColor(Vector *pos,Vector *dir)
{
  double t;
  Vector newpos;

  t=pos->z/dir->z;
  newpos.x=pos->x-t*dir->x;
  newpos.y=pos->y-t*dir->y;
  newpos.z=pos->z-t*dir->z; /* Should evaluate to zero */
  
  /* Ok, now we have the intersection point. Check them against our */
  /* rectangle and decide what to do with the color.                */
  /* ============================================================== */

  if (newpos.x>1.0 || newpos.x<0.0 || newpos.y>1.0 || newpos.y<0.0)
	 {
		/* Outside, so return background color */
		/* =================================== */

		return(BackGround);
	 }
  
  /* Yay, touchdown! Calculate surface color */
  /* ======================================= */

  return(ShadePicture(&newpos));
}

/***************************************************************/
/* This is the sort of main routine. It calculates the color   */
/* at a given position, taking into account mappings, light,   */
/* refraction and the lot. This is actually a quasi-raytracer. */
/***************************************************************/

RGBPixel Shade(Vector *pos)
{
  RGBPixel color,color2;
  Vector normal,ray;
  int ThicknessMapping=GetThicknessMapping(),LightType=GetLightType();
  double Thickness;

  /* Check if refraction is enabled */
  /* ============================== */

  if (GetRefractionMode()==REFRACTION_DISABLED) color=ShadePicture(pos);
  else
	 {
		/* Check if we're supposed to do anything with the position (thickness) */
		/* ==================================================================== */
		
		if (ThicknessMapping==MAPPED_CONSTANT) Thickness=CurrentRefraction.Thickness;
		else Thickness=GetThickness(pos,LayerThicknessMap,&CurrentRefraction.Thickness_Imagemap);

		/* If thickness is zero, there's per definition no layer, and we skip the whole thing */
		/* ================================================================================== */

		if (Thickness<=0.0) color=ShadePicture(pos);
		else
		  {
			 pos->z=Thickness;

			 /* Compute normal at this point on the layer */
			 /* ========================================= */

			 if (ThicknessMapping==MAPPED_CONSTANT) normal=Unit_Z;
			 else normal=GetNormal(pos,LayerThicknessMap,&CurrentRefraction.Thickness_Imagemap);

			 /* Now, do the same check with the IOR */
			 /* =================================== */

			 if (GetIORMapping()!=MAPPED_CONSTANT)
				CurrentRefraction.IOR_Material=GetIOR(pos,LayerIORMap,&CurrentRefraction.IOR_Imagemap);

			 /* Check if we're supposed to compute contribution from the light */
			 /* ============================================================== */

			 if (apply_light_toggles[1]==1)
				{
				  
				  /* Compute the color at this point on the layer */
				  /* ============================================ */

				  if (LightType==DIRECTIONAL_LIGHT)
				    color=PhongShade1(pos,&ViewPoint,&normal,&CurrentLight.Direction,
					   &CurrentRefMaterial.Color,&CurrentLight.Color,LightType);
				  else if (LightType==POINT_LIGHT)
					 color=PhongShade1(pos,&ViewPoint,&normal,&CurrentLight.Position,
					   &CurrentRefMaterial.Color,&CurrentLight.Color,LightType);
				  else
					 {
						/* Spot light */
						/* ========== */
					 }
				  RGBClamp(&color);
				}
			 else color=CurrentRefMaterial.Color;

			 /* Ok, ready to compute refracted ray */
			 /* ================================== */
  
			 ray=Refract(pos,&ViewPoint,&normal);
  
			 /* Now, get the color in the direction of the refracted ray */
			 /* If the ray hits the "picture" return the color at that   */
			 /* point. If not, return the current background color.      */
			 /* ======================================================== */
		
			 color2=GetRayColor(pos,&ray);

			 /* Add to final color */
			 /* ================== */
			 
			 RGBAdd(&color,&color2);
		  }
	 }
  return(color);
}

/****************************************************/
/* Compute cool wavy surface (z=cos(sqrt(x^2+y^2))) */
/****************************************************/

void ComputeWaves(WaveSettings *wave,unsigned char **DispMap)
{
  int xcounter,ycounter;
  double dampval,dampvalx,dampvaly,val,xpos,ypos;

  *DispMap=(unsigned char *)malloc(maxcounter*sizeof(unsigned char));
  AddToCleanupStack((void *)*DispMap);
  for (ycounter=0;ycounter<height;ycounter++)
	 {
		for (xcounter=0;xcounter<width;xcounter++)
		  {
			 xpos=((double)xcounter/(double)width)*(2.0*M_PI)-M_PI;
			 ypos=((double)ycounter/(double)height)*(2.0*M_PI)-M_PI;
			 
			 dampvalx=xpos/M_PI;
			 dampvaly=ypos/M_PI;
			 
			 dampval=1.0;
			 
			 /* dampval=1.0-sqrt(dampvalx*dampvalx+dampvaly*dampvaly); */
			 /* if (dampval<0.0) dampval=0.0; */
			 
			 xpos=wave->XPeriod*xpos;
			 ypos=wave->YPeriod*ypos;
			 
			 val=0.5*(1.0+cos(sqrt(wave->XAmpl*xpos*xpos+wave->YAmpl*ypos*ypos)))*dampval;
			 
			 /* Fit to range 0.0 .. 1.0 */
			 /* ======================= */
			 
			 PokeMap(*DispMap,xcounter,ycounter,(unsigned char)(255.0*val));
		  }
	 }
}

/************************************************************/
/* Check what mode(s) we're in and compute maps for surface */
/* and refractive layer (if necessary).                     */
/************************************************************/

void ComputeMaps()
{
  Image DummyImage;
  int x,SurfaceType=GetSurfaceType(),IORMapping,ThicknessMapping;
  double val,c,d;

  /* Compute Sine and Log transfer function maps */
  /* =========================================== */

  c=1.0/255.0;
  d=1.15*255.0;
  for (x=0;x<256;x++)
	 {
		SineMap[x]=(unsigned char)(255.0*(0.5*(sin((M_PI*c*(double)x)-0.5*M_PI)+1.0)));
		val=(d*exp(-1.0/(8.0*c*((double)x+5.0))));
		if (val>255.0) val=255.0;
		LogMap[x]=(unsigned char)val;
	 }

  /* Check out what kind of surface we have defined */
  /* ============================================== */

  if (SurfaceType!=FLAT_SURFACE)
	 {
		if (SurfaceType==WAVY_SURFACE) ComputeWaves(&CurrentSurfaceWave,&SurfaceMap);
 		else
		  {
			 DummyImage=gimp_get_input_image(CurrentSurfaceImageMap.Imagemap);
			 if (gimp_image_type(DummyImage)==RGB_IMAGE)
				RGBToGray(DummyImage,&SurfaceMap);
			 else SurfaceMap=gimp_image_data(DummyImage);
		  }
	 }

  /* Now, if we have enabled refraction we have to do some stuff */
  /* =========================================================== */

  if (GetRefractionMode()==REFRACTION_ENABLED)
	 {
		/* Check out the IOR settings */
		/* ========================== */

		IORMapping=GetIORMapping();
		if (IORMapping!=MAPPED_CONSTANT)
		  {
			 if (IORMapping==MAPPED_WAVE) ComputeWaves(&CurrentLayerIORWave,&LayerIORMap);
			 else
				{
				  CurrentRefraction.IOR_Image=gimp_get_input_image(CurrentRefraction.IOR_Imagemap.Imagemap);
				  if (gimp_image_type(CurrentRefraction.IOR_Image)==RGB_IMAGE)
					 RGBToGray(CurrentRefraction.IOR_Image,&LayerIORMap);
				  else LayerIORMap=gimp_image_data(CurrentRefraction.IOR_Image);
				}
		  }

		/* Check out the thickness settings */
		/* ================================ */

		ThicknessMapping=GetThicknessMapping();
		if (ThicknessMapping!=MAPPED_CONSTANT)
		  {
			 if (ThicknessMapping==MAPPED_WAVE)
				ComputeWaves(&CurrentLayerThicknessWave,&LayerThicknessMap);
			 else
				{
				  CurrentRefraction.Thickness_Image=
					 gimp_get_input_image(CurrentRefraction.Thickness_Imagemap.Imagemap);
				  if (gimp_image_type(CurrentRefraction.Thickness_Image)==RGB_IMAGE)
					 RGBToGray(CurrentRefraction.Thickness_Image,&LayerThicknessMap);
				  else LayerThicknessMap=gimp_image_data(CurrentRefraction.Thickness_Image);
				}
		  }
	 }
}

/*************/
/* Main loop */
/*************/

void doit(void)
{
  int xcount,ycount;
  long counter=0;
  Vector pos;
  RGBPixel color;
  unsigned char r,g,b;
  
  width=gimp_image_width(input);
  height=gimp_image_height(input);
  maxcounter=(long int)width*(long int)height;
  channels=gimp_image_channels(input);

  gimp_init_progress("Lighting Effects");
  ComputeMaps();

  gimp_background_color(&r,&g,&b);

  BackGround.r=(double)r/255.0;
  BackGround.g=(double)g/255.0;
  BackGround.b=(double)b/255.0;

  modulo=channels*width;
  
  dinput=gimp_image_data(input);
  doutput=gimp_image_data(output);

  for (ycount=0;ycount<height;ycount++)
	 {
		for (xcount=0;xcount<width;xcount++)
		  {
			 pos=IntToPos(xcount,ycount);
			 color=Shade(&pos);
			 Poke(doutput,xcount,ycount,&color);
			 counter++;
			 if ((counter % 100)==0) gimp_do_progress(counter,maxcounter);
		  }
	 }

  gimp_update_image(output);
}

/**************************/
/* Below is only UI stuff */
/**************************/

void KillAllDialogs(int mode)
{
  if (MainDialogID!=-1) gimp_close_dialog(MainDialogID, mode);
  if (ColorDialogID!=-1) gimp_close_dialog(ColorDialogID, mode);
  if (PointLightDialogID!=-1) gimp_close_dialog(PointLightDialogID, mode);
  if (MaterialDialogID!=-1) gimp_close_dialog(MaterialDialogID, mode);
  if (WaveDialogID!=-1) gimp_close_dialog(WaveDialogID, mode);
  if (RefractionDialogID!=-1) gimp_close_dialog(RefractionDialogID, mode);
  if (DirectionalLightDialogID!=-1) gimp_close_dialog(DirectionalLightDialogID, mode);
  if (ImageMapDialogID!=-1) gimp_close_dialog(ImageMapDialogID, mode);
}

/******************/
/* Callback stuff */
/******************/

static void main_ok_callback(int item_id, void *client_data, void *call_data)
{
  KillAllDialogs(1);
}

static void main_cancel_callback(int item_id, void *client_data, void *call_data)
{
  KillAllDialogs(0);
}

static void color_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(ColorDialogID, 1);
  ColorDialogID=-1;
}

static void color_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(ColorDialogID, 0);
  ColorDialogID=-1;
}

static void pointlight_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(PointLightDialogID, 1);
  PointLightDialogID=-1;
}

static void pointlight_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(PointLightDialogID, 0);
  PointLightDialogID=-1;
}

static void directional_light_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(DirectionalLightDialogID, 1);
  DirectionalLightDialogID=-1;
}

static void directional_light_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(DirectionalLightDialogID, 0);
  DirectionalLightDialogID=-1;
}

static void material_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(MaterialDialogID, 1);
  MaterialDialogID=-1;
}

static void material_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(MaterialDialogID, 0);
  MaterialDialogID=-1;
}

static void wave_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(WaveDialogID, 1);
  WaveDialogID=-1;
}

static void wave_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(WaveDialogID, 0);
  WaveDialogID=-1;
}

static void refraction_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(RefractionDialogID, 1);
  RefractionDialogID=-1;
}

static void refraction_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(RefractionDialogID, 0);
  RefractionDialogID=-1;
}

static void image_map_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(ImageMapDialogID, 1);
  ImageMapDialogID=-1;
}

static void image_map_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(ImageMapDialogID, 0);
  ImageMapDialogID=-1;
}

static void double_callback(int item_id, void *client_data, void *call_data)
{
  *((double *) client_data) = (double)atof(call_data);
}

static void radio_callback (int item_ID, void *client_data, void *call_data)
{
  *((long*) client_data) = *((long*) call_data);
}

static void scale_callback (int item_ID,void *client_data,void *call_data)
{
  *((double*) client_data) = (double)(*((long*) call_data))/255.0;
}

static void image_menu_callback (int item_ID, void *client_data, void *call_data)
{
  *((long*) client_data) = *((long*) call_data);
}

static void material_color_callback(int item_ID, void *client_data, void *call_data)
{
  RGBPixel *color=(RGBPixel *)client_data,old;

  if (ColorDialogID!=-1)
	 {
		gimp_message("Close the other color dialog first!\n");
		return;
	 }

  old=*color;
  CreateColorDialog(color);
  if (gimp_show_dialog(ColorDialogID)==0) *color=old;
}

static void LightSettings_callback(int item_ID, void *client_data, void *call_data)
{
  int LightType=GetLightType();

  /* Check what kind of light is selected and open the appropiate dialog */
  /* =================================================================== */

  if (apply_light_toggles[0]==0 && apply_light_toggles[1]==0) return; /* No light, so do nothing */
  else if (LightType==DIRECTIONAL_LIGHT)
	 {
		/* Open directional light dialog */
		/* ============================= */

		if (DirectionalLightDialogID!=-1)
		  {
			 gimp_message("Close the other directional light dialog first!\n");
			 return;
		  }
		OldLight=CurrentLight;
		CreateDirectionalLightDialog(&CurrentLight);
 		if (gimp_show_dialog(DirectionalLightDialogID)==0) CurrentLight=OldLight;
	 }
  else if (LightType==POINT_LIGHT)
	 {
		/* Open point light dialog */
		/* ======================= */
	
		if (PointLightDialogID!=-1)
		  {
			 gimp_message("Close the other point light dialog first!\n");
			 return;
		  }

		OldLight=CurrentLight;
		CreatePointLightDialog(&CurrentLight);
 		if (gimp_show_dialog(PointLightDialogID)==0) CurrentLight=OldLight;
	 }
  else
	 {
		/* Open spot light dialog */
		/* ====================== */

		gimp_message("Lighting: Light type not yet implemented! Sorry..\n");
	 }
}

static void LightColor_callback(int item_ID, void *client_data, void *call_data)
{
  /* No light means no action */
  /* ======================== */

  if (apply_light_toggles[0]==0 && apply_light_toggles[1]==0) return;

  if (ColorDialogID!=-1)
	 {
		gimp_message("Close the other color dialog first!\n");
		return;
	 }
  
  OldLight=CurrentLight;
  CreateColorDialog(&CurrentLight.Color);
  if (gimp_show_dialog(ColorDialogID)==0) CurrentLight=OldLight;
}

static void RefractionMaterial_callback(int item_ID, void *client_data, void *call_data)
{
  if (MaterialDialogID!=-1)
	 {
		gimp_message("Close the other one first!\n");
		return;
	 }
  
  OldRefMaterial=CurrentRefMaterial;
  CreateMaterialDialog(&CurrentRefMaterial,WITH_COLOR);

  if (gimp_show_dialog(MaterialDialogID)==0) CurrentRefMaterial=OldRefMaterial;
}

static void IOR_map_callback(int item_ID, void *client_data, void *call_data)
{
  int IOR_Mapping=GetIORMapping();
  ImageMapSettings Old;

  if (IOR_Mapping==MAPPED_CONSTANT) return; /* Do nothing if constant mapping */
  else if (IOR_Mapping==MAPPED_WAVE)
	 {
		/* Wavy mapping. Open waves dialog */
		/* =============================== */

		if (WaveDialogID!=-1)
		  {
			 gimp_message("Close the other wave dialog first!\n");
			 return;
		  }

		OldLayerIORWave=CurrentLayerIORWave;
		CreateWaveDialog(&CurrentLayerIORWave);
		if (gimp_show_dialog(WaveDialogID)==0) CurrentLayerIORWave=OldLayerIORWave;
	 }
  else
	 {
		/* Groovy imagemap mapping. Open map settings dialog. */
		/* ================================================== */

		if (ImageMapDialogID!=-1)
		  {
			 gimp_message("Close the other map dialog first!\n");
			 return;
		  }

		Old=CurrentRefraction.IOR_Imagemap;
		CreateImageMapDialog(&CurrentRefraction.IOR_Imagemap);
		if (gimp_show_dialog(ImageMapDialogID)==0) CurrentRefraction.IOR_Imagemap=Old;
	 }
}

static void thickness_map_callback(int item_ID, void *client_data, void *call_data)
{
  int Thickness_Mapping=GetThicknessMapping();
  ImageMapSettings Old;

  if (Thickness_Mapping==MAPPED_CONSTANT) return; /* Do nothing if constant mapping */
  else if (Thickness_Mapping==MAPPED_WAVE)
	 {
		/* Wavy mapping. Open waves dialog */
		/* =============================== */

		if (WaveDialogID!=-1)
		  {
			 gimp_message("Close the other wave dialog first!\n");
			 return;
		  }

		OldLayerThicknessWave=CurrentLayerThicknessWave;
		CreateWaveDialog(&CurrentLayerThicknessWave);
		if (gimp_show_dialog(WaveDialogID)==0) CurrentLayerThicknessWave=OldLayerThicknessWave;
	 }
  else
	 {
		/* Groovy imagemap mapping. Open map settings dialog. */
		/* ================================================== */

		if (ImageMapDialogID!=-1)
		  {
			 gimp_message("Close the other map dialog first!\n");
			 return;
		  }

		Old=CurrentRefraction.Thickness_Imagemap;
		CreateImageMapDialog(&CurrentRefraction.Thickness_Imagemap);
		if (gimp_show_dialog(ImageMapDialogID)==0) CurrentRefraction.Thickness_Imagemap=Old;
	 }
}

static void SurfaceSettings_callback(int item_ID, void *client_data, void *call_data)
{
  /* No light means no action */
  /* ======================== */

  if (apply_light_toggles[0]==0 && apply_light_toggles[1]==0) return;
  
  if (MaterialDialogID!=-1)
	 {
		gimp_message("Close the other material dialog first!\n");
		return;
	 }

  OldMaterial=CurrentMaterial;
  CreateMaterialDialog(&CurrentMaterial,WITHOUT_COLOR);
  if (gimp_show_dialog(MaterialDialogID)==0) CurrentMaterial=OldMaterial;
}

static void RefractionSettings_callback(int item_ID, void *client_data, void *call_data)
{
  if (GetRefractionMode()==REFRACTION_DISABLED) return; /* No refraction means no action */

  if (RefractionDialogID!=-1)
	 {
		gimp_message("Close the other refraction dialog first!\n");
		return;
	 }
  
  GetIORMapping();
  OldRefraction=CurrentRefraction;
  CreateRefractionDialog(&CurrentRefraction);
  if (gimp_show_dialog(RefractionDialogID)==0) CurrentRefraction=OldRefraction;
}

static void SurfaceMapping_callback(int item_ID, void *client_data, void *call_data)
{
  ImageMapSettings Old;
  int SurfaceType=GetSurfaceType();

  if (SurfaceType==FLAT_SURFACE) return; /* Flat means no action (at least for now) */
  else if (SurfaceType==WAVY_SURFACE)
	 {
		/* Cool wavy surface */
		/* ================= */

		if (WaveDialogID!=-1)
		  {
			 gimp_message("Close the other wave dialog first!\n");
			 return;
		  }
		
		OldSurfaceWave=CurrentSurfaceWave;
		CreateWaveDialog(&CurrentSurfaceWave);
		if (gimp_show_dialog(WaveDialogID)==0) CurrentSurfaceWave=OldSurfaceWave;
	 }
  else
	 {
		/* Even cooler bump-mapped surface */
		/* =============================== */
		
		if (ImageMapDialogID!=-1)
		  {
			 gimp_message("Close the other map dialog first!\n");
			 return;
		  }

		Old=CurrentSurfaceImageMap;
		CreateImageMapDialog(&CurrentSurfaceImageMap);
		if (gimp_show_dialog(ImageMapDialogID)==0) CurrentSurfaceImageMap=Old;
	 }
}

/***************************/
/* Main() and dialog stuff */
/***************************/

void CreateImageMapDialog(ImageMapSettings *map)
{
  int MainGroupID,FitToggleID,FrameID,ValID,DummyID,MenuID,RowID;
  int LinearID,LogID,SineID;
  char buf[100];

  ImageMapDialogID = gimp_new_dialog("Map from image settings");
  MainGroupID = gimp_new_row_group(ImageMapDialogID, DEFAULT, NORMAL, "");

  MenuID = gimp_new_image_menu (ImageMapDialogID, MainGroupID,
    IMAGE_CONSTRAIN_RGB | IMAGE_CONSTRAIN_GRAY, "Map from image:");
  gimp_add_callback (ImageMapDialogID, MenuID, image_menu_callback, &map->Imagemap);
  
  DummyID = gimp_new_column_group(ImageMapDialogID,MainGroupID, NORMAL, "");
  gimp_new_label(ImageMapDialogID, DummyID, "MinValue:");
  sprintf(buf, "%f", map->MinValue);
  ValID = gimp_new_text(ImageMapDialogID,DummyID,buf);
  gimp_add_callback(ImageMapDialogID, ValID, double_callback, &map->MinValue);

  DummyID = gimp_new_column_group(ImageMapDialogID,MainGroupID, NORMAL, "");
  gimp_new_label(ImageMapDialogID, DummyID, "MaxValue:");
  sprintf(buf, "%f", map->MaxValue);
  ValID = gimp_new_text(ImageMapDialogID,DummyID,buf);
  gimp_add_callback(ImageMapDialogID, ValID, double_callback, &map->MaxValue);

  FrameID =  gimp_new_frame (ImageMapDialogID, MainGroupID, "Map function");
  RowID = gimp_new_row_group(ImageMapDialogID,FrameID, RADIO, "");

  LinearID = gimp_new_radio_button (ImageMapDialogID, RowID, "Linear");
  gimp_change_item (ImageMapDialogID, LinearID, sizeof(long), &map->MapToggles[0]);
  gimp_add_callback (ImageMapDialogID, LinearID, radio_callback, &map->MapToggles[0]);
  LogID = gimp_new_radio_button (ImageMapDialogID, RowID, "Logarithmic");
  gimp_change_item (ImageMapDialogID, LogID, sizeof(long), &map->MapToggles[1]);
  gimp_add_callback (ImageMapDialogID, LogID, radio_callback, &map->MapToggles[1]);
  SineID = gimp_new_radio_button (ImageMapDialogID, RowID, "Sinusoidal");
  gimp_change_item (ImageMapDialogID, SineID, sizeof(long), &map->MapToggles[2]);
  gimp_add_callback (ImageMapDialogID, SineID, radio_callback, &map->MapToggles[2]);

  FitToggleID = gimp_new_radio_button(ImageMapDialogID, MainGroupID, "Autostretch to fit value range");
  gimp_change_item (ImageMapDialogID, FitToggleID, sizeof(long), &map->FitToRange);
  gimp_add_callback (ImageMapDialogID, FitToggleID, radio_callback, &map->FitToRange);

  /* Other callbacks */
  /* =============== */

  gimp_add_callback(ImageMapDialogID, gimp_ok_item_id(ImageMapDialogID), 
    image_map_ok_callback, NULL);
  gimp_add_callback(ImageMapDialogID, gimp_cancel_item_id(ImageMapDialogID),
    image_map_cancel_callback, NULL);
}

void CreateRefractionDialog(RefractionSettings *ref)
{
  int MainGroupID,FrameID,FrameGroupID,ValID,DummyID,RowID;
  int ConstantID,MappedID,MapSettingsID,WaveID,MaterialID;
  char buf[100];

  RefractionDialogID = gimp_new_dialog("Refraction settings");
  MainGroupID = gimp_new_row_group(RefractionDialogID, DEFAULT, NORMAL, "");
  MaterialID = gimp_new_push_button(RefractionDialogID,MainGroupID,"Material settings..");
  gimp_add_callback (RefractionDialogID, MaterialID, RefractionMaterial_callback, NULL);

  FrameID = gimp_new_frame (RefractionDialogID, MainGroupID, "Index of refraction");
  FrameGroupID = gimp_new_row_group(RefractionDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(RefractionDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(RefractionDialogID, DummyID, "IOR Air:");
  sprintf(buf, "%f", ref->IOR_Air);
  ValID = gimp_new_text(RefractionDialogID,DummyID,buf);
  gimp_add_callback(RefractionDialogID, ValID, double_callback, &ref->IOR_Air);
  DummyID = gimp_new_column_group(RefractionDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(RefractionDialogID, DummyID, "IOR Layer:");
  sprintf(buf, "%f", ref->IOR_Material);
  ValID = gimp_new_text(RefractionDialogID,DummyID,buf);
  gimp_add_callback(RefractionDialogID, ValID, double_callback, &ref->IOR_Material);

  RowID = gimp_new_row_group(RefractionDialogID,FrameGroupID, RADIO, "");
  ConstantID = gimp_new_radio_button (RefractionDialogID, RowID, "Uniform mapping");
  gimp_change_item (RefractionDialogID, ConstantID, sizeof(long), &ref->IOR_Toggles[0]);
  gimp_add_callback (RefractionDialogID, ConstantID, radio_callback, &ref->IOR_Toggles[0]);
  WaveID = gimp_new_radio_button (RefractionDialogID, RowID, "Wavy mapping");
  gimp_change_item (RefractionDialogID, WaveID, sizeof(long), &ref->IOR_Toggles[1]);
  gimp_add_callback (RefractionDialogID, WaveID, radio_callback, &ref->IOR_Toggles[1]);
  MappedID = gimp_new_radio_button (RefractionDialogID, RowID, "Mapped from image");
  gimp_change_item (RefractionDialogID, MappedID, sizeof(long), &ref->IOR_Toggles[2]);
  gimp_add_callback (RefractionDialogID, MappedID, radio_callback, &ref->IOR_Toggles[2]);

  MapSettingsID = gimp_new_push_button(RefractionDialogID,FrameGroupID,"Map settings..");
  gimp_add_callback (RefractionDialogID, MapSettingsID, IOR_map_callback, NULL);

  FrameID = gimp_new_frame (RefractionDialogID, MainGroupID, "Layer thickness");
  FrameGroupID = gimp_new_row_group(RefractionDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(RefractionDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(RefractionDialogID, DummyID, "Default thickness:");
  sprintf(buf, "%f", ref->Thickness);
  ValID = gimp_new_text(RefractionDialogID,DummyID,buf);
  gimp_add_callback(RefractionDialogID, ValID, double_callback, &ref->Thickness);

  RowID = gimp_new_row_group(RefractionDialogID,FrameGroupID, RADIO, "");
  ConstantID = gimp_new_radio_button (RefractionDialogID, RowID, "Uniform mapping");
  gimp_change_item (RefractionDialogID, ConstantID, sizeof(long), &ref->Thickness_Toggles[0]);
  gimp_add_callback (RefractionDialogID, ConstantID, radio_callback, &ref->Thickness_Toggles[0]);
  WaveID = gimp_new_radio_button (RefractionDialogID, RowID, "Wavy mapping");
  gimp_change_item (RefractionDialogID, WaveID, sizeof(long), &ref->Thickness_Toggles[1]);
  gimp_add_callback (RefractionDialogID, WaveID, radio_callback, &ref->Thickness_Toggles[1]);
  MappedID = gimp_new_radio_button (RefractionDialogID, RowID, "Mapped from image");
  gimp_change_item (RefractionDialogID, MappedID, sizeof(long), &ref->Thickness_Toggles[2]);
  gimp_add_callback (RefractionDialogID, MappedID, radio_callback, &ref->Thickness_Toggles[2]);

  MapSettingsID = gimp_new_push_button(RefractionDialogID,FrameGroupID,"Map settings..");
  gimp_add_callback (RefractionDialogID, MapSettingsID, thickness_map_callback, NULL);

  /* Other callbacks */
  /* =============== */
  
  gimp_add_callback(RefractionDialogID, gimp_ok_item_id(RefractionDialogID), 
    refraction_ok_callback, NULL);
  gimp_add_callback(RefractionDialogID, gimp_cancel_item_id(RefractionDialogID), 
    refraction_cancel_callback, NULL);
}

void CreateWaveDialog(WaveSettings *wave)
{
  int MainGroupID,FrameID,FrameGroupID,ValID,DummyID;
  char buf[100];

  WaveDialogID = gimp_new_dialog("Wave settings");
  
  MainGroupID = gimp_new_row_group(WaveDialogID, DEFAULT, NORMAL, "");
  FrameID = gimp_new_frame (WaveDialogID, MainGroupID, "Amplitude");
  FrameGroupID = gimp_new_row_group(WaveDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(WaveDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(WaveDialogID, DummyID, "X:");
  sprintf(buf, "%f", wave->XAmpl);
  ValID = gimp_new_text(WaveDialogID,DummyID,buf);
  gimp_add_callback(WaveDialogID, ValID, double_callback, &wave->XAmpl);

  DummyID = gimp_new_column_group(WaveDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(WaveDialogID, DummyID, "Y:");
  sprintf(buf, "%f", wave->YAmpl);
  ValID = gimp_new_text(WaveDialogID,DummyID,buf);
  gimp_add_callback(WaveDialogID, ValID, double_callback, &wave->YAmpl);

  FrameID = gimp_new_frame (WaveDialogID, MainGroupID, "Phase shift");
  FrameGroupID = gimp_new_row_group(WaveDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(WaveDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(WaveDialogID, DummyID, "X:");
  sprintf(buf, "%f", wave->XShift);
  ValID = gimp_new_text(WaveDialogID,DummyID,buf);
  gimp_add_callback(WaveDialogID, ValID, double_callback, &wave->XShift);

  DummyID = gimp_new_column_group(WaveDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(WaveDialogID, DummyID, "Y:");
  sprintf(buf, "%f", wave->YShift);
  ValID = gimp_new_text(WaveDialogID,DummyID,buf);
  gimp_add_callback(WaveDialogID, ValID, double_callback, &wave->YShift);

  FrameID = gimp_new_frame (WaveDialogID, MainGroupID, "Period");
  FrameGroupID = gimp_new_row_group(WaveDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(WaveDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(WaveDialogID, DummyID, "X:");
  sprintf(buf, "%f", wave->XPeriod);
  ValID = gimp_new_text(WaveDialogID,DummyID,buf);
  gimp_add_callback(WaveDialogID, ValID, double_callback, &wave->XPeriod);

  DummyID = gimp_new_column_group(WaveDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(WaveDialogID, DummyID, "Y:");
  sprintf(buf, "%f", wave->YPeriod);
  ValID = gimp_new_text(WaveDialogID,DummyID,buf);
  gimp_add_callback(WaveDialogID, ValID, double_callback, &wave->YPeriod);

  /* Other callbacks */
  /* =============== */

  gimp_add_callback(WaveDialogID, gimp_ok_item_id(WaveDialogID), wave_ok_callback, NULL);
  gimp_add_callback(WaveDialogID, gimp_cancel_item_id(WaveDialogID), wave_cancel_callback, NULL);
}

void CreateMaterialDialog(MaterialSettings *material, int mode)
{
  int MainGroupID,FrameID,FrameGroupID,ValID,DummyID;
  char buf[100];

  MaterialDialogID = gimp_new_dialog("Material settings");
  
  MainGroupID = gimp_new_row_group(MaterialDialogID, DEFAULT, NORMAL, "");
  FrameID = gimp_new_frame (MaterialDialogID, MainGroupID, "Intensity levels");
  FrameGroupID = gimp_new_row_group(MaterialDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(MaterialDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Ambient:");
  sprintf(buf, "%f", material->AmbientInt);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->AmbientInt);

  DummyID = gimp_new_column_group(MaterialDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Diffuse:");
  sprintf(buf, "%f", material->DiffuseInt);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->DiffuseInt);

  FrameID = gimp_new_frame (MaterialDialogID, MainGroupID, "Reflectivity");
  FrameGroupID = gimp_new_row_group(MaterialDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(MaterialDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Ambient:");
  sprintf(buf, "%f", material->AmbientRef);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->AmbientRef);

  DummyID = gimp_new_column_group(MaterialDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Diffuse:");
  sprintf(buf, "%f", material->DiffuseRef);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->DiffuseRef);

  DummyID = gimp_new_column_group(MaterialDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Specular:");
  sprintf(buf, "%f", material->SpecularRef);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->SpecularRef);

  DummyID = gimp_new_column_group(MaterialDialogID,MainGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Highlight:");
  sprintf(buf, "%f", material->Highlight);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->Highlight);

  if (mode==WITH_COLOR)
	 {
		/* Add a color button to the dialog */
		/* ================================ */

		DummyID = gimp_new_push_button(MaterialDialogID,MainGroupID,"Material color..");
		gimp_add_callback (MaterialDialogID, DummyID, material_color_callback, &material->Color);
	 }

  /* Other callbacks */
  /* =============== */

  gimp_add_callback(MaterialDialogID, gimp_ok_item_id(MaterialDialogID), material_ok_callback, NULL);
  gimp_add_callback(MaterialDialogID, gimp_cancel_item_id(MaterialDialogID), material_cancel_callback, NULL);
}

void CreateDirectionalLightDialog(LightSettings *light)
{
  int MainGroupID,FrameID,FrameGroupID,XValID,YValID,ZValID,DummyID;
  char buf[100];

  DirectionalLightDialogID = gimp_new_dialog("Directional light settings");
  
  MainGroupID = gimp_new_row_group(DirectionalLightDialogID, DEFAULT, NORMAL, "");
  FrameID = gimp_new_frame (DirectionalLightDialogID, MainGroupID, "Direction vector");
  FrameGroupID = gimp_new_row_group(DirectionalLightDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(DirectionalLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(DirectionalLightDialogID, DummyID, "X:");
  sprintf(buf, "%f", light->Direction.x);
  XValID = gimp_new_text(DirectionalLightDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(DirectionalLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(DirectionalLightDialogID, DummyID, "Y:");
  sprintf(buf, "%f", light->Direction.y);
  YValID = gimp_new_text(DirectionalLightDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(DirectionalLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(DirectionalLightDialogID, DummyID, "Z:");
  sprintf(buf, "%f", light->Direction.z);
  ZValID = gimp_new_text(DirectionalLightDialogID,DummyID,buf);

  /* Callbacks */
  /* ========= */

  gimp_add_callback(DirectionalLightDialogID, XValID, double_callback, &light->Direction.x);
  gimp_add_callback(DirectionalLightDialogID, YValID, double_callback, &light->Direction.y);
  gimp_add_callback(DirectionalLightDialogID, ZValID, double_callback, &light->Direction.z);

  gimp_add_callback(DirectionalLightDialogID, gimp_ok_item_id(DirectionalLightDialogID),
    directional_light_ok_callback, NULL);
  gimp_add_callback(DirectionalLightDialogID, gimp_cancel_item_id(DirectionalLightDialogID),
    directional_light_cancel_callback, NULL);
}

void CreatePointLightDialog(LightSettings *light)
{
  int MainGroupID,FrameID,FrameGroupID,XValID,YValID,ZValID,DummyID;
  char buf[100];

  PointLightDialogID = gimp_new_dialog("Point light settings");
  
  MainGroupID = gimp_new_row_group(PointLightDialogID, DEFAULT, NORMAL, "");
  FrameID = gimp_new_frame (PointLightDialogID, MainGroupID, "Position");
  FrameGroupID = gimp_new_row_group(PointLightDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(PointLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(PointLightDialogID, DummyID, "X:");
  sprintf(buf, "%f", light->Position.x);
  XValID = gimp_new_text(PointLightDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(PointLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(PointLightDialogID, DummyID, "Y:");
  sprintf(buf, "%f", light->Position.y);
  YValID = gimp_new_text(PointLightDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(PointLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(PointLightDialogID, DummyID, "Z:");
  sprintf(buf, "%f", light->Position.z);
  ZValID = gimp_new_text(PointLightDialogID,DummyID,buf);

  /* Callbacks */
  /* ========= */

  gimp_add_callback(PointLightDialogID, XValID, double_callback, &light->Position.x);
  gimp_add_callback(PointLightDialogID, YValID, double_callback, &light->Position.y);
  gimp_add_callback(PointLightDialogID, ZValID, double_callback, &light->Position.z);

  gimp_add_callback(PointLightDialogID, gimp_ok_item_id(PointLightDialogID), pointlight_ok_callback, NULL);
  gimp_add_callback(PointLightDialogID, gimp_cancel_item_id(PointLightDialogID), pointlight_cancel_callback, NULL);
}

void CreateColorDialog(RGBPixel *color)
{
  int MainGroupID,ColorFrameID,ColorGroupID,RedScaleID,GreenScaleID,BlueScaleID;

  ColorDialogID = gimp_new_dialog("Light color");
  MainGroupID = gimp_new_row_group(ColorDialogID, DEFAULT, NORMAL, "");

  ColorFrameID = gimp_new_frame (ColorDialogID, MainGroupID, "Color");
  ColorGroupID = gimp_new_row_group(ColorDialogID, ColorFrameID, NORMAL, "");
  RedScaleID = gimp_new_scale (ColorDialogID, ColorGroupID, 0, 255, (long)(255.0*color->r), 0);
  gimp_new_label(ColorDialogID, RedScaleID, "Red:");
  GreenScaleID = gimp_new_scale (ColorDialogID, ColorGroupID, 0, 255, (long)(255.0*color->g), 0);
  gimp_new_label(ColorDialogID, GreenScaleID, "Green:");
  BlueScaleID = gimp_new_scale (ColorDialogID, ColorGroupID, 0, 255, (long)(255.0*color->b), 0);
  gimp_new_label(ColorDialogID, BlueScaleID, "Blue:");

  /* Callbacks */
  /* ========= */

  gimp_add_callback (ColorDialogID, RedScaleID, scale_callback, &color->r);
  gimp_add_callback (ColorDialogID, GreenScaleID, scale_callback, &color->g);
  gimp_add_callback (ColorDialogID, BlueScaleID, scale_callback, &color->b);

  gimp_add_callback(ColorDialogID, gimp_ok_item_id(ColorDialogID), color_ok_callback, NULL);
  gimp_add_callback(ColorDialogID, gimp_cancel_item_id(ColorDialogID), color_cancel_callback, NULL);
}

void CreateMainDialog(void)
{
  int MainGroupID,LightFrameID,LightGroupID,DirectionalLightID,PointLightID,SpotLightID;
  int SurfaceFrameID,SurfaceGroupID,FlatID,WavyID,MappedID,RefractionToggleID;
  int LightSettingsID,LightColorID,SurfaceSettingsID,RefractionSettingsID;
  int MainSurfaceGroupID,MappingSettingsID,RadioLightID,LightToggle1ID,LightToggle2ID;

  MainDialogID = gimp_new_dialog("Lighting");
  MainGroupID = gimp_new_row_group(MainDialogID, DEFAULT, NORMAL, "");

  LightFrameID = gimp_new_frame (MainDialogID, MainGroupID, "Light source");
  LightGroupID = gimp_new_row_group(MainDialogID, LightFrameID, NORMAL, "");

  RadioLightID = gimp_new_row_group(MainDialogID, LightGroupID, RADIO, "");
  DirectionalLightID = gimp_new_radio_button (MainDialogID, RadioLightID, "Directional light");
  PointLightID = gimp_new_radio_button (MainDialogID, RadioLightID, "Point light");
  SpotLightID = gimp_new_radio_button (MainDialogID, RadioLightID, "Spot light");

  LightToggle1ID = gimp_new_radio_button(MainDialogID, LightGroupID, "Apply to image");
  LightToggle2ID = gimp_new_radio_button(MainDialogID, LightGroupID, "Apply to layer");

  LightSettingsID = gimp_new_push_button(MainDialogID,MainGroupID,"Lightsource settings..");
  LightColorID = gimp_new_push_button(MainDialogID,MainGroupID,"Lightsource color..");

  SurfaceFrameID = gimp_new_frame (MainDialogID, MainGroupID, "Image normal mapping");
  MainSurfaceGroupID = gimp_new_column_group(MainDialogID, SurfaceFrameID, NORMAL, "");
  SurfaceGroupID = gimp_new_row_group(MainDialogID, MainSurfaceGroupID, RADIO, "");

  FlatID = gimp_new_radio_button (MainDialogID, SurfaceGroupID, "No mapping");
  WavyID = gimp_new_radio_button (MainDialogID, SurfaceGroupID, "Wavy mapping");
  MappedID = gimp_new_radio_button (MainDialogID, SurfaceGroupID, "Bump mapping");

  MappingSettingsID = gimp_new_push_button(MainDialogID,MainGroupID,"Image mapping settings..");
  SurfaceSettingsID = gimp_new_push_button(MainDialogID,MainGroupID,"Image material settings..");

  SurfaceGroupID = gimp_new_column_group(MainDialogID, MainGroupID, NORMAL, "");
  RefractionToggleID = gimp_new_radio_button(MainDialogID, SurfaceGroupID, "Use a refractive layer");
  RefractionSettingsID = gimp_new_push_button(MainDialogID,SurfaceGroupID,"Settings..");

  /* Default settings */
  /* ================ */

  gimp_change_item (MainDialogID, DirectionalLightID, sizeof(light_toggles[1]), &light_toggles[0]);
  gimp_change_item (MainDialogID, PointLightID, sizeof(light_toggles[2]), &light_toggles[1]);
  gimp_change_item (MainDialogID, SpotLightID, sizeof(light_toggles[3]), &light_toggles[2]);
  gimp_change_item (MainDialogID, LightToggle1ID, sizeof(apply_light_toggles[0]), &apply_light_toggles[0]);
  gimp_change_item (MainDialogID, LightToggle2ID, sizeof(apply_light_toggles[1]), &apply_light_toggles[1]);

  gimp_change_item (MainDialogID, FlatID, sizeof(surface_toggles[0]), &surface_toggles[0]);
  gimp_change_item (MainDialogID, WavyID, sizeof(surface_toggles[1]), &surface_toggles[1]);
  gimp_change_item (MainDialogID, MappedID, sizeof(surface_toggles[2]), &surface_toggles[2]);

  /* Callbacks */
  /* ========= */

  gimp_add_callback (MainDialogID, DirectionalLightID, radio_callback, &light_toggles[0]);
  gimp_add_callback (MainDialogID, PointLightID, radio_callback, &light_toggles[1]);
  gimp_add_callback (MainDialogID, SpotLightID, radio_callback, &light_toggles[2]);
  gimp_add_callback (MainDialogID, LightToggle1ID, radio_callback, &apply_light_toggles[0]);
  gimp_add_callback (MainDialogID, LightToggle2ID, radio_callback, &apply_light_toggles[1]);

  gimp_add_callback (MainDialogID, FlatID, radio_callback, &surface_toggles[0]);
  gimp_add_callback (MainDialogID, WavyID, radio_callback, &surface_toggles[1]);
  gimp_add_callback (MainDialogID, MappedID, radio_callback, &surface_toggles[2]);
  gimp_add_callback (MainDialogID, RefractionToggleID, radio_callback, &refractionmode);
  gimp_add_callback (MainDialogID, RefractionSettingsID, RefractionSettings_callback, NULL);

  gimp_add_callback (MainDialogID, LightSettingsID, LightSettings_callback, NULL);
  gimp_add_callback (MainDialogID, LightColorID, LightColor_callback, NULL);
  gimp_add_callback (MainDialogID, SurfaceSettingsID, SurfaceSettings_callback, NULL);

  gimp_add_callback (MainDialogID, MappingSettingsID, SurfaceMapping_callback, NULL);

  gimp_add_callback(MainDialogID, gimp_ok_item_id(MainDialogID), main_ok_callback, NULL);
  gimp_add_callback(MainDialogID, gimp_cancel_item_id(MainDialogID), main_cancel_callback, NULL);
}

int main(int argc,char **argv)
{
  /* Standard stuff straight out of Marc's tutorial :) */
  /* ================================================= */

  if (!gimp_init(argc,argv)) return(0);

  input=gimp_get_input_image(0);
  output=gimp_get_output_image(0);

  if (!input || !output) return(0);

  /* We deal only with RGB images */
  /* ============================ */

  if (gimp_image_type(input)!=RGB_IMAGE) gimp_message("Lighting: On RGB type images only!\n");
  else
	 {
		/* Create a nice dialog and show it */
		/* ================================ */

		SetDefaultSettings();
		InitCleanup();
 		CreateMainDialog();
 		if (gimp_show_dialog(MainDialogID)) doit();
		Cleanup();
	 }

  /* Standard The End stuff.. */
  /* ======================== */

  gimp_free_image(input);
  gimp_free_image(output);

  gimp_quit();
  return(0);
}
