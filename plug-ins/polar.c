/*
 * polar 1.00 -- image filter plug-in for The GIMP
 * Copyright (C) 1996 Marc Bless
 *
 * E-mail: bless@ai-lab.fh-furtwangen.de
 * WWW:    www.ai-lab.fh-furtwangen.de/~bless
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 * This filter converts an image from rectangular to polar.
 */

/*
 * History:
 * 
 * v 1.00 - 1996 June 01 : standard function works.
 *               June 02 : top in middle, backwards, 
 *                         circle depth, angle
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gimp.h"

#define sqr(x)	((x) * (x))
#define WITHIN(a, b, c) ((((a) <= (b)) && ((b) <= (c))) ? 1 : 0)

typedef unsigned char uchar;

static void  polar (Image, Image);
static uchar bilinear (double x, double y, uchar *v);
static void  cb_scale (int, void *, void *);
static void  cb_ok (int, void *, void *);
static void  cb_cancel (int, void *, void *);
static void  cb_toggle (int, void *, void *);
static void  saveimage (void);
static void  freshen (void);

static char *prog_name;

static int   dialogID;
static long  inverse;
static int   inverseID;
static long  backward;
static int   backwardID;
static long  aapply;
static int   aapplyID;
static long  circle;
static int   circleID;
static long  angle;
static int   angleID;

Image input, output;

static unsigned char *saved;

/*
 * The main function.
 */

int
main (argc, argv)
     int argc;
     char **argv;
{
	int mainID;
	int labelID;

  prog_name = argv[0];

  if (gimp_init (argc, argv))
  {
    input = 0;
    output = 0;
      
    input = gimp_get_input_image (0);

    if (input)
			switch (gimp_image_type (input))
	  	{
	  		case RGB_IMAGE:
	  		case GRAY_IMAGE:
	    		output = gimp_get_output_image (0);
	    		if (output)
	      	{
						saveimage ();

						dialogID = gimp_new_dialog ("Polar");
						mainID   = gimp_new_row_group (dialogID, DEFAULT, NORMAL, "");
						labelID  = gimp_new_label (dialogID, mainID, "Circle depth in %");
						circleID = gimp_new_scale (dialogID, mainID, 0, 100, 100, 0);
						circle = 100;
						labelID  = gimp_new_label (dialogID, mainID, "Angle");
						angleID  = gimp_new_scale (dialogID, mainID, 0, 359, 0, 0);
						angle    = 0;
						inverseID = gimp_new_check_button (dialogID, mainID, "Top in middle");
						inverse = 0;
						gimp_change_item (dialogID, inverseID, sizeof (inverse), &inverse);
						backwardID = gimp_new_check_button (dialogID, mainID, "Backwards");
						backward = 0;
						gimp_change_item (dialogID, backwardID, sizeof (backward), &backward);
						aapplyID = gimp_new_check_button (dialogID, mainID, "Auto Apply");
						aapply   = 1;
						gimp_change_item (dialogID, aapplyID, sizeof (aapply), &aapply);
						gimp_add_callback (dialogID, circleID, cb_scale, &circle);
						gimp_add_callback (dialogID, angleID, cb_scale, &angle);
						gimp_add_callback (dialogID, inverseID, cb_toggle, &inverse);
						gimp_add_callback (dialogID, backwardID, cb_toggle, &backward);
						gimp_add_callback (dialogID, aapplyID, cb_toggle, &aapply);
						gimp_add_callback (dialogID, gimp_ok_item_id (dialogID), cb_ok, 0);
						gimp_add_callback (dialogID, gimp_cancel_item_id (dialogID), cb_cancel, 0);
						
						polar (input, output);
						gimp_update_image (output);

						if (!gimp_show_dialog (dialogID))
						{
							if (aapply)
								freshen ();
						}
						else
						{
							if (!aapply)
								polar (input, output);
						}

						gimp_update_image (output);
						free (saved);
	      	}
	    		break;
	  		case INDEXED_IMAGE:
	    		gimp_message ("polar: cannot operate on indexed image types");
	    		break;
	  		default:
	    		gimp_message ("polar: cannot operate on unknown image types");
	    		break;
			}

    if (input)
			gimp_free_image (input);
    if (output)
			gimp_free_image (output);

    gimp_quit ();
  }

  return 0;
}

/* 
 * The check button callback function.
 */

static void cb_toggle (int itemID, void *client_data, void *call_data)
{
	*((long*) client_data) = *((long*) call_data);

	if (aapply)
	{
		polar (input, output);
		gimp_update_image (output);
	}
	else
	{
		freshen ();
		gimp_update_image (output);
	}
}

/*
 * The scale callback function.
 */

static void cb_scale (int itemID, void *client_data, void *call_data)
{
	if (aapply && (*((long*) client_data) != *((long*) call_data)))
	{
		*((long*) client_data) = *((long*) call_data);
		polar (input, output);
		gimp_update_image (output);
	}

	*((long*) client_data) = *((long*) call_data);
}

/*
 * The saveimage function.
 */

static void saveimage (void)
{
	saved = (unsigned char *) malloc (gimp_image_width (input) *
	                          gimp_image_height (input) *
														gimp_image_channels (input));
	memcpy (saved, gimp_image_data (input),
	               gimp_image_width (input) *
								 gimp_image_height (input) *
								 gimp_image_channels (input));
}

/*
 * The freshen function.
 */

static void freshen (void)
{
	memcpy (gimp_image_data (output), saved,
		      gimp_image_width (input) *
					gimp_image_height (input) *
					gimp_image_channels (input));
}

/*
 * The ok button callback function.
 */

static void cb_ok (int itemID, void *client_data, void *call_data)
{
	gimp_close_dialog (dialogID, 1);
}

/*
 * The cancel button callback function.
 */

static void cb_cancel (int itemID, void *client_data, void *call_data)
{
	gimp_close_dialog (dialogID, 0);
}

/*
 * The polar filter function.
 */

static void
polar (linput, loutput)
     Image linput, loutput;
{
  long width, height;
  long channels, rowstride;
  unsigned char *src_row, *dest_row;
  unsigned char *src, *dest;
  unsigned char *src_org;
  uchar *p;
  uchar values[4];
  int k;
  uchar val;
  short row, col;
  int x1, y1, x2, y2;
  int xi, yi;
  double x, y, xm, ym, r, rmax, m;
  double xmax, ymax;
	double t;
  double phi;
	double angl;
  int progress, max_progress;
  
  gimp_image_area (linput, &x1, &y1, &x2, &y2);

  width     = gimp_image_width (linput);
  height    = gimp_image_height (linput);
  channels  = gimp_image_channels (linput);
  rowstride = width * channels;

  progress     = 0;
  max_progress = y2 - y1;

	src_org  = src_row = saved;
  dest_row = gimp_image_data (loutput);

  src_row  += rowstride * y1 + x1 * channels;
  dest_row += rowstride * y1 + x1 * channels;

  xm = (x2 - x1) / 2.0;
  ym = (y2 - y1) / 2.0;

	angl = (double)angle / 180.0 * M_PI;

  for (row = y1; row < y2; row++)
  {
    src     = src_row;
    dest    = dest_row;

    for (col = x1; col < x2; col++)
    {
			if (col >= xm)
			{
				if (row > ym)
				{
					phi = M_PI - atan (((double)(col - xm))/((double)(row - ym)));
					r   = sqrt (sqr (col - xm) + sqr (row - ym));
				}
				else
				if (row < ym)
				{
					phi = atan (((double)(col - xm))/((double)(ym - row)));
					r   = sqrt (sqr (col - xm) + sqr (ym - row));
				}
				else
				{
					phi = M_PI / 2;
					r   = col - xm; /* xm - x1; */
				}
			}
			else
			if (col < xm)
			{
				if (row < ym)
				{
					phi = 2 * M_PI - atan (((double)(xm - col))/((double)(ym - row)));
					r   = sqrt (sqr (xm - col) + sqr (ym - row));
				}
				else
				if (row > ym)
				{
					phi = M_PI + atan (((double)(xm - col))/((double)(row - ym)));
					r   = sqrt (sqr (xm - col) + sqr (row - ym));
				}
				else
				{
					phi = 1.5 * M_PI;
					r   = xm - col; /* xm - x1; */
				}
			}

			if (col != xm)
				m = fabs (((double)(row - ym)) / ((double)(col - xm)));
			else
				m = 0;

			if (m <= ((double)(y2 - y1) / (double)(x2 - x1)))
			{
				if (col == xm)
				{
					xmax = 0;
					ymax = ym - y1;
				}
				else
				{
	  			xmax = xm - x1;
	  			ymax = m * xmax;
				}
			}
			else
			{
	  		ymax = ym - y1;
	  		xmax = ymax / m;
			}
	
			rmax = sqrt ( (double)(sqr (xmax) + sqr (ymax)) );

			t = ((ym - y1) < (xm - x1)) ? (ym - y1) : (xm - x1);
			rmax = (rmax - t) / 100 * (100 - circle) + t;

			phi = fmod (phi + angl, 2*M_PI);

			if (backward)
				x = x2 - 1 - (x2 - x1 - 1)/(2*M_PI) * phi;
			else
				x = (x2 - x1 - 1)/(2*M_PI) * phi + x1;
			if (inverse)
				y = (y2 - y1)/rmax   * r   + y1;
			else
				y = y2 - (y2 - y1)/rmax * r;

			xi = rint ((int) x);
			yi = rint ((int) y);

			p = src_org + rowstride * yi + xi * channels;

			for (k = 0; k < channels; k++)
			{
				if (WITHIN(0, xi, width - 1) && WITHIN(0, yi, height - 1))
					values[0] = *(p + k);
				else
					values[0] = 0;

				if (WITHIN(0, xi+1, width - 1) && WITHIN(0, yi, height - 1))
					values[1] = *(p + channels + k);
				else
					values[1] = 0;

				if (WITHIN(0, xi, width - 1) && WITHIN(0, yi + 1, height - 1))
					values[2] = *(p + rowstride + k);
				else
					values[2] = 0;

				if (WITHIN(0, xi + 1, width - 1) && WITHIN(0, yi + 1, height - 1))
					values[3] = *(p + rowstride + channels + k);
				else
					values[3] = 0;

				val = bilinear (x, y, values);

				*dest++ = val;
			}

		}

    src_row += rowstride;
    dest_row += rowstride;

    progress++;
    if (progress % 2 == 0)
    	gimp_do_progress (progress, max_progress);
  }
}

static uchar
bilinear(double x, double y, uchar *v)
{
    double xx, yy, m0, m1;

    xx = fmod(x, 1.0);
    yy = fmod(y, 1.0);

    m0 = (1.0 - xx) * v[0] + xx * v[1];
    m1 = (1.0 - xx) * v[2] + xx * v[3];

    return (uchar) ((1.0 - yy) * m0 + yy * m1);
} 

