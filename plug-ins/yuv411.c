/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* This useless filter written by Josh MacDonald to load and save
 * images in YUV411 format (as used by mpeg encoders), derived from
 * png.c in the January distribution.  It assumes a concatenated
 * raw stream of y, u, and v channels, where y is 176x144 raw QCIF,
 * and u and v are the same, subsampled 4 to 1.
 */

#include <stdio.h>
#include <stdlib.h>
#include "gimp.h"

/* Declare some local functions.
 */
static void load_image (char *);
static void save_image (char *);

static char *prog_name;

#define WIDTH 176
#define HEIGHT 144

void
main (argc, argv)
     int argc;
     char **argv;
{
  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      /* This is a file filter so all it needs to know about is loading
       *  and saving images. So we'll install handlers for those two
       *  messages.
       */
      gimp_install_load_save_handlers (load_image, save_image);

      /* Run until something happens. That something could be getting
       *  a 'QUIT' message or getting a load or save message.
       */
      gimp_main_loop ();
    }
}

static unsigned char clip_array_uncentered[512] = { 0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
    0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,
    18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
    41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,
    65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,
    88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,
    108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,
    126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,
    143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,
    161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,
    178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,
    196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,
    213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,
    231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,
    248,249,250,251,252,253,254,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
    255,255,255,255,255,255,255,255,255,255};

static unsigned char *clip_array = clip_array_uncentered + 128;

static void
YUVtoRGB2(y, y2, u, v, rgb, rgb2)
     unsigned int y, y2, u, v;
     unsigned char *rgb, *rgb2;
{
    int iy = y;
    int iy2 = y2;
    int iu = u - 128;
    int iv = v - 128;

    int vcoef = ((22980 * iv)>>14);
    int ucoef = ((29049 * iu)>>14);
    int uvcoef = ((11705 * iv)>>14) + ((5643 * iu)>>14);

    /* This clip routine contains no branches, allowing it to be pipelined,
     * which should make it a win for RISC machines.
     * This was Pete Mattis' idea. */

    rgb[0] = clip_array[iy + vcoef];
    rgb[1] = clip_array[iy - uvcoef];
    rgb[2] = clip_array[iy + ucoef];

    rgb2[0] = clip_array[iy2 + vcoef];
    rgb2[1] = clip_array[iy2 - uvcoef];
    rgb2[2] = clip_array[iy2 + ucoef];
}

static void
load_image (filename)
     char *filename;
{
  FILE *fp;
  Image image;
  char * name_buf;
  int j, row;
  unsigned char *y, *u, *v;
  unsigned char *image_row_ptr_1, *image_row_ptr_2, *y2;

  y = (unsigned char *) malloc (WIDTH*HEIGHT);
  u = (unsigned char *) malloc (WIDTH*HEIGHT/4);
  v = (unsigned char *) malloc (WIDTH*HEIGHT/4);

  name_buf = malloc (strlen (filename) + 11);
  if (!name_buf)
    gimp_quit ();

  sprintf (name_buf, "Loading %s:", filename);
  gimp_init_progress (name_buf);
  free (name_buf);

  /* open the file */
  fp = fopen (filename, "rb");
  if (!fp)
    {
      fprintf (stderr, "%s: can't open \"%s\"\n", prog_name, filename);
      gimp_quit ();
    }

  /* Create a new image of the proper size and associate the filename with it.
   */
  image = gimp_new_image (filename, WIDTH, HEIGHT, RGB_IMAGE);

  if(fread(y, WIDTH, HEIGHT, fp) != HEIGHT ||
     fread(u, WIDTH/2, HEIGHT/2, fp) != HEIGHT/2 ||
     fread(v, WIDTH/2, HEIGHT/2, fp) != HEIGHT/2)
    {
      fprintf (stderr, "%s: invalid input file \"%s\"\n", prog_name, filename);
      gimp_quit ();
    }

  y2 = y + WIDTH;
  image_row_ptr_1 = gimp_image_data (image);
  image_row_ptr_2 = image_row_ptr_1 + 3 * WIDTH;

  for(row=0; row < HEIGHT; row+=2)
    { /* iterate over rows */
      for(j = WIDTH / 2; j; --j)
	{ /* iterate over cols */
	  YUVtoRGB2(*y++, *y2++, *u, *v, image_row_ptr_1, image_row_ptr_2);
	  image_row_ptr_1 += 3;
	  image_row_ptr_2 += 3;
	  YUVtoRGB2(*y++, *y2++, *u++, *v++, image_row_ptr_1, image_row_ptr_2);
	  image_row_ptr_1 += 3;
	  image_row_ptr_2 += 3;
	}

      y += WIDTH;
      y2 += WIDTH;

      if (row % 6 == 0)
	gimp_do_progress (row, HEIGHT);

      image_row_ptr_1 += 3*WIDTH;
      image_row_ptr_2 += 3*WIDTH;
    }

  /* close the file */
  fclose (fp);

  gimp_do_progress (1, 1);

  /* Tell the GIMP to display the image.
   */
  gimp_display_image (image);

  /* Tell the GIMP to update the image. (ie Redraw it).
   */
  gimp_update_image (image);

  /* Free the image. (This involves detaching the shared memory segment,
   *  which is a good thing.)
   */
  gimp_free_image (image);

  /* Quit.
   */
  gimp_quit ();
}

static void
save_image (filename)
     char *filename;
{
  fprintf(stderr, "Sorry, save not implemented for YUV411 images\n");
  gimp_quit ();
}
