/*
 * This is a plug-in for the GIMP.
 *
 * Copyright (C) 1996 Torsten Martinsen <bullestock@dk-online.dk>
 * Bresenham algorithm stuff hacked from HP2xx written by Heinz W. Werntges
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $id: mblur.c,v 1.10 1996/06/10 09:24:43 torsten Exp $
 */

#include "gimp.h"
#include <stdlib.h>
#include <math.h>

/*
 * This filter simulates the effect seen when photographing a
 * moving object at a slow shutter speed.
 * Done by adding multiple displaced copies.
 */

static void mblur(Image, Image);
static void mblur_linear(Image, Image);
static void mblur_radial(Image, Image);
static void mblur_zoom(Image, Image);

static int params[3] = { 5, 45, 0 };
static int dialog_id;

static void scale_callback(int, void *, void *);
static void ok_callback(int, void *, void *);
static void cancel_callback(int, void *, void *);
static void radio_callback(int item_id, void *client_data, void *call_data);

typedef void (*BlurFunc) (Image, Image);

#define NTYPES 3

int
main(int argc, char **argv)
{
    Image input = 0, output = 0;
    void *data;
    int group_id, scaled_id, scalea_id, rb_id[NTYPES];
    int frame_id, row_id;
    int set, i, id;
    static char * rb_label[NTYPES] = {
	"Linear", "Radial", "Zoom"
    };


    if (gimp_init(argc, argv)) {
	input = gimp_get_input_image(0);

	if (input) {
	  switch (gimp_image_type (input))
	    {
	    case RGB_IMAGE: case RGBA_IMAGE:
	    case GRAY_IMAGE: case GRAYA_IMAGE:
	      data = gimp_get_params();
	      if (data) {
		params[0] = ((int *) data)[0];
		params[1] = ((int *) data)[1];
		params[2] = ((int *) data)[2];
	      }
	      dialog_id = gimp_new_dialog("Motion Blur");
	      group_id = gimp_new_row_group(dialog_id, DEFAULT, NORMAL, "");
	      
	      frame_id = gimp_new_frame (dialog_id, group_id, "Blur Type");
	      row_id = gimp_new_row_group (dialog_id, frame_id, RADIO, "");
	      for (i = 0; i < NTYPES; ++i) {
		rb_id[i] = id = gimp_new_radio_button (dialog_id, row_id,
						       rb_label[i]);
		gimp_add_callback (dialog_id, id,
				   radio_callback, (void *) i);
		set = (i == params[2]) ? 1 : 0;
		gimp_change_item (dialog_id, id, sizeof (int), &set);
	      }
	      
	      gimp_new_label(dialog_id, group_id, "Distance");
	      scaled_id = gimp_new_scale(dialog_id, group_id, 1, 50,
					 params[0], 0);
	      gimp_new_label(dialog_id, group_id, "Angle");
	      scalea_id = gimp_new_scale(dialog_id, group_id, 0, 360,
					 params[1], 0);
	      gimp_add_callback(dialog_id, scaled_id,
				scale_callback, &params[0]);
	      gimp_add_callback(dialog_id, scalea_id,
				scale_callback, &params[1]);
	      gimp_add_callback(dialog_id, gimp_ok_item_id(dialog_id),
				ok_callback, 0);
	      gimp_add_callback(dialog_id, gimp_cancel_item_id(dialog_id),
				cancel_callback, 0);
	      
	      if (gimp_show_dialog(dialog_id)) {
		gimp_set_params(3*sizeof(int), &params);
		
		output = gimp_get_output_image(0);
		if (output) {
		  gimp_init_progress("Motion Blur");
		  mblur(input, output);
		  gimp_update_image(output);
		}
	      }
	      break;
	    default:
	      gimp_message("mblur: cannot operate on indexed color images");
	      break;
	    }
	}
	if (input)
	    gimp_free_image(input);
	if (output)
	    gimp_free_image(output);

	gimp_quit();
    }
    return 0;
}

static void
mblur(Image input, Image output)
{
    static BlurFunc mblur_funcs[] = {
	mblur_linear, mblur_radial, mblur_zoom
    };

    mblur_funcs[params[2]](input, output);
}

/*
 * Do a 'linear blur' - the camera is translated during exposure.
 * Each pixel at (x,y) is replaced by the average of the pixels located on a
 * straight line with start at (x,y), pointing in the specified direction
 * and having the specified length specified angle.
 */
static void
mblur_linear(Image input, Image output)
{
    long width, height;
    long channels, rowstride;
    unsigned char *src_base;
    unsigned char *dest;
    int x, y, i, xx, yy, n, sum;
    int dx, dy, px, py, swapdir, err, e, s1, s2;

    width = gimp_image_width(input);
    height = gimp_image_height(input);
    channels = gimp_image_channels(input);
    rowstride = width * channels;

    src_base = gimp_image_data(input);

    n = params[0];
    px = n*cos(params[1]/180.0*M_PI);
    py = n*sin(params[1]/180.0*M_PI);

    /*
     * Initialization for Bresenham algorithm:
     * dx = abs(x2-x1), s1 = sign(x2-x1)
     * dy = abs(y2-y1), s2 = sign(y2-y1)
     */
    if ((dx = px) != 0) {
      if (dx < 0) {
	  dx = -dx;
	  s1 = -1;
      }
      else
	  s1 = 1;
    } else
	s1 = 0;
    
    if ((dy = py) != 0) {
	if (dy < 0) {
	    dy = -dy;
	    s2 = -1;
	}
	else
	    s2 = 1;
    } else
	s2 = 0;

    if (dy > dx) {
	swapdir = dx;
	dx = dy;
	dy = swapdir;
	swapdir = 1;
    }
    else
	swapdir = 0;

    dy *= 2;
    err = dy - dx;	/* Initial error term	*/
    dx *= 2;

    s1 *= channels;
    dest = gimp_image_data(output);
    for (y = 0; y < height; ++y) {
	for (x = 0; x < rowstride; ++x) {
	    xx = x; yy = y; e = err;
	    for (i = 0, sum = 0; i < n; ++i) {
		while (e >= 0) {
		    if (swapdir)
			xx += s1;
		    else
			yy += s2;
		    e -= dx;
		}
		if (swapdir)
		    yy += s2;
		else
		    xx += s1;
		e += dy;
		if ((yy < 0) || (yy >= height) ||
		    (xx < 0) || (xx >= rowstride))
		    break;
		sum += src_base[yy*rowstride + xx];
	    }
	    if (i == 0)
		*dest++ = src_base[y*rowstride + x];
	    else
		*dest++ = sum/i;
	}
	if ((y % 5) == 0)
	    gimp_do_progress(y, height);
    }
}

/*
 * Do a 'radial blur' - the camera is rotated during exposure.
 * Each pixel at (x,y) is replaced by the average of the pixels located on an
 * arc with start at (x,y), center at (width/2,height/2) and covering the
 * specified angle.
 * To speed up computing the rotated coordinates, a table of sine and cosine
 * values is built before entering the loop. This is 8-10 times faster.
 */
static void
mblur_radial(Image input, Image output)
{
    long width, height;
    long channels, rowstride;
    unsigned char *src_base;
    unsigned char *dest;
    int x, y, i, j, xx, yy, n, sum, sumG, sumB, cx, cy;
    int x1, y1, x2, y2;
    float theta, * ct, * st;

    gimp_image_area(input, &x1, &y1, &x2, &y2);
    width = gimp_image_width(input);
    height = gimp_image_height(input);
    channels = gimp_image_channels(input);
    rowstride = width * channels;

    src_base = gimp_image_data(input);

    n = params[0];
    theta = params[1]/180.0*M_PI/((float) n);

    if (((ct = malloc(n*sizeof(float))) == NULL) ||
	((st = malloc(n*sizeof(float))) == NULL))
	return;
    for (i = 0; i < n; ++i) {
	ct[i] = cos(theta*i);
	st[i] = sin(theta*i);
    }

    dest = gimp_image_data(output);
    cx = (x1+x2)/2;
    cy = (y1+y2)/2;
    for (y = 0; y < height; ++y) {
	for (x = 0; x < width; ++x) {
	    sum = sumG = sumB = 0;
	    for (i = 0; i < n; ++i) {
		xx = cx + (x-cx)*ct[i] - (y-cy)*st[i];
		yy = cy + (x-cx)*st[i] + (y-cy)*ct[i];
		if ((yy < 0) || (yy >= height) ||
		    (xx < 0) || (xx >= width))
		    break;
		xx *= channels;
		sum += src_base[yy*rowstride + xx];
		if (channels > 1) {
		    sumG += src_base[yy*rowstride + xx + 1];
		    sumB += src_base[yy*rowstride + xx + 2];
		}
	    }
	    if (i == 0)
		for (j = 0; j < channels; ++j)
		    *dest++ = src_base[y*rowstride + x + j];
	    else {
		*dest++ = sum/i;
		if (channels > 1) {
		    *dest++ = sumG/i;
		    *dest++ = sumB/i;
		}
	    }
	}
	if ((y % 5) == 0)
	    gimp_do_progress(y, height);
    }
}

/*
 * Do a 'zoom blur' - the object is zoomed during exposure.
 * Each pixel at (x,y) is replaced by the average of the pixels located on a
 * straight line with start at (x,y), pointing towards (width/2,height/2) and
 * length dependent on the distance from the center. (The angle is ignored).
 */
static void
mblur_zoom(Image input, Image output)
{
    long width, height;
    long channels, rowstride;
    unsigned char *src_base;
    unsigned char *dest;
    int x, y, i, j, xx, yy, n, sum, sumG, sumB, cx, cy;
    int x1, y1, x2, y2;
    float f;
    
    gimp_image_area(input, &x1, &y1, &x2, &y2);
    width = gimp_image_width(input);
    height = gimp_image_height(input);
    channels = gimp_image_channels(input);
    rowstride = width * channels;

    src_base = gimp_image_data(input);

    n = params[0];

    dest = gimp_image_data(output);
    cx = (x1+x2)/2;
    cy = (y1+y2)/2;
    f = 0.01;
    for (y = 0; y < height; ++y) {
	for (x = 0; x < width; ++x) {
	    sum = sumG = sumB = 0;
	    for (i = 0; i < n; ++i) {
		xx = cx + (x-cx)*(1.0 + f*i);
		yy = cy + (y-cy)*(1.0 + f*i);
		if ((yy < 0) || (yy >= height) ||
		    (xx < 0) || (xx >= width))
		    break;
		xx *= channels;
		sum += src_base[yy*rowstride + xx];
		if (channels > 1) {
		    sumG += src_base[yy*rowstride + xx + 1];
		    sumB += src_base[yy*rowstride + xx + 2];
		}
	    }
	    if (i == 0)
		for (j = 0; j < channels; ++j)
		    *dest++ = src_base[y*rowstride + x + j];
	    else {
		*dest++ = sum/i;
		if (channels > 1) {
		    *dest++ = sumG/i;
		    *dest++ = sumB/i;
		}
	    }
	}
	if ((y % 5) == 0)
	    gimp_do_progress(y, height);
    }
}

static void
scale_callback(int item_id, void *client_data, void *call_data)
{
    *((int *) client_data) = *((long *) call_data);
}

static void
ok_callback(int item_id, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_id, 1);
}

static void
cancel_callback(int item_id, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_id, 0);
}

static void
radio_callback(int item_id, void *client_data, void *call_data)
{
  if (*((long *) call_data))
    params[2] = (int) client_data;
}
