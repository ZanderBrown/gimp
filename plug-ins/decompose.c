/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *             CMY, CMYK decomposition by Peter Kirchgessner
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* decompose a color image into its constituent channels:
 * - r, g, b
 * - hue, value, saturation
 * - cyan, magenta, yellow
 * - cyan, magenta, yellow, black
 */

#include <stdio.h>
#include "gimp.h"

/* Declare local functions.
 */
static void radio_callback (int, void *, void *);
static void ok_callback (int, void *, void *);
static void cancel_callback (int, void *, void *);
static void decompose_rgb (Image);
static void decompose_hsv (Image);
static void decompose_cmy (Image);
static void decompose_cmyk (Image);
static void rgb_to_hsv (int *, int *, int *);
static void rgb_to_cmy (int *, int *, int *);
static void rgb_to_cmyk (int *, int *, int *, int *);

static char *prog_name;
static int dialog_ID;

int
main (argc, argv)
     int argc;
     char **argv;
{
  Image src;
  int group_ID;
  int rgb_ID;
  int hsv_ID;
  int cmy_ID;
  int cmyk_ID;
  long rgb, hsv, cmy, cmyk;

  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      src = gimp_get_input_image (0);
      if (src)
	switch (gimp_image_type (src))
	  {
	  case RGB_IMAGE: case RGBA_IMAGE:
	    rgb = 1;
	    hsv = 0;
	    cmy = 0;
	    cmyk = 0;
	    
	    dialog_ID = gimp_new_dialog ("decompose");
	    group_ID = gimp_new_row_group (dialog_ID, DEFAULT, RADIO, "");
	    rgb_ID = gimp_new_radio_button (dialog_ID, group_ID, "RGB decomposition");
	    gimp_change_item (dialog_ID, rgb_ID, sizeof (rgb), &rgb);
	    hsv_ID = gimp_new_radio_button (dialog_ID, group_ID, "HSV decomposition");
	    gimp_change_item (dialog_ID, hsv_ID, sizeof (hsv), &hsv);
	    cmy_ID = gimp_new_radio_button (dialog_ID, group_ID, "CMY decomposition");
	    gimp_change_item (dialog_ID, cmy_ID, sizeof (cmy), &cmy);
	    cmyk_ID = gimp_new_radio_button (dialog_ID, group_ID, "CMYK decomposition");
	    gimp_change_item (dialog_ID, cmyk_ID, sizeof (cmyk), &cmyk);
	    gimp_add_callback (dialog_ID, rgb_ID, radio_callback, &rgb);
	    gimp_add_callback (dialog_ID, hsv_ID, radio_callback, &hsv);
	    gimp_add_callback (dialog_ID, cmy_ID, radio_callback, &cmy);
	    gimp_add_callback (dialog_ID, cmyk_ID, radio_callback, &cmyk);
	    gimp_add_callback (dialog_ID, gimp_ok_item_id (dialog_ID), ok_callback, 0);
	    gimp_add_callback (dialog_ID, gimp_cancel_item_id (dialog_ID), cancel_callback, 0);
	    
	    if (gimp_show_dialog (dialog_ID))
	      {
		if (rgb)
		  decompose_rgb (src);
		else if (hsv)
		  decompose_hsv (src);
		else if (cmy)
		  decompose_cmy (src);
		else if (cmyk)
		  decompose_cmyk (src);
	      }
	    break;
	  case GRAY_IMAGE: case GRAYA_IMAGE:
	    gimp_message ("decompose: cannot operate on grayscale images");
	    break;
	  case INDEXED_IMAGE: case INDEXEDA_IMAGE:
	    gimp_message ("decompose: cannot operate on indexed color images");
	    break;
	  default:
	    gimp_message ("decompose: cannot operate on unknown image types");
	    break;
	  }

      /* Free the image.
       */
      if (src)
	gimp_free_image (src);

      /* Quit
       */
      gimp_quit ();
    }

  return 0;
}

static void
radio_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *((long*) client_data) = *((long*) call_data);
}

static void
ok_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 1);
}
 
static void
cancel_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 0);
}

static void
decompose_rgb (src)
     Image src;
{
  Image rdest, gdest, bdest, adest;
  char name_buf[256];
  unsigned char *srcp;
  unsigned char *rdestp;
  unsigned char *gdestp;
  unsigned char *bdestp;
  unsigned char *adestp;
  long width, height;
  int alpha;
  int i, j;

  width = gimp_image_width (src);
  height = gimp_image_height (src);
  alpha = gimp_image_alpha (src);
  
  sprintf (name_buf, "%s-red", gimp_image_name (src));
  rdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);

  sprintf (name_buf, "%s-green", gimp_image_name (src));
  gdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);

  sprintf (name_buf, "%s-blue", gimp_image_name (src));
  bdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);
  
  if (alpha)
    {
      sprintf (name_buf, "%s-alpha", gimp_image_name (src));
      adest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);
    }

  srcp = gimp_image_data (src);
  rdestp = gimp_image_data (rdest);
  gdestp = gimp_image_data (gdest);
  bdestp = gimp_image_data (bdest);
  if (alpha)
    adestp = gimp_image_data (adest);

  gimp_init_progress ("decompose RGB");
  
  for (i = 0; i < height; i++)
    {
      for (j = 0; j < width; j++)
	{
	  *rdestp++ = *srcp++;
	  *gdestp++ = *srcp++;
	  *bdestp++ = *srcp++;
	  if (alpha) *adestp++ = *srcp++;
	}

      if ((i % 5) == 0)
	gimp_do_progress (i, height);
    }

  gimp_display_image (rdest);
  gimp_display_image (gdest);
  gimp_display_image (bdest);
  if (alpha)
    gimp_display_image (adest);

  gimp_update_image (rdest);
  gimp_update_image (gdest);
  gimp_update_image (bdest);
  if (alpha)
    gimp_update_image (adest);

  gimp_free_image (rdest);
  gimp_free_image (gdest);
  gimp_free_image (bdest);
  if (alpha)
    gimp_free_image (adest);
}

static void
decompose_hsv (src)
     Image src;
{
  Image hdest, sdest, vdest, adest;
  char name_buf[256];
  unsigned char *srcp;
  unsigned char *hdestp;
  unsigned char *sdestp;
  unsigned char *vdestp;
  unsigned char *adestp;
  long width, height;
  int h, s, v;
  int alpha;
  int i, j;

  width = gimp_image_width (src);
  height = gimp_image_height (src);
  alpha = gimp_image_alpha (src);
  
  sprintf (name_buf, "%s-hue", gimp_image_name (src));
  hdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);

  sprintf (name_buf, "%s-saturation", gimp_image_name (src));
  sdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);

  sprintf (name_buf, "%s-value", gimp_image_name (src));
  vdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);
  
  if (alpha)
    {
      sprintf (name_buf, "%s-alpha", gimp_image_name (src));
      adest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);
    }

  srcp = gimp_image_data (src);
  hdestp = gimp_image_data (hdest);
  sdestp = gimp_image_data (sdest);
  vdestp = gimp_image_data (vdest);
  if (alpha)
    adestp = gimp_image_data (adest);

  gimp_init_progress ("decompose HSV");

  for (i = 0; i < height; i++)
    {
      for (j = 0; j < width; j++)
	{
	  h = *srcp++;
	  s = *srcp++;
	  v = *srcp++;
	  
	  rgb_to_hsv (&h, &s, &v);
	  
	  *hdestp++ = h;
	  *sdestp++ = s;
	  *vdestp++ = v;
	  if (alpha) *adestp++ = *srcp++;
	}

      if ((i % 5) == 0)
	gimp_do_progress (i, height);
    }

  gimp_display_image (hdest);
  gimp_display_image (sdest);
  gimp_display_image (vdest);
  if (alpha)
    gimp_display_image (adest);

  gimp_update_image (hdest);
  gimp_update_image (sdest);
  gimp_update_image (vdest);
  if (alpha)
    gimp_update_image (adest);

  gimp_free_image (hdest);
  gimp_free_image (sdest);
  gimp_free_image (vdest);
  if (alpha)
    gimp_free_image (adest);
}

static void
decompose_cmy (src)
     Image src;
{
  Image cdest, mdest, ydest, adest;
  char name_buf[256];
  unsigned char *srcp;
  unsigned char *cdestp;
  unsigned char *mdestp;
  unsigned char *ydestp;
  unsigned char *adestp;
  long width, height;
  int c, m, y;
  int alpha;
  int i, j;

  width = gimp_image_width (src);
  height = gimp_image_height (src);
  alpha = gimp_image_alpha (src);
  
  sprintf (name_buf, "%s-cyan", gimp_image_name (src));
  cdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);

  sprintf (name_buf, "%s-magenta", gimp_image_name (src));
  mdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);

  sprintf (name_buf, "%s-yellow", gimp_image_name (src));
  ydest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);
  
  if (alpha)
    {
      sprintf (name_buf, "%s-alpha", gimp_image_name (src));
      adest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);
    }

  srcp = gimp_image_data (src);
  cdestp = gimp_image_data (cdest);
  mdestp = gimp_image_data (mdest);
  ydestp = gimp_image_data (ydest);
  if (alpha)
    adestp = gimp_image_data (adest);

  gimp_init_progress ("decompose CMY");

  for (i = 0; i < height; i++)
    {
      for (j = 0; j < width; j++)
	{
	  c = *srcp++;
	  m = *srcp++;
	  y = *srcp++;
	  
	  rgb_to_cmy (&c, &m, &y);
	  
	  *cdestp++ = c;
	  *mdestp++ = m;
	  *ydestp++ = y;
	  if (alpha) *adestp++ = *srcp++;
	}

      if ((i % 5) == 0)
	gimp_do_progress (i, height);
    }

  gimp_display_image (cdest);
  gimp_display_image (mdest);
  gimp_display_image (ydest);
  if (alpha)
    gimp_display_image (adest);

  gimp_update_image (cdest);
  gimp_update_image (mdest);
  gimp_update_image (ydest);
  if (alpha)
    gimp_update_image (adest);

  gimp_free_image (cdest);
  gimp_free_image (mdest);
  gimp_free_image (ydest);
  if (alpha)
    gimp_free_image (adest);
}

static void
decompose_cmyk (src)
     Image src;
{
  Image cdest, mdest, ydest, kdest, adest;
  char name_buf[256];
  unsigned char *srcp;
  unsigned char *cdestp;
  unsigned char *mdestp;
  unsigned char *ydestp;
  unsigned char *kdestp;
  unsigned char *adestp;
  long width, height;
  int c, m, y, k;
  int alpha;
  int i, j;

  width = gimp_image_width (src);
  height = gimp_image_height (src);
  alpha = gimp_image_alpha (src);
  
  sprintf (name_buf, "%s-cyan", gimp_image_name (src));
  cdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);

  sprintf (name_buf, "%s-magenta", gimp_image_name (src));
  mdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);

  sprintf (name_buf, "%s-yellow", gimp_image_name (src));
  ydest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);

  sprintf (name_buf, "%s-black", gimp_image_name (src));
  kdest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);
  
  if (alpha)
    {
      sprintf (name_buf, "%s-alpha", gimp_image_name (src));
      adest = gimp_new_image (name_buf, width, height, GRAY_IMAGE);
    }

  srcp = gimp_image_data (src);
  cdestp = gimp_image_data (cdest);
  mdestp = gimp_image_data (mdest);
  ydestp = gimp_image_data (ydest);
  kdestp = gimp_image_data (kdest);
  if (alpha)
    adestp = gimp_image_data (adest);

  gimp_init_progress ("decompose CMYK");

  for (i = 0; i < height; i++)
    {
      for (j = 0; j < width; j++)
	{
	  c = *srcp++;
	  m = *srcp++;
	  y = *srcp++;
	  
	  rgb_to_cmyk (&c, &m, &y, &k);
	  
	  *cdestp++ = c;
	  *mdestp++ = m;
	  *ydestp++ = y;
	  *kdestp++ = k;  /* Full intensity black is 255. This will be */
                          /* displayed as white in the greymap image.  */
	  if (alpha) *adestp++ = *srcp++;
	}

      if ((i % 5) == 0)
	gimp_do_progress (i, height);
    }

  gimp_display_image (cdest);
  gimp_display_image (mdest);
  gimp_display_image (ydest);
  gimp_display_image (kdest);
  if (alpha)
    gimp_display_image (adest);

  gimp_update_image (cdest);
  gimp_update_image (mdest);
  gimp_update_image (ydest);
  gimp_update_image (kdest);
  if (alpha)
    gimp_update_image (adest);

  gimp_free_image (cdest);
  gimp_free_image (mdest);
  gimp_free_image (ydest);
  gimp_free_image (kdest);
  if (alpha)
    gimp_free_image (adest);
}


static void
rgb_to_hsv (r, g, b)
     int *r, *g, *b;
{
  int red, green, blue;
  float h, s, v;
  int min, max;
  int delta;
  
  red = *r;
  green = *g;
  blue = *b;

  if (red > green)
    {
      if (red > blue)
	max = red;
      else
	max = blue;
      
      if (green < blue)
	min = green;
      else
	min = blue;
    }
  else
    {
      if (green > blue)
	max = green;
      else
	max = blue;
      
      if (red < blue)
	min = red;
      else
	min = blue;
    }
  
  v = max;
  
  if (max != 0)
    s = ((max - min) * 255) / (float) max;
  else
    s = 0;
  
  if (s == 0)
    h = 0;
  else
    {
      delta = max - min;
      if (red == max)
	h = (green - blue) / (float) delta;
      else if (green == max)
	h = 2 + (blue - red) / (float) delta;
      else if (blue == max)
	h = 4 + (red - green) / (float) delta;
      h *= 42.5;

      if (h < 0)
	h += 255;
      if (h > 255)
	h -= 255;
    }

  *r = h;
  *g = s;
  *b = v;
}


static void
rgb_to_cmy (r, g, b)
     int *r, *g, *b;
{
  int c, m, y;
  
  c = 255 - *r;
  m = 255 - *g;
  y = 255 - *b;

  *r = c;
  *g = m;
  *b = y;
}

static void
rgb_to_cmyk (r, g, b, blk)
     int *r, *g, *b, *blk;
{
  int c, m, y, k;
  
  c = 255 - *r;
  m = 255 - *g;
  y = 255 - *b;
  k = c;             /* Black intensity is the minimum of cyan, */
  if (m < k) k = m;  /* magenta and yellow intensity. */
  if (y < k) k = y;
  if (k > 0)         /* Remove common black part from cyan, magenta, yellow */
  {
    c -= k;
    m -= k;
    y -= k;
  }

  *r = c;
  *g = m;
  *b = y;
  *blk = k;
}
