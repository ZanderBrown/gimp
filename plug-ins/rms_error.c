/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*  find the mean square error between two images  */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gimp.h"

#define MIN(a,b) (((a) < (b)) ? (a) : (b))

/* Declare local functions.
 */
static void image_menu_callback (int, void *, void *);
static void ok_callback (int, void *, void *);
static void cancel_callback (int, void *, void *);
static void calculate_mse (Image, Image);

static char *prog_name;
static int dialog_ID;

int
main (argc, argv)
     int argc;
     char **argv;
{
  Image src1, src2;
  int group_ID;
  int image_menu1_ID;
  int image_menu2_ID;
  long src1_ID, src2_ID;

  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      src1 = src2 = 0;
      src1_ID = src2_ID = 0;
      
      dialog_ID = gimp_new_dialog ("RMS Error");
      group_ID = gimp_new_row_group (dialog_ID, DEFAULT, NORMAL, "");
      
      image_menu1_ID = gimp_new_image_menu (dialog_ID, group_ID, 
					    IMAGE_CONSTRAIN_RGB | IMAGE_CONSTRAIN_GRAY,
					    "First Image");
      image_menu2_ID = gimp_new_image_menu (dialog_ID, group_ID, 
					    IMAGE_CONSTRAIN_RGB | IMAGE_CONSTRAIN_GRAY,
					    "Second Image");
      gimp_add_callback (dialog_ID, image_menu1_ID, image_menu_callback, &src1_ID);
      gimp_add_callback (dialog_ID, image_menu2_ID, image_menu_callback, &src2_ID);
      gimp_add_callback (dialog_ID, gimp_ok_item_id (dialog_ID), ok_callback, 0);
      gimp_add_callback (dialog_ID, gimp_cancel_item_id (dialog_ID), cancel_callback, 0);

      if (gimp_show_dialog (dialog_ID))
	{
	  src1 = gimp_get_input_image (src1_ID);
	  
	  if (src2_ID == src1_ID)
	    src2 = src1;
	  else
	    src2 = gimp_get_input_image (src2_ID);
	  
	  gimp_init_progress ("Mean Square Error");
	  calculate_mse (src1, src2);
	}

      /* Free the images.
       */
      if (src1)
	gimp_free_image (src1);
      if (src2 && (src2_ID != src1_ID))
	gimp_free_image (src2);

      /* Quit
       */
      gimp_quit ();
    }

  return 0;
}

static void
image_menu_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *((long*) client_data) = *((long*) call_data);
}

static void
ok_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 1);
}

static void
cancel_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 0);
}

static void
calculate_mse (src1, src2)
     Image src1, src2;
{
  unsigned char *src1p;
  unsigned char *src2p;
  int src1_channels;
  int src2_channels;
  int channels;
  long width, height;
  double mse[4];
  double avg[4];
  int diff;
  int i, j, c;
  int color;
  char * color_names[4] = { "Red", "Green", "Blue", "Alpha" };
  char * gray_names[2] = { "Gray", "Alpha" };
  char buffer[256];
  int frame_ID;
  int row1_ID;
  int row2_ID;
  int row_group_ID;
  int col_group_ID;

  width = gimp_image_width (src1);
  height = gimp_image_height (src1);

  color = ((gimp_image_type (src1) == RGBA_IMAGE) ||
	   (gimp_image_type (src1) == RGB_IMAGE));

  src1p = gimp_image_data (src1);
  src1_channels = gimp_image_channels (src1);

  src2p = gimp_image_data (src2);
  src2_channels = gimp_image_channels (src2);

  channels = MIN (src1_channels, src2_channels);

  for (c = 0; c < 4; c++)
    {
      mse[c] = 0.0;
      avg[c] = 0.0;
    }

  for (i = 0; i < height; i++)
    {
      for (j = 0; j < width; j++)
	{
	  for (c = 0; c < channels; c++)
	    {
	      diff = src1p[c] - src2p[c];
	      mse[c] += diff * diff;
	      avg[c] += (diff > 0) ? diff : -diff;
	    }

	  src1p += src1_channels;
	  src2p += src2_channels;
	}

      if ((i % 5) == 0)
	gimp_do_progress (i, height);
    }

  gimp_do_progress (1, 1);

  dialog_ID = gimp_new_dialog ("Error Report");

  row_group_ID = gimp_new_row_group (dialog_ID, DEFAULT, NORMAL, "");

  frame_ID = gimp_new_frame (dialog_ID, row_group_ID, "RMS Error");
  col_group_ID = gimp_new_column_group (dialog_ID, frame_ID, NORMAL, "");
  row1_ID = gimp_new_row_group (dialog_ID, col_group_ID, NORMAL, "");
  row2_ID = gimp_new_row_group (dialog_ID, col_group_ID, NORMAL, "");

  for (c = 0; c < channels; c++)
    {
      if (color)
	sprintf (buffer, "%s:\t", color_names[c]);
      else
	sprintf (buffer, "%s:\t", gray_names[c]);

      gimp_new_label (dialog_ID, row1_ID, buffer);

      sprintf (buffer, "%f", sqrt (mse[c] / (width * height)));
      gimp_new_label (dialog_ID, row2_ID, buffer);
    }

  frame_ID = gimp_new_frame (dialog_ID, row_group_ID, "Average Error");
  col_group_ID = gimp_new_column_group (dialog_ID, frame_ID, NORMAL, "");
  row1_ID = gimp_new_row_group (dialog_ID, col_group_ID, NORMAL, "");
  row2_ID = gimp_new_row_group (dialog_ID, col_group_ID, NORMAL, "");

  for (c = 0; c < channels; c++)
    {
      if (color)
	sprintf (buffer, "%s:\t", color_names[c]);
      else
	sprintf (buffer, "%s:\t", gray_names[c]);

      gimp_new_label (dialog_ID, row1_ID, buffer);

      sprintf (buffer, "%f", avg[c] / (width * height));
      gimp_new_label (dialog_ID, row2_ID, buffer);
    }
  
  gimp_add_callback (dialog_ID, gimp_ok_item_id (dialog_ID), ok_callback, 0);
  gimp_add_callback (dialog_ID, gimp_cancel_item_id (dialog_ID), cancel_callback, 0);
  gimp_show_dialog (dialog_ID);
}
