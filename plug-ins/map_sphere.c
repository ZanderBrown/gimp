/*********************************************************************************/
/* Map_Sphere 0.11 -- image filter plug-in for The Gimp program                  */
/* Copyright (C) 1996 Tom Bech                                                   */
/*===============================================================================*/
/* E-mail: tomb@ii.uib.no                                                        */
/* You can contact the original The Gimp authors at gimp@xcf.berkeley.edu        */
/*===============================================================================*/
/* This program is free software; you can redistribute it and/or modify it under */
/* the terms of the GNU General Public License as published by the Free Software */
/* Foundation; either version 2 of the License, or (at your option) any later    */
/* version.                                                                      */
/*===============================================================================*/
/* This program is distributed in the hope that it will be useful, but WITHOUT   */
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS */
/* FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.*/
/*===============================================================================*/
/* You should have received a copy of the GNU General Public License along with  */
/* this program; if not, write to the Free Software Foundation, Inc., 675 Mass   */
/* Ave, Cambridge, MA 02139, USA.                                                */
/*===============================================================================*/
/* In other words, you can't sue me for whatever happens while using this ;)     */
/*********************************************************************************/
/* Changes (post 0.10):                                                          */
/* -> 0.11 Fixed a bug in PhongShade() (highlights appeared at the dark side of  */
/*         the sphere too).                                                      */
/*********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "gimp.h"

/************/
/* Typedefs */
/************/

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define MAX_CLEANUP_STACK 10

#define NO_LIGHT 0
#define DIRECTIONAL_LIGHT 1
#define POINT_LIGHT 2
#define SPOT_LIGHT 3

/***********************/
/* Some useful structs */
/***********************/

typedef struct
{
  double x,y,z;
} Vector;

typedef struct
{
  double r,g,b;
} RGBPixel;

typedef struct
{
  double AmbientInt;
  double DiffuseInt;
  double DiffuseRef;
  double SpecularRef;
  double Highlight;
  RGBPixel Color;
} MaterialSettings;


typedef struct
{
  Vector Position;
  Vector Direction;
  RGBPixel Color;
  double Radius;
  double Intensity;
} LightSettings;

/*****************************/
/* Global variables and such */
/*****************************/

Vector ZeroVec = {x:0.0,y:0.0,z:0.0};
Vector Unit_X = {x:1.0,y:0.0,z:0.0};
Vector Unit_Y = {x:0.0,y:1.0,z:0.0};
Vector Unit_Z = {x:0.0,y:0.0,z:1.0};
Vector ViewPoint = {x:0.5,y:0.5,z:1.0};
Vector Equator = {x:0.0,y:0.0,z:-1.0};
Vector NorthPole = {x:0.0,y:1.0,z:0.0};
Vector SpherePosition;
double SphereRadius;

RGBPixel White = {r:1.0,g:1.0,b:1.0};
RGBPixel Black = {r:0.0,g:0.0,b:0.0};
RGBPixel OldLightColor,LightColor = {r:1.0,g:1.0,b:1.0};
RGBPixel BackGround;

LightSettings CurrentLight,OldLight;
MaterialSettings CurrentMaterial,OldMaterial;
MaterialSettings CurrentRefMaterial,OldRefMaterial;

Image input,output;
unsigned char *dinput=NULL,*doutput=NULL;

int width,height,channels,modulo;
int MainDialogID=-1,ColorDialogID=-1,PointLightDialogID=-1,MaterialDialogID=-1;
int DirectionalLightDialogID=-1;

long light_toggles[4] = { 0,1,0,0 };
long maxcounter;

int CleanupStackCount=0;
void *CleanupStack[MAX_CLEANUP_STACK];

/**************************************/
/* Some neccesary function prototypes */
/**************************************/

void KillAllDialogs(int mode);
void CreateColorDialog(RGBPixel *color);
void CreatePointLightDialog(LightSettings *light);
void CreateDirectionalLightDialog(LightSettings *light);
void CreateMaterialDialog(MaterialSettings *material);

/************************/
/* Convenience routines */
/************************/

int GetLightType(void)
{
  if (light_toggles[0]==1) return(NO_LIGHT);
  else if (light_toggles[1]==1) return(DIRECTIONAL_LIGHT);
  else if (light_toggles[2]==1) return(POINT_LIGHT);
  else return(SPOT_LIGHT);
}

void InitCleanup(void)
{
  int cnt;
  for (cnt=0;cnt<MAX_CLEANUP_STACK;cnt++) CleanupStack[cnt]=NULL;
}

void Cleanup(void)
{
  int cnt;

  /* Free internally allocated buffers */
  /* ================================= */

  for (cnt=0;cnt<MAX_CLEANUP_STACK;cnt++)
	 {
		if (CleanupStack[cnt]!=NULL) free(CleanupStack[cnt]);
	 }
}

void AddToCleanupStack(void *element)
{
  if (CleanupStackCount>MAX_CLEANUP_STACK)
	 {
		gimp_message("Lighting: CleanupStack overflow! Terminating..\n");
		Cleanup();
		KillAllDialogs(0);
		exit(1);
	 }
  CleanupStack[CleanupStackCount++]=element;
}

void RGBAdd(RGBPixel *a,RGBPixel *b)
{
  a->r=a->r+b->r;
  a->g=a->g+b->g;
  a->b=a->b+b->b;
}

void RGBMul(RGBPixel *a,double b)
{
  a->r=a->r*b;
  a->g=a->g*b;
  a->b=a->b*b;
}

void RGBClamp(RGBPixel *a)
{
  if (a->r>1.0) a->r=1.0;
  if (a->g>1.0) a->g=1.0;
  if (a->b>1.0) a->b=1.0;
  if (a->r<0.0) a->r=0.0;
  if (a->g<0.0) a->g=0.0;
  if (a->b<0.0) a->b=0.0;
}

void SetColor(RGBPixel *a,double r,double g,double b)
{
  a->r=r; a->g=g; a->b=b;
}

double InnerProduct(Vector *a,Vector *b)
{
  return(a->x*b->x+a->y*b->y+a->z*b->z);
}

Vector CrossProduct(Vector *a,Vector *b)
{
  Vector normal;

  normal.x=a->y*b->z-a->z*b->y;
  normal.y=a->z*b->x-a->x*b->z;
  normal.z=a->x*b->y-a->y*b->x;

  return(normal);
}

void Normalize(Vector *a)
{
  double len;
  
  len=sqrt(a->x*a->x+a->y*a->y+a->z*a->z);

  if (len!=0.0)
	 {
		len=1.0/len;
		a->x=a->x*len;
		a->y=a->y*len;
		a->z=a->z*len;
	 }
  else *a=ZeroVec;
}

void MulVector(Vector *a,double b)
{
  a->x=a->x*b;
  a->y=a->y*b;
  a->z=a->z*b;
}

void SubVector(Vector *c,Vector *a,Vector *b)
{
  c->x=a->x-b->x;
  c->y=a->y-b->y;
  c->z=a->z-b->z;
}

void SetVector(Vector *a, double x,double y,double z)
{
  a->x=x;
  a->y=y;
  a->z=z;
}

void AddVector(Vector *c,Vector *a,Vector *b)
{
  c->x=a->x+b->x;
  c->y=a->y+b->y;
  c->z=a->z+b->z;
}

/******************/
/* Implementation */
/******************/

void SetDefaultSettings(void)
{
  SetVector(&CurrentLight.Position, -0.5,-0.5,0.8);
  SetVector(&CurrentLight.Direction, -1.0,-1.0,1.0);
  SetVector(&SpherePosition,0.5,0.5,0);
  SphereRadius=0.25;

  CurrentLight.Intensity = 1.0;
  CurrentLight.Color=White;

  CurrentMaterial.AmbientInt = 0.5;
  CurrentMaterial.DiffuseInt = 1.0;
  CurrentMaterial.DiffuseRef = 0.45;
  CurrentMaterial.SpecularRef = 0.5;
  CurrentMaterial.Highlight = 27.0;
}

/****************/
/* Main section */
/****************/

/***************************************/
/* Directional and point light variant */
/***************************************/

RGBPixel PhongShade1(Vector *pos,Vector *viewpoint,Vector *normal,Vector *light,
						  RGBPixel *diff_col,RGBPixel *spec_col,int type)
{
  RGBPixel ambientcolor,diffusecolor,specularcolor;
  double NL,RV;
  Vector L,NN,V,Normal=*normal;

  /* Compute ambient intensity */
  /* ========================= */

  ambientcolor=*diff_col;
  RGBMul(&ambientcolor,CurrentMaterial.AmbientInt);

  /* Compute (N*L) term of Phong's equation */
  /* ====================================== */

  if (type==POINT_LIGHT) SubVector(&L,light,pos);
  else L=*light;
  Normalize(&L);

  NL=2.0*InnerProduct(&Normal,&L);
		
  if (NL>=0.0)
	 {
		/* Compute (R*V)^alpha term of Phong's equation */
		/* ============================================ */

		SubVector(&V,viewpoint,pos);
		Normalize(&V);

		MulVector(&Normal,NL);
		SubVector(&NN,&Normal,&L);
		RV=InnerProduct(&NN,&V);
		RV=pow(RV,CurrentMaterial.Highlight);

		/* Compute diffuse and specular intensity contribution */
		/* =================================================== */

		diffusecolor=*diff_col;
		RGBMul(&diffusecolor,CurrentMaterial.DiffuseRef);
		RGBMul(&diffusecolor,NL);

		specularcolor=*spec_col;
		RGBMul(&specularcolor,CurrentMaterial.SpecularRef);
		RGBMul(&specularcolor,RV);

		RGBAdd(&diffusecolor,&specularcolor);
		RGBMul(&diffusecolor,CurrentMaterial.DiffuseInt);
		RGBClamp(&diffusecolor);

		RGBAdd(&ambientcolor,&diffusecolor);
	 }
  return(ambientcolor);
}

/**********************/
/* Spot light variant */
/**********************/

RGBPixel PhongShade2(Vector *pos,Vector *viewpoint,Vector *normal,Vector *light,
						  RGBPixel *diff_col,RGBPixel *spec_col)
{
  RGBPixel ambientcolor,diffusecolor,specularcolor;
  double NL,RV;
  Vector L,NN,V,Normal=*normal;

  /* Compute (N*L) term of Phong's equation */
  /* ====================================== */

  SubVector(&L,light,pos);
  Normalize(&L);

  NL=2.0*InnerProduct(&Normal,&L);
 
  /* Compute (R*V)^alpha term of Phong's equation */
  /* ============================================ */

  SubVector(&V,viewpoint,pos);
  Normalize(&V);

  MulVector(&Normal,NL);
  SubVector(&NN,&Normal,&L);
  RV=InnerProduct(&NN,&V);
  RV=pow(RV,CurrentMaterial.Highlight);

  /* Compute ambient, diffuse and specular intensity contribution */
  /* ============================================================ */

  ambientcolor=*diff_col;
  RGBMul(&ambientcolor,CurrentMaterial.AmbientInt);

  diffusecolor=*diff_col;
  RGBMul(&diffusecolor,CurrentMaterial.DiffuseRef);
  RGBMul(&diffusecolor,NL);

  specularcolor=*spec_col;
  RGBMul(&specularcolor,CurrentMaterial.SpecularRef);
  RGBMul(&specularcolor,RV);

  RGBAdd(&diffusecolor,&specularcolor);
  RGBMul(&diffusecolor,CurrentMaterial.DiffuseInt);
  RGBClamp(&diffusecolor);

  printf("A: %f %f %f D: %f %f %f\n",ambientcolor.r,ambientcolor.g,ambientcolor.b,
			diffusecolor.r,diffusecolor.g,diffusecolor.b);

  RGBAdd(&ambientcolor,&diffusecolor);

  return(ambientcolor);
}

long int XYToIndex(int x,int y)
{
  return((long int)x*(long int)channels+(long int)y*(long int)modulo);
}

int CheckBounds(int x,int y)
{
  if (x<0 || y<0 || x>width-1 || y>height-1) return(1);
  else return(0);
}

unsigned char PeekMap(unsigned char *DispMap,int x,int y)
{
  long int index;

  index=(long int)x+(long int)width*(long int)y;

  return(DispMap[index]);
}

void PokeMap(unsigned char *DispMap,int x,int y,unsigned char value)
{
  long int index;

  index=(long int)x+(long int)width*(long int)y;

  DispMap[index]=value;
}

RGBPixel Peek(unsigned char *data,int x,int y)
{
  long int index=XYToIndex(x,y);
  RGBPixel color;

/*   printf("%d %d\n",x,y); */
  color.r=((double)data[index])/255.0;
  color.g=((double)data[index+1])/255.0;
  color.b=((double)data[index+2])/255.0;
  return(color);
}

void Poke(unsigned char *data,int x,int y,RGBPixel *color)
{ 
  long int index=XYToIndex(x,y);

  data[index]=(unsigned char)(255.0*color->r);
  data[index+1]=(unsigned char)(255.0*color->g);
  data[index+2]=(unsigned char)(255.0*color->b);
}

/***********************************************************************/
/* Given the NorthPole, Equator and a third vector (normal) compute    */
/* the conversion from spherical oordinates to image space coordinates */
/***********************************************************************/

void SphereToImage(Vector *normal,double *u,double *v)
{
  static double alpha,fac;
  static Vector CrossP;

  alpha=acos(-InnerProduct(&NorthPole,normal));

  *v=alpha/M_PI;

  if (*v==0.0 || *v==1.0) *u=0.0;
  else
	 {
		fac=InnerProduct(&Equator,normal)/sin(alpha);

		/* Make sure that we map to -1.0..1.0 (take care of rounding errors) */
		/* ================================================================= */

		if (fac>1.0) fac=1.0;
		else if (fac<-1.0) fac=-1.0;
		*u=acos(fac)/(2.0*M_PI);
	  
 		CrossP=CrossProduct(&NorthPole,&Equator);
 		if (InnerProduct(&CrossP,normal)<0.0) *u=1.0-*u;
	 }
}

Vector IntToPos(int x,int y)
{
  Vector pos;
  
  pos.x=(double)x/(double)width;
  pos.y=(double)y/(double)height;
  pos.z=0.0;

  return(pos);
}

void PosToInt(double x,double y,int *scr_x,int *scr_y)
{
  *scr_x=(int)(x*(double)width);
  *scr_y=(int)(y*(double)height);
}

/***************************************************/
/* Compute intersection point with sphere (if any) */
/***************************************************/

int SphereIntersect(Vector *dir,Vector *viewp,Vector *spos)
{
  static double alpha,beta,tau,s1,s2;
  static Vector t;

  SubVector(&t,&SpherePosition,viewp);

  alpha=InnerProduct(dir,&t);
  beta=InnerProduct(&t,&t);
  
  tau=alpha*alpha-beta+SphereRadius*SphereRadius;

  if (tau>=0.0)
	 {
		tau=sqrt(tau);
		s1=alpha+tau;
		s2=alpha-tau;
		if (s2<s1) s1=s2;
		spos->x=viewp->x+s1*dir->x;
		spos->y=viewp->y+s1*dir->y;
		spos->z=viewp->z+s1*dir->z;
		return(1);
	 }
  else return(0);
}

/******************************/
/* Rendering stuff below here */
/******************************/

RGBPixel GetImageColor(double u,double v)
{
  int x,y;

  PosToInt(u,v,&x,&y);
/*   printf("%f %f -> %d %d\n",u,v,x,y); */
  return(Peek(dinput,x,y));
}

/*******************************************************************/
/* This routine computes the color of the surface at a given point */
/*******************************************************************/

RGBPixel ShadePicture(int x,int y)
{
  RGBPixel color=Black;
  int LightType=GetLightType();
  Vector ray,pos,spos,normal;
  double vx,vy;
  
  /* Check if the ray through image pos (x,y) hits our sphere */
  /* ======================================================== */

  pos=IntToPos(x,y);
  
  SubVector(&ray,&pos,&ViewPoint);
  Normalize(&ray);

  if (SphereIntersect(&ray,&ViewPoint,&spos)==1)
	 {
		SubVector(&normal,&spos,&SpherePosition);
		Normalize(&normal);

		/*		printf("Normal=%f %f %f\n",normal.x,normal.y,normal.z);*/
		
		/* Map image to sphere */
		/* =================== */

 		SphereToImage(&normal,&vx,&vy);
 		color=GetImageColor(vx,vy);

		/* Compute shading at this point */
		/* ============================= */

		if (LightType!=NO_LIGHT)
		  color=PhongShade1(&spos,&ViewPoint,&normal,&CurrentLight.Position,&color,
		  &CurrentLight.Color,LightType);
		RGBClamp(&color);
	 }
  return(color);
}

/*************/
/* Main loop */
/*************/

void ComputeLighting(void)
{
  int xcount,ycount;
  long counter=0;
  RGBPixel color;
  unsigned char r,g,b;
  
  width=gimp_image_width(input);
  height=gimp_image_height(input);
  maxcounter=(long int)width*(long int)height;
  channels=gimp_image_channels(input);
  modulo=channels*width;

  gimp_init_progress("Map to sphere");

  gimp_background_color(&r,&g,&b);

  BackGround.r=(double)r/255.0;
  BackGround.g=(double)g/255.0;
  BackGround.b=(double)b/255.0;
  
  dinput=gimp_image_data(input);
  doutput=gimp_image_data(output);

  for (ycount=0;ycount<height;ycount++)
	 {
		for (xcount=0;xcount<width;xcount++)
		  {
			 color=ShadePicture(xcount,ycount);
			 Poke(doutput,xcount,ycount,&color);
			 counter++;
			 if ((counter % width)==0) gimp_do_progress(counter,maxcounter);
		  }
	 }
  gimp_update_image(output);
}

/**************************/
/* Below is only UI stuff */
/**************************/

void KillAllDialogs(int mode)
{
  if (MainDialogID!=-1) gimp_close_dialog(MainDialogID, mode);
  if (ColorDialogID!=-1) gimp_close_dialog(ColorDialogID, mode);
  if (PointLightDialogID!=-1) gimp_close_dialog(PointLightDialogID, mode);
  if (MaterialDialogID!=-1) gimp_close_dialog(MaterialDialogID, mode);
  if (DirectionalLightDialogID!=-1) gimp_close_dialog(DirectionalLightDialogID, mode);
}

/******************/
/* Callback stuff */
/******************/

static void main_ok_callback(int item_id, void *client_data, void *call_data)
{
  KillAllDialogs(1);
}

static void main_cancel_callback(int item_id, void *client_data, void *call_data)
{
  KillAllDialogs(0);
}

static void color_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(ColorDialogID, 1);
  ColorDialogID=-1;
}

static void color_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(ColorDialogID, 0);
  ColorDialogID=-1;
}

static void pointlight_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(PointLightDialogID, 1);
  PointLightDialogID=-1;
}

static void pointlight_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(PointLightDialogID, 0);
  PointLightDialogID=-1;
}

static void directional_light_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(DirectionalLightDialogID, 1);
  DirectionalLightDialogID=-1;
}

static void directional_light_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(DirectionalLightDialogID, 0);
  DirectionalLightDialogID=-1;
}

static void material_ok_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(MaterialDialogID, 1);
  MaterialDialogID=-1;
}

static void material_cancel_callback(int item_id, void *client_data, void *call_data)
{
  gimp_close_dialog(MaterialDialogID, 0);
  MaterialDialogID=-1;
}

static void double_callback(int item_id, void *client_data, void *call_data)
{
  *((double *) client_data) = (double)atof(call_data);
}

static void radio_callback (int item_ID, void *client_data, void *call_data)
{
  *((long*) client_data) = *((long*) call_data);
}

static void scale_callback (int item_ID,void *client_data,void *call_data)
{
  *((double*) client_data) = (double)(*((long*) call_data))/255.0;
}

static void LightSettings_callback(int item_ID, void *client_data, void *call_data)
{
  int LightType=GetLightType();

  if (LightType==NO_LIGHT) return;

  /* Check what kind of light is selected and open the appropiate dialog */
  /* =================================================================== */

  if (LightType==DIRECTIONAL_LIGHT)
	 {
		/* Open directional light dialog */
		/* ============================= */

		if (DirectionalLightDialogID!=-1)
		  {
			 gimp_message("Close the other directional light dialog first!\n");
			 return;
		  }
		OldLight=CurrentLight;
		CreateDirectionalLightDialog(&CurrentLight);
 		if (gimp_show_dialog(DirectionalLightDialogID)==0) CurrentLight=OldLight;
	 }
  else if (LightType==POINT_LIGHT)
	 {
		/* Open point light dialog */
		/* ======================= */
	
		if (PointLightDialogID!=-1)
		  {
			 gimp_message("Close the other point light dialog first!\n");
			 return;
		  }

		OldLight=CurrentLight;
		CreatePointLightDialog(&CurrentLight);
 		if (gimp_show_dialog(PointLightDialogID)==0) CurrentLight=OldLight;
	 }
  else
	 {
		/* Open spot light dialog */
		/* ====================== */

		gimp_message("Map_Sphere: Light type not yet implemented! Sorry..\n");
	 }
}

static void LightColor_callback(int item_ID, void *client_data, void *call_data)
{
  if (GetLightType()==NO_LIGHT) return;

  if (ColorDialogID!=-1)
	 {
		gimp_message("Close the other color dialog first!\n");
		return;
	 }
  
  OldLight=CurrentLight;
  CreateColorDialog(&CurrentLight.Color);
  if (gimp_show_dialog(ColorDialogID)==0) CurrentLight=OldLight;
}

static void SurfaceSettings_callback(int item_ID, void *client_data, void *call_data)
{
  if (GetLightType()==NO_LIGHT) return;

  if (MaterialDialogID!=-1)
	 {
		gimp_message("Close the other material dialog first!\n");
		return;
	 }

  OldMaterial=CurrentMaterial;
  CreateMaterialDialog(&CurrentMaterial);
  if (gimp_show_dialog(MaterialDialogID)==0) CurrentMaterial=OldMaterial;
}

/***************************/
/* Main() and dialog stuff */
/***************************/

void CreateMaterialDialog(MaterialSettings *material)
{
  int MainGroupID,FrameID,FrameGroupID,ValID,DummyID;
  char buf[100];

  MaterialDialogID = gimp_new_dialog("Material settings");
  
  MainGroupID = gimp_new_row_group(MaterialDialogID, DEFAULT, NORMAL, "");
  FrameID = gimp_new_frame (MaterialDialogID, MainGroupID, "Intensity levels");
  FrameGroupID = gimp_new_row_group(MaterialDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(MaterialDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Ambient:");
  sprintf(buf, "%f", material->AmbientInt);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->AmbientInt);

  DummyID = gimp_new_column_group(MaterialDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Diffuse:");
  sprintf(buf, "%f", material->DiffuseInt);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->DiffuseInt);

  FrameID = gimp_new_frame (MaterialDialogID, MainGroupID, "Reflectivity");
  FrameGroupID = gimp_new_row_group(MaterialDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(MaterialDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Diffuse:");
  sprintf(buf, "%f", material->DiffuseRef);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->DiffuseRef);

  DummyID = gimp_new_column_group(MaterialDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Specular:");
  sprintf(buf, "%f", material->SpecularRef);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->SpecularRef);

  DummyID = gimp_new_column_group(MaterialDialogID,MainGroupID, NORMAL, "");
  gimp_new_label(MaterialDialogID, DummyID, "Highlight:");
  sprintf(buf, "%f", material->Highlight);
  ValID = gimp_new_text(MaterialDialogID,DummyID,buf);
  gimp_add_callback(MaterialDialogID, ValID, double_callback, &material->Highlight);

  /* Other callbacks */
  /* =============== */

  gimp_add_callback(MaterialDialogID, gimp_ok_item_id(MaterialDialogID), material_ok_callback, NULL);
  gimp_add_callback(MaterialDialogID, gimp_cancel_item_id(MaterialDialogID), material_cancel_callback, NULL);
}

void CreateDirectionalLightDialog(LightSettings *light)
{
  int MainGroupID,FrameID,FrameGroupID,XValID,YValID,ZValID,DummyID;
  char buf[100];

  DirectionalLightDialogID = gimp_new_dialog("Directional light settings");
  
  MainGroupID = gimp_new_row_group(DirectionalLightDialogID, DEFAULT, NORMAL, "");
  FrameID = gimp_new_frame (DirectionalLightDialogID, MainGroupID, "Direction vector");
  FrameGroupID = gimp_new_row_group(DirectionalLightDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(DirectionalLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(DirectionalLightDialogID, DummyID, "X:");
  sprintf(buf, "%f", light->Direction.x);
  XValID = gimp_new_text(DirectionalLightDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(DirectionalLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(DirectionalLightDialogID, DummyID, "Y:");
  sprintf(buf, "%f", light->Direction.y);
  YValID = gimp_new_text(DirectionalLightDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(DirectionalLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(DirectionalLightDialogID, DummyID, "Z:");
  sprintf(buf, "%f", light->Direction.z);
  ZValID = gimp_new_text(DirectionalLightDialogID,DummyID,buf);

  /* Callbacks */
  /* ========= */

  gimp_add_callback(DirectionalLightDialogID, XValID, double_callback, &light->Direction.x);
  gimp_add_callback(DirectionalLightDialogID, YValID, double_callback, &light->Direction.y);
  gimp_add_callback(DirectionalLightDialogID, ZValID, double_callback, &light->Direction.z);

  gimp_add_callback(DirectionalLightDialogID, gimp_ok_item_id(DirectionalLightDialogID),
    directional_light_ok_callback, NULL);
  gimp_add_callback(DirectionalLightDialogID, gimp_cancel_item_id(DirectionalLightDialogID),
    directional_light_cancel_callback, NULL);
}

void CreatePointLightDialog(LightSettings *light)
{
  int MainGroupID,FrameID,FrameGroupID,XValID,YValID,ZValID,DummyID;
  char buf[100];

  PointLightDialogID = gimp_new_dialog("Point light settings");
  
  MainGroupID = gimp_new_row_group(PointLightDialogID, DEFAULT, NORMAL, "");
  FrameID = gimp_new_frame (PointLightDialogID, MainGroupID, "Position");
  FrameGroupID = gimp_new_row_group(PointLightDialogID, FrameID, NORMAL, "");

  DummyID = gimp_new_column_group(PointLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(PointLightDialogID, DummyID, "X:");
  sprintf(buf, "%f", light->Position.x);
  XValID = gimp_new_text(PointLightDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(PointLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(PointLightDialogID, DummyID, "Y:");
  sprintf(buf, "%f", light->Position.y);
  YValID = gimp_new_text(PointLightDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(PointLightDialogID,FrameGroupID, NORMAL, "");
  gimp_new_label(PointLightDialogID, DummyID, "Z:");
  sprintf(buf, "%f", light->Position.z);
  ZValID = gimp_new_text(PointLightDialogID,DummyID,buf);

  /* Callbacks */
  /* ========= */

  gimp_add_callback(PointLightDialogID, XValID, double_callback, &light->Position.x);
  gimp_add_callback(PointLightDialogID, YValID, double_callback, &light->Position.y);
  gimp_add_callback(PointLightDialogID, ZValID, double_callback, &light->Position.z);

  gimp_add_callback(PointLightDialogID, gimp_ok_item_id(PointLightDialogID), pointlight_ok_callback, NULL);
  gimp_add_callback(PointLightDialogID, gimp_cancel_item_id(PointLightDialogID), pointlight_cancel_callback, NULL);
}

void CreateColorDialog(RGBPixel *color)
{
  int MainGroupID,ColorFrameID,ColorGroupID,RedScaleID,GreenScaleID,BlueScaleID;

  ColorDialogID = gimp_new_dialog("Light color");
  MainGroupID = gimp_new_row_group(ColorDialogID, DEFAULT, NORMAL, "");

  ColorFrameID = gimp_new_frame (ColorDialogID, MainGroupID, "Color");
  ColorGroupID = gimp_new_row_group(ColorDialogID, ColorFrameID, NORMAL, "");
  RedScaleID = gimp_new_scale (ColorDialogID, ColorGroupID, 0, 255, (long)(255.0*color->r), 0);
  gimp_new_label(ColorDialogID, RedScaleID, "Red:");
  GreenScaleID = gimp_new_scale (ColorDialogID, ColorGroupID, 0, 255, (long)(255.0*color->g), 0);
  gimp_new_label(ColorDialogID, GreenScaleID, "Green:");
  BlueScaleID = gimp_new_scale (ColorDialogID, ColorGroupID, 0, 255, (long)(255.0*color->b), 0);
  gimp_new_label(ColorDialogID, BlueScaleID, "Blue:");

  /* Callbacks */
  /* ========= */

  gimp_add_callback (ColorDialogID, RedScaleID, scale_callback, &color->r);
  gimp_add_callback (ColorDialogID, GreenScaleID, scale_callback, &color->g);
  gimp_add_callback (ColorDialogID, BlueScaleID, scale_callback, &color->b);

  gimp_add_callback(ColorDialogID, gimp_ok_item_id(ColorDialogID), color_ok_callback, NULL);
  gimp_add_callback(ColorDialogID, gimp_cancel_item_id(ColorDialogID), color_cancel_callback, NULL);
}

void CreateMainDialog(void)
{
  int MainGroupID,LightFrameID,LightGroupID,DirectionalLightID,PointLightID,SpotLightID,RadioLightID;
  int LightSettingsID,LightColorID,PosFrameID,PosGroupID,DummyID,XValID,YValID,ZValID,RadiusID;
  int SurfaceSettingsID,NoLightID;
  char buf[100];

  MainDialogID = gimp_new_dialog("Map Sphere");
  MainGroupID = gimp_new_row_group(MainDialogID, DEFAULT, NORMAL, "");

  LightFrameID = gimp_new_frame (MainDialogID, MainGroupID, "Light source");
  LightGroupID = gimp_new_row_group(MainDialogID, LightFrameID, NORMAL, "");

  RadioLightID = gimp_new_row_group(MainDialogID, LightGroupID, RADIO, "");
  NoLightID = gimp_new_radio_button (MainDialogID, RadioLightID, "No light");
  DirectionalLightID = gimp_new_radio_button (MainDialogID, RadioLightID, "Directional light");
  PointLightID = gimp_new_radio_button (MainDialogID, RadioLightID, "Point light");
  SpotLightID = gimp_new_radio_button (MainDialogID, RadioLightID, "Spot light");

  LightSettingsID = gimp_new_push_button(MainDialogID,MainGroupID,"Lightsource settings..");
  LightColorID = gimp_new_push_button(MainDialogID,MainGroupID,"Lightsource color..");

  PosFrameID = gimp_new_frame (MainDialogID, MainGroupID, "Position and size");
  PosGroupID = gimp_new_row_group(MainDialogID, PosFrameID, NORMAL, "");

  DummyID = gimp_new_column_group(MainDialogID,PosGroupID, NORMAL, "");
  gimp_new_label(MainDialogID, DummyID, "X:");
  sprintf(buf, "%f", SpherePosition.x);
  XValID = gimp_new_text(MainDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(MainDialogID,PosGroupID, NORMAL, "");
  gimp_new_label(MainDialogID, DummyID, "Y:");
  sprintf(buf, "%f", SpherePosition.y);
  YValID = gimp_new_text(MainDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(MainDialogID,PosGroupID, NORMAL, "");
  gimp_new_label(MainDialogID, DummyID, "Z:");
  sprintf(buf, "%f", SpherePosition.z);
  ZValID = gimp_new_text(MainDialogID,DummyID,buf);

  DummyID = gimp_new_column_group(MainDialogID,PosGroupID, NORMAL, "");
  gimp_new_label(MainDialogID, DummyID, "Radius:");
  sprintf(buf, "%f", SphereRadius);
  RadiusID = gimp_new_text(MainDialogID,DummyID,buf);

  SurfaceSettingsID = gimp_new_push_button(MainDialogID,MainGroupID,"Image material settings..");

  /* Default settings */
  /* ================ */

  gimp_change_item (MainDialogID, NoLightID, sizeof(light_toggles[0]), &light_toggles[0]);
  gimp_change_item (MainDialogID, DirectionalLightID, sizeof(light_toggles[1]), &light_toggles[1]);
  gimp_change_item (MainDialogID, PointLightID, sizeof(light_toggles[2]), &light_toggles[2]);
  gimp_change_item (MainDialogID, SpotLightID, sizeof(light_toggles[3]), &light_toggles[3]);

  /* Callbacks */
  /* ========= */

  gimp_add_callback (MainDialogID, NoLightID, radio_callback, &light_toggles[0]);
  gimp_add_callback (MainDialogID, DirectionalLightID, radio_callback, &light_toggles[1]);
  gimp_add_callback (MainDialogID, PointLightID, radio_callback, &light_toggles[2]);
  gimp_add_callback (MainDialogID, SpotLightID, radio_callback, &light_toggles[3]);

  gimp_add_callback (MainDialogID, LightSettingsID, LightSettings_callback, NULL);
  gimp_add_callback (MainDialogID, LightColorID, LightColor_callback, NULL);

  gimp_add_callback(MainDialogID, XValID, double_callback, &SpherePosition.x);
  gimp_add_callback(MainDialogID, YValID, double_callback, &SpherePosition.y);
  gimp_add_callback(MainDialogID, ZValID, double_callback, &SpherePosition.z);
  gimp_add_callback(MainDialogID, RadiusID, double_callback, &SphereRadius);

  gimp_add_callback(MainDialogID, SurfaceSettingsID, SurfaceSettings_callback, NULL);

  gimp_add_callback(MainDialogID, gimp_ok_item_id(MainDialogID), main_ok_callback, NULL);
  gimp_add_callback(MainDialogID, gimp_cancel_item_id(MainDialogID), main_cancel_callback, NULL);
}

int main(int argc,char **argv)
{
  /* Standard stuff straight out of Marc's tutorial :) */
  /* ================================================= */

  if (!gimp_init(argc,argv)) return(0);

  input=gimp_get_input_image(0);
  output=gimp_get_output_image(0);

  if (!input || !output) return(0);

  /* We deal only with RGB images */
  /* ============================ */

  if (gimp_image_type(input)!=RGB_IMAGE) gimp_message("Map_Sphere: On RGB type images only!\n");
  else
	 {
		/* Create a nice dialog and show it */
		/* ================================ */

		SetDefaultSettings();
		InitCleanup();
 		CreateMainDialog();
 		if (gimp_show_dialog(MainDialogID)) ComputeLighting();
		Cleanup();
	 }

  /* Standard The End stuff.. */
  /* ======================== */

  gimp_free_image(input);
  gimp_free_image(output);

  gimp_quit();
  return(0);
}
