/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*  saves and loads gimp brush files...
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gimp.h"

/* Declare some local functions.
 */
static void load_image (char *);
static void save_image (char *);

char *prog_name;

void
main (argc, argv)
     int argc;
     char **argv;
{
  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      /* This is a file filter so all it needs to know about is loading
       *  and saving images. So we'll install handlers for those two
       *  messages.
       */
      gimp_install_load_save_handlers (load_image, save_image);

      /* Run until something happens. That something could be getting
       *  a 'QUIT' message or getting a load or save message.
       */
      gimp_main_loop ();
    }
}


static void
load_image (filename)
     char *filename;
{
  Image image;
  FILE * fp;
  char name_buf[256];
  char * data_buf;
  unsigned char *dest;
  int val;
  int width, height;
  int i, j;

  /*  Open the requested file  */
  if (! (fp = fopen (filename, "r")))
    {
      printf ("%s: can't open \"%s\"\n", prog_name, filename);
      gimp_quit ();
    }

  /*  Parse past any comments  */
  fscanf (fp, "/*  GIMP icon image format -- S. Kimball, P. Mattis  */\n");
  fscanf (fp, "/*  Image name: %s  */\n", name_buf);

  /*  Get the width and height  */
  fscanf (fp, "#define %s %d\n", name_buf, &width);
  fscanf (fp, "#define %s %d\n", name_buf, &height);
  fscanf (fp, "static char *%s [] = \n{\n", name_buf);

  /*  Get a new image structure  */
  image = gimp_new_image (filename, width, height, GRAYA_IMAGE);
  dest = gimp_image_data (image);

  data_buf = (char *) malloc (width);

  for (i = 0; i < height; i++)
    {
      fscanf (fp, "  \"%s\",\n", data_buf);
      for (j = 0; j < width; j++)
	{
	  val = data_buf[j];
	  if (val == '.')
	    {
	      *dest++ = 0;
	      *dest++ = 0;  /*  set alpha channel to transparent  */
	    }
	  else
	    {
	      *dest++ = (255 * (val - 'a')) / 7;
	      *dest++ = 255;  /*  set alpha channel to opaque  */
	    }
	}
    }

  /*  Clean up  */
  fclose (fp);
  gimp_display_image (image);
  gimp_update_image (image);
  gimp_free_image (image);

  free (data_buf);
  gimp_quit ();
}


static void
save_image (filename)
     char *filename;
{
  Image image;
  FILE * fp;
  int i, j;
  int w, h;
  int bytes;
  int has_alpha;
  int val;
  char ch;
  unsigned char *src;

  image = gimp_get_input_image (0);
  switch (gimp_image_type (image))
    {
    case GRAY_IMAGE:
    case GRAYA_IMAGE:
      break;
    default:
      gimp_message ("gicon: can only operate on gray images");
      gimp_free_image (image);
      gimp_quit ();
      break;
    }

  w = gimp_image_width (image);
  h = gimp_image_height (image);
  has_alpha = gimp_image_alpha (image);
  bytes = gimp_image_channels (image);

  /*  open the file for writing  */
  if ((fp = fopen (filename, "w")))
    {
      fprintf (fp, "/*  GIMP icon image format -- S. Kimball, P. Mattis  */\n");
      fprintf (fp, "/*  Image name: %s  */\n", gimp_image_name (image));
      fprintf (fp, "\n\n");
      fprintf (fp, "#define %s_width %d\n", gimp_image_name (image), w);
      fprintf (fp, "#define %s_height %d\n", gimp_image_name (image), h);
      fprintf (fp, "static char *%s_bits [] = \n{\n", gimp_image_name (image));

      /*  write the brush data to the file  */
      src = gimp_image_data (image);
      for (i = 0; i < h; i++)
	{
	  fprintf (fp, "  \"");
	  for (j = 0; j < w; j++)
	    {
	      if (has_alpha && !src[1])
		{
		  ch = '.';
		}
	      else
		{
		  val = (src[0] * 7) / 255;
		  ch = 'a' + val;
		}
	      fputc (ch, fp);

	      src += bytes;
	    }

	  fprintf (fp, (i == (h-1)) ? "\"\n};\n" : "\",\n");
	}

      fclose (fp);
    }

  /*  Clean up  */
  gimp_free_image (image);
  gimp_quit ();
}


