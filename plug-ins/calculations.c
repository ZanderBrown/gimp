/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 * Performs a variety of calculations on different source images,
 *   in all cases yielding a new, destination image...
 *  The different calculations:
 *    Add,
 *    Composite,
 *    Difference,
 *    Multiply,
 *    Screen,
 *    Subtract
 */

#include <stdio.h>
#include <stdlib.h>
#include "gimp.h"

#define ADD         0
#define DIFFERENCE  1
#define SUBTRACT    2
#define MULTIPLY    3
#define SCREEN      4
#define COMPOSITE   5

#define MAX(a,b) (((a) > (b)) ? (a) : (b));

/* Declare local functions.
 */
static void radio_callback (int, void *, void *);
static void image_menu_callback (int, void *, void *);
static void ok_callback (int, void *, void *);
static void cancel_callback (int, void *, void *);
static void calculations (Image, Image, Image);

typedef struct
{
       char * name;  /*  Mode name  */
       int src1;     /*  Requires src  */
       int src2;     /*  Requires 2nd src  */
       int mask;     /*  Requires Mask  */
} Mode;

static char *prog_name;
static int dialog_id;
static int mode;
static int num_modes = 6;
static Mode modes[6] = 
{
  { "Add", 1, 1, 0 },
  { "Difference", 1, 1, 0 },
  { "Subtract", 1, 1, 0 },
  { "Multiply", 1, 1, 0 },
  { "Screen", 1, 1, 0 },
  { "Composite", 1, 1, 1 }
};

int
main (argc, argv)
     int argc;
     char **argv;
{
  Image src1, src2, mask;
  int outer_group_id;
  int group_id;
  int frame_id;
  int radio_id;
  int image_menu1_id;
  int image_menu2_id;
  int image_menu3_id;
  long src1_id, src2_id, mask_id;
  char buffer[256];
  void * data;
  int on;
  int i;

  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      data = gimp_get_params ();

      if (data)
	mode = *((int *) data);
      else
	mode = ADD;

      src1 = src2 = 0;
      src1_id = src2_id = mask_id = 0;
      
      dialog_id = gimp_new_dialog ("Calculations");
      outer_group_id = gimp_new_row_group (dialog_id, DEFAULT, NORMAL, "");
      
      group_id = gimp_new_row_group (dialog_id, outer_group_id, NORMAL, "");
      image_menu1_id = gimp_new_image_menu (dialog_id, group_id, 
					    IMAGE_CONSTRAIN_RGB | IMAGE_CONSTRAIN_GRAY,
					    "First Image");
      image_menu2_id = gimp_new_image_menu (dialog_id, group_id, 
					    IMAGE_CONSTRAIN_RGB | IMAGE_CONSTRAIN_GRAY,
					    "Second Image");
      image_menu3_id = gimp_new_image_menu (dialog_id, group_id, 
					    IMAGE_CONSTRAIN_GRAY,
					    "Mask Image");
      gimp_add_callback (dialog_id, image_menu1_id, image_menu_callback, &src1_id);
      gimp_add_callback (dialog_id, image_menu2_id, image_menu_callback, &src2_id);
      gimp_add_callback (dialog_id, image_menu3_id, image_menu_callback, &mask_id);
      gimp_add_callback (dialog_id, gimp_ok_item_id (dialog_id), ok_callback, 0);
      gimp_add_callback (dialog_id, gimp_cancel_item_id (dialog_id), cancel_callback, 0);

      frame_id = gimp_new_frame (dialog_id, outer_group_id, "Mode");
      group_id = gimp_new_row_group (dialog_id, frame_id, RADIO, "");
      for (i = 0; i < num_modes; i++)
	{
	  radio_id = gimp_new_radio_button (dialog_id, group_id, modes[i].name);
	  on = (mode == i);
	  gimp_change_item (dialog_id, radio_id, sizeof (int), &on);
	  gimp_add_callback (dialog_id, radio_id, radio_callback, (void *) i);
	}

      if (gimp_show_dialog (dialog_id))
	{
	  gimp_set_params(sizeof(mode), &mode);

	  src1 = gimp_get_input_image (src1_id);
	  
	  if (src2_id == src1_id)
	    src2 = src1;
	  else
	    src2 = gimp_get_input_image (src2_id);
	  
	  if ((mask_id == src1_id) || (mask_id == src2_id))
	    mask = (mask_id == src1_id) ? src1 : src2;
	  else
	    mask = gimp_get_input_image (mask_id);
	  
	  if (modes[mode].src1 && !src1)
	    {
	      sprintf (buffer, "%s requires a valid source #1", modes[mode].name);
	      gimp_message (buffer);
	    }
	  else if (modes[mode].src2 && !src2)
	    {
	      sprintf (buffer, "%s requires a valid source #2", modes[mode].name);
	      gimp_message (buffer);
	    }
	  else if (modes[mode].mask && !mask)
	    {
	      sprintf (buffer, "%s requires a valid mask", modes[mode].name);
	      gimp_message (buffer);
	    }
	  else
	    {
	      gimp_init_progress (modes[mode].name);
	      calculations (src1, src2, mask);
	    }
	}

      /* Free the images.
       */
      if (src1)
	gimp_free_image (src1);
      if (src2 && (src2_id != src1_id))
	gimp_free_image (src2);
      if (mask && (mask_id != src1_id) && (mask_id != src2_id))
	gimp_free_image (mask);

      /* Quit
       */
      gimp_quit ();
    }

  return 0;
}

static void
radio_callback (item_id, client_data, call_data)
     int item_id;
     void *client_data;
     void *call_data;
{
  if (*((long *) call_data))
    mode = (int) client_data;
}

static void
image_menu_callback (item_id, client_data, call_data)
     int item_id;
     void *client_data;
     void *call_data;
{
  *((long*) client_data) = *((long*) call_data);
}

static void
ok_callback (item_id, client_data, call_data)
     int item_id;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_id, 1);
}

static void
cancel_callback (item_id, client_data, call_data)
     int item_id;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_id, 0);
}


static void
multiply_pixels (src1, src2, dest, length, bytes, has_alpha)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int bytes;
     int has_alpha;
{
  int alpha, b;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = (has_alpha) ? 
	  ((src1[b] * src1[alpha]) * (src2[b] * src2[alpha])) / 16777216 :
	(src1[b] * src2[b]) / 255;

      if (has_alpha)
	dest[alpha] = MAX (src1[alpha], src2[alpha]);

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


static void
screen_pixels (src1, src2, dest, length, bytes, has_alpha)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int bytes;
     int has_alpha;
{
  int alpha, b;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = (has_alpha) ? 
	  255 - ((65025 - (src1[b] * src1[alpha])) *
		 (65025 - (src2[b] * src2[alpha]))) / 16777216 :
	255 - ((255 - src1[b]) * (255 - src2[b])) / 255;

      if (has_alpha)
	dest[alpha] = MAX (src1[alpha], src2[alpha]);

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


static void
composite_pixels (src1, src2, mask, dest, length, bytes, mask_bytes, has_alpha)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * mask;
     unsigned char * dest;
     int length;
     int bytes;
     int mask_bytes;
     int has_alpha;
{
  int alpha, b;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = (has_alpha) ? 
	  ((src1[b] * src1[alpha] * mask[0]) +
	   (src2[b] * src2[alpha] * (255 - mask[0]))) / 65025 :
	(src1[b] * mask[0] + src2[b] * (255 - mask[0])) / 255;

      if (has_alpha)
	dest[alpha] = MAX (src1[alpha], src2[alpha]);

      src1 += bytes;
      src2 += bytes;
      mask += mask_bytes;
      dest += bytes;
    }
}


static void
add_pixels (src1, src2, dest, length, bytes, has_alpha)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int bytes;
     int has_alpha;
{
  int alpha, b;
  int sum;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	{
	  sum = (has_alpha) ? 
	    (src1[b] * src1[alpha] + src2[b] * src2[alpha])/255 :
	    (src1[b] + src2[b]);
	  dest[b] = (sum > 255) ? 255 : sum;
	}

      if (has_alpha)
	dest[alpha] = MAX (src1[alpha], src2[alpha]);

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


static void
subtract_pixels (src1, src2, dest, length, bytes, has_alpha)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int bytes;
     int has_alpha;
{
  int alpha, b;
  int diff;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	{
	  diff = (has_alpha) ? 
	    (src1[b] * src1[alpha] - src2[b] * src2[alpha])/255 :
	    (src1[b] - src2[b]);
	  dest[b] = (diff < 0) ? 0 : diff;
	}

      if (has_alpha)
	dest[alpha] = MAX (src1[alpha], src2[alpha]);

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


static void
difference_pixels (src1, src2, dest, length, bytes, has_alpha)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int length;
     int bytes;
     int has_alpha;
{
  int alpha, b;
  int diff;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	{
	  diff = (has_alpha) ? 
	    (src1[b] * src1[alpha] - src2[b] * src2[alpha])/255 :
	    (src1[b] - src2[b]);
	  dest[b] = (diff < 0) ? -diff : diff;
	}

      if (has_alpha)
	dest[alpha] = MAX (src1[alpha], src2[alpha]);

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


static void
add_alpha_pixels (src, dest, length, bytes)
     unsigned char * src, * dest;
     int length;
     int bytes;
{
  int alpha, b;
  
  alpha = bytes + 1;
  while (length --)
    {
      for (b = 0; b < bytes; b++)
	dest[b] = src[b];

      dest[b] = 255;

      src += bytes;
      dest += alpha;
    }
}


static void
colorize_pixels (src, dest, length, bytes, has_alpha)
     unsigned char * src, * dest;
     int length;
     int bytes;      /*  bytes of src  */
     int has_alpha;  /*  does src have an alpha channel?  */
{
  int b, dest_bytes;
  
  dest_bytes = (has_alpha) ? 4 : 3;

  while (length --)
    {
      for (b = 0; b < 3; b++)
	dest[b] = src[0];

      if (has_alpha)
	dest[3] = src[1];

      src += bytes;
      dest += dest_bytes;
    }
}


static void
colorize_pixels_add_alpha (src, dest, length, bytes, has_alpha)
     unsigned char * src, * dest;
     int length;
     int bytes;      /*  bytes of src  */
     int has_alpha;  /*  does src have an alpha channel?  */
{
  int b;
  
  while (length --)
    {
      for (b = 0; b < 3; b++)
	dest[b] = src[0];

      if (has_alpha)
	dest[3] = src[1];
      else
	dest[3] = 255;

      src += bytes;
      dest += 4;
    }
}


static void
calculations (src1, src2, mask)
     Image src1, src2, mask;
{
  Image dest;
  ImageType dest_type;
  ImageType src1_type;
  ImageType src2_type;
  int src1_rowstride;
  int src2_rowstride;
  int mask_rowstride;
  int dest_rowstride;
  int src1_channels;
  int src2_channels;
  int mask_channels;
  int channels;
  unsigned char *src1p;
  unsigned char *src2p;
  unsigned char *maskp;
  unsigned char *destp;
  unsigned char *buf1, *buf2;
  long width, height;
  int has_alpha;
  int has_color;
  int i;

  src1_type = gimp_image_type (src1);
  if (src2)
    src2_type = gimp_image_type (src2);
  else
    src2_type = -1;

  has_alpha = (gimp_image_alpha (src1) || gimp_image_alpha (src2)) ? 1 : 0;

  if (src1_type == RGBA_IMAGE || src2_type == RGBA_IMAGE ||
      src1_type == RGB_IMAGE || src2_type == RGB_IMAGE)
    has_color = 1;
  else
    has_color = 0;

  if (has_color)
    {
      if (has_alpha)
	dest_type = RGBA_IMAGE;
      else
	dest_type = RGB_IMAGE;
    }
  else
    {
      if (has_alpha)
	dest_type = GRAYA_IMAGE;
      else
	dest_type = GRAY_IMAGE;
    }

  width = gimp_image_width (src1);
  height = gimp_image_height (src1);
  
  dest = gimp_new_image (0, width, height, dest_type);
  destp = gimp_image_data (dest);
  channels = gimp_image_channels (dest);
  dest_rowstride = width * channels;
  has_alpha = gimp_image_alpha (dest);

  if (src1)
    {
      src1p = gimp_image_data (src1);
      src1_channels = gimp_image_channels (src1);
      src1_rowstride = src1_channels * width;
      if ((has_alpha && !gimp_image_alpha (src1)) ||
	  (has_color && (src1_type == GRAYA_IMAGE || src1_type == GRAY_IMAGE)))
	buf1 = (unsigned char *) malloc (width * channels);
      else
	buf1 = NULL;
    }
  if (src2)
    {
      src2p = gimp_image_data (src2);
      src2_channels = gimp_image_channels (src2);
      src2_rowstride = src2_channels * width;
      if ((has_alpha && !gimp_image_alpha (src2)) ||
	  (has_color && (src2_type == GRAYA_IMAGE || src2_type == GRAY_IMAGE)))
	buf2 = (unsigned char *) malloc (width * channels);
      else
	buf2 = NULL;
    }
  if (mask)
    {
      if (gimp_image_type (mask) == GRAYA_IMAGE)
	gimp_message ("Mask image has an alpha channel, which will be ignored");
      maskp = gimp_image_data (mask);
      mask_channels = gimp_image_channels (mask);
      mask_rowstride = mask_channels * width;
    }

  for (i = 0; i < height; i++)
    {
      /*  Make sure we add alpha channels to simplify the process  */
      if (has_color && has_alpha && src1_type == GRAY_IMAGE)
	colorize_pixels_add_alpha (src1p, buf1, width, src1_channels,
				   gimp_image_alpha (src1));
      else if (has_color && (src1_type == GRAYA_IMAGE || src1_type == GRAY_IMAGE))
	colorize_pixels (src1p, buf1, width, src1_channels,
			 gimp_image_alpha (src1));
      else if (has_alpha && !gimp_image_alpha (src1))
	add_alpha_pixels (src1p, buf1, width, src1_channels);
	  
      if (has_color && has_alpha && src2_type == GRAY_IMAGE)
	colorize_pixels_add_alpha (src2p, buf2, width, src2_channels,
				   gimp_image_alpha (src2));
      else if (has_color && (src2_type == GRAYA_IMAGE || src2_type == GRAY_IMAGE))
	colorize_pixels (src2p, buf2, width, src2_channels,
			 gimp_image_alpha (src2));
      else if (has_alpha && !gimp_image_alpha (src2))
	add_alpha_pixels (src2p, buf2, width, src2_channels);

      switch (mode)
	{
	case ADD:
	  add_pixels ((buf1) ? buf1 : src1p, (buf2) ? buf2 : src2p,
		      destp, width, channels, has_alpha);
	  break;
	case DIFFERENCE:
	  difference_pixels ((buf1) ? buf1 : src1p, (buf2) ? buf2 : src2p,
			     destp, width, channels, has_alpha);
	  break;
	case SUBTRACT:
	  subtract_pixels ((buf1) ? buf1 : src1p, (buf2) ? buf2 : src2p,
			   destp, width, channels, has_alpha);
	  break;
	case MULTIPLY:
	  multiply_pixels ((buf1) ? buf1 : src1p, (buf2) ? buf2 : src2p,
			   destp, width, channels, has_alpha);
	  break;
	case SCREEN:
	  screen_pixels ((buf1) ? buf1 : src1p, (buf2) ? buf2 : src2p,
			 destp, width, channels, has_alpha);
	  break;
	case COMPOSITE:
	  composite_pixels ((buf1) ? buf1 : src1p, (buf2) ? buf2 : src2p,
			    maskp, destp, width,
			    channels, mask_channels, has_alpha);
	  break;
	}

      if (src1)
	src1p += src1_rowstride;
      if (src2)
	src2p += src2_rowstride;
      if (mask)
	maskp += mask_rowstride;

      destp += dest_rowstride;

      if ((i % 5) == 0)
	gimp_do_progress (i, height);
    }

  gimp_display_image (dest);
  gimp_update_image (dest);
  gimp_free_image (dest);
}
