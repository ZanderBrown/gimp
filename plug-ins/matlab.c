/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*  saves matlab intensity matrices...
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gimp.h"

/* Declare some local functions.
 */
static void load_image (char *);
static void save_image (char *);

char *prog_name;

void
main (argc, argv)
     int argc;
     char **argv;
{
  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      /* This is a file filter so all it needs to know about is loading
       *  and saving images. So we'll install handlers for those two
       *  messages.
       */
      gimp_install_load_save_handlers (load_image, save_image);

      /* Run until something happens. That something could be getting
       *  a 'QUIT' message or getting a load or save message.
       */
      gimp_main_loop ();
    }
}


static void
load_image (filename)
     char *filename;
{
  gimp_quit ();
}


static void
save_image (filename)
     char *filename;
{
  Image image;
  FILE * fp;
  int i, j;
  int w, h;
  int bytes;
  float val;
  unsigned char *src;

  image = gimp_get_input_image (0);
  switch (gimp_image_type (image))
    {
    case GRAY_IMAGE:
      break;
    default:
      gimp_message ("gicon: can only operate on gray images");
      gimp_free_image (image);
      gimp_quit ();
      break;
    }

  w = gimp_image_width (image);
  h = gimp_image_height (image);
  bytes = gimp_image_channels (image);

  /*  open the file for writing  */
  if ((fp = fopen (filename, "w")))
    {
      fprintf (fp, "%%  GIMP to matlab -- S. Kimball\n");
      fprintf (fp, "%%  Image name: %s\n", gimp_image_name (image));
      fprintf (fp, "\n");
      fprintf (fp, "[");

      /*  write the brush data to the file  */
      src = gimp_image_data (image);
      for (i = 0; i < h; i++)
	{
	  fprintf (fp, (i == 0) ? " " : "  ");
	  for (j = 0; j < w; j++)
	    {
	      val = (float) src[0] / 255.0;
	      fprintf (fp, "%1.3f", val);
	      if (j != (w - 1))
		fprintf (fp, ", ");
	      src += bytes;
	    }

	  fprintf (fp, (i == (h-1)) ? " ]\n\n" : ";\n");
	}

      fclose (fp);
    }

  /*  Clean up  */
  gimp_free_image (image);
  gimp_quit ();
}


