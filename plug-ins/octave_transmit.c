#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define RGB       0
#define GRAY      2
#define INDEXED   4

int  dnet_connect (char *);
int  dnet_disconnect (void);
int  dnet_send (unsigned char *, int);
void load_octave_file (FILE *);

/* The name of this program, as taken from argv[0]. */
char *progname;

static void fatal(message)
char *message;
{
  if(message != NULL) {
    fprintf(stderr,"octave_transmit: %s\n",message);
  }
  exit(1);
}

int
main (argc, argv)
     int argc;
     char **argv;
{
  char *oct_file_name;
  FILE *oct_file;

  progname = argv[0];

  if (argc < 2)
    {
      fprintf (stderr, "usage: octave_transmit <file> [<server>]\n");
      exit (1);
    }

  dnet_connect ((argc == 2) ? "localhost" : argv[2]);

  oct_file_name = argv[1];
  if((oct_file = fopen(oct_file_name,"r")) == NULL) {
    fatal("unable to open input file");
  }

  load_octave_file (oct_file);

  return 0;
}

void
load_octave_file (oct_file)
     FILE * oct_file;
{
  int row, col;
  int cmap_rows, cmap_cols, img_rows, img_cols;
  int gray, image_type;
  char * buffer;
  unsigned char **rgb, byte;
  unsigned short **img;
  char cmap_name[4], cmap_type[7], img_name[2], img_type[7];
  double mat_val;

  if(fscanf(oct_file,"# name: %s\n",cmap_name) != 1 || 
     strcmp(cmap_name,"map") != 0) {
    fatal("not a valid octave image file");
  }

  if(fscanf(oct_file,"# type: %s\n",cmap_type) != 1 || 
     strcmp(cmap_type,"matrix") != 0) {
    fatal("not a valid octave image file");
  }

  if(fscanf(oct_file,"# rows: %d\n",&cmap_rows) != 1) {
    fatal("error reading octave image file");
  }

  if(fscanf(oct_file,"# columns: %d\n",&cmap_cols) != 1) {
    fatal("error reading octave image file");
  }

  if(cmap_cols != 3) {
    fatal("invalid color map in octave image file");
  }

  if((rgb = (unsigned char **)
      malloc(cmap_rows*sizeof(unsigned char *))) == NULL) {
    fatal("out of memory");
  }

  if((rgb[0] = (unsigned char *)
      malloc(cmap_rows*cmap_cols*sizeof(unsigned char))) == NULL) {
    fatal("out of memory");
  }

  for(row=1; row<cmap_rows; row++) {
    rgb[row] = rgb[row-1]+3;
  }

  gray = 1;
  for(row=0; row<cmap_rows; row++) {
    for(col=0; col<cmap_cols; col++) {
      if(fscanf(oct_file,"%lf",&mat_val) != 1) {
        fatal("error reading color map entries");
      }
      if(mat_val < 0) mat_val = 0.;
      if(mat_val > 1) mat_val = 1.;
      rgb[row][col] = mat_val*255;
    }
    if(gray) {
      if(rgb[row][0] != rgb[row][1] || rgb[row][0] != rgb[row][2]) {
        /* It's a color image. */
        gray = 0;
      }
    }
  }

  if(fscanf(oct_file,"\n# name: %s\n",img_name) != 1) {
    fatal("not a valid octave image file");
  }

  if(fscanf(oct_file,"# type: %s\n",img_type) != 1 || 
     strcmp(img_type,"matrix") != 0) {
    fatal("not a valid octave image file");
  }

  if(fscanf(oct_file,"# rows: %d\n",&img_rows) != 1) {
    fatal("error reading octave image file");
  }

  if(fscanf(oct_file,"# columns: %d\n",&img_cols) != 1) {
    fatal("error reading octave image file");
  }

  if((img = (unsigned short **)
      malloc(img_rows*sizeof(unsigned short *))) == NULL) {
    fatal("out of memory");
  }

  if((img[0] = (unsigned short *)
      malloc(img_rows*img_cols*sizeof(unsigned short))) == NULL) {
    fatal("out of memory");
  }

  for(row=1; row<img_rows; row++) {
    img[row] = img[row-1]+img_cols;
  }

  for(row=0; row<img_rows; row++) {
    for(col=0; col<img_cols; col++) {
      if(fscanf(oct_file,"%lf",&mat_val) != 1) {
        fatal("error reading color map entries");
      }
      if(mat_val < 1) mat_val = 1.;
      if(mat_val > cmap_rows) mat_val = cmap_rows;
      img[row][col] = mat_val;
    }
  }

  image_type = (gray) ? GRAY : RGB;

  buffer = (unsigned char *) malloc (img_cols * ((image_type == GRAY) ? 1 : 3));

  sprintf (buffer, "G%c%c%c%c%c%c", image_type, strlen (img_name) + 1,
	   (img_cols >> 8), (img_cols & 0xff), (img_rows >> 8), (img_rows & 0xff));
  dnet_send (buffer, 7);
  dnet_send (img_name, strlen (img_name) + 1);
  
  switch (image_type)
    {
    case GRAY:
      for(row=0; row<img_rows; row++)
	{
	  for(col=0; col<img_cols; col++)
	    buffer[col] = rgb[img[row][col]-1][0];
	  dnet_send (buffer, img_cols);
	}
      break;
    case RGB:
      for(row=0; row<img_rows; row++)
	{
	  for(col=0; col<img_cols; col++)
	    {
	      buffer[col*3 + 0] = rgb[img[row][col]-1][0];
	      buffer[col*3 + 1] = rgb[img[row][col]-1][1];
	      buffer[col*3 + 2] = rgb[img[row][col]-1][2];
	    }
	  dnet_send (buffer, img_cols * 3);
	}
      break;
    }

  dnet_disconnect ();
}


void 
init_sockaddr (struct sockaddr_in *name,
               const char *hostname,
               unsigned short int port)
{
  struct hostent *hostinfo;

  name->sin_family = AF_INET;
  name->sin_port = htons (port);
  hostinfo = gethostbyname (hostname);
  if (hostinfo == NULL) 
    {
      fprintf (stderr, "Unknown host %s.\n", hostname);
      exit (-1);
    }
  name->sin_addr = *(struct in_addr *) hostinfo->h_addr;
}

static int sock;

int 
dnet_connect (server)
     char *server;
{
#define PORT 5556

  struct sockaddr_in servername;

  /* Create the socket.  */
  sock = socket (PF_INET, SOCK_STREAM, 0);
  if (sock < 0)
    {
      perror ("socket (client)");
      exit (-1);
    }

  /* Connect to the server.  */
  init_sockaddr (&servername, server, PORT);
  if (0 > connect (sock,
                   (struct sockaddr *) &servername,
                   sizeof (servername)))
    {
      perror ("connection to GIMP octave server");
      exit (-1);
    }

  return 0;
}

int
dnet_disconnect ()
{
  close (sock);
  return 0;
}

int 
dnet_send (data, len)
     unsigned char *data;
     int len;
{
  int nbytes;

  nbytes = write (sock, data, len);
  if (nbytes < 0)
    {
      perror ("write");
      return 1;
    }

  return 0;
}
