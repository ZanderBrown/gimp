/*
 * This is a plug-in for the GIMP.
 *
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 * Copyright (C) 1996 Torsten Martinsen <bullestock@dk-online.dk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: engrave.c,v 1.3 1996/06/10 17:54:45 torsten Exp $
 */

/*
 * This plug-in creates a black-and-white 'engraved' version of an image.
 * Much of the code is stolen from the Pixelize plug-in.
 */

#include "gimp.h"

static void scale_callback(int, void *, void *);
static void ok_callback(int, void *, void *);
static void cancel_callback(int, void *, void *);
static void engrave(Image, Image);

long engraveheight = 4;
int dialog_ID;

#define R 0
#define G 1
#define B 2

#define INTENSITY(r,g,b) (r * 0.30 + g * 0.59 + b * 0.11)

int
main(int argc, char **argv)
{
    Image input, output;
    void *data;
    int scale_ID;

    if (gimp_init(argc, argv)) {
	data = gimp_get_params();
	if (data)
	    engraveheight = *((long *) data);

	input = output = 0;

	input = gimp_get_input_image(0);

	if (input)
	    switch (gimp_image_type(input)) {
	    case RGB_IMAGE:
	    case GRAY_IMAGE:
		dialog_ID = gimp_new_dialog("Engrave");
		scale_ID = gimp_new_scale(dialog_ID, DEFAULT, 2, 16,
					  engraveheight, 0);
		gimp_add_callback(dialog_ID,
				  scale_ID, scale_callback, &engraveheight);
		gimp_add_callback(dialog_ID,
				  gimp_ok_item_id(dialog_ID), ok_callback, 0);
		gimp_add_callback(dialog_ID, gimp_cancel_item_id(dialog_ID),
				  cancel_callback, 0);

		if (gimp_show_dialog(dialog_ID)) {
		    gimp_set_params(sizeof(engraveheight), &engraveheight);

		    output = gimp_get_output_image(0);
		    if (output) {
			engrave(input, output);
			gimp_update_image(output);
		    }
		}
		break;
	    case INDEXED_IMAGE:
		gimp_message("engrave: cannot operate on indexed color images");
		break;
	    default:
		gimp_message("engrave: cannot operate on unknown image types");
		break;
	    }
	/* Free both images.
	 */
	if (input)
	    gimp_free_image(input);
	if (output)
	    gimp_free_image(output);

	/* Quit
	 */
	gimp_quit();
    }
    return 0;
}

static void
scale_callback(int item_ID, void *client_data, void *call_data)
{
    *((long *) client_data) = *((long *) call_data);
}

static void
ok_callback(int item_ID, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_ID, 1);
}

static void
cancel_callback(int item_ID, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_ID, 0);
}

static void
engrave(Image input, Image output)
{
    long width, height;
    long channels, rowstride;
    unsigned char *src_row, *dest_row;
    unsigned char *src, *dest;
    short row, col;
    int x1, y1, x2, y2, j, inten, v;
    unsigned int red_average, green_average, blue_average, count;

    gimp_image_area(input, &x1, &y1, &x2, &y2);

    width = gimp_image_width(input);
    height = gimp_image_height(input);
    channels = gimp_image_channels(input);
    rowstride = width * channels;

    src_row = gimp_image_data(input);
    dest_row = gimp_image_data(output);

    src_row += rowstride * y1;
    dest_row += rowstride * y1;

    for (row = y1; row < y2; row += engraveheight - (row % engraveheight)) {
	src = src_row;
	dest = dest_row;
	for (col = x1; col < x2; ++col) {
	    red_average = 0;
	    green_average = 0;
	    blue_average = 0;
	    count = 0;
	    /* Compute the average values of the engrave square area */
	    for (j = 0;
		 (j < engraveheight - (row % engraveheight)) && ((j + row) < y2);
		 j++) {
		src = src_row + j * rowstride + col * channels;
		red_average += *src++;
		if (channels > 1) {
		    green_average += *src++;
		    blue_average += *src++;
		}
		count++;
	    }

	    if (count) {
		red_average = red_average / count;
		green_average = green_average / count;
		blue_average = blue_average / count;
	    }

	    inten = INTENSITY(red_average,
			      green_average, blue_average)*engraveheight/255;

	    for (j = 0;
		 (j < engraveheight - (row % engraveheight)) && ((j + row) < y2);
		 j++) {
		dest = dest_row + j * rowstride + col * channels;
		v = inten >= j ? 255 : 0;
		*dest++ = v;
		if (channels > 1) {
		    *dest++ = v;
		    *dest++ = v;
		}
	    }
	}
	src_row += rowstride * (engraveheight - (row % engraveheight));
	dest_row += rowstride * (engraveheight - (row % engraveheight));
    }
}
