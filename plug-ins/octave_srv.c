/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 * Provide a server which can be communicated with via a UNIX port
 * The server accepts requests to display images in the ascii octave
 * image format...  This plug in does not currently exit, unless an error occurs.
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "gimp.h"

#define PORT	5556
#define HEADER  7
#define MAGIC   'G'

/*  image information  */

/*  Header format...
 *    bytes: 1        2        2        3        4        5        6
 *           MAGIC    TYPE     NAME_LEN WIDTH_H  WIDTH_L  HEIGHT_H HEIGHT_L
 */
#define MAGIC_BYTE      0
#define TYPE_BYTE       1
#define NAME_LEN_BYTE   2
#define WIDTH_H_BYTE    3
#define WIDTH_L_BYTE    4
#define HEIGHT_H_BYTE   5
#define HEIGHT_L_BYTE   6

/* Declare local functions.
 */
static void display (int, int, int, int, int);
static int  read_from_client (int);
static int  make_socket (unsigned short int);


static char *prog_name;

int
main (argc, argv)
     int argc;
     char **argv;
{
  extern int make_socket (unsigned short int port);
  int sock;
  fd_set active_fd_set, read_fd_set;
  int i;
  struct sockaddr_in clientname;
  size_t size;

  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {

      /* Create the socket and set it up to accept connections. */
      sock = make_socket (PORT);
      if (listen (sock, 1) < 0)
	{
	  perror ("listen");
	  gimp_quit ();
	}

      gimp_message ("Octave Server initialized and listening...");

      /* Initialize the set of active sockets. */
      FD_ZERO (&active_fd_set);
      FD_SET (sock, &active_fd_set);

      while (1)
	{
	  /* Block until input arrives on one or more active sockets. */
	  read_fd_set = active_fd_set;
	  if (select (FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0)
	    {
	      perror ("select");
	      gimp_quit ();
	    }

	  /* Service all the sockets with input pending. */
	  for (i = 0; i < FD_SETSIZE; ++i)
	    if (FD_ISSET (i, &read_fd_set))
	      {
		if (i == sock)
		  {
		    /* Connection request on original socket. */
		    int new;
		    size = sizeof (clientname);
		    new = accept (sock,
				  (struct sockaddr *) &clientname,
				  &size);
		    if (new < 0)
		      {
			perror ("accept");
			gimp_quit ();
		      }
		    /*
		      fprintf (stderr,
		      "Server: connect from host %s, port %hd.\n",
		      inet_ntoa (clientname.sin_addr),
		      ntohs (clientname.sin_port));
		      */
		    FD_SET (new, &active_fd_set);
		  }
		else
		  {
		    /* Data arriving on an already-connected socket. */
		    if (read_from_client (i) < 0)
		      {
			/*			
			  fprintf (stderr,
			  "Server: disconnect from host %s, port %hd.\n",
			  inet_ntoa (clientname.sin_addr),
			  ntohs (clientname.sin_port));
			  */
		    
			close (i);
			FD_CLR (i, &active_fd_set);
		      }
		  }
	      }
	}

      /* Quit
       */
      gimp_quit ();
    }

  return 0;
}

static void
display (name_len, w, h, type, fd)
     int name_len;
     int w, h;
     int type;
     int fd;
{
  Image output;
  char name [256];
  unsigned char *d;
  unsigned char cmap[768];
  int nbytes;
  int width;
  int i, j;

  switch (type)
    {
    case RGB_IMAGE:
    case GRAY_IMAGE:
    case INDEXED_IMAGE:
      break;
    default:
      printf ("type: %d\n", type);
      gimp_message ("Invalid image type");
      return;
    }

  nbytes = read (fd, name, name_len);
  output = gimp_new_image (name, w, h, type);
  width = gimp_image_width (output) * gimp_image_channels (output);
  d = gimp_image_data (output);

  if (type == INDEXED_IMAGE)
    {
      memset (cmap, 0, 768);
      for (j = 0; j < 768; j++)
	if (! (nbytes = read (fd, cmap + j, 1)))
	  {
	    gimp_message ("Error in octave image transmission.");
	    return;
	  }
    }

  for (i = 0; i < h; i++)
    {
      for (j = 0; j < width; j++)
	if (! (nbytes = read (fd, d++, 1)))
	  {
	    gimp_message ("Error in octave image transmission.");
	    return;
	  }

      if ((i % 5) == 0)
	gimp_do_progress (i, h);
    }

  /*  Make the progress bar disappear  */
  gimp_do_progress (1, 1);

  gimp_display_image (output);
  gimp_update_image (output);
  gimp_free_image (output);
}


static int
read_from_client (int filedes)
{
  unsigned char buffer[HEADER];
  int i;
  int name_len;
  int type;
  int w, h;
  int nbytes;

  for (i = 0; i < HEADER; i++)
    {
      if ((nbytes = read (filedes, buffer+i, 1)) < 0)
	{
	  /* Read error. */
	  perror ("read");
	  return 0;
	}
      else if (nbytes == 0)
	/* End-of-file. */
	return -1;
    }

  if (buffer[MAGIC_BYTE] != MAGIC)
    {
      gimp_message ("Error in octave image transmission");
      return -1;
    }
  name_len = buffer[NAME_LEN_BYTE];
  type = buffer[TYPE_BYTE];
  w = (buffer [WIDTH_H_BYTE] << 8) | buffer [WIDTH_L_BYTE];
  h = (buffer [HEIGHT_H_BYTE] << 8) | buffer [HEIGHT_L_BYTE];
  
  gimp_init_progress ("Octave image transmission");
  display (name_len, w, h, type, filedes);

  return 0;
}


static int 
make_socket (unsigned short int port)
{
  int sock;
  struct sockaddr_in name;

  /* Create the socket. */
  sock = socket (PF_INET, SOCK_STREAM, 0);
  if (sock < 0)
    {
      perror ("socket");
      gimp_quit ();
    }

  /* Give the socket a name. */
  name.sin_family = AF_INET;
  name.sin_port = htons (port);
  name.sin_addr.s_addr = htonl (INADDR_ANY);
  if (bind (sock, (struct sockaddr *) &name, sizeof (name)) < 0)
    {
      perror ("bind");
      gimp_quit ();
    }

  return sock;
}

