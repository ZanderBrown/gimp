/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* 
 * Blends two images together.
 */

#include "gimp.h"
#include "stdlib.h"

/* Declare local functions.
 */
static void image_menu_callback (int, void *, void *);
static void scale_callback (int, void *, void *);
static void ok_callback (int, void *, void *);
static void cancel_callback (int, void *, void *);
static void blend (Image, Image);

#define MAX(a,b) (((a) > (b)) ? (a) : (b));

static char *prog_name;
static int dialog_ID;
static long amount = 50;

int
main (argc, argv)
     int argc;
     char **argv;
{
  Image src1, src2;
  int group_ID;
  int frame_ID;
  int scale_ID;
  int image_menu1_ID;
  int image_menu2_ID;
  int src1_ID, src2_ID;
  
  /* Save the program name so we can use it later in reporting errors
   */
  prog_name = argv[0];

  /* Call 'gimp_init' to initialize this filter.
   * 'gimp_init' makes sure that the filter was properly called and
   *  it opens pipes for reading and writing.
   */
  if (gimp_init (argc, argv))
    {
      src1 = src2 = 0;
      src1_ID = src2_ID = 0;
      
      dialog_ID = gimp_new_dialog ("Offset");
      group_ID = gimp_new_row_group (dialog_ID, DEFAULT, NORMAL, "");
      image_menu1_ID = gimp_new_image_menu (dialog_ID, group_ID, 
					    IMAGE_CONSTRAIN_RGB | IMAGE_CONSTRAIN_GRAY,
					    "First Image");
      image_menu2_ID = gimp_new_image_menu (dialog_ID, group_ID, 
					    IMAGE_CONSTRAIN_RGB | IMAGE_CONSTRAIN_GRAY,
					    "Second Image");
      frame_ID = gimp_new_frame (dialog_ID, group_ID, "% Source 1");
      scale_ID = gimp_new_scale (dialog_ID, frame_ID, 0, 100, amount, 0);
      gimp_add_callback (dialog_ID, image_menu1_ID, image_menu_callback, &src1_ID);
      gimp_add_callback (dialog_ID, image_menu2_ID, image_menu_callback, &src2_ID);
      gimp_add_callback (dialog_ID, scale_ID, scale_callback, &amount);
      gimp_add_callback (dialog_ID, gimp_ok_item_id (dialog_ID), ok_callback, 0);
      gimp_add_callback (dialog_ID, gimp_cancel_item_id (dialog_ID), cancel_callback, 0);

      if (gimp_show_dialog (dialog_ID))
	{
	  src1 = gimp_get_input_image (src1_ID);
	  src2 = (src2_ID != src1_ID) ? gimp_get_input_image (src2_ID) : src1;
	  
	  if (src1 && src2)
	    {
	      gimp_init_progress ("blend");
	      blend (src1, src2);
	    }
	}

      /* Free the images.
       */
      if (src1)
	gimp_free_image (src1);
      if (src2)
	gimp_free_image (src2);

      /* Quit
       */
      gimp_quit ();
    }

  return 0;
}

static void
image_menu_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *((long*) client_data) = *((long*) call_data);
}

static void
scale_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  *((long*) client_data) = *((long*) call_data);
}

static void
ok_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 1);
}

static void
cancel_callback (item_ID, client_data, call_data)
     int item_ID;
     void *client_data;
     void *call_data;
{
  gimp_close_dialog (dialog_ID, 0);
}


static void
blend_pixels (src1, src2, dest, blend_val, length, bytes, has_alpha)
     unsigned char * src1;
     unsigned char * src2;
     unsigned char * dest;
     int blend_val;
     int length;
     int bytes;
     int has_alpha;
{
  int alpha, b;

  alpha = (has_alpha) ? bytes - 1 : bytes;
  while (length --)
    {
      for (b = 0; b < alpha; b++)
	dest[b] = (has_alpha) ? 
	  ((src1[b] * src1[alpha] * blend_val) +
	   (src2[b] * src2[alpha] * (255 - blend_val))) / 65025 :
	((src1[b] * blend_val) + (src2[b] * (255 - blend_val))) / 255;

      if (has_alpha)
	dest[alpha] = MAX (src1[alpha], src2[alpha]);

      src1 += bytes;
      src2 += bytes;
      dest += bytes;
    }
}


static void
add_alpha_pixels (src, dest, length, bytes)
     unsigned char * src, * dest;
     int length;
     int bytes;
{
  int alpha, b;
  
  alpha = bytes + 1;
  while (length --)
    {
      for (b = 0; b < bytes; b++)
	dest[b] = src[b];

      dest[b] = 255;

      src += bytes;
      dest += alpha;
    }
}


static void
colorize_pixels (src, dest, length, bytes, has_alpha)
     unsigned char * src, * dest;
     int length;
     int bytes;      /*  bytes of src  */
     int has_alpha;  /*  does src have an alpha channel?  */
{
  int b, dest_bytes;
  
  dest_bytes = (has_alpha) ? 4 : 3;

  while (length --)
    {
      for (b = 0; b < 3; b++)
	dest[b] = src[0];

      if (has_alpha)
	dest[3] = src[1];

      src += bytes;
      dest += dest_bytes;
    }
}


static void
colorize_pixels_add_alpha (src, dest, length, bytes, has_alpha)
     unsigned char * src, * dest;
     int length;
     int bytes;      /*  bytes of src  */
     int has_alpha;  /*  does src have an alpha channel?  */
{
  int b;
  
  while (length --)
    {
      for (b = 0; b < 3; b++)
	dest[b] = src[0];

      if (has_alpha)
	dest[3] = src[1];
      else
	dest[3] = 255;

      src += bytes;
      dest += 4;
    }
}


static void
blend (src1, src2)
     Image src1, src2;
{
  Image dest;
  ImageType dest_type;
  unsigned char *src1p;
  unsigned char *src2p;
  unsigned char *destp;
  unsigned char *buf1, *buf2;
  int width, height;
  int src1_channels;
  int src2_channels;
  int dest_channels;
  int src1_rowstride;
  int src2_rowstride;
  int dest_rowstride;
  int src1_type, src2_type;
  int blend_val;
  int color, alpha;
  int i;

  src1_type = gimp_image_type (src1);
  src2_type = gimp_image_type (src2);
  color = (src1_type == RGB_IMAGE ||
	   src1_type == RGBA_IMAGE ||
	   src2_type == RGB_IMAGE ||
	   src2_type == RGBA_IMAGE)
    ? 1 : 0;
  alpha = (gimp_image_alpha (src1) || gimp_image_alpha (src2));

  width = gimp_image_width (src1);
  height = gimp_image_height (src1);
  
  src1_channels = gimp_image_channels (src1);
  src2_channels = gimp_image_channels (src2);
  src1_rowstride = src1_channels * width;
  src2_rowstride = src2_channels * height;
  src1p = gimp_image_data (src1);
  src2p = gimp_image_data (src2);

  if (color)
    dest_type = (alpha) ? RGBA_IMAGE : RGB_IMAGE;
  else
    dest_type = (alpha) ? GRAYA_IMAGE : GRAY_IMAGE;

  dest = gimp_new_image (0, width, height, dest_type);
  dest_channels = gimp_image_channels (dest);
  dest_rowstride = dest_channels * width;
  destp = gimp_image_data (dest);

  blend_val = amount * 255 / 100;

  /*  buffer for temporary conversions  */
  if (src1_type != dest_type)
    buf1 = (unsigned char *) malloc (width * dest_channels);
  else
    buf1 = NULL;

  if (src2_type != dest_type)
    buf2 = (unsigned char *) malloc (width * dest_channels);
  else
    buf2 = NULL;

  for (i = 0; i < height; i++)
    {
      /*  Make sure we add alpha channels to simplify the process  */
      if (color && alpha && src1_type == GRAY_IMAGE)
	colorize_pixels_add_alpha (src1p, buf1, width, src1_channels,
				   gimp_image_alpha (src1));
      else if (color && (src1_type == GRAYA_IMAGE || src1_type == GRAY_IMAGE))
	colorize_pixels (src1p, buf1, width, src1_channels,
			 gimp_image_alpha (src1));
      else if (alpha && !gimp_image_alpha (src1))
	add_alpha_pixels (src1p, buf1, width, src1_channels);
	  
      if (color && alpha && src2_type == GRAY_IMAGE)
	colorize_pixels_add_alpha (src2p, buf2, width, src2_channels,
				   gimp_image_alpha (src2));
      else if (color && (src2_type == GRAYA_IMAGE || src2_type == GRAY_IMAGE))
	colorize_pixels (src2p, buf2, width, src2_channels,
			 gimp_image_alpha (src2));
      else if (alpha && !gimp_image_alpha (src2))
	add_alpha_pixels (src2p, buf2, width, src2_channels);

      blend_pixels ((buf1) ? buf1 : src1p, (buf2) ? buf2 : src2p,
		    destp, blend_val, width, dest_channels, alpha);

      if (src1)
	src1p += src1_rowstride;
      if (src2)
	src2p += src2_rowstride;
      destp += dest_rowstride;

      if ((i % 5) == 0)
	gimp_do_progress (i, height);
    }

  if (buf1)
    free (buf1);
  if (buf2)
    free (buf2);

  gimp_display_image (dest);
  gimp_update_image (dest);
  gimp_free_image (dest);
}
