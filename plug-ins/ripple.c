/*
 * This is a plug-in for the GIMP.
 *
 * Copyright (C) 1996 Torsten Martinsen <bullestock@dk-online.dk>
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: ripple.c,v 1.3 1996/05/08 17:47:28 torsten Exp $
 */

/* 
 * This plug-in displaces each row of the image according to a sine function,
 * a triangular function, or a random function.
 * (You can displace the columns by rotating the image
 * 90 degrees before applying the filter).
 */

#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "gimp.h"

static void scale_callback(int, void *, void *);
static void ok_callback(int, void *, void *);
static void cancel_callback(int, void *, void *);
static void radio_callback(int item_ID, void *client_data, void *call_data);
static void ripple(Image input, Image output);

static int dialog_ID;
static int params[3] = { 8, 5, 0 };

#define NSHAPES 3

int
main(int argc, char **argv)
{
    Image input, output;
    void *data;
    int group_ID, scalep_ID, scalea_ID, rb_ID[NSHAPES];
    int set, i, ID;
    static char * rb_label[NSHAPES] = {
	"Sine", "Triangle", "Random"
    };

    if (gimp_init(argc, argv)) {
	output = 0;
	data = gimp_get_params();
	if (data) {
	    params[0] = ((int *) data)[0];
	    params[1] = ((int *) data)[1];
	    params[2] = ((int *) data)[2];
	}
	input = gimp_get_input_image(0);

	dialog_ID = gimp_new_dialog("Ripple");
	group_ID = gimp_new_row_group(dialog_ID, DEFAULT, RADIO, "");

	for (i = 0; i < NSHAPES; ++i) {
	    rb_ID[i] = ID = gimp_new_radio_button (dialog_ID, group_ID,
							   rb_label[i]);
	    gimp_add_callback (dialog_ID, ID, radio_callback, (void *) i);
	    set = (i == params[2]) ? 1 : 0;
	    gimp_change_item (dialog_ID, ID, sizeof (int), &set);
	}

	scalep_ID = gimp_new_scale(dialog_ID, group_ID, 1, 200, params[0], 0);
	gimp_new_label(dialog_ID, scalep_ID, "Period:");

	scalea_ID = gimp_new_scale(dialog_ID, group_ID, 1, 100, params[1], 0);
	gimp_new_label(dialog_ID, scalea_ID, "Amplitude:");

	gimp_add_callback(dialog_ID, scalep_ID, scale_callback, &params[0]);
	gimp_add_callback(dialog_ID, scalea_ID, scale_callback, &params[1]);
	gimp_add_callback(dialog_ID, gimp_ok_item_id(dialog_ID), ok_callback, 0);
	gimp_add_callback(dialog_ID, gimp_cancel_item_id(dialog_ID),
			  cancel_callback, 0);

	if (gimp_show_dialog(dialog_ID)) {
	    gimp_set_params(3 * sizeof(int), &params[0]);

	    output = gimp_get_output_image(0);

	    if (input && output) {
		if (gimp_image_type(input) == INDEXED_IMAGE)
		    gimp_set_image_colors(output, gimp_image_cmap(input),
					  gimp_image_colors(input));
		gimp_display_image(output);
		ripple(input, output);
		gimp_update_image(output);
	    }
	}
	if (input)
	    gimp_free_image(input);
	if (output)
	    gimp_free_image(output);

	gimp_quit();
    }
    return 0;
}


static void
ripple(Image input, Image output)
{
    long width, height;
    long channels, rowstride;
    unsigned char *src_row, *dest_row;
    unsigned char *src, *dest;
    short row, col;
    int x1, y1, x2, y2;
    int shift, i, amp, per;

    gimp_image_area(input, &x1, &y1, &x2, &y2);

    width = gimp_image_width(input);
    height = gimp_image_height(input);
    channels = gimp_image_channels(input);
    rowstride = width * channels;

    src_row = gimp_image_data(input);
    dest_row = gimp_image_data(output);

    src_row += rowstride * y1 + x1 * channels;
    dest_row += rowstride * y1 + x1 * channels;

    per = params[0];
    amp = params[1];
    if (params[2] == 1)
	srand (time (NULL));
    
    for (row = y1; row < y2; row++) {
	src = src_row;
	dest = dest_row;

	switch (params[2]) {
	case 0:		/* sine */
	    shift = amp * sin((row-y1)*2.0*M_PI/per);
	    break;
	case 1:		/* triangle */
	    shift = ((row-y1) % per)*2*amp/per;
	    if (shift > amp)
		shift = 2*amp - shift;
	    break;
	default:	/* random */
	    shift = (rand () % (2*amp+1)) - amp;
	    break;
	}
	if (shift < 0) {
	    shift = -shift;
	    src += shift * channels;
	    
	    if ((x2 - shift) > width) {
		for (col = x1; col < x2 - shift; col++)
		    for (i = 0; i < channels; i++)
			*dest++ = *src++;
		
		src -= channels;
		for (; col < x2; col++)
		    for (i = 0; i < channels; i++)
			*dest++ = src[i];
	    } else {
		for (col = x1; col < x2; col++)
		    for (i = 0; i < channels; i++)
			*dest++ = *src++;
	    }
	} else {
	    for (col = x1; col < x1 + shift; col++)
		for (i = 0; i < channels; i++)
		    *dest++ = src[i];
	    
	    for (; col < x2; col++)
		for (i = 0; i < channels; i++)
		    *dest++ = *src++;
	}
	
	src_row += rowstride;
	dest_row += rowstride;
    }
}

static void
scale_callback(int item_ID, void *client_data, void *call_data)
{
    *((int *) client_data) = *((long *) call_data);
}

static void
ok_callback(int item_ID, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_ID, 1);
}

static void
cancel_callback(int item_ID, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_ID, 0);
}

static void
radio_callback(int item_ID, void *client_data, void *call_data)
{
    params[2] = (int) client_data;
}
