/*
 * This is a plug-in for the GIMP.
 *
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 * Copyright (C) 1996 Torsten Martinsen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * $Id: autocrop.c,v 1.4 1996/05/03 11:49:08 torsten Exp $
 *
 * $Log: autocrop.c,v $
 * Revision 1.4  1996/05/03  11:49:08  torsten
 * Fixed several bugs.
 * Support for colour tolerance (except for indexed images).
 *
 * Revision 1.3  1996/05/03  10:32:48  torsten
 * Compare colours using coldist().
 *
 */

/* 
 * This filter removes any borders from the image.
 * The border colour is defined as the colour that at least two of
 * the corner pixels share.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gimp.h"

static void autocrop(Image input, Image output);
static int get_new_size(Image input, long * nw, long * nh);
static unsigned long coldistrgb(unsigned char * c1, unsigned char * c2);
static unsigned long coldistgrey(unsigned char * c1, unsigned char * c2);
static void scale_callback(int, void *, void *);
static void ok_callback(int, void *, void *);
static void cancel_callback(int, void *, void *);

typedef unsigned long (*CmpFun) (unsigned char *, unsigned char *);

static int dialog_ID;
static long tolerance = 0L;

int
main(int argc, char **argv)
{
    Image input, output;
    void *data;
    long new_width;
    long new_height;
    int group_ID, scale_ID, e;


    if (gimp_init(argc, argv)) {
	output = 0;
	data = gimp_get_params();
	if (data)
	    tolerance = ((long *) data)[0];

	input = gimp_get_input_image(0);

	dialog_ID = gimp_new_dialog("Autocrop");
	group_ID = gimp_new_row_group(dialog_ID, DEFAULT, NORMAL, "");
	scale_ID = gimp_new_scale(dialog_ID, group_ID, 0, 255, tolerance, 0);
	gimp_add_callback(dialog_ID, scale_ID, scale_callback, &tolerance);
	gimp_add_callback(dialog_ID, gimp_ok_item_id(dialog_ID), ok_callback, 0);
	gimp_add_callback(dialog_ID, gimp_cancel_item_id(dialog_ID),
			  cancel_callback, 0);

	if ((gimp_image_type(input) == INDEXED_IMAGE) ||
	    gimp_show_dialog(dialog_ID)) {
	    gimp_set_params(sizeof(int), &tolerance);

	    if (gimp_image_type(input) == INDEXED_IMAGE)
		tolerance = 0L;
	    else
		tolerance = 3*tolerance*tolerance;

	    e = get_new_size(input, &new_width, &new_height);
	    switch (e) {
	    case 0:
		output = gimp_new_image(0, new_width, new_height,
					gimp_image_type(input));

		if (gimp_image_type(input) == INDEXED_IMAGE)
		    gimp_set_image_colors(output, gimp_image_cmap(input),
					  gimp_image_colors(input));

		if (input && output) {
		    gimp_display_image(output);
		    autocrop(input, output);
		    gimp_update_image(output);
		}
		break;
	    case 1:
		gimp_message("No corners with same colour");
		break;
	    case 2:
		gimp_message("Resulting image too small");
		break;
#if 0
	    case 3:
		gimp_message("No change");
		break;
#endif
	    }
	}
	if (input)
	    gimp_free_image(input);
	if (output)
	    gimp_free_image(output);

	gimp_quit();
    }
    return 0;
}

static int x1, x2, y1, y2;

static int
get_new_size(Image input, long * nw, long * nh)
{
    unsigned char corner[4][3], * background;
    int width, channels, x, y, right, left, top, bottom, rowstride, i;
    unsigned char * src, * p;
    CmpFun coldist;


    gimp_image_area(input, &x1, &y1, &x2, &y2);
    src = gimp_image_data(input);
    width = gimp_image_width(input);
    channels = gimp_image_channels(input);
    rowstride = width * channels;

    if (channels == 1)
	coldist = coldistgrey;
    else
	coldist = coldistrgb;

    /*
     * Get the colours of the four corners.
     */
    for (y = y1, i = 0; y < y2; y += y2-y1-1)
	for (x = x1; x < x2; x += x2-x1-1) {
	    p = src + rowstride * y + x * channels;
	    corner[i][0] = *p++;
	    if (channels > 1) {
		corner[i][1] = *p++;
		corner[i][2] = *p;
	    }
	    ++i;
	}

    /*
     * The background colour is the colour which appears in at least two corners.
     */
    if ((coldist(corner[0], corner[1]) <= tolerance) ||
	(coldist(corner[0], corner[2]) <= tolerance) ||
	(coldist(corner[0], corner[3]) <= tolerance))
	background = corner[0];
    else if ((coldist(corner[1], corner[2]) <= tolerance) ||
	     (coldist(corner[1], corner[3]) <= tolerance))
	background = corner[1];
    else if (coldist(corner[2], corner[3]) <= tolerance)
	background = corner[2];
    else
	return 1;

    /* Find first non-background line. */
    for (top = y1; top < y2; ++top)
	for (x = x1; x < x2; ++x) {
	    p = src + rowstride * top + x * channels;
	    if (coldist(background, p) > tolerance)
		goto foundtop;
	}	    
foundtop:

    /* Find last non-background line. */
    for (bottom = y2-1; bottom > top; --bottom)
 	for (x = x1; x < x2; ++x) {
	    p = src + rowstride * bottom + x * channels;
	    if (coldist(background, p) > tolerance)
		goto foundbottom;
	}
foundbottom:

    /* Find first non-background column. */
    left = x2-1;
    for (y = top; y <= bottom; ++y)
	for (x = x1; x < left; x++) {
	    p = src + rowstride * y + x * channels;
	    if (coldist(background, p) > tolerance) {
		left = x;
		break;
	    }
	}

    /* Find last non-background column. */
    right = left;
    for (y = top; y <= bottom; ++y)
	for (x = x2-1; x > right; --x) {
	    p = src + rowstride * y + x * channels;
	    if (coldist(background, p) > tolerance) {
		right = x;
		break;
	    }
	}
    
    /* Check that the size has changed and that the new size is reasonable. */
    if ((x1 == left) && (x2-1 == right) && (y1 == top) && (y2-1 == bottom))
	return 3;
    if ((bottom - top < 1) || (right - left < 1))
	return 2;

    x1 = left;
    x2 = right;
    y1 = top;
    y2 = bottom;

    *nw = x2-x1+1;
    *nh = y2-y1+1;

    return 0;
}

static void
autocrop(Image input, Image output)
{
    int width, height;
    int channels, rowstride;
    unsigned char *src, *dest;
    int row, rowstride_o;

    width = gimp_image_width(input);
    height = gimp_image_height(input);
    channels = gimp_image_channels(input);
    rowstride = width * channels;

    src = gimp_image_data(input);
    dest = gimp_image_data(output);

    src += rowstride * y1 + x1 * channels;

    rowstride_o = (x2-x1+1) * channels;
    for (row = y1; row <= y2; row++) {
	memcpy(dest, src, rowstride_o);
	dest += rowstride_o;
	src += rowstride;
    }
}

/*
 * Return the squared Euclidean three-dimensional distance
 * between the two colours in RGB colour space.
 */
static unsigned long
coldistrgb(unsigned char * c1, unsigned char * c2)
{
    int r, g, b;

    r = *c1++ - *c2++;
    g = *c1++ - *c2++;
    b = *c1++ - *c2++;
    return r*r + g*g + b*b;
}

/*
 * Return the squared distance between the two greyscale values.
 */
static unsigned long
coldistgrey(unsigned char * c1, unsigned char * c2)
{
    int g;

    g = *c1 - *c2;
    return g*g;
}

static void
scale_callback(int item_ID, void *client_data, void *call_data)
{
    *((long *) client_data) = *((long *) call_data);
}

static void
ok_callback(int item_ID, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_ID, 1);
}

static void
cancel_callback(int item_ID, void *client_data, void *call_data)
{
    gimp_close_dialog(dialog_ID, 0);
}
